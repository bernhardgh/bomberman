# bgh

Bernhard Häussermann (bernhardgh@icloud.com)  
Built for Entelect Challenge 2016


The zipped file bgh.zip contains the following files/folders:

- `bot.json` (contains basic meta-data about the bot)
- `src` (the folder containing the source code of the program)
- `pom.xml` (provides maven with the information that is necessary to build the project from the source code)
- `readme.md` (this file; contains general information about the program)
- `.classpath` & `.project` (Eclipse project files so that the project can be easily imported into Eclipse)

## Usage

- To build the program, execute the command  
  `mvn clean package`
- To run the program, execute  
  `java -jar target/Bomberman-bgh-1.0-SNAPSHOT.jar [player-key] [path-to-working-folder]`

### GUI

If you'd like to fiddle with the GUI, run the following command after building:  
	`java -cp target/Bomberman-bgh-1.0-SNAPSHOT.jar za.co.entelect.challenge.gui.BombermanGui`

The "Search" player type represents the bot that plays in the contest.

The "Human" player type allows the user to play (the time-limit is not applied).  
Use the following keys to control the bot (remember to click the "Start" button first to start the game):

- Arrows to move.
- Z to plant a bomb.
- X to trigger a bomb.
- C to do nothing.

## Implementation

The program's entry point is in the `za.co.entelect.challenge.program.Main` class. While running, the program persists data in the following two files for reading during the next round:

- `coverage` (stores which blocks have been visited by which players; this allows for more accurately calculating player scores during tree searches)
- `dodgeCounts` (this file facilitates the "instant kill" strategy implemented by the `InstantKillBombermanPlayer` class (see corresonding section below))

The game logic is implemented by the `BombermanEngine` class. This class has methods for executing moves and for advancing the game once all players' moves have been executed. Its base-class `GameEngine` features an efficient undo-mechanism. The undo-mechanism facilitates faster depth-first searches (as opposed to making in-memory copies of the game state).

The artificial-intelligence component of the program is implemented by a number of "player" implementations and features a design that allows these to be combined in various ways.

The following tree depicts the composition of the bot that is created to play the game (in the `Main` class). Each node represents a class in the source code.

- `TimeLimitedGamePlayer`
	- `InstantKillBombermanPlayer`
		- `HybridSearchBombermanPlayer`
			- `BeamBombermanPlayer`
			- `HybridAdversarialSearchBombermanPlayer`
				- `MultiThreadedAlphaBetaBombermanPlayer`
				- `MultiThreadedDFSBombermanPlayer`
	- `SafeBombermanPlayer`

Each class is briefly described in the following paragraphs:

### TimeLimitedGamePlayer
*(sub-class of GamePlayer)*

This class combines two `GamePlayer` objects: an `innerPlayer` and a `backupPlayer`.  
It gets the next move by invoking the `innerPlayer` on a separate thread, ensuring that the player does not exceed the time limit (which is set to 1.3 seconds).  
The `innerPlayer` repeatedly sets the preliminary moves based on increasingly deep searches. When the time limit expires, `TimeLimitedGamePlayer` interrupts the thread, causing `innerPlayer` to immediately terminate its current round search. `TimeLimitedGamePlayer` then returns the last preliminary move that had been set.  
If no preliminary move had been set, the `backupPlayer` is used to determine a move. The `backupPlayer` is supposed to be a simple, fail-safe player that makes a move very quickly. This facilitates a very robust bot that will recover and still make a move when `innerPlayer` fails for some reason.

### SafeBombermanPlayer
*(sub-class of GamePlayer)*

A simple backup-player that detects when it needs to move away from a bomb in order to avoid being caught in its bomb blast when it goes off.

### InstantKillBombermanPlayer
*(sub-class of GamePlayer)*

This class implements a naive strategy that executes the trigger-bomb move whenever this move may cause an opponent to die. In most cases the opponent will be able to react to this by moving out of the way of the bomb blast. This class uses the `dodgeCounts` file to keep a record of which players seem to be clever enough to do this. If a player has been seen to "dodge" twice, the maneuver will not be attempted against that player again, unless we notice that the player will not be able to move out of the way within one round.  
If the algorithm decides not to execute the trigger-bomb move, it then queries its secondary player for its move.

### HybridSearchBombermanPlayer
*(sub-class of HybridGamePlayer)*

A player that dynamically selects one of its two inner players based on the current state of the game. If we find that there are no opponent players within 7 blocks from our player, we then select the (non-adversarial) beam-search player class. The search in this class specializes in planting the bombs in an optimal manner in terms of maximizing the future score. It does not account for the moves of the opponents.

Otherwise, we select either one of the adversarial search classes, which don't search as deep, but take into account the opponents' moves. This option is also selected when there are only a few destructible walls left.

### BeamBombermanPlayer
*(sub-class of GamePlayer)*

Implements a beam search with a beam size of 200. It also detects and takes into account game states where an opponent player may trigger a bomb, and simulates this in the search. The algorithm searches one level at a time (up to a limit of 50), setting the best move found so far as the preliminary move after each level until the limit runs out.

### HybridAdversarialSearchBombermanPlayer
*(sub-class of HybridGamePlayer)*

A player that dynamically selects between one of two different adversarial search algorithms based on how many opponents are found in the immediate vicinity of the player.

### MultiThreadedAlphaBetaBombermanPlayer
*(sub-class of GamePlayer)*

Uses the `SearchThreadCoordinator` class to run each depth of an iterative deepening process on a separate thread (one thread per processor core). This should theoretically speed up the progress through the depths.  
An individual thread is implemented by the `SearchRunnable` inner-class, which is a sub-class of the single-threaded version of `MultiThreadedAlphaBetaBombermanPlayer`, namely `AlphaBetaBombermanPlayer`.

### AlphaBetaBombermanPlayer
*(sub-class of GamePlayer)*

Implements an iterative deepening alpha-beta search involving the player and its closest opponent. This algorithm implements 50/50 "chance"-nodes wherever the players collide (and either one's move may be selected by the official game engine).

### MultiThreadedDFSBombermanPlayer
*(sub-class of GamePlayer)*

Uses the `SearchThreadCoordinator` class to run each depth of an iterative deepening process on a separate thread. An individual thread is implemented by the `SearchRunnable` inner-class, which is a sub-class of the single-threaded version of `MultiThreadedDFSBombermanPlayer`, namely `DFSBombermanPlayer`.

### DFSBombermanPlayer
*(sub-class of GamePlayer)*

Implements an n-way alpha-beta search (with iterative deepening) including up to two of the closest opponents (for a total of three players in the search). This type of search is a compromise since a search involving three players is typically much shallower than a two-player search (in terms of game rounds), so this type of search is used only when two opponents are nearby.
