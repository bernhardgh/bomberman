package za.co.entelect.challenge.bench;

import za.co.entelect.challenge.model.GamePlayer;
import za.co.entelect.challenge.model.TimeLimitedGamePlayer;
import za.co.entelect.challenge.model.bomberman.BombermanCell;
import za.co.entelect.challenge.model.bomberman.BombermanEngine;
import za.co.entelect.challenge.model.bomberman.BombermanPlayer;
import za.co.entelect.challenge.model.bomberman.BombermanState;
import za.co.entelect.challenge.model.bomberman.heuristics.BasicBombermanHeuristic;
import za.co.entelect.challenge.model.bomberman.players.DFSBombermanPlayer;
import za.co.entelect.challenge.model.bomberman.players.LazyBombermanPlayer;

public class BagSizeBench
{
    public static void main(String[] args) throws InterruptedException
    {
        BombermanPlayer playerA = new BombermanPlayer('A');
        playerA.setBombRadius(4);
        BombermanPlayer playerB = new BombermanPlayer('B');
        
        int[] totals = new int[4];
        for (int i = 0; i < 8; i++)
        {
            System.out.println("--- " + i + " ---");
            BombermanState gameState = BenchHelpers.createGameState(playerA, playerB);
            
            for (int bagSize = 1; bagSize <= 4; bagSize++)
            {
                playerA.setBombCounts(bagSize);
                
                int score = play(gameState);
                totals[bagSize - 1] += score;
                
                System.out.println(bagSize + ") " + score);
            }
        }
        
        System.out.println();
        for (int i = 0; i < totals.length; i++)
            System.out.println(i + ": " + totals[i]);
    }
    
    private static int play(BombermanState gameState0) throws InterruptedException
    {
        BombermanState gameState = (BombermanState)gameState0.clone();

        BombermanPlayer playerA = gameState.getPlayer('A');
        GamePlayer<BombermanCell> gamePlayer = new TimeLimitedGamePlayer<BombermanCell>(
                new DFSBombermanPlayer(playerA, DFSBombermanPlayer.PlayerSelection.NEAREST_PLAYER, new BasicBombermanHeuristic()),
                new LazyBombermanPlayer(playerA), 1400, 1600);
        
        BombermanEngine gameEngine = new BombermanEngine();
        for (int round = 0; (round < 80) && (!gameEngine.gameDidEnd(gameState)); round++)
            gameEngine.advanceRound(gameState, gamePlayer.getNextMove(gameState));
        
        return playerA.getScore();
    }
}
