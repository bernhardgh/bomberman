package za.co.entelect.challenge.bench;

import za.co.entelect.challenge.model.GamePlayer;
import za.co.entelect.challenge.model.bomberman.BombermanCell;
import za.co.entelect.challenge.model.bomberman.BombermanEngine;
import za.co.entelect.challenge.model.bomberman.BombermanPlayer;
import za.co.entelect.challenge.model.bomberman.BombermanState;
import za.co.entelect.challenge.model.bomberman.heuristics.TermBombermanHeuristic;
import za.co.entelect.challenge.model.bomberman.players.BeamBombermanPlayer;

public class AreaProsperityFactorsBench
{
    public static void main(String[] args) throws InterruptedException
    {
        BombermanPlayer playerA = new BombermanPlayer('A');
        BombermanPlayer playerB = new BombermanPlayer('B');
        
        int[][] totals = new int[6][6];
        for (int i = 0; i < 8; i++)
        {
            System.out.println("--- " + i + " ---");
            BombermanState gameState = BenchHelpers.createGameState(playerA, playerB);
            
            for (int factorIndex = 0; factorIndex < 6; factorIndex++)
                for (int distanceFactorIndex = 0; distanceFactorIndex < 6; distanceFactorIndex++)
                {
                    double factor = 1.6 + 0.1*factorIndex;
                    double distanceFactor = 0.92 + 0.01*distanceFactorIndex;
                    int score = play(gameState, factor, distanceFactor);
                    totals[factorIndex][distanceFactorIndex] += score;
                    
                    System.out.printf("%f, %f) %d\n", factor, distanceFactor, score);
                }
        }
        
        System.out.print("\n\t\t");
        for (int distanceFactorIndex = 0; distanceFactorIndex < 6; distanceFactorIndex++)
            System.out.printf("%f\t", 0.92 + 0.01*distanceFactorIndex);
        System.out.println();
        for (int factorIndex = 0; factorIndex < 6; factorIndex++)
        {
            System.out.printf("%f\t", 1.6 + 0.1*factorIndex);
            for (int distanceFactorIndex = 0; distanceFactorIndex < 6; distanceFactorIndex++)
                System.out.printf("%d\t\t", totals[factorIndex][distanceFactorIndex]);
            System.out.println();
        }
    }
    
    private static int play(BombermanState gameState0, double factor, double distanceFactor) throws InterruptedException
    {
        BombermanState gameState = (BombermanState)gameState0.clone();

        BombermanPlayer playerA = gameState.getPlayer('A');
        GamePlayer<BombermanCell> gamePlayer = BenchHelpers.makeTimeLimited(
                new BeamBombermanPlayer(playerA, new TermBombermanHeuristic(factor, TermBombermanHeuristic.DEFAULT_AREA_PROSPERITY_RADIUS, distanceFactor)), 1200);
        
        BombermanEngine gameEngine = new BombermanEngine();
        for (int round = 0; (round < 100) && (!gameEngine.gameDidEnd(gameState)); round++)
            gameEngine.advanceRound(gameState, gamePlayer.getNextMove(gameState));
        
        return playerA.getScore();
    }
}
