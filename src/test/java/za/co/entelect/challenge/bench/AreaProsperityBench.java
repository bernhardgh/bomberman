package za.co.entelect.challenge.bench;

import za.co.entelect.challenge.model.GamePlayer;
import za.co.entelect.challenge.model.bomberman.BombermanCell;
import za.co.entelect.challenge.model.bomberman.BombermanEngine;
import za.co.entelect.challenge.model.bomberman.BombermanPlayer;
import za.co.entelect.challenge.model.bomberman.BombermanState;
import za.co.entelect.challenge.model.bomberman.heuristics.TermBombermanHeuristic;
import za.co.entelect.challenge.model.bomberman.players.BeamBombermanPlayer;

public class AreaProsperityBench
{
    public static void main(String[] args) throws InterruptedException
    {
        BombermanPlayer playerA = new BombermanPlayer('A');
        BombermanPlayer playerB = new BombermanPlayer('B');

        int[] totals = new int[2];
        for (int i = 0; i < 10; i++)
        {
            System.out.println("--- " + i + " ---");
            BombermanState gameState = BenchHelpers.createGameState(playerA, playerB);
            
            int score = play(gameState, false);
            System.out.println("Without: " + score);
            totals[0] += score;
            score = play(gameState, true);
            System.out.println("With: " + score);
            totals[1] += score;
        }
        
        System.out.println("=== Totals ===");
        System.out.println("Without\t" + totals[0]);
        System.out.println("With\t" + totals[1]);
    }
    
    private static int play(BombermanState gameState0, boolean includeAreaProsperityTerm) throws InterruptedException
    {
        BombermanState gameState = (BombermanState)gameState0.clone();

        BombermanPlayer playerA = gameState.getPlayer('A');
        GamePlayer<BombermanCell> gamePlayer = BenchHelpers.makeTimeLimited(
                new BeamBombermanPlayer(playerA, new TermBombermanHeuristic(includeAreaProsperityTerm)), 1200);
        
        BombermanEngine gameEngine = new BombermanEngine();
        for (int round = 0; (round < 100) && (!gameEngine.gameDidEnd(gameState)); round++)
            gameEngine.advanceRound(gameState, gamePlayer.getNextMove(gameState));
        
        return playerA.getScore();
    }
}
