package za.co.entelect.challenge.bench;

import za.co.entelect.challenge.logging.Logger;
import za.co.entelect.challenge.logging.Logger.LogLevel;
import za.co.entelect.challenge.model.GamePlayer;
import za.co.entelect.challenge.model.bomberman.BombermanCell;
import za.co.entelect.challenge.model.bomberman.BombermanPlayer;
import za.co.entelect.challenge.model.bomberman.heuristics.TermBombermanHeuristic;
import za.co.entelect.challenge.model.bomberman.players.BeamBombermanPlayer;
import za.co.entelect.challenge.model.bomberman.players.DFSBombermanPlayer;
import za.co.entelect.challenge.model.bomberman.players.DFSBombermanPlayer.PlayerSelection;
import za.co.entelect.challenge.model.bomberman.players.HybridSearchBombermanPlayer;
import za.co.entelect.challenge.model.bomberman.players.InstantKillBombermanPlayer;

public class DFSPlayerSelectionBench extends RotationBench
{
    public static void main(String[] args) throws InterruptedException
    {
        Logger.getInstance().setLogLevel(LogLevel.INFO);
        
        int[] totals = (new DFSPlayerSelectionBench()).run(20);
        
        System.out.println(PlayerSelection.NEAREST_PLAYER + ": " + (totals[0] + totals[3]));
        System.out.println(PlayerSelection.NEAR_PLAYERS + ": " + (totals[1] + totals[2]));
    }

    @Override
    public GamePlayer<BombermanCell> createPlayerForIndex(int index, BombermanPlayer player)
    {
        PlayerSelection playerSelection = (index % 4 == 0) || (index % 4 == 3) ? PlayerSelection.NEAREST_PLAYER : PlayerSelection.NEAR_PLAYERS;
        return BenchHelpers.makeTimeLimited(new InstantKillBombermanPlayer(
                new HybridSearchBombermanPlayer(
                        new BeamBombermanPlayer(player, new TermBombermanHeuristic()), 
                        new DFSBombermanPlayer(player, playerSelection, new TermBombermanHeuristic()))), 
                1600);
    }
}
