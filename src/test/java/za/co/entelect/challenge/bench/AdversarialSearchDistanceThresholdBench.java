package za.co.entelect.challenge.bench;

import za.co.entelect.challenge.model.GamePlayer;
import za.co.entelect.challenge.model.bomberman.BombermanCell;
import za.co.entelect.challenge.model.bomberman.BombermanPlayer;
import za.co.entelect.challenge.model.bomberman.heuristics.BombermanHeuristic;
import za.co.entelect.challenge.model.bomberman.heuristics.TermBombermanHeuristic;
import za.co.entelect.challenge.model.bomberman.players.AlphaBetaBombermanPlayer;
import za.co.entelect.challenge.model.bomberman.players.BeamBombermanPlayer;
import za.co.entelect.challenge.model.bomberman.players.DFSBombermanPlayer;
import za.co.entelect.challenge.model.bomberman.players.HybridAdversarialSearchBombermanPlayer;
import za.co.entelect.challenge.model.bomberman.players.HybridSearchBombermanPlayer;
import za.co.entelect.challenge.model.bomberman.players.InstantKillBombermanPlayer;

public class AdversarialSearchDistanceThresholdBench extends RotationBench
{
    public static void main(String[] args) throws InterruptedException
    {
        int[] totals = (new AdversarialSearchDistanceThresholdBench()).run(30);
        
        for (int i = 0; i < totals.length; i++)
            System.out.println(i + ": " + totals[i]);
    }

    @Override
    public GamePlayer<BombermanCell> createPlayerForIndex(int index, BombermanPlayer player)
    {
        int distance = 3 + index;
        
        BombermanHeuristic heuristic = new TermBombermanHeuristic();
        HybridAdversarialSearchBombermanPlayer inner = 
                new HybridAdversarialSearchBombermanPlayer(
                        new AlphaBetaBombermanPlayer(player, heuristic),
                        new DFSBombermanPlayer(player, new DFSBombermanPlayer.NearPlayersSelectionStrategyFactory(distance), heuristic));
        inner.setPlayerDistanceThreshold(distance);
        HybridSearchBombermanPlayer hybrid = new HybridSearchBombermanPlayer(
                new BeamBombermanPlayer(player, heuristic), 
                inner);
        
        return BenchHelpers.makeTimeLimited(new InstantKillBombermanPlayer(hybrid), 1200);
    }
}
