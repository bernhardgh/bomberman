package za.co.entelect.challenge.bench;

import za.co.entelect.challenge.logging.Logger;
import za.co.entelect.challenge.logging.Logger.LogLevel;
import za.co.entelect.challenge.model.GamePlayer;
import za.co.entelect.challenge.model.bomberman.BombermanCell;
import za.co.entelect.challenge.model.bomberman.BombermanEngine;
import za.co.entelect.challenge.model.bomberman.BombermanPlayer;
import za.co.entelect.challenge.model.bomberman.BombermanState;

public abstract class RotationBench
{
    private static final int PLAYER_COUNT = 4;
    
    public abstract GamePlayer<BombermanCell> createPlayerForIndex(int index, BombermanPlayer player);
    
    public int[] run(int rounds) throws InterruptedException
    {
        BombermanPlayer[] players = new BombermanPlayer[PLAYER_COUNT];
        for (int i = 0; i < PLAYER_COUNT; i++)
            players[i] = new BombermanPlayer((char)('A' + i));
        int[] totals = new int[PLAYER_COUNT];
        for (int i = 0; i < rounds; i++)
        {
            BombermanState gameState = BenchHelpers.createGameState(players);
            int[] playerIndices = new int[PLAYER_COUNT];
            for (int j = 0; j < PLAYER_COUNT; j++)
                playerIndices[j] = (i + j) % PLAYER_COUNT;
            
            int winnerIndex = play(gameState, playerIndices);
            totals[playerIndices[winnerIndex]]++;
            Logger.getInstance().log(LogLevel.WARNING, "%d) Player %d won (%d)", i, winnerIndex, playerIndices[winnerIndex]);
        }
        System.out.println();
        
        return totals;
    }
    
    private int play(BombermanState gameState0, int[] playerIndices) throws InterruptedException
    {
        BombermanState gameState = (BombermanState)gameState0.clone();

        @SuppressWarnings("unchecked")
        GamePlayer<BombermanCell>[] gamePlayers = new GamePlayer[playerIndices.length];
        for (int i = 0; i < gamePlayers.length; i++)
        {
            BombermanPlayer player = gameState.getPlayer((char)('A' + i));
            gamePlayers[i] = createPlayerForIndex(playerIndices[i], player);
        }
        
        BombermanEngine gameEngine = new BombermanEngine();
        for (int round = 0; (round < 200) && (!gameEngine.gameDidEnd(gameState)); round++)
        {
            for (GamePlayer<BombermanCell> nextGamePlayer : gamePlayers)
                gameEngine.applyMove(gameState, nextGamePlayer.getPlayer(), nextGamePlayer.getNextMove(gameState));
            gameEngine.advanceRound(gameState);
        }
        
        if (gameEngine.gameDidEnd(gameState))
        {
            for (int i = 0; i < gamePlayers.length; i++)
                if (gameEngine.playerDidWin(gameState, gamePlayers[i].getPlayer()))
                    return i;
        }
        
        int largestScoreIndex = -1;
        int largestScore = Integer.MIN_VALUE;
        for (int i = 0; i < gamePlayers.length; i++)
        {
            BombermanPlayer player = (BombermanPlayer)gamePlayers[i].getPlayer();
            if (player.isAlive())
            {
                int score = player.getScore();
                if (score > largestScore)
                {
                    largestScore = score;
                    largestScoreIndex = i;
                }
            }
        }
        
        if (largestScoreIndex == -1)
        {
            for (int i = 0; i < gamePlayers.length; i++)
            {
                BombermanPlayer player = (BombermanPlayer)gamePlayers[i].getPlayer();
                int score = player.getScore();
                if (score > largestScore)
                {
                    largestScore = score;
                    largestScoreIndex = i;
                }
            }
        }
        
        return largestScoreIndex;
    }
}
