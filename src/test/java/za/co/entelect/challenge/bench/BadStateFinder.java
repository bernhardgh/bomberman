package za.co.entelect.challenge.bench;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import za.co.entelect.challenge.model.GamePlayer;
import za.co.entelect.challenge.model.Move;
import za.co.entelect.challenge.model.MoveInfo;
import za.co.entelect.challenge.model.bomberman.BombermanCell;
import za.co.entelect.challenge.model.bomberman.BombermanEngine;
import za.co.entelect.challenge.model.bomberman.BombermanPlayer;
import za.co.entelect.challenge.model.bomberman.BombermanState;
import za.co.entelect.challenge.model.bomberman.players.BombermanPlayerFactory;

public class BadStateFinder
{
    public static void main(String[] args) throws Exception
    {
        while (true)
        {
            BombermanPlayer playerA = new BombermanPlayer('A');
            BombermanPlayer playerB = new BombermanPlayer('B');
            BombermanPlayer playerC = new BombermanPlayer('C');
            BombermanPlayer playerD = new BombermanPlayer('D');
            BombermanState gameState = BenchHelpers.createGameState(playerA, playerB, playerC, playerD);
            
            play(gameState);
        }
    }
    
    private static void play(BombermanState gameState) throws Exception
    {
        @SuppressWarnings("unchecked")
        GamePlayer<BombermanCell>[] gamePlayers = new GamePlayer[4];
        for (int i = 0; i < gamePlayers.length; i++)
            gamePlayers[i] = BombermanPlayerFactory.createPlayer("DFS", gameState.getPlayers()[i], 1800);
        BombermanEngine gameEngine = new BombermanEngine();
        
        while (!gameEngine.gameDidEnd(gameState))
        {
            BombermanState previous = (BombermanState)gameState.clone();
            
            MoveInfo moveInfo = null;
            ArrayList<Move> moves = new ArrayList<>();
            try
            {
                for (GamePlayer<BombermanCell> nextGamePlayer : gamePlayers)
                {
                    nextGamePlayer.setPlayer(gameState.getPlayer(nextGamePlayer.getPlayer().getPlayerKey()));
                    Move move = nextGamePlayer.getNextMove(gameState);
                    moves.add(move);
                }
                
                for (int i = 0; i < gamePlayers.length; i++)
                    moveInfo = concatenate(moveInfo, gameEngine.applyMove(gameState, gameState.getPlayers()[i], moves.get(i)));
                moveInfo = concatenate(moveInfo, gameEngine.advanceRound(gameState));
            }
            catch (Exception e)
            {
                log(previous, moves, "error_clone");
                store(gameState, "error");
                throw e;
            }
            assertConsistent(gameState);
            BombermanState next = (BombermanState)gameState.clone();
            assertConsistent(next);
            
            gameEngine.undo(moveInfo);
            try
            {
                BenchAssertHelpers.assertEqual(previous, gameState);
                gameState = next;
            }
            catch (AssertionError e)
            {
                log(previous, moves, "corrupt");
                throw e;
            }
        }
    }
    
    private static MoveInfo concatenate(MoveInfo one, MoveInfo two)
    {
        return one == null ? two : one.concatenate(two);
    }
    
    private static void assertConsistent(BombermanState gameState) throws IOException
    {
        try
        {
            BenchAssertHelpers.assertConsistent(gameState);
        }
        catch (AssertionError e)
        {
            log(gameState, "inconsistent");
            throw e;
        }
    }
    
    private static void log(BombermanState gameState, String category) throws IOException
    {
        log(gameState, null, category);
    }
    
    private static void log(BombermanState gameState, List<Move> moves, String category) throws IOException
    {
        System.out.println(" ==== " + category + " ====");
        System.out.println(gameState.getGrid());
        if (moves != null)
            System.out.println(moves);
        store(gameState, category);
    }
    
    private static void store(BombermanState gameState, String category) throws IOException
    {
        try (ObjectOutputStream stream = new ObjectOutputStream(new FileOutputStream("/Users/bernhard/Documents/Projects/Java/bomberman/boards/badstate/" + category)))
        {
            stream.writeObject(gameState);
        }
    }
}
