package za.co.entelect.challenge.bench;

import za.co.entelect.challenge.model.GamePlayer;
import za.co.entelect.challenge.model.bomberman.BombermanCell;
import za.co.entelect.challenge.model.bomberman.BombermanEngine;
import za.co.entelect.challenge.model.bomberman.BombermanPlayer;
import za.co.entelect.challenge.model.bomberman.BombermanState;
import za.co.entelect.challenge.model.bomberman.players.BombermanPlayerFactory;

public class GamePlayerTypeBench
{
    public static void main(String[] args) throws InterruptedException
    {
        String[] playerTypes = new String[] { "DFS", "Beam" };
        
        BombermanPlayer playerA = new BombermanPlayer('A');
        BombermanPlayer playerB = new BombermanPlayer('B');
        int[] totals = new int[playerTypes.length];
        for (int i = 0; i < 15; i++)
        {
            System.out.println("--- " + i + " ---");
            BombermanState gameState = BenchHelpers.createGameState(playerA, playerB);
            
            for (int playerTypeIndex = 0; playerTypeIndex < playerTypes.length; playerTypeIndex++)
            {
                int score = play(gameState, playerTypes[playerTypeIndex]);
                totals[playerTypeIndex] += score;
                System.out.println(playerTypes[playerTypeIndex] + ": " + score);
            }
        }
        
        System.out.println();
        for (int i = 0; i < playerTypes.length; i++)
            System.out.println(playerTypes[i] + ": " + totals[i]);
    }
    
    private static int play(BombermanState gameState0, String playerType) throws InterruptedException
    {
        BombermanState gameState = (BombermanState)gameState0.clone();

        BombermanPlayer playerA = gameState.getPlayer('A');
        GamePlayer<BombermanCell> gamePlayer = BombermanPlayerFactory.createPlayer(playerType, playerA, 1900);
        
        BombermanEngine gameEngine = new BombermanEngine();
        for (int round = 0; (round < 80) && (!gameEngine.gameDidEnd(gameState)); round++)
            gameEngine.advanceRound(gameState, gamePlayer.getNextMove(gameState));
        
        return playerA.getScore();
    }
}
