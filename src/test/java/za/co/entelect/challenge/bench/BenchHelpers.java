package za.co.entelect.challenge.bench;

import za.co.entelect.challenge.model.GamePlayer;
import za.co.entelect.challenge.model.TimeLimitedGamePlayer;
import za.co.entelect.challenge.model.bomberman.BombermanCell;
import za.co.entelect.challenge.model.bomberman.BombermanGrid;
import za.co.entelect.challenge.model.bomberman.BombermanGridGenerator;
import za.co.entelect.challenge.model.bomberman.BombermanPlayer;
import za.co.entelect.challenge.model.bomberman.BombermanState;
import za.co.entelect.challenge.model.bomberman.players.SafeBombermanPlayer;

public class BenchHelpers
{
    public static BombermanState createGameState(BombermanPlayer... players)
    {
        BombermanGridGenerator generator = new BombermanGridGenerator(21, 21, players);
        BombermanGrid grid = generator.getBombermanGrid();
        BombermanState gameState = new BombermanState(grid, players);
        gameState.setPlayerKillPoints(generator.getPlayerKillPoints());
        gameState.initializeCodeIntegration();
        
        return gameState;
    }

    public static GamePlayer<BombermanCell> makeTimeLimited(GamePlayer<BombermanCell> gamePlayer, long timeLimit)
    {
        return timeLimit == -1 ? gamePlayer : new TimeLimitedGamePlayer<BombermanCell>(gamePlayer, new SafeBombermanPlayer(gamePlayer.getPlayer()), Math.max(0, timeLimit - 200), timeLimit);
    }
}
