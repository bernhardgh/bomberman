package za.co.entelect.challenge.bench;

import za.co.entelect.challenge.model.GamePlayer;
import za.co.entelect.challenge.model.bomberman.BombermanCell;
import za.co.entelect.challenge.model.bomberman.BombermanPlayer;
import za.co.entelect.challenge.model.bomberman.heuristics.BombermanHeuristic;
import za.co.entelect.challenge.model.bomberman.heuristics.TermBombermanHeuristic;
import za.co.entelect.challenge.model.bomberman.players.AlphaBetaBombermanPlayer;
import za.co.entelect.challenge.model.bomberman.players.BeamBombermanPlayer;
import za.co.entelect.challenge.model.bomberman.players.DFSBombermanPlayer;
import za.co.entelect.challenge.model.bomberman.players.HybridAdversarialSearchBombermanPlayer;
import za.co.entelect.challenge.model.bomberman.players.DFSBombermanPlayer.PlayerSelection;
import za.co.entelect.challenge.model.bomberman.players.HybridSearchBombermanPlayer;
import za.co.entelect.challenge.model.bomberman.players.InstantKillBombermanPlayer;

public class AlphaBetaDFSBench extends RotationBench
{
    public static void main(String[] args) throws InterruptedException
    {
        int[] totals = (new AlphaBetaDFSBench()).run(30);
        
        for (int i = 0; i < totals.length; i++)
            System.out.println(i + ": " + totals[i]);
    }

    @Override
    public GamePlayer<BombermanCell> createPlayerForIndex(int index, BombermanPlayer player)
    {
        BombermanHeuristic heuristic = new TermBombermanHeuristic();
        GamePlayer<BombermanCell> inner = null;
        switch (index)
        {
            case 0 : inner = new AlphaBetaBombermanPlayer(player, heuristic); break;
            case 1 : inner = new DFSBombermanPlayer(player, PlayerSelection.NEAREST_PLAYER, heuristic); break;
            case 2 : inner = new HybridAdversarialSearchBombermanPlayer(
                    new AlphaBetaBombermanPlayer(player, heuristic),
                    new DFSBombermanPlayer(player, PlayerSelection.NEAR_PLAYERS, heuristic)); break;
            case 3 : inner = new HybridAdversarialSearchBombermanPlayer(
                    new AlphaBetaBombermanPlayer(player, heuristic),
                    new DFSBombermanPlayer(player, new DFSBombermanPlayer.NearPlayersSelectionStrategyFactory(HybridAdversarialSearchBombermanPlayer.DEFAULT_PLAYER_DISTANCE_THRESHOLD), heuristic)); 
                break;
        }
        
        return BenchHelpers.makeTimeLimited(new InstantKillBombermanPlayer(
                new HybridSearchBombermanPlayer(
                        new BeamBombermanPlayer(player, new TermBombermanHeuristic()), 
                        inner)), 
                1200);
    }
}
