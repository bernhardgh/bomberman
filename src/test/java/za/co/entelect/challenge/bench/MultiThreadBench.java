package za.co.entelect.challenge.bench;

import za.co.entelect.challenge.model.GamePlayer;
import za.co.entelect.challenge.model.bomberman.BombermanCell;
import za.co.entelect.challenge.model.bomberman.BombermanPlayer;
import za.co.entelect.challenge.model.bomberman.heuristics.TermBombermanHeuristic;
import za.co.entelect.challenge.model.bomberman.players.AlphaBetaBombermanPlayer;
import za.co.entelect.challenge.model.bomberman.players.BeamBombermanPlayer;
import za.co.entelect.challenge.model.bomberman.players.HybridSearchBombermanPlayer;
import za.co.entelect.challenge.model.bomberman.players.InstantKillBombermanPlayer;
import za.co.entelect.challenge.model.bomberman.players.MultiThreadedAlphaBetaBombermanPlayer;

public class MultiThreadBench extends RotationBench
{
    public static void main(String[] args) throws InterruptedException
    {
        int[] totals = (new MultiThreadBench()).run(30);

        System.out.println("Simple: " + (totals[1] + totals[2]));
        System.out.println("Multi-threaded: " + (totals[0] + totals[3]));
    }

    @Override
    public GamePlayer<BombermanCell> createPlayerForIndex(int index, BombermanPlayer player)
    {
        boolean multiThreaded = (index % 4 == 0) || (index % 4 == 3);
        return BenchHelpers.makeTimeLimited(new InstantKillBombermanPlayer(
                new HybridSearchBombermanPlayer(
                        new BeamBombermanPlayer(player, new TermBombermanHeuristic()), 
                        multiThreaded
                        ? new MultiThreadedAlphaBetaBombermanPlayer(player, new TermBombermanHeuristic())
                        : new AlphaBetaBombermanPlayer(player, new TermBombermanHeuristic()))), 
                1200);
    }
}
