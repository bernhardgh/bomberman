package za.co.entelect.challenge.bench;

import java.io.IOException;

import za.co.entelect.challenge.logging.CompositeLogTarget;
import za.co.entelect.challenge.logging.ConsoleLogTarget;
import za.co.entelect.challenge.logging.ListLogTarget;
import za.co.entelect.challenge.logging.Logger;
import za.co.entelect.challenge.logging.Logger.LogLevel;
import za.co.entelect.challenge.model.GamePlayer;
import za.co.entelect.challenge.model.bomberman.BombermanCell;
import za.co.entelect.challenge.model.bomberman.BombermanEngine;
import za.co.entelect.challenge.model.bomberman.BombermanState;
import za.co.entelect.challenge.model.bomberman.players.BombermanPlayerFactory;
import za.co.entelect.challenge.tests.FileHelpers;

public class Profiling
{
    public static void main(String[] args) throws ClassNotFoundException, IOException, InterruptedException
    {
        ListLogTarget listLogTarget = new ListLogTarget();
        Logger logger = Logger.getInstance();
        logger.setLogTarget(new CompositeLogTarget(new ConsoleLogTarget(), listLogTarget));
        logger.setLogLevel(LogLevel.INFO);
        
        BombermanState gameState = FileHelpers.loadTestStateFromResource("profiling/nearby");
        GamePlayer<BombermanCell> gamePlayer = BombermanPlayerFactory.createPlayer("DFS-Nearest", gameState.getPlayer('A'), 1200);

        BombermanEngine gameEngine = new BombermanEngine();
        int round;
        for (round = 0; (round < 20) && (!gameEngine.gameDidEnd(gameState)); round++)
            gameEngine.advanceRound(gameState, gamePlayer.getNextMove(gameState));
        
        int totalDepth = 0, totalNodeCount = 0;
        for (String nextLog : listLogTarget.getLogs())
        {
            totalDepth += tryParseInt(nextLog, "Depth reached: ");
            totalNodeCount += tryParseInt(nextLog, "Node count: ");
        }
        
        System.out.println("Average depth: " + ((double)totalDepth / round));
        System.out.println("Average node count: " + ((double)totalNodeCount / round));
    }
    
    private static int tryParseInt(String line, String identifier)
    {
        int identifierIndex = line.indexOf(identifier);
        if (identifierIndex == -1)
            return 0;
        
        int startIndex = identifierIndex + identifier.length();
        int endIndex = line.indexOf(' ', startIndex);
        return Integer.parseInt(line.substring(startIndex, endIndex));
    }
}
