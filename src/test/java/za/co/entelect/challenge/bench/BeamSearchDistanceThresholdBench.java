package za.co.entelect.challenge.bench;

import za.co.entelect.challenge.model.GamePlayer;
import za.co.entelect.challenge.model.bomberman.BombermanCell;
import za.co.entelect.challenge.model.bomberman.BombermanPlayer;
import za.co.entelect.challenge.model.bomberman.heuristics.BombermanHeuristic;
import za.co.entelect.challenge.model.bomberman.heuristics.TermBombermanHeuristic;
import za.co.entelect.challenge.model.bomberman.players.AlphaBetaBombermanPlayer;
import za.co.entelect.challenge.model.bomberman.players.BeamBombermanPlayer;
import za.co.entelect.challenge.model.bomberman.players.HybridSearchBombermanPlayer;
import za.co.entelect.challenge.model.bomberman.players.InstantKillBombermanPlayer;

public class BeamSearchDistanceThresholdBench extends RotationBench
{
    public static void main(String[] args) throws InterruptedException
    {
        int[] totals = (new BeamSearchDistanceThresholdBench()).run(30);
        
        for (int i = 0; i < totals.length; i++)
            System.out.println(i + ": " + totals[i]);
    }

    @Override
    public GamePlayer<BombermanCell> createPlayerForIndex(int index, BombermanPlayer player)
    {
        int distance = 5 + index;
        
        BombermanHeuristic heuristic = new TermBombermanHeuristic();
        GamePlayer<BombermanCell> inner = new AlphaBetaBombermanPlayer(player, heuristic);
        HybridSearchBombermanPlayer hybrid = new HybridSearchBombermanPlayer(
                new BeamBombermanPlayer(player, heuristic), 
                inner);
        hybrid.setPlayerDistanceThreshold(distance);
        
        return BenchHelpers.makeTimeLimited(new InstantKillBombermanPlayer(hybrid), 1200);
    }
}
