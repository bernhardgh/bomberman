package za.co.entelect.challenge.tests.players;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import static org.junit.Assert.*;

import org.junit.Test;

import za.co.entelect.challenge.dto.DodgeCounts;
import za.co.entelect.challenge.model.GamePlayer;
import za.co.entelect.challenge.model.GameState;
import za.co.entelect.challenge.model.Move;
import za.co.entelect.challenge.model.Player;
import za.co.entelect.challenge.model.bomberman.BombermanCell;
import za.co.entelect.challenge.model.bomberman.BombermanEngine;
import za.co.entelect.challenge.model.bomberman.BombermanMove;
import za.co.entelect.challenge.model.bomberman.BombermanPlayer;
import za.co.entelect.challenge.model.bomberman.BombermanState;
import za.co.entelect.challenge.model.bomberman.players.InstantKillBombermanPlayer;
import za.co.entelect.challenge.tests.FileHelpers;

public class TestInstantKillBombermanPlayer
{
    public TestInstantKillBombermanPlayer()
    {
        InstantKillBombermanPlayer.resetDodgeCountsFile();
    }
    
    @Test
    public void testCannotDodge() throws ClassNotFoundException, InterruptedException, IOException
    {
        setPlayerDodgeCount('B', InstantKillBombermanPlayer.DODGE_COUNT_THRESHOLD + 1);
        assertEquals(BombermanMove.TRIGGER_BOMB, getMoveFromState("triggerCannotDodge", 'A'));
    }
    
    @Test
    public void testCanDodgeLowDodgeCount() throws ClassNotFoundException, InterruptedException, IOException
    {
        assertEquals(BombermanMove.TRIGGER_BOMB, getMoveFromState("triggerCanDodge", 'A'));
    }
    
    @Test
    public void testCanDodgeHighDodgeCount() throws ClassNotFoundException, InterruptedException, IOException
    {
        setPlayerDodgeCount('B', InstantKillBombermanPlayer.DODGE_COUNT_THRESHOLD);
        assertEquals(BombermanMove.RIGHT, getMoveFromState("triggerCanDodge", 'A'));
    }
    
    
    private static void setPlayerDodgeCount(char playerKey, int playerDodgeCount) throws FileNotFoundException, IOException
    {
        BombermanPlayer player = new BombermanPlayer(playerKey);
        player.setDodgeCount(playerDodgeCount);
        try (FileOutputStream stream = new FileOutputStream(InstantKillBombermanPlayer.DODGE_COUNTS_FILE_PATH))
        {
            (new DodgeCounts(player)).saveCounts(stream);
        }
    }
    
    private static BombermanMove getMoveFromState(String stateName, char playerKey) throws InterruptedException, ClassNotFoundException, IOException
    {
        BombermanState gameState = FileHelpers.loadTestStateFromResource("teststates/" + stateName);
        BombermanPlayer player = gameState.getPlayer(playerKey);
        GamePlayer<BombermanCell> gamePlayer = new InstantKillBombermanPlayer(new GoRightBombermanPlayer(player));
        
        return (BombermanMove)gamePlayer.getNextMove(gameState);
    }
    
    
    private static class GoRightBombermanPlayer extends GamePlayer<BombermanCell>
    {
        public GoRightBombermanPlayer(Player player)
        {
            super(new BombermanEngine(), player);
        }

        @Override
        public Move getNextMove(GameState<BombermanCell> gameState) throws InterruptedException
        {
            return getGameEngine().isMoveValid(gameState, getPlayer(), BombermanMove.RIGHT) ? BombermanMove.RIGHT : BombermanMove.DO_NOTHING;
        }
    }
}
