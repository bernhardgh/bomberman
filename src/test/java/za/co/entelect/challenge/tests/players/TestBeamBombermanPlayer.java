package za.co.entelect.challenge.tests.players;

import java.io.IOException;

import static org.junit.Assert.*;
import org.junit.Test;

import za.co.entelect.challenge.model.GamePlayer;
import za.co.entelect.challenge.model.bomberman.BombermanCell;
import za.co.entelect.challenge.model.bomberman.BombermanMove;
import za.co.entelect.challenge.model.bomberman.BombermanPlayer;
import za.co.entelect.challenge.model.bomberman.BombermanState;
import za.co.entelect.challenge.model.bomberman.heuristics.TermBombermanHeuristic;
import za.co.entelect.challenge.model.bomberman.players.BeamBombermanPlayer;
import za.co.entelect.challenge.tests.FileHelpers;

public class TestBeamBombermanPlayer
{
    @Test
    public void testDontGoAfterOpponent() throws ClassNotFoundException, InterruptedException, IOException
    {
        BombermanMove move = getMoveFromState("stagnant2", 'A');
        assertNotEquals(BombermanMove.DO_NOTHING, move);
    }
    
    private BombermanMove getMoveFromState(String stateName, char playerKey) throws InterruptedException, ClassNotFoundException, IOException
    {
        BombermanState gameState = FileHelpers.loadTestStateFromResource("teststates/" + stateName);
        BombermanPlayer player = gameState.getPlayer(playerKey);
        GamePlayer<BombermanCell> gamePlayer = new BeamBombermanPlayer(player, new TermBombermanHeuristic());
        
        return (BombermanMove)gamePlayer.getNextMove(gameState);
    }
}
