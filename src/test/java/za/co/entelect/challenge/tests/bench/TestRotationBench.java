package za.co.entelect.challenge.tests.bench;

import static org.junit.Assert.*;

import org.junit.Test;

import za.co.entelect.challenge.bench.RotationBench;
import za.co.entelect.challenge.logging.Logger;
import za.co.entelect.challenge.logging.Logger.LogLevel;
import za.co.entelect.challenge.model.GamePlayer;
import za.co.entelect.challenge.model.GameState;
import za.co.entelect.challenge.model.Move;
import za.co.entelect.challenge.model.Player;
import za.co.entelect.challenge.model.bomberman.BombermanCell;
import za.co.entelect.challenge.model.bomberman.BombermanEngine;
import za.co.entelect.challenge.model.bomberman.BombermanMove;
import za.co.entelect.challenge.model.bomberman.BombermanPlayer;
import za.co.entelect.challenge.model.bomberman.players.LazyBombermanPlayer;

public class TestRotationBench
{
    @Test
    public void testBench() throws InterruptedException
    {
        Logger logger = Logger.getInstance();
        LogLevel previousLogLevel = logger.getLogLevel();
        logger.setLogLevel(LogLevel.ERROR);
        try
        {
            RotationBenchTest tester = new RotationBenchTest();
            int[] totals = tester.run(8);
            
            assertEquals(8, totals[0]);
            assertEquals(0, totals[1]);
            assertEquals(0, totals[2]);
            assertEquals(0, totals[3]);
        }
        finally
        {
            logger.setLogLevel(previousLogLevel);
        }
    }
    
    
    private class RotationBenchTest extends RotationBench
    {
        @Override
        public GamePlayer<BombermanCell> createPlayerForIndex(int index, BombermanPlayer player)
        {
            return index == 0 ? new LazyBombermanPlayer(player) : new PlantBombBombermanPlayer(player);
        }
    }
    
    
    private class PlantBombBombermanPlayer extends GamePlayer<BombermanCell>
    {
        public PlantBombBombermanPlayer(Player player)
        {
            super(new BombermanEngine(), player);
        }

        @Override
        public Move getNextMove(GameState<BombermanCell> gameState) throws InterruptedException
        {
            return getGameEngine().isMoveValid(gameState, getPlayer(), BombermanMove.PLACE_BOMB) ? BombermanMove.PLACE_BOMB : BombermanMove.DO_NOTHING;
        }
    }
}
