package za.co.entelect.challenge.tests.model;

import static org.junit.Assert.*;

import org.junit.Test;

import za.co.entelect.challenge.model.bomberman.BombermanGrid;
import za.co.entelect.challenge.model.bomberman.BombermanPlayer;
import za.co.entelect.challenge.model.bomberman.BombermanState;
import za.co.entelect.challenge.model.bomberman.BombermanCell.CellType;
import za.co.entelect.challenge.util.ComputedHash;

public class TestGameStateHash
{
    private BombermanPlayer player = new BombermanPlayer('A'); 
    private BombermanState gameState;
    
    public TestGameStateHash()
    {
        BombermanGrid grid = new BombermanGrid(3, 3);
        gameState = new BombermanState(grid, player);
    }
    
    @Test
    public void testGameStateHash()
    {
        initializeCodeIntegration();
        ComputedHash initialHash = gameState.getComputedHash();
        gameState.getGrid().getCell(1, 1).setCell(CellType.BOMB_EXPLOSION);
        assertNotEquals(initialHash, gameState.getComputedHash());
        
        ComputedHash secondHash = gameState.getComputedHash();
        player.increaseBombBagSize();
        assertNotEquals(secondHash, gameState.getComputedHash());
        
        player.setBombCounts(player.getBombBagSize() - 1);
        gameState.getGrid().getCell(1, 1).setCell(CellType.CLEAR);
        assertEquals(initialHash, gameState.getComputedHash());
    }
    
    @Test
    public void testGameStateHashBomb()
    {
        initializeCodeIntegration();
        ComputedHash initialHash = gameState.getComputedHash();
        gameState.getGrid().getCell(1, 1).setBomb(3, 1, player);
        assertNotEquals(initialHash, gameState.getComputedHash());
        
        gameState.getGrid().getCell(1, 1).setBombDidExplode();
        gameState.getGrid().getCell(1, 1).grantBomb();
        gameState.getGrid().getCell(1, 1).performCellChange(CellType.CLEAR);
        assertEquals(initialHash, gameState.getComputedHash());
    }
    
    private void initializeCodeIntegration()
    {
        assertEquals(new ComputedHash(0, 0), gameState.getComputedHash());

        gameState.initializeCodeIntegration();
        assertNotEquals(new ComputedHash(0, 0), gameState.getComputedHash());
    }
}
