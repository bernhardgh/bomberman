package za.co.entelect.challenge.tests.model;

import static org.junit.Assert.*;

import org.junit.Test;

import za.co.entelect.challenge.model.bomberman.BombermanEngine;
import za.co.entelect.challenge.model.bomberman.BombermanGrid;
import za.co.entelect.challenge.model.bomberman.BombermanMove;
import za.co.entelect.challenge.model.bomberman.BombermanPlayer;
import za.co.entelect.challenge.model.bomberman.BombermanState;
import za.co.entelect.challenge.model.bomberman.BombermanCell.CellType;

public class TestBombs
{
    private BombermanEngine gameEngine = new BombermanEngine();
    private BombermanPlayer playerA = new BombermanPlayer('A'), playerB = new BombermanPlayer('B');
    private BombermanState gameState;
    
    public TestBombs()
    {
        BombermanGrid grid = new BombermanGrid(9, 9);
        gameState = new BombermanState(grid, playerA, playerB);
        gameState.setPlayerKillPoints(15);
    }
    
    @Test
    public void testTriggerBombNoBombs()
    {
        assertFalse(gameEngine.isMoveValid(gameState, playerA, BombermanMove.TRIGGER_BOMB));
    }
    
    @Test
    public void testTriggerBomb()
    {
        playerA.setBombCounts(2);
        gameState.getGrid().getCell(2, 3).performCellChange(playerA);
        gameEngine.advanceRound(gameState, BombermanMove.PLACE_BOMB);
        gameEngine.advanceRound(gameState, BombermanMove.UP);
        gameEngine.advanceRound(gameState, BombermanMove.LEFT);
        gameEngine.advanceRound(gameState, BombermanMove.PLACE_BOMB);

        assertEquals(0, playerA.getAvailableBombCount());
        assertEquals(3, gameState.getGrid().getCell(2, 3).getBombCountdown());
        assertEquals(6, gameState.getGrid().getCell(1, 2).getBombCountdown());
        assertTrue(gameEngine.isMoveValid(gameState, playerA, BombermanMove.TRIGGER_BOMB));
        gameEngine.advanceRound(gameState, BombermanMove.TRIGGER_BOMB);

        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(2, 3).getCellType());
        assertEquals(5, gameState.getGrid().getCell(1, 2).getBombCountdown());
        assertEquals(0, playerA.getAvailableBombCount());
        gameEngine.advanceRound(gameState, BombermanMove.LEFT);
        assertEquals(1, playerA.getAvailableBombCount());
        gameEngine.advanceRound(gameState, BombermanMove.DOWN);

        assertEquals(3, gameState.getGrid().getCell(1, 2).getBombCountdown());
        gameEngine.advanceRound(gameState, BombermanMove.TRIGGER_BOMB);

        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(1, 2).getCellType());
        assertEquals(1, playerA.getAvailableBombCount());
        assertFalse(gameEngine.isMoveValid(gameState, playerA, BombermanMove.TRIGGER_BOMB));
        
        gameEngine.advanceRound(gameState);
        assertEquals(2, playerA.getAvailableBombCount());
    }
    
    @Test
    public void testPlantBombWhenNoBombsLeft()
    {
        gameState.getGrid().getCell(1, 1).performCellChange(playerA);
        playerA.setBombCounts(0);
        
        assertFalse(gameEngine.isMoveValid(gameState, playerA, BombermanMove.PLACE_BOMB));
    }
    
    @Test
    public void testPlantTwoBombs()
    {
        playerA.setBombCounts(2);
        gameState.getGrid().getCell(1, 1).performCellChange(playerA);
        gameState.getGrid().getCell(1, 1).plantBomb();
        
        assertEquals(1, playerA.getAvailableBombCount());
        assertEquals(2, playerA.getBombBagSize());
        assertFalse(gameEngine.isMoveValid(gameState, playerA, BombermanMove.PLACE_BOMB));
    }
    
    @Test
    public void testPlantBomb()
    {
        gameState.getGrid().getCell(1, 2).performCellChange(playerA);
        
        assertTrue(gameEngine.isMoveValid(gameState, playerA, BombermanMove.PLACE_BOMB));
        gameEngine.advanceRound(gameState, BombermanMove.PLACE_BOMB);
        
        assertEquals(3, gameState.getGrid().getCell(1, 2).getBombCountdown());
        assertEquals(1, gameState.getGrid().getCell(1, 2).getBombRadius());
        assertEquals(0, playerA.getAvailableBombCount());
        assertEquals(1, playerA.getBombBagSize());
        gameEngine.advanceRound(gameState, BombermanMove.LEFT);

        assertEquals(2, gameState.getGrid().getCell(1, 2).getBombCountdown());
        assertEquals(playerA, gameState.getGrid().getCell(1, 2).getCellOwner());
        gameEngine.advanceRound(gameState, BombermanMove.DOWN);
        
        assertEquals(1, gameState.getGrid().getCell(1, 2).getBombCountdown());
        gameEngine.advanceRound(gameState, BombermanMove.DO_NOTHING);

        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(1, 1).getCellType());
        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(1, 2).getCellType());
        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(1, 3).getCellType());
        assertEquals(CellType.CLEAR, gameState.getGrid().getCell(1, 4).getCellType());
        assertEquals(CellType.INDESTRUCTIBLE_WALL, gameState.getGrid().getCell(0, 2).getCellType());
        assertEquals(CellType.INDESTRUCTIBLE_WALL, gameState.getGrid().getCell(2, 2).getCellType());
        assertEquals(0, playerA.getAvailableBombCount());
        assertEquals(1, playerA.getBombBagSize());
        gameEngine.advanceRound(gameState, BombermanMove.DO_NOTHING);

        assertEquals(CellType.CLEAR, gameState.getGrid().getCell(1, 1).getCellType());
        assertEquals(CellType.CLEAR, gameState.getGrid().getCell(1, 2).getCellType());
        assertEquals(CellType.CLEAR, gameState.getGrid().getCell(1, 3).getCellType());
        assertNull(gameState.getGrid().getCell(1, 2).getCellOwner());
        assertEquals(1, playerA.getAvailableBombCount());
        assertEquals(1, playerA.getBombBagSize());
    }
    
    @Test
    public void testDestroyWalls()
    {
        gameState.getGrid().getCell(1, 1).performCellChange(CellType.DESTRUCTIBLE_WALL);
        gameState.getGrid().getCell(1, 4).performCellChange(CellType.DESTRUCTIBLE_WALL);
        gameState.getGrid().getCell(1, 5).performCellChange(CellType.DESTRUCTIBLE_WALL);
        gameState.getGrid().getCell(1, 3).performCellChange(playerA);
        gameState.getGrid().getCell(1, 3).plantBomb();
        gameState.getGrid().getCell(1, 3).setBomb(1, 2, playerA);
        gameState.getGrid().getCell(1, 3).performCellChange(CellType.CLEAR);
        assertEquals(0, playerA.getAvailableBombCount());
        assertEquals(2, playerA.getScore());
        gameEngine.advanceRound(gameState);

        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(1, 1).getCellType());
        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(1, 4).getCellType());
        assertEquals(CellType.DESTRUCTIBLE_WALL, gameState.getGrid().getCell(1, 5).getCellType());
        assertEquals(0, playerA.getAvailableBombCount());
        assertEquals(22, playerA.getScore());
        gameEngine.advanceRound(gameState);

        assertEquals(CellType.CLEAR, gameState.getGrid().getCell(1, 1).getCellType());
        assertEquals(CellType.CLEAR, gameState.getGrid().getCell(1, 4).getCellType());
        assertEquals(CellType.DESTRUCTIBLE_WALL, gameState.getGrid().getCell(1, 5).getCellType());
        assertEquals(1, playerA.getAvailableBombCount());
    }
    
    @Test
    public void testBothDestroyWall()
    {
        gameState.getGrid().getCell(1, 2).performCellChange(CellType.BOMB_BAG_POWER_UP);
        gameState.getGrid().getCell(1, 3).performCellChange(CellType.DESTRUCTIBLE_WALL);
        gameState.getGrid().getCell(1, 1).performCellChange(playerA);
        gameState.getGrid().getCell(1, 1).plantBomb();
        gameState.getGrid().getCell(1, 1).setBomb(1, 2, playerA);
        gameState.getGrid().getCell(1, 1).performCellChange(CellType.CLEAR);
        assertEquals(0, playerA.getAvailableBombCount());
        assertEquals(2, playerA.getScore());
        gameState.getGrid().getCell(1, 4).performCellChange(playerB);
        gameState.getGrid().getCell(1, 4).plantBomb();
        gameState.getGrid().getCell(1, 4).setBomb(1, 2, playerB);
        gameState.getGrid().getCell(1, 4).performCellChange(CellType.CLEAR);
        assertEquals(0, playerB.getAvailableBombCount());
        assertEquals(2, playerB.getScore());
        gameEngine.advanceRound(gameState);

        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(1, 1).getCellType());
        assertEquals(CellType.BOMB_BAG_POWER_UP, gameState.getGrid().getCell(1, 2).getCellType());
        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(1, 3).getCellType());
        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(1, 4).getCellType());
        assertEquals(0, playerA.getAvailableBombCount());
        assertEquals(12, playerA.getScore());
        assertEquals(0, playerB.getAvailableBombCount());
        assertEquals(12, playerB.getScore());
        gameEngine.advanceRound(gameState);

        assertEquals(CellType.CLEAR, gameState.getGrid().getCell(1, 1).getCellType());
        assertEquals(CellType.BOMB_BAG_POWER_UP, gameState.getGrid().getCell(1, 2).getCellType());
        assertEquals(CellType.CLEAR, gameState.getGrid().getCell(1, 3).getCellType());
        assertEquals(CellType.CLEAR, gameState.getGrid().getCell(1, 4).getCellType());
        assertEquals(1, playerA.getAvailableBombCount());
        assertEquals(1, playerB.getAvailableBombCount());
    }
    
    @Test
    public void testBombChain()
    {
        gameState.getGrid().getCell(1, 1).performCellChange(CellType.DESTRUCTIBLE_WALL);
        gameState.getGrid().getCell(2, 3).performCellChange(CellType.DESTRUCTIBLE_WALL);
        gameState.getGrid().getCell(1, 3).performCellChange(playerA);
        gameState.getGrid().getCell(1, 3).plantBomb();
        gameState.getGrid().getCell(1, 3).performCellChange(CellType.CLEAR);
        assertEquals(0, playerA.getAvailableBombCount());
        assertEquals(2, playerA.getScore());
        gameState.getGrid().getCell(1, 4).performCellChange(playerB);
        gameState.getGrid().getCell(1, 4).plantBomb();
        gameState.getGrid().getCell(1, 4).setBomb(1, 3, playerB);
        gameState.getGrid().getCell(1, 4).performCellChange(CellType.CLEAR);
        assertEquals(0, playerB.getAvailableBombCount());
        assertEquals(2, playerB.getScore());
        gameEngine.advanceRound(gameState);
        
        assertEquals(CellType.DESTRUCTIBLE_WALL, gameState.getGrid().getCell(1, 1).getCellType());
        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(1, 2).getCellType());
        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(1, 3).getCellType());
        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(1, 4).getCellType());
        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(2, 3).getCellType());
        assertEquals(0, playerA.getAvailableBombCount());
        assertEquals(12, playerA.getScore());
        assertEquals(0, playerB.getAvailableBombCount());
        assertEquals(12, playerB.getScore());

        gameEngine.advanceRound(gameState);
        assertEquals(1, playerA.getAvailableBombCount());
        assertEquals(1, playerB.getAvailableBombCount());
    }
    
    @Test
    public void testBombsIndependent()
    {
        gameState.getGrid().getCell(1, 1).performCellChange(CellType.DESTRUCTIBLE_WALL);
        gameState.getGrid().getCell(1, 2).performCellChange(playerA);
        gameState.getGrid().getCell(1, 2).plantBomb();
        gameState.getGrid().getCell(1, 2).setBomb(1, 1, playerA);
        gameState.getGrid().getCell(1, 2).performCellChange(CellType.CLEAR);
        assertEquals(0, playerA.getAvailableBombCount());
        assertEquals(2, playerA.getScore());
        gameState.getGrid().getCell(3, 1).performCellChange(CellType.DESTRUCTIBLE_WALL);
        gameState.getGrid().getCell(3, 2).performCellChange(playerB);
        gameState.getGrid().getCell(3, 2).plantBomb();
        gameState.getGrid().getCell(3, 2).setBomb(1, 1, playerB);
        gameState.getGrid().getCell(3, 2).performCellChange(CellType.CLEAR);
        assertEquals(0, playerB.getAvailableBombCount());
        assertEquals(2, playerB.getScore());
        gameEngine.advanceRound(gameState);

        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(1, 1).getCellType());
        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(1, 2).getCellType());
        assertEquals(0, playerA.getAvailableBombCount());
        assertEquals(12, playerA.getScore());
        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(3, 1).getCellType());
        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(3, 2).getCellType());
        assertEquals(0, playerB.getAvailableBombCount());
        assertEquals(12, playerB.getScore());

        gameEngine.advanceRound(gameState);
        assertEquals(1, playerA.getAvailableBombCount());
        assertEquals(1, playerB.getAvailableBombCount());
    }
    
    @Test
    public void testConcurrentBombChain()
    {
        gameState.getGrid().getCell(1, 1).performCellChange(CellType.DESTRUCTIBLE_WALL);
        gameState.getGrid().getCell(1, 2).performCellChange(playerA);
        gameState.getGrid().getCell(1, 2).plantBomb();
        gameState.getGrid().getCell(1, 2).setBomb(1, 1, playerA);
        gameState.getGrid().getCell(1, 2).performCellChange(CellType.CLEAR);
        assertEquals(0, playerA.getAvailableBombCount());
        assertEquals(2, playerA.getScore());
        gameState.getGrid().getCell(1, 4).performCellChange(playerB);
        gameState.getGrid().getCell(1, 4).plantBomb();
        gameState.getGrid().getCell(1, 4).setBomb(1, 2, playerB);
        gameState.getGrid().getCell(1, 4).performCellChange(CellType.CLEAR);
        assertEquals(0, playerB.getAvailableBombCount());
        assertEquals(2, playerB.getScore());
        gameEngine.advanceRound(gameState);

        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(1, 1).getCellType());
        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(1, 2).getCellType());
        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(1, 3).getCellType());
        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(1, 4).getCellType());
        assertEquals(0, playerA.getAvailableBombCount());
        assertEquals(12, playerA.getScore());
        assertEquals(0, playerB.getAvailableBombCount());
        assertEquals(12, playerB.getScore());

        gameEngine.advanceRound(gameState);
        assertEquals(1, playerA.getAvailableBombCount());
        assertEquals(1, playerB.getAvailableBombCount());
    }
    
    @Test
    public void testPlayerDiesTwice()
    {
        gameState.getGrid().getCell(1, 1).performCellChange(playerA);
        gameState.getGrid().getCell(1, 1).plantBomb();
        gameState.getGrid().getCell(1, 1).setBomb(1, 1, playerA);
        assertEquals(0, playerA.getAvailableBombCount());
        assertEquals(2, playerA.getScore());
        playerB.setBombCounts(2);
        gameState.getGrid().getCell(1, 3).performCellChange(playerB);
        gameState.getGrid().getCell(1, 3).plantBomb();
        gameState.getGrid().getCell(1, 3).setBomb(2, 2, playerB);
        gameState.getGrid().getCell(1, 3).performCellChange(CellType.CLEAR);
        gameState.getGrid().getCell(2, 3).performCellChange(playerB);
        gameState.getGrid().getCell(2, 3).plantBomb();
        gameState.getGrid().getCell(2, 3).setBomb(1, 1, playerB);
        gameState.getGrid().getCell(2, 3).performCellChange(CellType.CLEAR);
        assertEquals(0, playerB.getAvailableBombCount());
        assertEquals(4, playerB.getScore());
        gameEngine.advanceRound(gameState);
        
        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(1, 1).getCellType());
        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(1, 2).getCellType());
        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(1, 3).getCellType());
        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(1, 4).getCellType());
        assertFalse(playerA.isAlive());
        assertEquals(-13, playerA.getScore());
        assertEquals(0, playerB.getAvailableBombCount());
        assertEquals(19, playerB.getScore());

        gameEngine.advanceRound(gameState);
        assertEquals(2, playerB.getAvailableBombCount());
    }
    
    @Test
    public void testBombChainSamePlayer()
    {
        playerA.setBombCounts(2);
        gameState.getGrid().getCell(1, 1).performCellChange(CellType.DESTRUCTIBLE_WALL);
        gameState.getGrid().getCell(1, 2).performCellChange(playerA);
        gameState.getGrid().getCell(1, 2).plantBomb();
        gameState.getGrid().getCell(1, 2).setBomb(2, 1, playerA);
        gameState.getGrid().getCell(1, 2).performCellChange(CellType.CLEAR);
        gameState.getGrid().getCell(1, 3).performCellChange(playerA);
        gameState.getGrid().getCell(1, 3).plantBomb();
        gameState.getGrid().getCell(1, 3).setBomb(1, 2, playerA);
        gameState.getGrid().getCell(1, 3).performCellChange(CellType.CLEAR);
        assertEquals(0, playerA.getAvailableBombCount());
        assertEquals(4, playerA.getScore());
        gameEngine.advanceRound(gameState);

        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(1, 1).getCellType());
        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(1, 2).getCellType());
        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(1, 3).getCellType());
        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(1, 4).getCellType());
        assertEquals(0, playerA.getAvailableBombCount());
        assertEquals(14, playerA.getScore());

        gameEngine.advanceRound(gameState);
        assertEquals(2, playerA.getAvailableBombCount());
    }
    
    @Test
    public void testBombChainDestroyWallTwice()
    {
        playerA.setBombCounts(3);
        gameState.getGrid().getCell(3, 1).performCellChange(CellType.DESTRUCTIBLE_WALL);
        gameState.getGrid().getCell(1, 1).performCellChange(playerA);
        gameState.getGrid().getCell(1, 1).plantBomb();
        gameState.getGrid().getCell(1, 1).setBomb(1, 2, playerA);
        gameState.getGrid().getCell(1, 1).performCellChange(CellType.CLEAR);
        gameState.getGrid().getCell(1, 3).performCellChange(playerA);
        gameState.getGrid().getCell(1, 3).plantBomb();
        gameState.getGrid().getCell(1, 3).setBomb(2, 2, playerA);
        gameState.getGrid().getCell(1, 3).performCellChange(CellType.CLEAR);
        gameState.getGrid().getCell(3, 3).performCellChange(playerA);
        gameState.getGrid().getCell(3, 3).plantBomb();
        gameState.getGrid().getCell(3, 3).setBomb(2, 2, playerA);
        gameState.getGrid().getCell(3, 3).performCellChange(CellType.CLEAR);
        assertEquals(0, playerA.getAvailableBombCount());
        assertEquals(7, playerA.getScore());
        gameEngine.advanceRound(gameState);

        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(1, 1).getCellType());
        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(1, 2).getCellType());
        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(1, 3).getCellType());
        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(2, 1).getCellType());
        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(3, 1).getCellType());
        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(3, 2).getCellType());
        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(3, 3).getCellType());
        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(2, 3).getCellType());
        assertEquals(0, playerA.getAvailableBombCount());
        assertEquals(27, playerA.getScore());
        
        gameEngine.advanceRound(gameState);
        assertEquals(3, playerA.getAvailableBombCount());
    }
    
    @Test
    public void testBombChainDestroyWallTwiceCooperatively()
    {
        playerA.setBombCounts(2);
        gameState.getGrid().getCell(3, 1).performCellChange(CellType.DESTRUCTIBLE_WALL);
        gameState.getGrid().getCell(1, 1).performCellChange(playerB);
        gameState.getGrid().getCell(1, 1).plantBomb();
        gameState.getGrid().getCell(1, 1).setBomb(1, 2, playerB);
        gameState.getGrid().getCell(1, 1).performCellChange(CellType.CLEAR);
        gameState.getGrid().getCell(1, 3).performCellChange(playerA);
        gameState.getGrid().getCell(1, 3).plantBomb();
        gameState.getGrid().getCell(1, 3).setBomb(2, 2, playerA);
        gameState.getGrid().getCell(1, 3).performCellChange(CellType.CLEAR);
        gameState.getGrid().getCell(3, 3).performCellChange(playerA);
        gameState.getGrid().getCell(3, 3).plantBomb();
        gameState.getGrid().getCell(3, 3).setBomb(2, 2, playerA);
        gameState.getGrid().getCell(3, 3).performCellChange(CellType.CLEAR);
        assertEquals(0, playerA.getAvailableBombCount());
        assertEquals(4, playerA.getScore());
        assertEquals(0, playerB.getAvailableBombCount());
        assertEquals(2, playerB.getScore());
        gameEngine.advanceRound(gameState);

        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(1, 1).getCellType());
        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(1, 2).getCellType());
        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(1, 3).getCellType());
        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(2, 1).getCellType());
        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(3, 1).getCellType());
        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(3, 2).getCellType());
        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(3, 3).getCellType());
        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(2, 3).getCellType());
        assertEquals(0, playerA.getAvailableBombCount());
        assertEquals(24, playerA.getScore());
        assertEquals(0, playerB.getAvailableBombCount());
        assertEquals(22, playerB.getScore());
        
        gameEngine.advanceRound(gameState);
        assertEquals(2, playerA.getAvailableBombCount());
        assertEquals(1, playerB.getAvailableBombCount());
    }
    
    @Test
    public void testBombChainForumScenario0()
    {
        playerA.setBombCounts(2);
        gameState.getGrid().getCell(1, 3).performCellChange(CellType.DESTRUCTIBLE_WALL);
        gameState.getGrid().getCell(3, 1).performCellChange(CellType.DESTRUCTIBLE_WALL);
        gameState.getGrid().getCell(5, 3).performCellChange(CellType.DESTRUCTIBLE_WALL);
        gameState.getGrid().getCell(3, 3).performCellChange(playerA);
        gameState.getGrid().getCell(3, 3).plantBomb();
        gameState.getGrid().getCell(3, 3).setBomb(2, 2, playerA);
        gameState.getGrid().getCell(3, 3).performCellChange(CellType.CLEAR);
        gameState.getGrid().getCell(3, 5).performCellChange(playerA);
        gameState.getGrid().getCell(3, 5).plantBomb();
        gameState.getGrid().getCell(3, 5).setBomb(1, 4, playerA);
        gameState.getGrid().getCell(3, 5).performCellChange(CellType.CLEAR);
        assertEquals(0, playerA.getAvailableBombCount());
        assertEquals(4, playerA.getScore());
        gameEngine.advanceRound(gameState);

        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(1, 3).getCellType());
        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(2, 3).getCellType());
        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(3, 3).getCellType());
        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(4, 3).getCellType());
        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(5, 3).getCellType());
        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(3, 1).getCellType());
        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(3, 2).getCellType());
        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(3, 4).getCellType());
        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(3, 5).getCellType());
        assertEquals(0, playerA.getAvailableBombCount());
        assertEquals(34, playerA.getScore());

        gameEngine.advanceRound(gameState);
        assertEquals(2, playerA.getAvailableBombCount());
    }
    
    @Test
    public void testBombChainForumScenario1()
    {
        playerA.setBombCounts(2);
        gameState.getGrid().getCell(1, 3).performCellChange(CellType.DESTRUCTIBLE_WALL);
        gameState.getGrid().getCell(3, 1).performCellChange(CellType.DESTRUCTIBLE_WALL);
        gameState.getGrid().getCell(5, 3).performCellChange(CellType.DESTRUCTIBLE_WALL);
        gameState.getGrid().getCell(3, 3).performCellChange(playerA);
        gameState.getGrid().getCell(3, 3).plantBomb();
        gameState.getGrid().getCell(3, 3).setBomb(1, 2, playerA);
        gameState.getGrid().getCell(3, 3).performCellChange(CellType.CLEAR);
        gameState.getGrid().getCell(3, 5).performCellChange(playerA);
        gameState.getGrid().getCell(3, 5).plantBomb();
        gameState.getGrid().getCell(3, 5).setBomb(2, 4, playerA);
        gameState.getGrid().getCell(3, 5).performCellChange(CellType.CLEAR);
        assertEquals(0, playerA.getAvailableBombCount());
        assertEquals(4, playerA.getScore());
        gameEngine.advanceRound(gameState);

        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(1, 3).getCellType());
        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(2, 3).getCellType());
        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(3, 3).getCellType());
        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(4, 3).getCellType());
        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(5, 3).getCellType());
        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(3, 1).getCellType());
        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(3, 2).getCellType());
        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(3, 4).getCellType());
        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(3, 5).getCellType());
        assertEquals(0, playerA.getAvailableBombCount());
        assertEquals(34, playerA.getScore());

        gameEngine.advanceRound(gameState);
        assertEquals(2, playerA.getAvailableBombCount());
    }
    
    @Test
    public void testBombChainForumScenario2()
    {
        playerA.setBombCounts(3);
        gameState.getGrid().getCell(1, 3).performCellChange(CellType.DESTRUCTIBLE_WALL);
        gameState.getGrid().getCell(3, 1).performCellChange(CellType.DESTRUCTIBLE_WALL);
        gameState.getGrid().getCell(5, 3).performCellChange(CellType.DESTRUCTIBLE_WALL);
        gameState.getGrid().getCell(3, 3).performCellChange(playerA);
        gameState.getGrid().getCell(3, 3).plantBomb();
        gameState.getGrid().getCell(3, 3).setBomb(3, 2, playerA);
        gameState.getGrid().getCell(3, 3).performCellChange(CellType.CLEAR);
        gameState.getGrid().getCell(3, 4).performCellChange(playerA);
        gameState.getGrid().getCell(3, 4).plantBomb();
        gameState.getGrid().getCell(3, 4).setBomb(2, 3, playerA);
        gameState.getGrid().getCell(3, 4).performCellChange(CellType.CLEAR);
        gameState.getGrid().getCell(3, 5).performCellChange(playerA);
        gameState.getGrid().getCell(3, 5).plantBomb();
        gameState.getGrid().getCell(3, 5).setBomb(1, 4, playerA);
        gameState.getGrid().getCell(3, 5).performCellChange(CellType.CLEAR);
        assertEquals(0, playerA.getAvailableBombCount());
        assertEquals(7, playerA.getScore());
        gameEngine.advanceRound(gameState);

        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(1, 3).getCellType());
        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(2, 3).getCellType());
        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(3, 3).getCellType());
        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(4, 3).getCellType());
        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(5, 3).getCellType());
        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(3, 1).getCellType());
        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(3, 2).getCellType());
        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(3, 4).getCellType());
        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(3, 5).getCellType());
        assertEquals(0, playerA.getAvailableBombCount());
        assertEquals(37, playerA.getScore());

        gameEngine.advanceRound(gameState);
        assertEquals(3, playerA.getAvailableBombCount());
    }
    
    @Test
    public void testKillPlayerInBlast()
    {
        gameState.getGrid().getCell(1, 2).performCellChange(playerB);
        gameState.getGrid().getCell(1, 2).plantBomb();
        gameState.getGrid().getCell(1, 2).setBomb(1, 1, playerB);
        gameState.getGrid().getCell(1, 2).performCellChange(CellType.CLEAR);
        gameState.getGrid().getCell(2, 1).performCellChange(playerA);
        assertEquals(2, playerA.getScore());
        assertEquals(0, playerB.getAvailableBombCount());
        assertEquals(2, playerB.getScore());
        gameEngine.advanceRound(gameState, BombermanMove.UP);
        
        assertEquals(CellType.CLEAR, gameState.getGrid().getCell(2, 1).getCellType());
        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(1, 1).getCellType());
        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(1, 2).getCellType());
        assertFalse(playerA.isAlive());
        assertEquals(-11, playerA.getScore());
        assertEquals(0, playerB.getAvailableBombCount());
        assertEquals(17, playerB.getScore());

        gameEngine.advanceRound(gameState);
        assertEquals(1, playerB.getAvailableBombCount());
    }
    
    @Test
    public void testSuicide()
    {
        gameState.getGrid().getCell(1, 1).performCellChange(playerA);
        gameState.getGrid().getCell(1, 1).plantBomb();
        gameState.getGrid().getCell(1, 1).setBomb(1, 1, playerA);
        assertEquals(0, playerA.getAvailableBombCount());
        assertEquals(2, playerA.getScore());
        gameEngine.advanceRound(gameState);

        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(1, 1).getCellType());
        assertFalse(playerA.isAlive());
        assertEquals(-13, playerA.getScore());
        assertEquals(0, playerB.getScore());
    }
}
