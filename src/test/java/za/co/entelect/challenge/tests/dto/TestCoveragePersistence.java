package za.co.entelect.challenge.tests.dto;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

import static org.junit.Assert.*;

import org.junit.Test;

import za.co.entelect.challenge.dto.Coverage;
import za.co.entelect.challenge.model.bomberman.BombermanGrid;
import za.co.entelect.challenge.model.bomberman.BombermanPlayer;
import za.co.entelect.challenge.model.bomberman.BombermanState;
import za.co.entelect.challenge.model.bomberman.BombermanCell.CellType;

public class TestCoveragePersistence
{
    private BombermanState gameState;
    
    public TestCoveragePersistence()
    {
        gameState = createBlankGameState();
        
        gameState.getGrid().getCell(1, 1).performCellChange(gameState.getPlayers()[0]);
        gameState.getGrid().getCell(1, 1).performCellChange(CellType.CLEAR);
        gameState.getGrid().getCell(1, 3).performCellChange(gameState.getPlayers()[1]);
        gameState.getGrid().getCell(1, 3).performCellChange(CellType.CLEAR);
        gameState.getGrid().getCell(1, 4).performCellChange(gameState.getPlayers()[0]);
        gameState.getGrid().getCell(1, 4).performCellChange(gameState.getPlayers()[1]);
        gameState.getGrid().getCell(1, 4).performCellChange(CellType.CLEAR);
        gameState.getGrid().getCell(1, 5).performCellChange(gameState.getPlayers()[0]);
        gameState.getGrid().getCell(1, 5).performCellChange(CellType.CLEAR);
    }
    
    @Test
    public void testStoreCoverage() throws IOException
    {
        byte[] coverageBytes;
        try (ByteArrayOutputStream stream = new ByteArrayOutputStream())
        {
            (new Coverage(gameState)).saveCoverage(stream);
            coverageBytes = stream.toByteArray();
        }
        
        BombermanState restoredGameState = createBlankGameState();
        try (ByteArrayInputStream stream = new ByteArrayInputStream(coverageBytes))
        {
            (new Coverage(restoredGameState)).loadCoverage(stream);
        }
        
        assertEquals("D", charactersToString(restoredGameState.getGrid().getCell(1, 1).getPlayersVisited()));
        assertEquals("", charactersToString(restoredGameState.getGrid().getCell(1, 2).getPlayersVisited()));
        assertEquals("L", charactersToString(restoredGameState.getGrid().getCell(1, 3).getPlayersVisited()));
        assertEquals("DL", charactersToString(restoredGameState.getGrid().getCell(1, 4).getPlayersVisited()));
        assertEquals("D", charactersToString(restoredGameState.getGrid().getCell(1, 5).getPlayersVisited()));
        
        assertEquals(3, restoredGameState.getPlayers()[0].getGridCoverage());
        assertEquals(2, restoredGameState.getPlayers()[1].getGridCoverage());
    }
    
    
    private BombermanState createBlankGameState()
    {
        BombermanPlayer player1 = new BombermanPlayer('D'), player2 = new BombermanPlayer('L');
        return new BombermanState(new BombermanGrid(9, 9), player1, player2);
    }
    
    private static String charactersToString(List<Character> characters)
    {
        StringBuilder sb = new StringBuilder(characters.size());
        for (char next : characters)
            sb.append(next);
        return sb.toString();
    }
}
