package za.co.entelect.challenge.tests.model;

import static org.junit.Assert.*;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

import org.junit.Test;
import org.junit.runner.RunWith;

import za.co.entelect.challenge.model.bomberman.BombermanEngine;
import za.co.entelect.challenge.model.bomberman.BombermanGrid;
import za.co.entelect.challenge.model.bomberman.BombermanMove;
import za.co.entelect.challenge.model.bomberman.BombermanPlayer;
import za.co.entelect.challenge.model.bomberman.BombermanState;
import za.co.entelect.challenge.model.bomberman.BombermanCell.CellType;

@RunWith(JUnitParamsRunner.class)
public class TestMovement
{
    private BombermanEngine gameEngine = new BombermanEngine();
    private BombermanState gameState;
    private BombermanPlayer player = new BombermanPlayer('B');
    private BombermanPlayer opponentPlayer = new BombermanPlayer('A');
    
    public TestMovement()
    {
        gameState = new BombermanState(new BombermanGrid(5, 5), player, opponentPlayer);
        gameState.getGrid().getCell(1, 1).performCellChange(player);
    }
    
    @Test
    public void testDontMove()
    {
        assertEquals(0, gameState.getGameRound());
        assertEquals(11, player.getScore());
        assertTrue(gameEngine.isMoveValid(gameState, player, BombermanMove.DO_NOTHING));
        gameEngine.advanceRound(gameState, BombermanMove.DO_NOTHING);
        
        assertEquals(1, player.getColumn());
        assertEquals(CellType.PLAYER, gameState.getGrid().getCell(1, 1).getCellType());
        assertEquals(player, gameState.getGrid().getCell(1, 1).getCellOwner());
        assertEquals(1, gameState.getGameRound());
        assertEquals(11, player.getScore());
    }
    
    @Test
    public void testMoveToEmptyCell()
    {
        testMoveToTheRight();
        assertEquals(22, player.getScore());
        testMoveToTheLeft();
        assertEquals(22, player.getScore());
    }
    
    private void testMoveToTheRight()
    {
        assertTrue(gameEngine.isMoveValid(gameState, player, BombermanMove.RIGHT));
        gameEngine.advanceRound(gameState, BombermanMove.RIGHT);
        
        assertEquals(2, player.getColumn());
        assertEquals(CellType.CLEAR, gameState.getGrid().getCell(1, 1).getCellType());
        assertNull(gameState.getGrid().getCell(1, 1).getCellOwner());
        assertEquals(CellType.PLAYER, gameState.getGrid().getCell(1, 2).getCellType());
        assertEquals(player, gameState.getGrid().getCell(1, 2).getCellOwner());
    }
    
    private void testMoveToTheLeft()
    {
        assertTrue(gameEngine.isMoveValid(gameState, player, BombermanMove.LEFT));
        gameEngine.advanceRound(gameState, BombermanMove.LEFT);
        
        assertEquals(1, player.getColumn());
        assertEquals(CellType.CLEAR, gameState.getGrid().getCell(1, 2).getCellType());
        assertNull(gameState.getGrid().getCell(1, 2).getCellOwner());
        assertEquals(CellType.PLAYER, gameState.getGrid().getCell(1, 1).getCellType());
        assertEquals(player, gameState.getGrid().getCell(1, 1).getCellOwner());
    }
    
    @Test
    public void testMoveToEmptyCellWhenDead()
    {
        player.setIsAlive(false);
        
        for (BombermanMove nextMove : BombermanMove.values())
        {
            if (nextMove == BombermanMove.DO_NOTHING)
                assertTrue(gameEngine.isMoveValid(gameState, player, BombermanMove.DO_NOTHING));
            else
                assertFalse(gameEngine.isMoveValid(gameState, player, nextMove));
        }
    }
    
    @Test
    @Parameters({"INDESTRUCTIBLE_WALL", "DESTRUCTIBLE_WALL", "BOMB_EXPLOSION", "PLAYER"})
    public void testMoveToOccupiedCell(CellType occupiedCellType)
    {
        gameState.getGrid().getCell(1, 2).setCell(occupiedCellType);
        assertFalse(gameEngine.isMoveValid(gameState, player, BombermanMove.RIGHT));
    }
    
    @Test
    public void testMoveToCellWithBomb()
    {
        gameState.getGrid().getCell(1, 2).setBomb(1, 1, player);
        assertFalse(gameEngine.isMoveValid(gameState, player, BombermanMove.RIGHT));
    }

    @Test
    public void testMoveToExplodingCellWithPowerUp()
    {
        gameState.getGrid().getCell(1, 1).performCellChange(CellType.SUPER_POWER_UP);
        gameState.getGrid().getCell(1, 2).performCellChange(opponentPlayer);
        gameState.getGrid().getCell(1, 2).plantBomb();
        gameState.getGrid().getCell(1, 2).setBomb(1, 3, opponentPlayer);
        gameState.getGrid().getCell(1, 2).performCellChange(CellType.CLEAR);
        gameState.getGrid().getCell(2, 1).performCellChange(player);
        gameEngine.advanceRound(gameState);
        
        assertEquals(CellType.SUPER_POWER_UP, gameState.getGrid().getCell(1, 1).getCellType());
        assertEquals(CellType.BOMB_EXPLOSION, gameState.getGrid().getCell(1, 2).getCellType());
        assertFalse(gameEngine.isMoveValid(gameState, player, BombermanMove.UP));
    }
    
    @Test
    public void testPickUpBombRadiusPowerUp()
    {
        player.setBombRadius(2);
        gameState.getGrid().getCell(1, 2).setCell(CellType.BOMB_RADIUS_POWER_UP);
        testMoveToTheRight();
        
        assertEquals(4, player.getBombRadius());
        assertEquals(22, player.getScore());
    }
    
    @Test
    public void testPickUpBombBagPowerUp()
    {
        gameState.getGrid().getCell(1, 2).setCell(CellType.BOMB_BAG_POWER_UP);
        testMoveToTheRight();
        
        assertEquals(2, player.getAvailableBombCount());
        assertEquals(2, player.getBombBagSize());
        assertEquals(22, player.getScore());
    }
    
    @Test
    public void testPickUpSuperPowerUp()
    {
        player.setBombRadius(2);
        gameState.getGrid().getCell(1, 2).setCell(CellType.SUPER_POWER_UP);
        testMoveToTheRight();
        
        assertEquals(4, player.getBombRadius());
        assertEquals(2, player.getAvailableBombCount());
        assertEquals(2, player.getBombBagSize());
        assertEquals(72, player.getScore());
    }
    
    @Test
    public void TestMoveToSameCell()
    {
        gameState.getGrid().getCell(1, 3).performCellChange(opponentPlayer);
        assertTrue(gameEngine.isMoveValid(gameState, player, BombermanMove.RIGHT));
        assertTrue(gameEngine.isMoveValid(gameState, opponentPlayer, BombermanMove.LEFT));
        
        gameEngine.advanceRound(gameState, BombermanMove.RIGHT, BombermanMove.LEFT);

        assertEquals(CellType.CLEAR, gameState.getGrid().getCell(1, 1).getCellType());
        assertEquals(2, player.getColumn());
        assertEquals(CellType.PLAYER, gameState.getGrid().getCell(1, 2).getCellType());
        assertEquals(player, gameState.getGrid().getCell(1, 2).getCellOwner());
        assertEquals(22, player.getScore());
        assertEquals(3, opponentPlayer.getColumn());
        assertEquals(CellType.PLAYER, gameState.getGrid().getCell(1, 3).getCellType());
        assertEquals(opponentPlayer, gameState.getGrid().getCell(1, 3).getCellOwner());
        assertEquals(11, opponentPlayer.getScore());
    }
}
