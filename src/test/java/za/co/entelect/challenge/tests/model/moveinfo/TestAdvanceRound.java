package za.co.entelect.challenge.tests.model.moveinfo;

import org.junit.Test;

import za.co.entelect.challenge.model.bomberman.BombermanEngine;
import za.co.entelect.challenge.model.bomberman.BombermanGrid;
import za.co.entelect.challenge.model.bomberman.BombermanPlayer;
import za.co.entelect.challenge.model.bomberman.BombermanState;
import za.co.entelect.challenge.model.bomberman.BombermanCell.CellType;
import za.co.entelect.challenge.model.bomberman.moveinfo.BombermanMoveInfo;

public class TestAdvanceRound
{
    private BombermanEngine gameEngine = new BombermanEngine();
    private BombermanState gameState;
    private BombermanPlayer playerA = new BombermanPlayer('A');
    private BombermanPlayer playerB = new BombermanPlayer('B');
    
    public TestAdvanceRound()
    {
        gameState = new BombermanState(new BombermanGrid(9, 9), playerA, playerB);
        
        gameState.getGrid().getCell(1, 1).performCellChange(CellType.BOMB_EXPLOSION);
        gameState.getExplodingCells().add(gameState.getGrid().getCell(1, 1));
        
        gameState.getGrid().getCell(1, 2).performCellChange(playerA);
        gameState.getGrid().getCell(1, 2).plantBomb();
        gameState.getGrid().getCell(1, 2).performCellChange(CellType.CLEAR);
        
        gameState.getGrid().getCell(3, 3).performCellChange(playerA);
        gameState.getGrid().getCell(3, 3).plantBomb();
        gameState.getGrid().getCell(3, 3).setBomb(1, 1, playerA);
        gameState.getGrid().getCell(3, 3).performCellChange(CellType.CLEAR);
        
        gameState.getGrid().getCell(2, 3).performCellChange(CellType.DESTRUCTIBLE_WALL);
        
        gameState.getGrid().getCell(4, 3).performCellChange(playerB);
    }
    
    @Test
    public void testAdvanceRound()
    {
        BombermanState newGameState = (BombermanState)gameState.clone();
        BombermanMoveInfo moveInfo = gameEngine.advanceRound(newGameState);
        gameEngine.undo(moveInfo);
        
        BombermanStateHelpers.assertEqual(gameState, newGameState);
    }
}
