package za.co.entelect.challenge.tests.players;

import static org.junit.Assert.*;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

//import org.junit.Ignore;
import org.junit.Test;

import za.co.entelect.challenge.model.GamePlayer;
import za.co.entelect.challenge.model.bomberman.BombermanCell;
import za.co.entelect.challenge.model.bomberman.BombermanEngine;
import za.co.entelect.challenge.model.bomberman.BombermanMove;
import za.co.entelect.challenge.model.bomberman.BombermanPlayer;
import za.co.entelect.challenge.model.bomberman.BombermanState;
import za.co.entelect.challenge.model.bomberman.players.ReplayBombermanPlayer;
import za.co.entelect.challenge.tests.FileHelpers;

//@Ignore
public class TestReplayBombermanPlayer
{
    @Test
    public void testMoves() throws ClassNotFoundException, IOException, URISyntaxException, InterruptedException
    {
        GamePlayer<BombermanCell> gamePlayer = new ReplayBombermanPlayer(new BombermanPlayer('A'));
        
        Path targetPath = Paths.get(ReplayBombermanPlayer.REPLAY_FILE_BASE);
        if (targetPath.toFile().exists())
            FileHelpers.deleteDirectory(targetPath.toFile());
        targetPath.toFile().mkdirs();
        
        for (int i = 1; i <= 6; i++)
        {
            Path targetFolderPath = targetPath.resolve(Integer.toString(i));
            targetFolderPath.toFile().mkdir();
            Files.copy(FileHelpers.getResourcePath("replay/state" + i + ".json"), targetFolderPath.resolve("state.json"));
        }
        
        try
        {
            assertEquals(BombermanMove.RIGHT, gamePlayer.getNextMove(getGameState(targetPath, 1)));
            assertEquals(BombermanMove.DOWN, gamePlayer.getNextMove(getGameState(targetPath, 2)));
            assertEquals(BombermanMove.DO_NOTHING, gamePlayer.getNextMove(getGameState(targetPath, 3)));
            assertEquals(BombermanMove.PLACE_BOMB, gamePlayer.getNextMove(getGameState(targetPath, 4)));
            assertEquals(BombermanMove.TRIGGER_BOMB, gamePlayer.getNextMove(getGameState(targetPath, 5)));
            assertEquals(BombermanMove.DO_NOTHING, gamePlayer.getNextMove(getGameState(targetPath, 6)));
        }
        finally
        {
            FileHelpers.deleteDirectory(targetPath.toFile());
        }
    }
    
    
    private static BombermanState getGameState(Path targetPath, int number) throws ClassNotFoundException, IOException
    {
        BombermanState gameState = FileHelpers.loadTestStateFromFile(targetPath.resolve(Integer.toString(number)).resolve("state.json").toString());
        (new BombermanEngine()).advanceRound(gameState);
        return gameState;
    }
}
