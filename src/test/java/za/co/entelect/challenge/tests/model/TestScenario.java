package za.co.entelect.challenge.tests.model;

import java.io.IOException;

import static org.junit.Assert.*;

import org.junit.Test;

import za.co.entelect.challenge.model.GameCoordinator;
import za.co.entelect.challenge.model.GamePlayer;
import za.co.entelect.challenge.model.InvalidMoveException;
import za.co.entelect.challenge.model.bomberman.BombermanCell;
import za.co.entelect.challenge.model.bomberman.BombermanEngine;
import za.co.entelect.challenge.model.bomberman.BombermanPlayer;
import za.co.entelect.challenge.model.bomberman.BombermanState;
import za.co.entelect.challenge.model.bomberman.players.LazyBombermanPlayer;
import za.co.entelect.challenge.tests.FileHelpers;

public class TestScenario
{
    @Test
    public void testPlayerDies() throws IOException, ClassNotFoundException, InvalidMoveException, InterruptedException
    {
        BombermanState gameState = FileHelpers.loadTestStateFromResource("teststates/die");
        BombermanEngine gameEngine = new BombermanEngine();
        BombermanPlayer player1 = gameState.getPlayers()[0], player2 = gameState.getPlayers()[1];
        @SuppressWarnings("unchecked")
        GameCoordinator<BombermanCell> gameCoordinator = new GameCoordinator<>(gameEngine, gameState, 
                (GamePlayer<BombermanCell>[])new GamePlayer[] { 
                new LazyBombermanPlayer(player1), new LazyBombermanPlayer(player2) 
                });

        assertTrue(player2.isAlive());
        assertFalse(gameEngine.playerDidWin(gameState, player1));
        assertFalse(gameEngine.playerDidWin(gameState, player2));
        gameCoordinator.move();

        assertFalse(player2.isAlive());
        assertTrue(gameEngine.playerDidWin(gameState, player1));
        assertFalse(gameEngine.playerDidWin(gameState, player2));
    }
}
