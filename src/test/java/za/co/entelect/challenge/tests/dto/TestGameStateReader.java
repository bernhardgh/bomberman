package za.co.entelect.challenge.tests.dto;

import static org.junit.Assert.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import za.co.entelect.challenge.dto.reader.GameStateReaderFactory;
import za.co.entelect.challenge.model.bomberman.BombermanCell;
import za.co.entelect.challenge.model.bomberman.BombermanPlayer;
import za.co.entelect.challenge.model.bomberman.BombermanState;

@RunWith(Parameterized.class)
public class TestGameStateReader
{
    @Parameters
    public static Collection<Boolean[]> loadCoverageFlags()
    {
        return Arrays.asList(new Boolean[][] { { false }, { true } });
    }
    
    private BombermanState gameState;
    private boolean loadCoverage;
    
    public TestGameStateReader(boolean loadCoverage) throws IOException
    {
        this.loadCoverage = loadCoverage;
        
        ClassLoader resourceHandle = TestGameStateReader.class.getClassLoader();
        try (
                InputStream stateStream = resourceHandle.getResourceAsStream("state.json");
                InputStream coverageStream = loadCoverage ? resourceHandle.getResourceAsStream("coverage") : null
                )
        {
            gameState = (BombermanState)GameStateReaderFactory.getDefaultGameStateReader().read(stateStream, coverageStream);
        }
    }
    
    @Test
    public void testBasicInfo()
    {
        assertEquals(164, gameState.getGameRound());
        assertEquals(620, gameState.getPlayerKillPoints());
        assertEquals(21, gameState.getGrid().getHeight());
        assertEquals(21, gameState.getGrid().getWidth());
    }
    
    @Test
    public void testPlayers()
    {
        assertEquals(3, gameState.getPlayers().length);
        
        BombermanPlayer player = gameState.getPlayers()[0];
        assertEquals('A', player.getPlayerKey());
        assertTrue(player.isAlive());
        assertEquals(346, player.getScore());
        assertEquals(loadCoverage ? 14 : 1, player.getGridCoverage());
        assertEquals(1, player.getAvailableBombCount());
        assertEquals(3, player.getBombBagSize());
        assertEquals(4, player.getBombRadius());
        
        player = gameState.getPlayers()[1];
        assertEquals('B', player.getPlayerKey());
        assertTrue(player.isAlive());
        assertEquals(395, player.getScore());
        assertEquals(loadCoverage ? 28 : 1, player.getGridCoverage());
        assertEquals(3, player.getAvailableBombCount());
        assertEquals(4, player.getBombBagSize());
        assertEquals(2, player.getBombRadius());
        
        player = gameState.getPlayers()[2];
        assertEquals('C', player.getPlayerKey());
        assertFalse(player.isAlive());
        assertEquals(-560, player.getScore());
    }
    
    @Test
    public void testPlayerCoverage()
    {
        if (loadCoverage)
        {
            assertEquals(1, gameState.getGrid().getCell(17, 3).getPlayersVisited().size());
            assertEquals('B', (char)gameState.getGrid().getCell(17, 3).getPlayersVisited().get(0));
        }
        
        assertEquals(1, gameState.getGrid().getCell(17, 2).getPlayersVisited().size());
        assertEquals('B', (char)gameState.getGrid().getCell(17, 2).getPlayersVisited().get(0));

        if (loadCoverage)
        {
            assertEquals(1, gameState.getGrid().getCell(1, 13).getPlayersVisited().size());
            assertEquals('A', (char)gameState.getGrid().getCell(1, 13).getPlayersVisited().get(0));
        }
        
        assertEquals(1, gameState.getGrid().getCell(2, 13).getPlayersVisited().size());
        assertEquals('A', (char)gameState.getGrid().getCell(2, 13).getPlayersVisited().get(0));
    }
    
    @Test
    public void testPlayerBombs()
    {
        BombermanPlayer player = gameState.getPlayers()[0];
        assertEquals('A', player.getPlayerKey());
        List<BombermanCell> cellsWithBombs = player.getCellsWithBombs();
        assertEquals(2, cellsWithBombs.size());
        assertSame(gameState.getGrid().getCell(1, 12), cellsWithBombs.get(0));
        assertSame(gameState.getGrid().getCell(3, 11), cellsWithBombs.get(1));

        player = gameState.getPlayers()[1];
        assertEquals('B', player.getPlayerKey());
        cellsWithBombs = player.getCellsWithBombs();
        assertEquals(1, cellsWithBombs.size());
        assertSame(gameState.getGrid().getCell(17, 2), cellsWithBombs.get(0));
    }
    
    @Test
    public void testGrid()
    {
        String[] gridLines = gameState.getGrid().toString().split("\\n");
        assertEquals("#####################", gridLines[0]);
        assertEquals("#           1   +++ #", gridLines[1]);
        assertEquals("# # # # # # #A# # # #", gridLines[2]);
        assertEquals("#          7        #", gridLines[3]);
        assertEquals("# # # # # # # # # # #", gridLines[4]);
        assertEquals("#   ++    +      ++ #", gridLines[5]);
        assertEquals("# # # #+# # # # # #+#", gridLines[6]);
        assertEquals("#+ +   + + +     + +#", gridLines[7]);
        assertEquals("#+# # # #+#+# # # #+#", gridLines[8]);
        assertEquals("#   +  +++ +++  +   #", gridLines[9]);
        assertEquals("#+# # #+#&$!#+# # #+#", gridLines[10]);
        assertEquals("#       +       +   #", gridLines[11]);
        assertEquals("#+# # # # # # # # #+#", gridLines[12]);
        assertEquals("#+               + +#", gridLines[13]);
        assertEquals("#+# # # # # # # # #+#", gridLines[14]);
        assertEquals("#         +         #", gridLines[15]);
        assertEquals("# # #+#+#+#+# # # #+#", gridLines[16]);
        assertEquals("#+b  + +++ +++      #", gridLines[17]);
        assertEquals("# # # #+# # #+# # # #", gridLines[18]);
        assertEquals("# +++  ++   ++      #", gridLines[19]);
        assertEquals("#####################", gridLines[20]);
    }
}
