package za.co.entelect.challenge.tests.model.moveinfo;

import static org.junit.Assert.*;

import za.co.entelect.challenge.model.bomberman.BombermanCell;
import za.co.entelect.challenge.model.bomberman.BombermanGrid;
import za.co.entelect.challenge.model.bomberman.BombermanPlayer;
import za.co.entelect.challenge.model.bomberman.BombermanState;

public class BombermanStateHelpers
{
    public static void assertEqual(BombermanState s1, BombermanState s2)
    {
        assertEquals(s1.getGameRound(), s2.getGameRound());
        assertEquals(s1.getMaximumGameRounds(), s2.getMaximumGameRounds());
        assertEquals(s1.getPlayerKillPoints(), s2.getPlayerKillPoints());
        
        assertEquals(s1.getPlayers().length, s2.getPlayers().length);
        for (int i = 0; i < s1.getPlayers().length; i++)
            assertEqual(s1.getPlayers()[i], s2.getPlayers()[i]);
        assertEqual(s1.getGrid(), s2.getGrid());
        
        assertEquals(s1.getExplodingCells(), s2.getExplodingCells());
    }
    
    private static void assertEqual(BombermanPlayer p1, BombermanPlayer p2)
    {
        assertEquals(p1.getPlayerKey(), p2.getPlayerKey());
        assertEquals(p1.getRow(), p2.getRow());
        assertEquals(p1.getColumn(), p2.getColumn());
        assertEquals(p1.getScore(), p2.getScore());
        assertEquals(p1.isAlive(), p2.isAlive());
        assertEquals(p1.getBombBagSize(), p2.getBombBagSize());
        assertEquals(p1.getBombRadius(), p2.getBombRadius());
        assertEquals(p1.hasBombToTrigger(), p2.hasBombToTrigger());
    }
    
    private static void assertEqual(BombermanGrid g1, BombermanGrid g2)
    {
        assertEquals(g1.getHeight(), g2.getHeight());
        assertEquals(g1.getWidth(), g2.getWidth());
        for (int row = 0; row < g1.getHeight(); row++)
            for (int column = 0; column < g1.getWidth(); column++)
                assertEqual(g1.getCell(row, column), g2.getCell(row, column));
    }

    private static void assertEqual(BombermanCell c1, BombermanCell c2)
    {
        assertEquals(c1.getCellType(), c2.getCellType());
        assertTrue(((c1.getCellOwner() == null) == (c2.getCellOwner() == null)) 
                && ((c1.getCellOwner() == null) || (c1.getCellOwner().getPlayerKey() == c2.getCellOwner().getPlayerKey())));
        assertEquals(c1.getBombCountdown(), c2.getBombCountdown());
        assertEquals(c1.getBombRadius(), c2.getBombRadius());
        assertEquals(c1.getPlayersVisited(), c2.getPlayersVisited());
    }
}
