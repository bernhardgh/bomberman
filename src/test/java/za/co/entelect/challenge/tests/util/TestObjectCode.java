package za.co.entelect.challenge.tests.util;

import static org.junit.Assert.*;

import org.junit.Test;

import za.co.entelect.challenge.util.ComputedHash;
import za.co.entelect.challenge.util.ObjectCode;
import za.co.entelect.challenge.util.ObjectCodeComponent;

public class TestObjectCode
{
    private ObjectCode objectCode = new ObjectCode();
    private ObjectCodeComponent component1, component2;
    
    public TestObjectCode()
    {
        component1 = new ObjectCodeComponent(objectCode);
        component2 = new ObjectCodeComponent(objectCode);
    }
    
    @Test
    public void testObjectCode()
    {
        assertEquals(new ComputedHash(0, 0), objectCode.getComputedHash());
        
        component1.refreshCodes(2);
        component2.refreshCodes(5);
        ComputedHash initialHash = objectCode.getComputedHash();
        assertNotEquals(new ComputedHash(0, 0), initialHash);

        component1.refreshCodes(5);
        component2.refreshCodes(2);
        assertNotEquals(initialHash, objectCode.getComputedHash());
        
        component1.refreshCodes(2);
        component2.refreshCodes(5);
        assertEquals(initialHash, objectCode.getComputedHash());
    }
}
