package za.co.entelect.challenge.tests.dto;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.junit.Test;

import za.co.entelect.challenge.dto.DodgeCounts;
import za.co.entelect.challenge.model.bomberman.BombermanPlayer;

public class TestDodgeCountsPersistence
{
    private BombermanPlayer[] players;
    
    public TestDodgeCountsPersistence()
    {
        players = new BombermanPlayer[] { new BombermanPlayer('A'), new BombermanPlayer('B'), new BombermanPlayer('C') };
        players[0].incrementDodgeCount();
        players[2].incrementDodgeCount();
        players[2].incrementDodgeCount();
    }
    
    @Test
    public void testStoreDodgeCounts() throws IOException
    {
        byte[] countsBytes = saveCounts(new DodgeCounts(players));
        
        BombermanPlayer[] restoredPlayers = new BombermanPlayer[] { new BombermanPlayer('A'), new BombermanPlayer('B'), new BombermanPlayer('C') };
        DodgeCounts dodgeCounts = new DodgeCounts(restoredPlayers);
        loadCounts(dodgeCounts, countsBytes);
        
        assertDodgeCounts(restoredPlayers, 1, 0, 2);
        
        restoredPlayers[1].incrementDodgeCount();
        countsBytes = saveCounts(dodgeCounts);
        
        restoredPlayers = new BombermanPlayer[] { new BombermanPlayer('A'), new BombermanPlayer('B'), new BombermanPlayer('C') };
        dodgeCounts = new DodgeCounts(restoredPlayers);
        loadCounts(dodgeCounts, countsBytes);
        
        assertDodgeCounts(restoredPlayers, 1, 1, 2);
    }
    
    private byte[] saveCounts(DodgeCounts dodgeCounts) throws IOException
    {
        try (ByteArrayOutputStream stream = new ByteArrayOutputStream())
        {
            dodgeCounts.saveCounts(stream);
            return stream.toByteArray();
        }
    }
    
    private void loadCounts(DodgeCounts dodgeCounts, byte[] countsBytes) throws IOException
    {
        try (ByteArrayInputStream stream = new ByteArrayInputStream(countsBytes))
        {
            dodgeCounts.loadCounts(stream);
        }
    }
    
    private void assertDodgeCounts(BombermanPlayer[] players, int... expectedCounts)
    {
        for (int i = 0; i < players.length; i++)
            assertEquals(expectedCounts[i], players[i].getDodgeCount());
    }
}
