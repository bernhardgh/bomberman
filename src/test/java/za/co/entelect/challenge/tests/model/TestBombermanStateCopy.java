package za.co.entelect.challenge.tests.model;

import static org.junit.Assert.*;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

import org.junit.Test;

import za.co.entelect.challenge.model.bomberman.BombermanGrid;
import za.co.entelect.challenge.model.bomberman.BombermanPlayer;
import za.co.entelect.challenge.model.bomberman.BombermanState;
import za.co.entelect.challenge.model.bomberman.BombermanCell.CellType;
import za.co.entelect.challenge.util.ComputedHash;

public class TestBombermanStateCopy
{
    private BombermanState gameState;
    
    public TestBombermanStateCopy()
    {
        BombermanPlayer player = new BombermanPlayer('A');
        BombermanGrid grid = new BombermanGrid(5, 5);
        gameState = new BombermanState(grid, player);
        
        grid.getCell(1, 1).performCellChange(player);
        grid.getCell(1, 1).plantBomb();
        grid.getCell(1, 2).performCellChange(CellType.BOMB_EXPLOSION);
        gameState.getExplodingCells().add(grid.getCell(1, 2));
        
        gameState.initializeCodeIntegration();
    }
    
    @Test
    public void testClone()
    {
        assertGameState(gameState, (BombermanState)gameState.clone());
    }
    
    @Test
    public void testSerialization() throws IOException, ClassNotFoundException
    {
        BombermanState deserialized;
        try (
                PipedOutputStream outPipeStream = new PipedOutputStream();
                PipedInputStream inPipeStream = new PipedInputStream(outPipeStream);
                ObjectOutputStream objectWriterStream = new ObjectOutputStream(outPipeStream);
                ObjectInputStream objectReaderStream = new ObjectInputStream(inPipeStream))
        {
            (new Thread()
            {
                public void run()
                {
                    try
                    {
                        objectWriterStream.writeObject(gameState);
                    }
                    catch (IOException e)
                    {
                        e.printStackTrace();
                    }
                }
            }).start();
        
            deserialized = (BombermanState)objectReaderStream.readObject();
        }
        assertGameState(gameState, deserialized);
    }
    
    
    private static void assertGameState(BombermanState original, BombermanState copy)
    {
        assertNotSame(original, copy);
        assertNotSame(original.getPlayers()[0].getCellsWithBombs().get(0), copy.getPlayers()[0].getCellsWithBombs().get(0));
        assertTrue(copy.getPlayers()[0].isActive());
        assertNotNull(copy.getPlayers()[0].getProcessingInfo());
        assertNotSame(original.getPlayers()[0].getProcessingInfo(), copy.getPlayers()[0].getProcessingInfo());
        assertNotSame(original.getGrid().getCell(1, 1).getCellOwner(), copy.getGrid().getCell(1, 1).getCellOwner());
        assertNotSame(original.getExplodingCells().iterator().next(), copy.getExplodingCells().iterator().next());
        
        assertEquals(CellType.PLAYER, copy.getGrid().getCell(1, 1).getCellType());
        assertSame(copy.getPlayers()[0], copy.getGrid().getCell(1, 1).getCellOwner());
        assertSame(copy.getPlayers()[0].getCellsWithBombs().get(0), copy.getGrid().getCell(1, 1));
        assertSame(copy.getExplodingCells().iterator().next(), copy.getGrid().getCell(1, 2));
        
        ComputedHash originalHash = original.getComputedHash();
        assertEquals(originalHash, copy.getComputedHash());
        
        copy.getGrid().getCell(1, 3).setCell(CellType.BOMB_EXPLOSION);
        copy.getPlayers()[0].increaseBombRadius();
        assertNotEquals(originalHash, copy.getComputedHash());
        assertEquals(originalHash, original.getComputedHash());
    }
}
