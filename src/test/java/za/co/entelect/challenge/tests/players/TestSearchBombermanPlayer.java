package za.co.entelect.challenge.tests.players;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;

//import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import za.co.entelect.challenge.model.GameCoordinator;
import za.co.entelect.challenge.model.GamePlayer;
import za.co.entelect.challenge.model.InvalidMoveException;
import za.co.entelect.challenge.model.bomberman.BombermanCell;
import za.co.entelect.challenge.model.bomberman.BombermanEngine;
import za.co.entelect.challenge.model.bomberman.BombermanMove;
import za.co.entelect.challenge.model.bomberman.BombermanPlayer;
import za.co.entelect.challenge.model.bomberman.BombermanState;
import za.co.entelect.challenge.model.bomberman.players.BombermanPlayerFactory;
import za.co.entelect.challenge.model.bomberman.players.LazyBombermanPlayer;
import za.co.entelect.challenge.tests.FileHelpers;

@RunWith(Parameterized.class)
//@Ignore
public class TestSearchBombermanPlayer
{
    private static final int DEFAULT_PLAYER_TIMEOUT = 1600;
    
    @Parameters
    public static Collection<String[]> gamePlayerNames()
    {
        return Arrays.asList(new String[][] { { "Multi-DFS" }, { "Multi-AlphaBeta" }, { "Search" } });
    }
    
    private String gamePlayerName;
    
    public TestSearchBombermanPlayer(String gamePlayerName)
    {
        this.gamePlayerName = gamePlayerName;
    }
    
    @Test
    public void testStuck() throws ClassNotFoundException, IOException, InvalidMoveException, InterruptedException
    {
        BombermanState gameState = FileHelpers.loadTestStateFromResource("teststates/stuck");
        BombermanPlayer player = gameState.getPlayer('A');
        @SuppressWarnings("unchecked")
        GameCoordinator<BombermanCell> gameCoordinator = new GameCoordinator<>(new BombermanEngine(), gameState, 
                (GamePlayer<BombermanCell>[])new GamePlayer[] {
                    BombermanPlayerFactory.createPlayer(gamePlayerName, player, DEFAULT_PLAYER_TIMEOUT)
                    });
        
        int playerColumn = player.getColumn();
        for (int i = 0; i < 6; i++)
        {
            gameCoordinator.move();
            if ((player.getColumn() != playerColumn) || (player.getAvailableBombCount() != 1))
                return;
        }
        
        fail("Player is stuck.\n" + gameState.getGrid());
    }
    
    @Test
    public void testClose() throws ClassNotFoundException, IOException, InvalidMoveException, InterruptedException
    {
        BombermanState gameState = FileHelpers.loadTestStateFromResource("teststates/close");
        BombermanPlayer player = gameState.getPlayer('B');
        @SuppressWarnings("unchecked")
        GameCoordinator<BombermanCell> gameCoordinator = new GameCoordinator<>(new BombermanEngine(), gameState, 
                (GamePlayer<BombermanCell>[])new GamePlayer[] {
                    new LazyBombermanPlayer(player),
                    BombermanPlayerFactory.createPlayer(gamePlayerName, player, DEFAULT_PLAYER_TIMEOUT) 
                    });

        int playerRow = player.getRow(), playerColumn = player.getColumn();
        for (int i = 0; i < 6; i++)
        {
            gameCoordinator.move();
            if ((player.getColumn() != playerColumn) || (player.getRow() > playerRow) || (player.getAvailableBombCount() != 1))
                return;
        }
        
        fail("Player is stuck.\n" + gameState.getGrid());
    }
    
    @Test
    public void testStale() throws ClassNotFoundException, IOException, InvalidMoveException, InterruptedException
    {
        BombermanState gameState = FileHelpers.loadTestStateFromResource("teststates/stale");
        BombermanPlayer player = gameState.getPlayer('A');
        @SuppressWarnings("unchecked")
        GameCoordinator<BombermanCell> gameCoordinator = new GameCoordinator<>(new BombermanEngine(), gameState, 
                (GamePlayer<BombermanCell>[])new GamePlayer[] {
                    BombermanPlayerFactory.createPlayer(gamePlayerName, player, DEFAULT_PLAYER_TIMEOUT) 
                    });

        for (int i = 0; i < 3; i++)
        {
            gameCoordinator.move();
            if (!player.getCellsWithBombs().isEmpty())
                return;
        }
        
        fail("Player is stuck.\n" + gameState.getGrid());
    }
    
    @Test
    public void testMoveAlong() throws ClassNotFoundException, IOException, InterruptedException
    {
        BombermanMove move = getMoveFromState("moveAlong", 'A');
        assertEquals(BombermanMove.RIGHT, move);
    }
    
    @Test
    public void testTrap() throws ClassNotFoundException, IOException, InterruptedException
    {
        BombermanMove move = getMoveFromState("trap", 'A');
        assertNotEquals(BombermanMove.RIGHT, move);
    }
    
    @Test
    public void testAvoidDanger() throws ClassNotFoundException, InterruptedException, IOException
    {
        BombermanMove move = getMoveFromState("danger", 'C');
        assertNotEquals(BombermanMove.DOWN, move);
    }
    
    @Test
    public void testDontPlaceBomb() throws ClassNotFoundException, InterruptedException, IOException
    {
        BombermanMove move = getMoveFromState("dontPlaceBomb", 'B');
        assertNotEquals(BombermanMove.PLACE_BOMB, move);
        
        move = getMoveFromState("dontPlaceBomb", 'B');
        assertNotEquals(BombermanMove.PLACE_BOMB, move);
    }
    
    @Test
    public void testAvoidLongDistanceThreat() throws ClassNotFoundException, InterruptedException, IOException
    {
        BombermanMove move = getMoveFromState("threat", 'C');
        assertTrue((move == BombermanMove.LEFT) || (move == BombermanMove.RIGHT));
    }
    
    @Test
    public void testAnticipateThreat() throws ClassNotFoundException, InterruptedException, IOException
    {
        BombermanMove move = getMoveFromState("futureStep", 'A');
        assertNotEquals(BombermanMove.PLACE_BOMB, move);
    }
    
    @Test
    public void testGetsTrapped() throws ClassNotFoundException, InterruptedException, IOException
    {
        if (!gamePlayerName.equals("Multi-DFS"))
        {
            BombermanMove move = getMoveFromState("getsTrapped", 'D');
            assertNotEquals(BombermanMove.DOWN, move);
        }
    }
    
    @Test
    public void testAvoidTrap() throws ClassNotFoundException, InterruptedException, IOException
    {
        if (!gamePlayerName.equals("Multi-AlphaBeta"))
        {
            BombermanMove move = getMoveFromState("avoidTrap", 'C', 3000);
            assertEquals(BombermanMove.DOWN, move);
        }
    }
    
    @Test
    public void testDontPlaceBombNow() throws ClassNotFoundException, InterruptedException, IOException
    {
        BombermanMove move = getMoveFromState("dontPlaceBombNow", 'A');
        assertNotEquals(BombermanMove.PLACE_BOMB, move);
    }
    
    @Test
    public void testStagnant() throws ClassNotFoundException, InterruptedException, IOException
    {
        BombermanMove move = getMoveFromState("stagnant1", 'A');
        assertNotEquals(BombermanMove.DO_NOTHING, move);
        
        if (!gamePlayerName.equals("Multi-DFS"))
        {
            move = getMoveFromState("stagnant2", 'C');
            assertNotEquals(BombermanMove.DO_NOTHING, move);
    
            move = getMoveFromState("stagnant3", 'C');
            assertNotEquals(BombermanMove.DO_NOTHING, move);
        }
    }
    
    @Test
    public void testSuicide() throws ClassNotFoundException, InterruptedException, IOException
    {
        BombermanMove move = getMoveFromState("suicide", 'B');
        assertEquals(BombermanMove.DO_NOTHING, move);
    }
    
    @Test
    public void testGameOver() throws ClassNotFoundException, InterruptedException, IOException
    {
        BombermanMove move = getMoveFromState("gameOver", 'A');
        assertEquals(BombermanMove.DO_NOTHING, move);
        
        move = getMoveFromState("gameOver", 'B');
        assertNotEquals(BombermanMove.RIGHT, move);
    }
    

    private BombermanMove getMoveFromState(String stateName, char playerKey) throws InterruptedException, ClassNotFoundException, IOException
    {
        return getMoveFromState(stateName, playerKey, DEFAULT_PLAYER_TIMEOUT);
    }
    
    private BombermanMove getMoveFromState(String stateName, char playerKey, int playerTimeout) throws InterruptedException, ClassNotFoundException, IOException
    {
        BombermanState gameState = FileHelpers.loadTestStateFromResource("teststates/" + stateName);
        BombermanPlayer player = gameState.getPlayer(playerKey);
        GamePlayer<BombermanCell> gamePlayer = BombermanPlayerFactory.createPlayer(gamePlayerName, player, playerTimeout);
        
        return (BombermanMove)gamePlayer.getNextMove(gameState);
    }
}
