package za.co.entelect.challenge.tests.util;

import static org.junit.Assert.*;

import org.junit.Test;

import za.co.entelect.challenge.util.IntMath;

public class TestIntMath
{
    @Test
    public void testLogBase2()
    {
        assertEquals(0, IntMath.logBase2(1));
        assertEquals(1, IntMath.logBase2(2));
        assertEquals(1, IntMath.logBase2(3));
        assertEquals(2, IntMath.logBase2(4));
        assertEquals(2, IntMath.logBase2(5));
        assertEquals(2, IntMath.logBase2(7));
        assertEquals(3, IntMath.logBase2(8));
        assertEquals(3, IntMath.logBase2(9));
        assertEquals(4, IntMath.logBase2(16));
        assertEquals(7, IntMath.logBase2(128));
    }
}
