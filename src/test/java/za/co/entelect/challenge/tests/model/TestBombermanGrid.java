package za.co.entelect.challenge.tests.model;

import static org.junit.Assert.*;

import org.junit.Test;

import za.co.entelect.challenge.model.bomberman.BombermanCell.CellType;
import za.co.entelect.challenge.model.bomberman.BombermanGrid;
import za.co.entelect.challenge.model.bomberman.BombermanGridGenerator;
import za.co.entelect.challenge.model.bomberman.BombermanPlayer;
import za.co.entelect.challenge.model.bomberman.PowerUp.PowerUpType;

public class TestBombermanGrid
{
    @Test
    public void testBorder()
    {
        BombermanGrid grid = new BombermanGrid(3, 3);
        
        assertEquals(CellType.INDESTRUCTIBLE_WALL, grid.getCell(0, 0).getCellType());
        assertEquals(CellType.INDESTRUCTIBLE_WALL, grid.getCell(0, 1).getCellType());
        assertEquals(CellType.INDESTRUCTIBLE_WALL, grid.getCell(0, 2).getCellType());
        assertEquals(CellType.INDESTRUCTIBLE_WALL, grid.getCell(1, 0).getCellType());
        assertEquals(CellType.CLEAR, grid.getCell(1, 1).getCellType());
        assertEquals(CellType.INDESTRUCTIBLE_WALL, grid.getCell(1, 2).getCellType());
        assertEquals(CellType.INDESTRUCTIBLE_WALL, grid.getCell(2, 0).getCellType());
        assertEquals(CellType.INDESTRUCTIBLE_WALL, grid.getCell(2, 1).getCellType());
        assertEquals(CellType.INDESTRUCTIBLE_WALL, grid.getCell(2, 2).getCellType());
    }
    
    @Test
    public void testPillars()
    {
        BombermanGrid grid = new BombermanGrid(5, 7);
        
        assertEquals(CellType.INDESTRUCTIBLE_WALL, grid.getCell(2, 2).getCellType());
        assertEquals(CellType.CLEAR, grid.getCell(2, 3).getCellType());
        assertEquals(CellType.INDESTRUCTIBLE_WALL, grid.getCell(2, 4).getCellType());
        assertEquals(CellType.CLEAR, grid.getCell(2, 5).getCellType());
        assertEquals(CellType.INDESTRUCTIBLE_WALL, grid.getCell(2, 6).getCellType());
    }
    
    @Test
    public void testGridGenerator()
    {
        BombermanPlayer[] players = new BombermanPlayer[] { 
                new BombermanPlayer('A'), new BombermanPlayer('B'), new BombermanPlayer('C'), new BombermanPlayer('D')
        };
        BombermanGridGenerator generator = new BombermanGridGenerator(19, 19, players);
        BombermanGrid grid = generator.getBombermanGrid();
        
        assertEquals(226, grid.getUsableCellCount());
        
        assertEquals(1, players[0].getRow());
        assertEquals(1, players[0].getColumn());
        assertEquals(17, players[1].getRow());
        assertEquals(17, players[1].getColumn());
        assertEquals(17, players[2].getRow());
        assertEquals(1, players[2].getColumn());
        assertEquals(1, players[3].getRow());
        assertEquals(17, players[3].getColumn());
        
        assertEquals(1, players[0].getGridCoverage());
        assertEquals(1, players[1].getGridCoverage());
        assertEquals(1, players[2].getGridCoverage());
        assertEquals(1, players[3].getGridCoverage());
        
        assertEquals(CellType.PLAYER, grid.getCell(1, 1).getCellType());
        assertSame(players[0], grid.getCell(1, 1).getCellOwner());
        assertEquals(CellType.PLAYER, grid.getCell(17, 17).getCellType());
        assertSame(players[1], grid.getCell(17, 17).getCellOwner());
        assertEquals(CellType.PLAYER, grid.getCell(17, 1).getCellType());
        assertSame(players[2], grid.getCell(17, 1).getCellOwner());
        assertEquals(CellType.PLAYER, grid.getCell(1, 17).getCellType());
        assertSame(players[3], grid.getCell(1, 17).getCellOwner());
        assertEquals(CellType.SUPER_POWER_UP, grid.getCell(9, 9).getCellType());
        
        assertEquals(2 * 4, grid.getHiddenPowerUps().stream().filter(p -> p.getPowerUpType() == PowerUpType.BOMB_BAG_POWER_UP).count());
        assertEquals(4 * 4, grid.getHiddenPowerUps().stream().filter(p -> p.getPowerUpType() == PowerUpType.BOMB_RADIUS_POWER_UP).count());
    }
}
