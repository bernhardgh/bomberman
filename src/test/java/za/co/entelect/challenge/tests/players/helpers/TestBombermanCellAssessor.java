package za.co.entelect.challenge.tests.players.helpers;

import java.io.IOException;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;

import za.co.entelect.challenge.model.bomberman.BombermanGrid;
import za.co.entelect.challenge.model.bomberman.BombermanPlayer;
import za.co.entelect.challenge.model.bomberman.BombermanCell.CellType;
import za.co.entelect.challenge.model.bomberman.BombermanState;
import za.co.entelect.challenge.model.bomberman.players.helpers.BombermanCellAssessor;
import za.co.entelect.challenge.tests.FileHelpers;

@RunWith(JUnitParamsRunner.class)
public class TestBombermanCellAssessor
{
    private BombermanCellAssessor assessor = new BombermanCellAssessor();
    
    @Test
    @Parameters({ "2, 1", "5, 7", "1, 3" })
    public void testSafe(int row, int column)
    {
        BombermanState gameState = getScenario();
        assertNull(assessor.getThreatForLocation(row, column, gameState, 0));
    }
    
    @Test
    @Parameters({ "5, 2", "3, 1", "5, 6" })
    public void testVulnerable(int row, int column)
    {
        BombermanState gameState = getScenario();
        assertSame(gameState.getPlayer('C'), assessor.getThreatForLocation(row, column, gameState, 0));
    }
    
    @Test
    public void testAssumePlayerWontKillItself()
    {
        BombermanState gameState = getSuicideScenario();
        gameState.getPlayer('B').setIsActive(false);
        assertNull(assessor.getThreatForLocation(1, 3, gameState, 0));
    }
    
    @Test
    public void testComprehendPlayerCantKillItself()
    {
        BombermanState gameState = getNonSuicideScenario();
        gameState.getPlayer('B').setIsActive(false);
        assertSame(gameState.getPlayer('B'), assessor.getThreatForLocation(1, 4, gameState, 0));
    }
    
    @Test
    public void testBombTriggerIsSuicide()
    {
        BombermanState gameState = getSuicideScenario();
        assertTrue(assessor.bombTriggerIsSuicide(gameState.getPlayer('B'), gameState.getGrid()));
    }
    
    @Test
    public void testAtopBombTriggerIsSuicide()
    {
        BombermanState gameState = getAtopBombSuicideScenario();
        assertTrue(assessor.bombTriggerIsSuicide(gameState.getPlayer('B'), gameState.getGrid()));
    }
    
    @Test
    public void testBombTriggerIsNotSuicide()
    {
        BombermanState gameState = getNonSuicideScenario();
        assertFalse(assessor.bombTriggerIsSuicide(gameState.getPlayer('B'), gameState.getGrid()));
    }
    
    @Test
    public void testFutureStep() throws ClassNotFoundException, IOException
    {
        BombermanState gameState = FileHelpers.loadTestStateFromResource("teststates/futureStep");
        gameState.getPlayer('C').setIsActive(false);
        int playerRow = gameState.getPlayer('A').getRow(), playerColumn = gameState.getPlayer('A').getColumn();
        
        assertNull(assessor.getThreatForLocation(playerRow, playerColumn, gameState, 0));
        assertSame(gameState.getPlayer('C'), assessor.getThreatForLocation(playerRow, playerColumn, gameState, 1));
    }
    
    
    private static BombermanState getScenario()
    {
        BombermanPlayer playerA = new BombermanPlayer('A');
        BombermanPlayer playerB = new BombermanPlayer('B');
        playerB.setBombCounts(2);
        BombermanPlayer playerC = new BombermanPlayer('C');
        playerC.setBombCounts(2);
        playerC.setIsActive(false);
        
        BombermanGrid grid = new BombermanGrid(9, 9);
        grid.getCell(5, 1).performCellChange(playerA);
        grid.getCell(5, 1).plantBomb();
        grid.getCell(5, 1).setBomb(3, 2, playerA);
        grid.getCell(5, 1).performCellChange(CellType.CLEAR);
        grid.getCell(5, 4).performCellChange(playerC);
        grid.getCell(5, 4).plantBomb();
        grid.getCell(5, 4).setBomb(2, 3, playerC);
        grid.getCell(5, 4).performCellChange(CellType.CLEAR);
        grid.getCell(5, 5).performCellChange(playerB);
        grid.getCell(5, 5).plantBomb();
        grid.getCell(5, 5).setBomb(3, 1, playerB);
        grid.getCell(5, 5).performCellChange(CellType.CLEAR);
        grid.getCell(2, 3).performCellChange(playerB);
        grid.getCell(2, 3).plantBomb();
        grid.getCell(2, 3).setBomb(3, 1, playerB);
        grid.getCell(2, 3).performCellChange(CellType.CLEAR);
        grid.getCell(4, 3).performCellChange(playerC);
        grid.getCell(4, 3).plantBomb();
        grid.getCell(4, 3).setBomb(3, 2, playerC);
        grid.getCell(4, 3).performCellChange(CellType.CLEAR);
        
        return new BombermanState(grid, playerA, playerB, playerC);
    }
    
    private static BombermanState getSuicideScenario()
    {
        BombermanPlayer playerA = new BombermanPlayer('A');
        BombermanPlayer playerB = new BombermanPlayer('B');
        
        BombermanGrid grid = new BombermanGrid(9, 9);
        grid.getCell(1, 2).performCellChange(playerB);
        grid.getCell(1, 2).plantBomb();
        grid.getCell(1, 2).performCellChange(CellType.CLEAR);
        grid.getCell(1, 1).performCellChange(playerB);
        
        return new BombermanState(grid, playerA, playerB);
    }
    
    private static BombermanState getNonSuicideScenario()
    {
        BombermanPlayer playerA = new BombermanPlayer('A');
        BombermanPlayer playerB = new BombermanPlayer('B');
        
        BombermanGrid grid = new BombermanGrid(9, 9);
        grid.getCell(1, 2).performCellChange(CellType.DESTRUCTIBLE_WALL);
        grid.getCell(1, 3).performCellChange(playerB);
        grid.getCell(1, 3).plantBomb();
        grid.getCell(1, 3).setBomb(3, 2, playerB);
        grid.getCell(1, 3).performCellChange(CellType.CLEAR);
        grid.getCell(1, 1).performCellChange(playerB);
        
        return new BombermanState(grid, playerA, playerB);
    }
    
    private static BombermanState getAtopBombSuicideScenario()
    {
        BombermanPlayer playerA = new BombermanPlayer('A');
        BombermanPlayer playerB = new BombermanPlayer('B');
        
        BombermanGrid grid = new BombermanGrid(9, 9);
        grid.getCell(1, 2).performCellChange(playerB);
        grid.getCell(1, 2).plantBomb();
        
        return new BombermanState(grid, playerA, playerB);
    }
}
