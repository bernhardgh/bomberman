package za.co.entelect.challenge.tests.model.moveinfo;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

import org.junit.Test;
import org.junit.runner.RunWith;

import za.co.entelect.challenge.model.bomberman.BombermanEngine;
import za.co.entelect.challenge.model.bomberman.BombermanGrid;
import za.co.entelect.challenge.model.bomberman.BombermanMove;
import za.co.entelect.challenge.model.bomberman.BombermanPlayer;
import za.co.entelect.challenge.model.bomberman.BombermanState;
import za.co.entelect.challenge.model.bomberman.BombermanCell.CellType;
import za.co.entelect.challenge.model.bomberman.moveinfo.BombermanMoveInfo;

@RunWith(JUnitParamsRunner.class)
public class TestPlayerMoves
{
    private BombermanEngine gameEngine = new BombermanEngine();
    private BombermanState gameState;
    private BombermanPlayer player = new BombermanPlayer('A');
    
    public TestPlayerMoves()
    {
        gameState = new BombermanState(new BombermanGrid(9, 9), player);
        
        gameState.getGrid().getCell(1, 1).performCellChange(player);
        gameState.getGrid().getCell(1, 1).plantBomb();
        gameState.getGrid().getCell(1, 1).performCellChange(CellType.CLEAR);
        gameState.getGrid().getCell(3, 3).performCellChange(player);
        gameState.getGrid().getCell(4, 3).performCellChange(CellType.SUPER_POWER_UP);
    }
    
    @Test
    @Parameters({"LEFT", "RIGHT", "UP", "DOWN", "PLACE_BOMB", "TRIGGER_BOMB", "DO_NOTHING"})
    public void testMove(BombermanMove move)
    {
        BombermanState newGameState = (BombermanState)gameState.clone();
        BombermanMoveInfo moveInfo = gameEngine.applyMove(newGameState, player, move);
        gameEngine.undo(moveInfo);
        
        BombermanStateHelpers.assertEqual(gameState, newGameState);
    }
}
