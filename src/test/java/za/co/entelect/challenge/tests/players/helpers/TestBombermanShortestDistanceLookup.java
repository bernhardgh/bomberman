package za.co.entelect.challenge.tests.players.helpers;

import static org.junit.Assert.*;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

import org.junit.Test;
import org.junit.runner.RunWith;

import za.co.entelect.challenge.model.bomberman.BombermanGrid;
import za.co.entelect.challenge.model.bomberman.BombermanCell.CellType;
import za.co.entelect.challenge.model.bomberman.BombermanPlayer;
import za.co.entelect.challenge.model.bomberman.players.helpers.BombermanShortestDistanceLookup;

@RunWith(JUnitParamsRunner.class)
public class TestBombermanShortestDistanceLookup
{
    private BombermanShortestDistanceLookup lookup;
    
    public TestBombermanShortestDistanceLookup()
    {
        BombermanGrid grid = createTestGrid();
        lookup = new BombermanShortestDistanceLookup(grid, 2, 3);
    }
    
    @Test
    @Parameters({ "0, 2, 3", "1, 1, 3", "6, 4, 1", "5, 1, 7", "13, 7, 5", "12, 5, 4", "14, 4, 3" })
    public void testDistance(int expectedDistance, int row, int column)
    {
        assertEquals(expectedDistance, lookup.getDistance(row, column));
    }
    
    @Test
    @Parameters({ "2, 2", "3, 3", "6, 1" })
    public void testUnreachable(int row, int column)
    {
        assertEquals(Integer.MAX_VALUE, lookup.getDistance(row, column));
    }
    
    private static BombermanGrid createTestGrid()
    {
        BombermanGrid grid = new BombermanGrid(9, 9);
        for (int column = 3; column <= 5; column++)
            grid.getCell(3, column).performCellChange(CellType.DESTRUCTIBLE_WALL);
        for (int column = 1; column <= 3; column++)
            grid.getCell(5, column).performCellChange(CellType.DESTRUCTIBLE_WALL);
        grid.getCell(7, 4).performCellChange(CellType.DESTRUCTIBLE_WALL);
        BombermanPlayer player = new BombermanPlayer('A');
        grid.getCell(2, 7).performCellChange(player);
        grid.getCell(4, 7).setBomb(3, 1, player);
        grid.getCell(5, 7).performCellChange(CellType.BOMB_EXPLOSION);
        return grid;
    }
}
