package za.co.entelect.challenge.tests.logging;

import java.util.ArrayList;

import static org.junit.Assert.*;

import org.junit.Test;

import za.co.entelect.challenge.logging.LogTarget;
import za.co.entelect.challenge.logging.Logger;
import za.co.entelect.challenge.logging.Logger.LogLevel;

public class TestLogger
{
    private Logger logger = Logger.getInstance();
    private ArrayList<String> log = new ArrayList<>();
    
    public TestLogger()
    {
        logger.setLogTarget(new MockLogTarget());
        logger.setLogLevel(LogLevel.INFO);
    }
    
    @Test
    public void testLogLevel()
    {
        logger.log(LogLevel.TRACE, "Trace");
        logger.log(LogLevel.INFO, "Info");
        logger.log(LogLevel.ERROR, "Error");
        
        assertArrayEquals(new Object[] { "INFO: Info", "ERROR: Error" }, log.toArray());
    }
    
    @Test
    public void testFormatStrings()
    {
        logger.log(LogLevel.INFO, "One %s Three %s", "Two", "Four");
        assertArrayEquals(new Object[] { "INFO: One Two Three Four" }, log.toArray());
    }
    
    @Test
    public void testLogException()
    {
        try
        {
            throw new Exception("Bad things are happening!!!");
        }
        catch (Exception e)
        {
            logger.log(LogLevel.ERROR, "Something failed with ", e);
        }
        
        assertEquals(1, log.size());
        assertTrue("Log was " + log.get(0), log.get(0).startsWith("ERROR: Something failed with java.lang.Exception: Bad things are happening!!!"));
    }
    
    private class MockLogTarget implements LogTarget
    {
        @Override
        public void log(String message)
        {
            log.add(message);
        }
    }
}
