package za.co.entelect.challenge.tests.model.moveinfo;

import org.junit.Test;

import za.co.entelect.challenge.model.bomberman.BombermanCell.CellType;
import za.co.entelect.challenge.model.bomberman.BombermanEngine;
import za.co.entelect.challenge.model.bomberman.BombermanGrid;
import za.co.entelect.challenge.model.bomberman.BombermanMove;
import za.co.entelect.challenge.model.bomberman.BombermanPlayer;
import za.co.entelect.challenge.model.bomberman.BombermanState;
import za.co.entelect.challenge.model.bomberman.moveinfo.BombermanMoveInfo;

public class TestScenario
{
    private BombermanEngine gameEngine = new BombermanEngine();
    private BombermanState gameState;
    private BombermanPlayer playerA = new BombermanPlayer('A');
    private BombermanPlayer playerB = new BombermanPlayer('B');

    public TestScenario()
    {
        gameState = new BombermanState(new BombermanGrid(9, 9), playerA, playerB);
        
        gameState.getGrid().getCell(2, 3).performCellChange(playerA);
        gameState.getGrid().getCell(2, 3).plantBomb();
        gameState.getGrid().getCell(2, 3).setBomb(1, 3, playerA);
        gameState.getGrid().getCell(2, 3).performCellChange(CellType.CLEAR);
        gameState.getGrid().getCell(2, 5).performCellChange(playerA);
        gameState.getGrid().getCell(2, 5).plantBomb();
        gameState.getGrid().getCell(2, 5).performCellChange(CellType.CLEAR);
        gameState.getGrid().getCell(1, 1).performCellChange(playerA);
        
        gameState.getGrid().getCell(5, 3).performCellChange(playerB);
        gameState.getGrid().getCell(5, 3).plantBomb();
        gameState.getGrid().getCell(5, 3).performCellChange(CellType.CLEAR);
        gameState.getGrid().getCell(3, 4).performCellChange(playerB);
        
        gameState.getGrid().getCell(1, 4).performCellChange(CellType.BOMB_EXPLOSION);
        gameState.getExplodingCells().add(gameState.getGrid().getCell(1, 4));
        gameState.getGrid().getCell(3, 3).performCellChange(CellType.SUPER_POWER_UP);
        gameState.getGrid().getCell(5, 4).performCellChange(CellType.DESTRUCTIBLE_WALL);
    }
    
    @Test
    public void testAdvanceRound()
    {
        BombermanState newGameState = (BombermanState)gameState.clone();
        BombermanMoveInfo moveInfo1 = gameEngine.advanceRound(newGameState, BombermanMove.PLACE_BOMB, BombermanMove.LEFT);
        BombermanMoveInfo moveInfo2 = gameEngine.advanceRound(newGameState, BombermanMove.DOWN);
        gameEngine.undo(moveInfo2);
        gameEngine.undo(moveInfo1);
        
        BombermanStateHelpers.assertEqual(gameState, newGameState);
    }
}
