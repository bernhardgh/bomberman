package za.co.entelect.challenge.tests;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.net.URISyntaxException;
import java.nio.file.Path;

import za.co.entelect.challenge.dto.reader.GameStateReaderFactory;
import za.co.entelect.challenge.model.bomberman.BombermanState;

public class FileHelpers
{
    public static Path getResourcePath(String resourceName) throws URISyntaxException
    {
        ClassLoader resourceHandle = FileHelpers.class.getClassLoader();
        return (new File(resourceHandle.getResource(resourceName).toURI())).toPath();
    }
    
    public static BombermanState loadTestStateFromResource(String name) throws IOException, ClassNotFoundException
    {
        ClassLoader resourceHandle = FileHelpers.class.getClassLoader();
        try (InputStream resourceStream = resourceHandle.getResourceAsStream(name))
        {
            return loadTestState(resourceStream);
        }
    }
    
    public static BombermanState loadTestStateFromFile(String filePath) throws IOException, ClassNotFoundException
    {
        try (FileInputStream fileStream = new FileInputStream(filePath))
        {
            return filePath.toLowerCase().endsWith(".json")
                ? (BombermanState)GameStateReaderFactory.getDefaultGameStateReader().read(fileStream, null)
                : loadTestState(fileStream);
        }
    }
    
    private static BombermanState loadTestState(InputStream stream) throws IOException, ClassNotFoundException
    {
        try (ObjectInputStream objectStream = new ObjectInputStream(stream))
        {
            return (BombermanState)objectStream.readObject();
        }
    }
    
    public static void deleteDirectory(File directory)
    {
        if (directory.exists())
        {
            for (File nextFile : directory.listFiles())
            {
                if (nextFile.isDirectory())
                    deleteDirectory(nextFile);
                else
                    nextFile.delete();
            }
        }
        directory.delete();
    }
}
