package za.co.entelect.challenge.tests.model;

import static org.junit.Assert.*;
import org.junit.Test;

import za.co.entelect.challenge.model.bomberman.BombermanEngine;
import za.co.entelect.challenge.model.bomberman.BombermanGrid;
import za.co.entelect.challenge.model.bomberman.BombermanPlayer;
import za.co.entelect.challenge.model.bomberman.BombermanState;

public class TestWinConditions
{
    private BombermanEngine gameEngine = new BombermanEngine();
    private BombermanPlayer playerA = new BombermanPlayer('A'), playerB = new BombermanPlayer('B');
    private BombermanState gameState;
    
    public TestWinConditions()
    {
        BombermanGrid grid = new BombermanGrid(9, 9);
        gameState = new BombermanState(grid, playerA, playerB);
    }
    
    @Test
    public void testLastAliveWins()
    {
        gameState.getGrid().getCell(1, 1).performCellChange(playerA);
        gameState.getGrid().getCell(1, 1).plantBomb();
        gameState.getGrid().getCell(1, 1).setBomb(1, 1, playerA);
        assertFalse(gameEngine.playerDidWin(gameState, playerA));
        assertFalse(gameEngine.playerDidWin(gameState, playerB));
        assertFalse(gameEngine.gameDidEnd(gameState));
        gameEngine.advanceRound(gameState);

        assertFalse(gameEngine.playerDidWin(gameState, playerA));
        assertTrue(gameEngine.playerDidWin(gameState, playerB));
        assertTrue(gameEngine.gameDidEnd(gameState));
    }
    
    @Test
    public void testHighestPoints()
    {
        gameState.setGameRound(81);
        gameState.getGrid().getCell(1, 1).performCellChange(playerB);
        assertFalse(gameEngine.playerDidWin(gameState, playerA));
        assertFalse(gameEngine.playerDidWin(gameState, playerB));
        assertFalse(gameEngine.gameDidEnd(gameState));
        gameEngine.advanceRound(gameState);

        assertFalse(gameEngine.playerDidWin(gameState, playerA));
        assertTrue(gameEngine.playerDidWin(gameState, playerB));
        assertTrue(gameEngine.gameDidEnd(gameState));
    }
}
