package za.co.entelect.challenge.tests.program;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

//import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import za.co.entelect.challenge.program.Main;
import za.co.entelect.challenge.tests.FileHelpers;

@RunWith(JUnitParamsRunner.class)
//@Ignore
public class TestProgram
{
    @Test
    @Parameters({"A", "B"})
    public void testFirstMove(String playerKey) throws IOException, URISyntaxException
    {
        Path tempPath = Files.createTempDirectory("bomberman");
        File coverageFile = new File("coverage");
        if (coverageFile.exists())
            coverageFile.delete();
        
        try
        {
            Files.copy(
                    FileHelpers.getResourcePath("initialstate.json"),
                    tempPath.resolve(Paths.get("state.json")));
            
            long start = System.currentTimeMillis();
            Main.main(playerKey, tempPath.toString());
            long duration = System.currentTimeMillis() - start;

            assertTrue("The move took too long (" + duration + ')', duration < 1900);
            File moveFile = tempPath.resolve("move.txt").toFile();
            assertTrue("Output file does not exist.", moveFile.exists());
            assertTrue("coverage file was not created.", coverageFile.exists());
            
            List<String> lines = Files.readAllLines(moveFile.toPath());
            assertEquals(1, lines.size());
            assertNotEquals(0, Integer.parseInt(lines.get(0)));
        }
        finally
        {
            FileHelpers.deleteDirectory(tempPath.toFile());
            coverageFile.delete();
        }
    }
    
    @Test
    public void testMove() throws IOException, URISyntaxException
    {
        Path tempPath = Files.createTempDirectory("bomberman");
        File coverageFile = new File("coverage");
        if (coverageFile.exists())
            coverageFile.delete();
        
        try
        {
            ClassLoader resourceHandle = TestProgram.class.getClassLoader();
            Files.copy(
                    (new File(resourceHandle.getResource("state.json").toURI())).toPath(),
                    tempPath.resolve(Paths.get("state.json")));
            Files.copy(
                    (new File(resourceHandle.getResource("coverage").toURI())).toPath(),
                    coverageFile.toPath());
            coverageFile.setLastModified(coverageFile.lastModified() - 1000);
            long coverageFileSize = coverageFile.length();
            long coverageFileModifiedDate = coverageFile.lastModified();
            
            long start = System.currentTimeMillis();
            Main.main("A", tempPath.toString());
            long duration = System.currentTimeMillis() - start;

            assertTrue("The move took too long (" + duration + ')', duration < 1950);
            File moveFile = tempPath.resolve("move.txt").toFile();
            assertTrue("Output file does not exist.", moveFile.exists());
            assertTrue("coverage file was not updated.", coverageFile.lastModified() > coverageFileModifiedDate);
            assertEquals("coverage file has wrong length.", coverageFileSize, coverageFile.length());
            
            List<String> lines = Files.readAllLines(moveFile.toPath());
            assertEquals(1, lines.size());
            assertNotEquals(1, Integer.parseInt(lines.get(0)));
        }
        finally
        {
            FileHelpers.deleteDirectory(tempPath.toFile());
            coverageFile.delete();
        }
    }
}
