package za.co.entelect.challenge.dto;

import com.google.gson.annotations.SerializedName;

public class BombOwner
{
    @SerializedName("Key")
    private char key;
    
    public BombOwner() {}
    
    public BombOwner(char key)
    {
        this.key = key;
    }
    
    public char getKey()
    {
        return key;
    }
}
