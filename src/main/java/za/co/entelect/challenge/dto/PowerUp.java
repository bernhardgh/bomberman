package za.co.entelect.challenge.dto;

import com.google.gson.annotations.SerializedName;

public class PowerUp
{
    @SerializedName("$type")
    private String powerUpType;
    
    public PowerUp() {}
    
    public PowerUp(String powerUpType)
    {
        this.powerUpType = powerUpType;
    }
    
    public String getPowerUpType()
    {
        return powerUpType;
    }
}
