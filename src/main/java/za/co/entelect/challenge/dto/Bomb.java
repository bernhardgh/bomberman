package za.co.entelect.challenge.dto;

import com.google.gson.annotations.SerializedName;

public class Bomb
{
    @SerializedName("Owner")
    private BombOwner owner;
    @SerializedName("BombRadius")
    private int bombRadius;
    @SerializedName("BombTimer")
    private int bombTimer;
    @SerializedName("IsExploding")
    private boolean isExploding;
    
    public BombOwner getOwner()
    {
        return owner;
    }
    
    public void setOwner(BombOwner owner)
    {
        this.owner = owner;
    }
    
    public int getBombRadius()
    {
        return bombRadius;
    }
    
    public void setBombRadius(int bombRadius)
    {
        this.bombRadius = bombRadius;
    }
    
    public int getBombTimer()
    {
        return bombTimer;
    }
    
    public void setBombTimer(int bombTimer)
    {
        this.bombTimer = bombTimer;
    }
    
    public boolean isExploding()
    {
        return isExploding;
    }
    
    public void setIsExploding(boolean flag)
    {
        isExploding = flag;
    }
}
