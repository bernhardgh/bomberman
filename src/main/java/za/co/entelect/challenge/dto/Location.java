package za.co.entelect.challenge.dto;

import com.google.gson.annotations.SerializedName;

public class Location
{
    @SerializedName("X")
    private int x;
    @SerializedName("Y")
    private int y;
    
    public Location() {}
    
    public Location(int x, int y)
    {
        this.x = x;
        this.y = y;
    }
    
    public int getX()
    {
        return x;
    }
    
    public int getY()
    {
        return y;
    }
}
