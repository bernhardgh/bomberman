package za.co.entelect.challenge.dto;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class GameState
{
    @SerializedName("MapHeight")
    private int mapHeight;
    @SerializedName("MapWidth")
    private int mapWidth;
    @SerializedName("CurrentRound")
    private int currentRound;
    @SerializedName("PlayerBounty")
    private int playerKillPoints;
    @SerializedName("RegisteredPlayerEntities")
    private List<Player> players;
    @SerializedName("GameBlocks")
    private List<List<Block>> blocks;

    public int getMapHeight()
    {
        return mapHeight;
    }
    
    public int getMapWidth()
    {
        return mapWidth;
    }
    
    public void setMapSize(int height, int width)
    {
        mapHeight = height;
        mapWidth = width;
    }
    
    public int getCurrentRound()
    {
        return currentRound;
    }
    
    public void setCurrentRound(int round)
    {
        currentRound = round;
    }
    
    public int getPlayerKillPoints()
    {
        return playerKillPoints;
    }
    
    public void setPlayerKillPoints(int playerKillPoints)
    {
        this.playerKillPoints = playerKillPoints;
    }
    
    public List<Player> getPlayers()
    {
        return players;
    }
    
    public void setPlayers(List<Player> players)
    {
        this.players = players;
    }
    
    public List<List<Block>> getBlocks()
    {
        return blocks;
    }
    
    public void setBlocks(List<List<Block>> blocks)
    {
        this.blocks = blocks;
    }
}
