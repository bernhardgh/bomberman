package za.co.entelect.challenge.dto;

import com.google.gson.annotations.SerializedName;

public class Block
{
    @SerializedName("Entity")
    private Entity entity;
    @SerializedName("Bomb")
    private Bomb bomb;
    @SerializedName("PowerUp")
    private PowerUp powerUp;
    @SerializedName("Exploding")
    private boolean isExploding;
    @SerializedName("Location")
    private Location location;
    
    public Entity getEntity()
    {
        return entity;
    }
    
    public void setEntity(Entity entity)
    {
        this.entity = entity;
    }
    
    public Bomb getBomb()
    {
        return bomb;
    }
    
    public void setBomb(Bomb bomb)
    {
        this.bomb = bomb;
    }
    
    public PowerUp getPowerUp()
    {
        return powerUp;
    }
    
    public void setPowerUp(PowerUp powerUp)
    {
        this.powerUp = powerUp;
    }
    
    public boolean isExploding()
    {
        return isExploding;
    }
    
    public void setIsExploding(boolean isExploding)
    {
        this.isExploding = isExploding;
    }
    
    public Location getLocation()
    {
        return location;
    }
    
    public void setLocation(Location location)
    {
        this.location = location;
    }
}
