package za.co.entelect.challenge.dto.reader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import com.google.gson.Gson;

import za.co.entelect.challenge.dto.BombermanStateBuilder;
import za.co.entelect.challenge.dto.GameState;
import za.co.entelect.challenge.model.bomberman.BombermanCell;
import za.co.entelect.challenge.model.bomberman.BombermanState;

public class GsonGameStateReader implements GameStateReader<BombermanCell>
{
    @Override
    public BombermanState read(InputStream jsonInputStream, InputStream coverageStream) throws IOException 
    {
        GameState gameState;
        try (InputStreamReader reader = new InputStreamReader(jsonInputStream)) 
        {
            gameState = (new Gson()).fromJson(reader, GameState.class);
        }
        return (new BombermanStateBuilder(gameState, coverageStream)).getBombermanState();
    }
}
