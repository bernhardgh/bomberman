package za.co.entelect.challenge.dto;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import za.co.entelect.challenge.model.bomberman.BombermanCell;
import za.co.entelect.challenge.model.bomberman.BombermanGrid;
import za.co.entelect.challenge.model.bomberman.BombermanPlayer;
import za.co.entelect.challenge.model.bomberman.BombermanState;

public class Coverage
{
    private BombermanState gameState;
    
    public Coverage(BombermanState gameState)
    {
        this.gameState = gameState;
    }
    
    public void saveCoverage(OutputStream stream) throws IOException
    {
        BombermanGrid grid = gameState.getGrid();
        int height = grid.getHeight(), width = grid.getWidth();
        for (int row = 0; row < height; row++)
            for (int column = 0; column < width; column++)
            {
                BombermanCell cell = grid.getCell(row, column);
                
                int visitedBits = 0;
                for (char next : cell.getPlayersVisited())
                    visitedBits |= (1 << ((int)next - (int)'A'));
                
                stream.write(visitedBits >>> 8);
                stream.write(visitedBits);
            }
    }
    
    public void loadCoverage(InputStream stream) throws IOException
    {
        BombermanPlayer[] players = new BombermanPlayer[16];
        for (BombermanPlayer nextPlayer : gameState.getPlayers())
            players[nextPlayer.getPlayerKey() - 'A'] = nextPlayer;
        
        BombermanGrid grid = gameState.getGrid();
        int height = grid.getHeight(), width = grid.getWidth();
        for (int row = 0; row < height; row++)
            for (int column = 0; column < width; column++)
            {
                List<Character> playersVisited = grid.getCell(row, column).getPlayersVisited();
                
                int visitedBits = stream.read() << 8;
                visitedBits |= stream.read();
                
                for (int currentBitPosition = 0; visitedBits > 0; visitedBits >>>= 1, currentBitPosition++)
                    if ((visitedBits & 1) == 1)
                    {
                        playersVisited.add((char)('A' + currentBitPosition));
                        if (players[currentBitPosition] != null)
                            players[currentBitPosition].incrementGridCoverage();
                    }
            }
    }
}
