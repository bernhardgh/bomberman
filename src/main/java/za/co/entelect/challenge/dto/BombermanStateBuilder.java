package za.co.entelect.challenge.dto;

import java.io.IOException;
import java.io.InputStream;
import java.util.Comparator;
import java.util.List;

import za.co.entelect.challenge.model.bomberman.BombermanCell;
import za.co.entelect.challenge.model.bomberman.BombermanGrid;
import za.co.entelect.challenge.model.bomberman.BombermanPlayer;
import za.co.entelect.challenge.model.bomberman.BombermanState;
import za.co.entelect.challenge.model.bomberman.BombermanCell.CellType;

public class BombermanStateBuilder
{
    private GameState dtoGameState;
    private InputStream coverageStream;
    
    public BombermanStateBuilder(GameState gameState, InputStream coverageStream)
    {
        dtoGameState = gameState;
        this.coverageStream = coverageStream;
    }
    
    public BombermanState getBombermanState() throws IOException
    {
        BombermanGrid grid = new BombermanGrid(dtoGameState.getMapHeight(), dtoGameState.getMapWidth());
        BombermanPlayer[] players = getPlayers(dtoGameState.getPlayers(), grid.getUsableCellCount());
        
        BombermanState gameState = new BombermanState(grid, players);
        gameState.setGameRound(dtoGameState.getCurrentRound() - 1);
        gameState.setPlayerKillPoints(dtoGameState.getPlayerKillPoints());

        if (coverageStream != null)
            (new Coverage(gameState)).loadCoverage(coverageStream);
        
        populateGrid(dtoGameState.getBlocks(), grid, players);
        
        for (BombermanPlayer nextPlayer : players)
        {
            Player dtoPlayer = dtoGameState.getPlayers().stream().filter(p -> p.getKey() == nextPlayer.getPlayerKey()).findFirst().get();
            nextPlayer.setScore(dtoPlayer.getPoints());
            int bombBagSize = dtoPlayer.getBombBag();
            nextPlayer.setBombCounts(bombBagSize - nextPlayer.getCellsWithBombs().size(), bombBagSize);
            
            nextPlayer.getCellsWithBombs().sort(new BombCountdownComparator());
        }
        
        return gameState;
    }
    
    private BombermanPlayer[] getPlayers(List<Player> dtoPlayers, int usableCellCount)
    {
        BombermanPlayer[] players = new BombermanPlayer[dtoPlayers.size()];
        int playerIndex = 0;
        for (Player nextPlayer : dtoPlayers)
            players[playerIndex++] = toPlayer(nextPlayer, usableCellCount);
        return players;
    }
    
    private BombermanPlayer toPlayer(Player dtoPlayer, int usableCellCount)
    {
        BombermanPlayer player = new BombermanPlayer(dtoPlayer.getKey());
        player.setUsableCellCount(usableCellCount);
        player.setIsAlive(!dtoPlayer.isKilled());
        player.setBombRadius(dtoPlayer.getBombRadius());
        return player;
    }
    
    private void populateGrid(List<List<Block>> blocks, BombermanGrid grid, BombermanPlayer[] players)
    {
        for (List<Block> nextRow : blocks)
            for (Block nextBlock : nextRow)
            {
                int row = nextBlock.getLocation().getY() - 1, column = nextBlock.getLocation().getX() - 1;
                
                Entity entity = nextBlock.getEntity();
                if (entity != null)
                {
                    switch (entity.getEntityType())
                    {
                        case "Domain.Entities.PlayerEntity, Domain": {
                            BombermanPlayer player = getPlayerForKey(entity.getKey(), players);
                            grid.getCell(row, column).performCellChange(player);
                            break;
                        }
                        case "Domain.Entities.DestructibleWallEntity, Domain":
                            grid.getCell(row, column).setCell(CellType.DESTRUCTIBLE_WALL); break;
                    }
                }
                
                Bomb bomb = nextBlock.getBomb();
                if (bomb != null)
                {
                    BombermanPlayer player = getPlayerForKey(bomb.getOwner().getKey(), players);
                    BombermanCell cell = grid.getCell(row, column);
                    cell.setBomb(bomb.getBombTimer(), bomb.getBombRadius(), player);
                    player.getCellsWithBombs().add(cell);
                }
                
                PowerUp powerUp = nextBlock.getPowerUp();
                if (powerUp != null)
                {
                    CellType cellType;
                    switch (powerUp.getPowerUpType())
                    {
                        case "Domain.Entities.PowerUps.BombBagPowerUpEntity, Domain": cellType = CellType.BOMB_BAG_POWER_UP; break;
                        case "Domain.Entities.PowerUps.BombRaduisPowerUpEntity, Domain": cellType = CellType.BOMB_RADIUS_POWER_UP; break;
                        case "Domain.Entities.PowerUps.SuperPowerUp, Domain": cellType = CellType.SUPER_POWER_UP; break;
                        default: throw new RuntimeException("Unknown power-up type: " + powerUp.getPowerUpType());
                    }
                    grid.getCell(row, column).setCell(cellType);
                }
            }
    }
    
    private static BombermanPlayer getPlayerForKey(char key, BombermanPlayer[] players)
    {
        for (BombermanPlayer nextPlayer : players)
            if (nextPlayer.getPlayerKey() == key)
                return nextPlayer;
        
        throw new RuntimeException("Player with key " + key + " not found!");
    }
    
    
    private static class BombCountdownComparator implements Comparator<BombermanCell>
    {
        @Override
        public int compare(BombermanCell cell1, BombermanCell cell2)
        {
            return cell1.getBombCountdown() - cell2.getBombCountdown();
        }
    }
}
