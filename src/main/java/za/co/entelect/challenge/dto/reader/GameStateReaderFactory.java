package za.co.entelect.challenge.dto.reader;

import za.co.entelect.challenge.model.bomberman.BombermanCell;

public class GameStateReaderFactory
{
    public static GameStateReader<BombermanCell> getDefaultGameStateReader()
    {
        return new JsonSimpleGameStateReader();
    }
}
