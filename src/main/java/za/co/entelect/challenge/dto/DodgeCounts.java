package za.co.entelect.challenge.dto;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import za.co.entelect.challenge.model.bomberman.BombermanPlayer;

public class DodgeCounts
{
    private static final int MAX_PLAYER_COUNT = 12;
    
    private BombermanPlayer[] players;
    private byte[] counts = new byte[MAX_PLAYER_COUNT];
    
    public DodgeCounts(BombermanPlayer... players)
    {
        this.players = players;
    }
    
    public void saveCounts(OutputStream stream) throws IOException
    {
        for (int i = 0; i < counts.length; i++)
            counts[i] = 0;
        for (BombermanPlayer nextPlayer : players)
            counts[nextPlayer.getPlayerKey() - 'A'] = (byte)nextPlayer.getDodgeCount();
        
        stream.write(counts);
    }
    
    public void loadCounts(InputStream stream) throws IOException
    {
        stream.read(counts);
        
        for (BombermanPlayer nextPlayer : players)
            nextPlayer.setDodgeCount(counts[nextPlayer.getPlayerKey() - 'A']);
    }
}
