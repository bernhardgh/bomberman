package za.co.entelect.challenge.dto.reader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import za.co.entelect.challenge.dto.Block;
import za.co.entelect.challenge.dto.Bomb;
import za.co.entelect.challenge.dto.BombOwner;
import za.co.entelect.challenge.dto.BombermanStateBuilder;
import za.co.entelect.challenge.dto.Entity;
import za.co.entelect.challenge.dto.GameState;
import za.co.entelect.challenge.dto.Location;
import za.co.entelect.challenge.dto.Player;
import za.co.entelect.challenge.dto.PowerUp;
import za.co.entelect.challenge.model.bomberman.BombermanCell;
import za.co.entelect.challenge.model.bomberman.BombermanState;

public class JsonSimpleGameStateReader implements GameStateReader<BombermanCell>
{
    @Override
    public BombermanState read(InputStream jsonInputStream, InputStream coverageStream) throws IOException
    {
        GameState gameState = null;
        JSONParser parser = new JSONParser();
        try (InputStreamReader reader = new InputStreamReader(jsonInputStream))
        {
            JSONObject object = (JSONObject)parser.parse(reader);
            gameState = parseGameState(object);
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }
        return (new BombermanStateBuilder(gameState, coverageStream)).getBombermanState();
    }
    
    private static GameState parseGameState(JSONObject object)
    {
        GameState gameState = new GameState();
        gameState.setMapSize((int)(long)object.get("MapHeight"), (int)(long)object.get("MapWidth"));
        gameState.setCurrentRound((int)(long)object.get("CurrentRound"));
        gameState.setPlayerKillPoints((int)(long)object.get("PlayerBounty"));
        gameState.setPlayers(getPlayers(object));
        gameState.setBlocks(getBlocks(object, gameState.getMapHeight(), gameState.getMapWidth()));
        return gameState;
    }
    
    private static List<Player> getPlayers(JSONObject object)
    {
        List<Player> players = new ArrayList<Player>();
        for (Object nextPlayer : (JSONArray)object.get("RegisteredPlayerEntities"))
        {
            Player player = new Player();
            JSONObject playerObject = (JSONObject)nextPlayer;
            player.setKey(playerObject.get("Key").toString().charAt(0));
            player.setPoints((int)(long)playerObject.get("Points"));
            player.setIsKilled((boolean)playerObject.get("Killed"));
            player.setBombBag((int)(long)playerObject.get("BombBag"));
            player.setBombRadius((int)(long)playerObject.get("BombRadius"));
            players.add(player);
        }
        return players;
    }
    
    private static List<List<Block>> getBlocks(JSONObject object, int height, int width)
    {
        List<List<Block>> columnsList = new ArrayList<List<Block>>(width);
        for (Object nextColumn : (JSONArray)object.get("GameBlocks"))
        {
            List<Block> blocks = new ArrayList<Block>(height);
            for (Object nextBlock : (JSONArray)nextColumn)
            {
                Block block = new Block();
                JSONObject blockObject = (JSONObject)nextBlock;
                
                Object entityObject = blockObject.get("Entity");
                if (entityObject != null)
                    block.setEntity(getEntity((JSONObject)entityObject));
                
                Object bombObject = blockObject.get("Bomb");
                if (bombObject != null)
                    block.setBomb(getBomb((JSONObject)bombObject));
                
                Object powerUpObject = blockObject.get("PowerUp");
                if (powerUpObject != null)
                    block.setPowerUp(new PowerUp(((JSONObject)powerUpObject).get("$type").toString()));
                
                block.setIsExploding((boolean)blockObject.get("Exploding"));
                
                Object locationObject = blockObject.get("Location");
                if (locationObject != null)
                    block.setLocation(getLocation((JSONObject)locationObject));
                
                blocks.add(block);
            }
            columnsList.add(blocks);
        }
        return columnsList;
    }
    
    private static Entity getEntity(JSONObject object)
    {
        Entity entity = new Entity();
        entity.setEntityType(object.get("$type").toString());
        Object key = object.get("Key");
        if (key != null)
            entity.setKey(key.toString().charAt(0));
        return entity;
    }
    
    private static Bomb getBomb(JSONObject object)
    {
        Bomb bomb = new Bomb();
        bomb.setOwner(new BombOwner(((JSONObject)object.get("Owner")).get("Key").toString().charAt(0)));
        bomb.setBombRadius((int)(long)object.get("BombRadius"));
        bomb.setBombTimer((int)(long)object.get("BombTimer"));
        bomb.setIsExploding((boolean)object.get("IsExploding"));
        return bomb;
    }
    
    private static Location getLocation(JSONObject object)
    {
        return new Location((int)(long)object.get("X"), (int)(long)object.get("Y"));
    }
}
