package za.co.entelect.challenge.dto;

import com.google.gson.annotations.SerializedName;

public class Entity
{
    @SerializedName("$type")
    private String entityType;
    @SerializedName("Key")
    private char key;
    
    public String getEntityType()
    {
        return entityType;
    }
    
    public void setEntityType(String entityType)
    {
        this.entityType = entityType;
    }
    
    public char getKey()
    {
        return key;
    }
    
    public void setKey(char key)
    {
        this.key = key;
    }
}
