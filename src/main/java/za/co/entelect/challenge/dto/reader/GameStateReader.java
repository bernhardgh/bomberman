package za.co.entelect.challenge.dto.reader;

import java.io.IOException;
import java.io.InputStream;

import za.co.entelect.challenge.model.Cell;

public interface GameStateReader<CellType extends Cell>
{
    public za.co.entelect.challenge.model.GameState<CellType> read(InputStream jsonInputStream, InputStream coverageStream) throws IOException;
}
