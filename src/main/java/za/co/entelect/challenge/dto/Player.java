package za.co.entelect.challenge.dto;

import com.google.gson.annotations.SerializedName;

public class Player
{
    @SerializedName("Key")
    private char key;
    @SerializedName("Points")
    private int points;
    @SerializedName("Killed")
    private boolean isKilled;
    @SerializedName("BombBag")
    private int bombBag;
    @SerializedName("BombRadius")
    private int bombRadius;
    
    public char getKey()
    {
        return key;
    }
    
    public void setKey(char key)
    {
        this.key = key;
    }
    
    public int getPoints()
    {
        return points;
    }
    
    public void setPoints(int points)
    {
        this.points = points;
    }
    
    public boolean isKilled()
    {
        return isKilled;
    }
    
    public void setIsKilled(boolean flag)
    {
        isKilled = flag;
    }
    
    public int getBombBag()
    {
        return bombBag;
    }
    
    public void setBombBag(int bombBag)
    {
        this.bombBag = bombBag;
    }
    
    public int getBombRadius()
    {
        return bombRadius;
    }
    
    public void setBombRadius(int bombRadius)
    {
        this.bombRadius = bombRadius;
    }
}
