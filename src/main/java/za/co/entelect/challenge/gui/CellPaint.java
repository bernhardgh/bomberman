package za.co.entelect.challenge.gui;

import za.co.entelect.challenge.model.Cell;
import javafx.scene.paint.Paint;

public interface CellPaint<C extends Cell>
{
    public Paint getClearPaint();
    
    public Paint getPaintForCell(C cell);
}
