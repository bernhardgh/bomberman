package za.co.entelect.challenge.gui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.URL;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.ResourceBundle;

import za.co.entelect.challenge.dto.reader.GameStateReaderFactory;
import za.co.entelect.challenge.gui.MessageBox.Buttons;
import za.co.entelect.challenge.model.GameCoordinator;
import za.co.entelect.challenge.model.GamePlayer;
import za.co.entelect.challenge.model.Player;
import za.co.entelect.challenge.model.bomberman.BombermanCell;
import za.co.entelect.challenge.model.bomberman.BombermanCell.CellType;
import za.co.entelect.challenge.model.bomberman.BombermanEngine;
import za.co.entelect.challenge.model.bomberman.BombermanGrid;
import za.co.entelect.challenge.model.bomberman.BombermanGridGenerator;
import za.co.entelect.challenge.model.bomberman.BombermanPlayer;
import za.co.entelect.challenge.model.bomberman.BombermanState;
import za.co.entelect.challenge.model.bomberman.moveinfo.BombermanMoveInfo;
import za.co.entelect.challenge.model.bomberman.moveinfo.MoveEvent;
import za.co.entelect.challenge.model.bomberman.players.BombermanPlayerFactory;
import za.co.entelect.challenge.model.bomberman.players.HumanBombermanPlayer;
import za.co.entelect.challenge.model.bomberman.players.InstantKillBombermanPlayer;
import za.co.entelect.challenge.model.bomberman.players.ReplayBombermanPlayer;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.stage.WindowEvent;

public class BombermanController implements Initializable
{
    @FXML private Pane canvasPane;
    private GridCanvas<BombermanCell> gridCanvas;
    private BombermanState gameState;
    private BombermanEngine gameEngine = new BombermanEngine();
    
    @FXML private Pane controlPane;
    @FXML private TextField playersCountField;
    @FXML private Button playersCountRefreshButton;
    @FXML private TitledPane player1TypePane;
    @FXML private TitledPane player2TypePane;
    @FXML private TitledPane player3TypePane;
    @FXML private TitledPane player4TypePane;
    @FXML private TitledPane gameSpeedPane;
    @FXML private ComboBox<String> player1SelectorBox;
    @FXML private ComboBox<String> player2SelectorBox;
    @FXML private ComboBox<String> player3SelectorBox;
    @FXML private ComboBox<String> player4SelectorBox;
    @FXML private TextField player1TimeLimitField;
    @FXML private TextField player2TimeLimitField;
    @FXML private TextField player3TimeLimitField;
    @FXML private TextField player4TimeLimitField;
    @FXML private Label player1ScoreLabel;
    @FXML private Label player1BagLabel;
    @FXML private Label player1RadiusLabel;
    @FXML private Label player2ScoreLabel;
    @FXML private Label player2BagLabel;
    @FXML private Label player2RadiusLabel;
    @FXML private Label player3ScoreLabel;
    @FXML private Label player3BagLabel;
    @FXML private Label player3RadiusLabel;
    @FXML private Label player4ScoreLabel;
    @FXML private Label player4BagLabel;
    @FXML private Label player4RadiusLabel;
    @FXML private Label gameRoundLabel;
    @FXML private Button resetButton;
    @FXML private Button startStopButton;
    @FXML private Button stepForwardButton;
    @FXML private Button stepBackButton;
    @FXML private Button openFileButton;
    private FileChooser openFileChooser = new FileChooser(), saveFileChooser = new FileChooser();
    private ArrayList<EventHandler<KeyEvent>> keyEventHandlers = new ArrayList<EventHandler<KeyEvent>>();

    private Thread gameThread = null;
    private GamePlayer<BombermanCell> player1 = null, player2 = null, player3 = null, player4 = null;
    private EventHandler<KeyEvent> player1EventHandler = null, player2EventHandler = null, player3EventHandler = null, player4EventHandler = null;
    private int playerCount = 2;
    private GameCoordinator<BombermanCell> gameCoordinator;
    
    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        assert canvasPane != null : "fx:id=\"canvasPane\" was not injected: check your FXML file 'BombermanFrontend.fxml'!";
        
        Platform.runLater(new Runnable() {
           public void run()
           {
               canvasPane.getScene().getWindow().setOnCloseRequest(new EventHandler<WindowEvent>()
               {
                    public void handle(WindowEvent event)
                    {
                        if (gameThread != null)
                        {
                            gameThread.interrupt();
                            try
                            {
                                while (gameThread.isAlive())
                                    Thread.sleep(100);
                            }
                            catch (InterruptedException e) {}
                            gameThread = null;
                        }
                    }
               });
           }
        });
        
        player1SelectorBox.getItems().clear();
        player2SelectorBox.getItems().clear();
        player3SelectorBox.getItems().clear();
        player4SelectorBox.getItems().clear();
        for (BombermanPlayerFactory nextFactory : BombermanPlayerFactory.getPlayerFactories())
            if (nextFactory.shouldDisplayOnGui())
            {
                player1SelectorBox.getItems().add(nextFactory.getName());
                player2SelectorBox.getItems().add(nextFactory.getName());
                player3SelectorBox.getItems().add(nextFactory.getName());
                player4SelectorBox.getItems().add(nextFactory.getName());
            }
        player1SelectorBox.setValue(player1SelectorBox.getItems().get(0));
        player2SelectorBox.setValue(player1SelectorBox.getItems().get(0));
        player3SelectorBox.setValue(player1SelectorBox.getItems().get(0));
        player4SelectorBox.setValue(player1SelectorBox.getItems().get(0));
        
        gridCanvas = new GridCanvas<BombermanCell>(new BombermanCellPaint());
        gridCanvas.widthProperty().bind(canvasPane.widthProperty());
        gridCanvas.heightProperty().bind(canvasPane.heightProperty());
        canvasPane.getChildren().add(gridCanvas);
        
        resetBoard(null);
    }

    public void refreshPlayerCount(ActionEvent event)
    {
        try
        {
            int newPlayerCount = Integer.parseInt(playersCountField.getText());
            if ((newPlayerCount < 2) || (newPlayerCount > 4))
            {
                (new MessageBox("Please enter an integer between 2 and 4.", "Players", Buttons.OK)).showAndWait();
                return;
            }
            playerCount = newPlayerCount;
        }
        catch (NumberFormatException e)
        {
            (new MessageBox("Please enter an integer.", "Players", Buttons.OK)).showAndWait();
            return;
        }
                
        resetBoard(null);
    }
    
    public void resetBoard(ActionEvent event)
    {
        BombermanPlayer[] players = new BombermanPlayer[playerCount];
        for (int i = 0; i < playerCount; i++)
            players[i] = new BombermanPlayer((char)('A' + i));
        
        BombermanGridGenerator generator = new BombermanGridGenerator(21, 21, players);
        BombermanGrid grid = generator.getBombermanGrid();
        gameState = new BombermanState(grid, players);
        gameState.setPlayerKillPoints(generator.getPlayerKillPoints());
        gameState.initializeCodeIntegration();
        
        resetGame();
    }
    
    private void resetGame()
    {
        player3TypePane.setDisable(playerCount < 3);
        player4TypePane.setDisable(playerCount < 4);
        stepBackButton.setDisable(true);
        stepForwardButton.setDisable(false);
        startStopButton.setDisable(false);
        gridCanvas.setGrid(gameState.getGrid());
        player1 = player2 = player3 = player4 = null;
        gameCoordinator = new GameCoordinator<BombermanCell>(gameEngine, gameState, getPlayers());
        updateStatsLabels();
        
        InstantKillBombermanPlayer.resetDodgeCountsFile();
    }

    private void updateStatsLabels()
    {
        BombermanPlayer player = gameState.getPlayers()[0];
        player1ScoreLabel.setText("Score: " + player.getScore());
        player1BagLabel.setText("Bag: " + player.getAvailableBombCount());
        player1RadiusLabel.setText("Radius: " + player.getBombRadius());
        player = gameState.getPlayers()[1];
        player2ScoreLabel.setText("Score: " + player.getScore());
        player2BagLabel.setText("Bag: " + player.getAvailableBombCount());
        player2RadiusLabel.setText("Radius: " + player.getBombRadius());
        if (gameState.getPlayers().length > 2)
        {
            player = gameState.getPlayers()[2];
            player3ScoreLabel.setText("Score: " + player.getScore());
            player3BagLabel.setText("Bag: " + player.getAvailableBombCount());
            player3RadiusLabel.setText("Radius: " + player.getBombRadius());
        }
        if (gameState.getPlayers().length > 3)
        {
            player = gameState.getPlayers()[3];
            player4ScoreLabel.setText("Score: " + player.getScore());
            player4BagLabel.setText("Bag: " + player.getAvailableBombCount());
            player4RadiusLabel.setText("Radius: " + player.getBombRadius());
        }
        
        gameRoundLabel.setText("Round: " + gameState.getGameRound());
    }

    private GamePlayer<BombermanCell>[] getPlayers()
    {
        @SuppressWarnings("unchecked")
        GamePlayer<BombermanCell>[] players = new GamePlayer[playerCount];
        for (int i = 0; i < playerCount; i++)
        {
            GamePlayer<BombermanCell> player;
            switch (i)
            {
                case 0: player = getPlayer1(); break;
                case 1: player = getPlayer2(); break;
                case 2: player = getPlayer3(); break;
                default: player = getPlayer4(); break;
            }
            players[i] = player;
        }
        return players;
    }
    
    private GamePlayer<BombermanCell> getPlayer1()
    {
        if (player1EventHandler != null)
        {
            keyEventHandlers.remove(player1EventHandler);
            player1EventHandler = null;
        }
        player1 = getGamePlayer(gameState.getPlayers()[0], player1SelectorBox.getValue());
        if (player1 instanceof HumanBombermanPlayer)
        {
            HumanBombermanPlayer humanPlayer = (HumanBombermanPlayer) player1;
            keyEventHandlers.add(player1EventHandler = new HumanBombermanPlayerKeyEventHandler(humanPlayer));
        }
        return player1;
    }
    
    private GamePlayer<BombermanCell> getPlayer2()
    {
        if (player2EventHandler != null)
        {
            keyEventHandlers.remove(player2EventHandler);
            player2EventHandler = null;
        }
        player2 = getGamePlayer(gameState.getPlayers()[1], player2SelectorBox.getValue());
        if (player2 instanceof HumanBombermanPlayer)
        {
            HumanBombermanPlayer humanPlayer = (HumanBombermanPlayer) player2;
            keyEventHandlers.add(player2EventHandler = new HumanBombermanPlayerKeyEventHandler(humanPlayer));
        }
        return player2;
    }
    
    private GamePlayer<BombermanCell> getPlayer3()
    {
        if (player3EventHandler != null)
        {
            keyEventHandlers.remove(player3EventHandler);
            player3EventHandler = null;
        }
        player3 = getGamePlayer(gameState.getPlayers()[2], player3SelectorBox.getValue());
        if (player3 instanceof HumanBombermanPlayer)
        {
            HumanBombermanPlayer humanPlayer = (HumanBombermanPlayer) player3;
            keyEventHandlers.add(player3EventHandler = new HumanBombermanPlayerKeyEventHandler(humanPlayer));
        }
        return player3;
    }
    
    private GamePlayer<BombermanCell> getPlayer4()
    {
        if (player4EventHandler != null)
        {
            keyEventHandlers.remove(player4EventHandler);
            player4EventHandler = null;
        }
        player4 = getGamePlayer(gameState.getPlayers()[3], player4SelectorBox.getValue());
        if (player4 instanceof HumanBombermanPlayer)
        {
            HumanBombermanPlayer humanPlayer = (HumanBombermanPlayer) player4;
            keyEventHandlers.add(player4EventHandler = new HumanBombermanPlayerKeyEventHandler(humanPlayer));
        }
        return player4;
    }
    
    private GamePlayer<BombermanCell> getGamePlayer(Player player, String selectedPlayerType)
    {
        long timeLimit = -1;
        if (!selectedPlayerType.equals("Human"))
        {
            switch (player.getPlayerKey())
            {
                case 'A' : timeLimit = (long)(Double.parseDouble(player1TimeLimitField.getText()) * 1000); break;
                case 'B' : timeLimit = (long)(Double.parseDouble(player2TimeLimitField.getText()) * 1000); break;
                case 'C' : timeLimit = (long)(Double.parseDouble(player3TimeLimitField.getText()) * 1000); break;
                case 'D' : timeLimit = (long)(Double.parseDouble(player4TimeLimitField.getText()) * 1000); break;
            }
        }
        
        for (BombermanPlayerFactory nextFactory : BombermanPlayerFactory.getPlayerFactories())
            if (nextFactory.getName().equals(selectedPlayerType))
                return nextFactory.createPlayer(player, timeLimit);
        return null;
    }

    private void disableControlsForMove()
    {
        playersCountField.setDisable(true);
        playersCountRefreshButton.setDisable(true);
        player1TypePane.setDisable(true);
        player2TypePane.setDisable(true);
        player3TypePane.setDisable(true);
        player4TypePane.setDisable(true);
        resetButton.setDisable(true);
        stepBackButton.setDisable(true);
        stepForwardButton.setDisable(true);
        openFileButton.setDisable(true);
    }
    
    private void enableControlsAfterMove()
    {
        playersCountField.setDisable(false);
        playersCountRefreshButton.setDisable(false);
        player1TypePane.setDisable(false);
        player2TypePane.setDisable(false);
        player3TypePane.setDisable(playerCount < 3);
        player4TypePane.setDisable(playerCount < 4);
        resetButton.setDisable(false);
        openFileButton.setDisable(false);
        stepBackButton.setDisable(!gameCoordinator.canUndoMove());
        stepForwardButton.setDisable(false);
    }
    
    private void setGameEnded()
    {
        gameThread = null;
        enableControlsAfterMove();
        startStopButton.setText("Start");
        gridCanvas.redraw();
        updateStatsLabels();
    }
    
    public void playerSelectionChanged(ActionEvent event)
    {
        if (event.getSource() == player1SelectorBox)
        {
            boolean hasHumanPlayerSelected = ((player1SelectorBox.getValue() != null) && (player1SelectorBox.getValue().equals("Human")));
            player1TimeLimitField.setDisable(hasHumanPlayerSelected);
        }
        else if (event.getSource() == player2SelectorBox)
        {
            boolean hasHumanPlayerSelected = ((player2SelectorBox.getValue() != null) && (player2SelectorBox.getValue().equals("Human")));
            player2TimeLimitField.setDisable(hasHumanPlayerSelected);
        }
        else if (event.getSource() == player3SelectorBox)
        {
            boolean hasHumanPlayerSelected = ((player3SelectorBox.getValue() != null) && (player3SelectorBox.getValue().equals("Human")));
            player3TimeLimitField.setDisable(hasHumanPlayerSelected);
        }
        else
        {
            boolean hasHumanPlayerSelected = ((player4SelectorBox.getValue() != null) && (player4SelectorBox.getValue().equals("Human")));
            player4TimeLimitField.setDisable(hasHumanPlayerSelected);
        }
    }
    
    public void startStop(ActionEvent event)
    {
        if (gameThread == null)
        {
            try
            {
                gameThread = new GameThread(getPlayers());
                gameThread.start();
                
                disableControlsForMove();
                startStopButton.setText("Stop");
            }
            catch (NumberFormatException e)
            {
                (new MessageBox(canvasPane.getScene().getWindow(), "Please enter a number for the time limit!", "Start")).showAndWait();
            }
        }
        else
        {
            gameThread.interrupt();
            try
            {
                while (gameThread.isAlive())
                    Thread.sleep(100);
            }
            catch (InterruptedException e) {}
            setGameEnded();
        }
    }
    
    public void stepForward(ActionEvent event)
    {
        try
        {
            gameCoordinator.setPlayers(getPlayers());
            disableControlsForMove();
            startStopButton.setText("Stop");
            gameThread = new StepThread();
            gameThread.start();
        }
        catch (NumberFormatException e)
        {
            (new MessageBox(canvasPane.getScene().getWindow(), "Please enter a number for the time limit!", "Start")).showAndWait();
        }
    }
    
    public void stepBack(ActionEvent event)
    {
        gameState = (BombermanState)gameCoordinator.undoMove();
        startStopButton.setDisable(false);
        stepForwardButton.setDisable(false);
        stepBackButton.setDisable(!gameCoordinator.canUndoMove());
        gridCanvas.setGrid(gameState.getGrid());
        updateStatsLabels();
    }
    
    public void openFile(ActionEvent event)
    {
        File file = openFileChooser.showOpenDialog(canvasPane.getScene().getWindow());
        if (file != null)
        {
            if (file.getName().toLowerCase().endsWith(".json"))
            {
                try (FileInputStream jsonInputStream = new FileInputStream(file))
                {
                    gameState = (BombermanState)GameStateReaderFactory.getDefaultGameStateReader().read(jsonInputStream, null);
                    gameEngine.advanceRound(gameState);
                    gameState.initializeCodeIntegration();
                }
                catch (IOException e)
                {
                    (new MessageBox(canvasPane.getScene().getWindow(), "Error loading file: "+e.getMessage(), "Open File")).showAndWait();
                    return;
                }
            }
            else
            {
                try (ObjectInputStream stream = new ObjectInputStream(new FileInputStream(file)))
                {
                    gameState = (BombermanState)stream.readObject();
                } 
                catch (IOException e)
                {
                    (new MessageBox(canvasPane.getScene().getWindow(), "Error loading file: "+e.getMessage(), "Open File")).showAndWait();
                    return;
                } 
                catch (ClassNotFoundException e)
                {
                    (new MessageBox(canvasPane.getScene().getWindow(), "Class not found: "+e.getMessage(), "Open File")).showAndWait();
                    return;
                }
            }
            
            playerCount = gameState.getPlayers().length;
            playersCountField.setText(Integer.toString(playerCount));
            resetGame();
        }
    }

    public void saveFile(ActionEvent event)
    {
        File file = saveFileChooser.showSaveDialog(canvasPane.getScene().getWindow());
        if (file != null)
        {
            try (ObjectOutputStream stream = new ObjectOutputStream(new FileOutputStream(file)))
            {
                stream.writeObject(gameState);
            } 
            catch (IOException e)
            {
                (new MessageBox(canvasPane.getScene().getWindow(), "Error saving file: "+e.getMessage(), "Save File")).showAndWait();
            }
        }
    }
    
    public void handleKeyPress(KeyEvent event)
    {
        for (EventHandler<KeyEvent> nextKeyEventHandler : keyEventHandlers)
            nextKeyEventHandler.handle(event);
    }

    
    private class StepThread extends Thread
    {
        public void run()
        {
            try
            {
                gameState = (BombermanState)gameCoordinator.move();
                synchronizeGameState();
                gridCanvas.setGrid(gameState.getGrid());
            }
            catch (InterruptedException e) {}
            finally
            {
                Platform.runLater(new Runnable() {
                    public void run()
                    {
                        enableControlsAfterMove();
                        BombermanPlayer winner = null;
                        for (BombermanPlayer nextPlayer : gameState.getPlayers())
                            if (gameEngine.playerDidWin(gameState, nextPlayer))
                            {
                                winner = nextPlayer;
                                break;
                            }
                        
                        if (winner != null)
                        {
                            String message = "Player " + winner.getPlayerKey() + " won the game!";
                            (new MessageBox(canvasPane.getScene().getWindow(), message, "Game Result")).showAndWait();
                            startStopButton.setDisable(true);
                            stepForwardButton.setDisable(true);
                        }
                        
                        setGameEnded();
                    }
                });
            }
        }
    }
    
    
    private class GameThread extends Thread
    {
        private GamePlayer<BombermanCell>[] players;
        
        public GameThread(GamePlayer<BombermanCell>[] players)
        {
            this.players = players;
        }
        
        public void run()
        {
            gameCoordinator.setPlayers(players);
            boolean hasHumanPlayer = (player1 instanceof HumanBombermanPlayer)
                    || (player2 instanceof HumanBombermanPlayer)
                    || (player3 instanceof HumanBombermanPlayer)
                    || (player4 instanceof HumanBombermanPlayer);
            try
            {
                BombermanPlayer winner;
                GAME_LOOP:
                while (true)
                {
                    for (BombermanPlayer nextPlayer : gameState.getPlayers())
                        if (gameEngine.playerDidWin(gameState, nextPlayer))
                        {
                            winner = nextPlayer;
                            break GAME_LOOP;
                        }

                    long start = System.currentTimeMillis();
                    gameState = (BombermanState)gameCoordinator.move();
                    synchronizeGameState();
                    Platform.runLater(new Runnable() {
                        public void run()
                        {
                            gridCanvas.setGrid(gameState.getGrid());
                            updateStatsLabels();
                        }
                    });
                    
                    if (!hasHumanPlayer)
                    {
                        long duration = System.currentTimeMillis() - start;
                        Thread.sleep(Math.max(0, 800 - duration));
                    }
                }
                
                final String message = "Player " + winner.getPlayerKey() + " won the game!";
                Platform.runLater(new Runnable() {
                    public void run()
                    {
                        (new MessageBox(canvasPane.getScene().getWindow(), message, "Game Result")).showAndWait();
                        setGameEnded();
                        startStopButton.setDisable(true);
                        stepForwardButton.setDisable(true);
                    }
                });
            }
            catch (InterruptedException e) {}
        }
    }
    
    
    private void synchronizeGameState()
    {
        boolean hasReplayPlayer = (player1 instanceof ReplayBombermanPlayer) 
                || (player2 instanceof ReplayBombermanPlayer) 
                || (player3 instanceof ReplayBombermanPlayer) 
                || (player4 instanceof ReplayBombermanPlayer);
        if (hasReplayPlayer)
        {
            BombermanState externalState;
            try (FileInputStream stream = new FileInputStream(Paths.get(ReplayBombermanPlayer.REPLAY_FILE_BASE, String.format(ReplayBombermanPlayer.FILE_NAME_FORMAT, gameState.getGameRound())).toFile()))
            {
                externalState = (BombermanState)GameStateReaderFactory.getDefaultGameStateReader().read(stream, null);
            }
            catch (IOException e)
            {
                return;
            }
            
            BombermanMoveInfo moveInfo = new BombermanMoveInfo(new ArrayList<MoveEvent>());
            for (int row = 0; row < gameState.getGrid().getHeight(); row++)
                for (int column = 0; column < gameState.getGrid().getWidth(); column++)
                {
                    BombermanCell cell = externalState.getGrid().getCell(row, column);
                    if ((cell.getCellType() == CellType.BOMB_BAG_POWER_UP) || (cell.getCellType() == CellType.BOMB_RADIUS_POWER_UP))
                        moveInfo.addMoveEvent(gameState.getGrid().getCell(row, column).performCellChange(cell.getCellType()));
                }
            
            gameCoordinator.addMoveInfo(moveInfo);
        }
    }
}
