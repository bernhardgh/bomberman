package za.co.entelect.challenge.gui;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;

public class MessageBox extends Stage
{
    public static final int BUTTONS_OK = 1,BUTTONS_CANCEL = 1<<1,BUTTONS_YES = 1<<2,BUTTONS_NO = 1<<3;
    public static final int BUTTONS_OK_CANCEL = BUTTONS_OK | BUTTONS_CANCEL;
    public static final int BUTTONS_YES_NO = BUTTONS_YES | BUTTONS_NO;
    public static final int BUTTONS_YES_NO_CANCEL = BUTTONS_YES | BUTTONS_NO | BUTTONS_CANCEL;
    
    public enum Buttons { OK,OK_CANCEL,YES_NO,YES_NO_CANCEL };
    
    public enum DialogResult { OK,CANCEL,YES,NO };
    
    
    private TextField textField;
    private DialogResult dialogResult;
    
    private static int getMask(Buttons buttons)
    {
        switch (buttons)
        {
            case OK : return BUTTONS_OK;
            case OK_CANCEL : return BUTTONS_OK_CANCEL;
            case YES_NO : return BUTTONS_YES_NO;
            case YES_NO_CANCEL : return BUTTONS_YES_NO_CANCEL;
        }
        return 0;
    }
    
    public MessageBox(String message,String title,Buttons buttons)
    {
        this(null,message,title,buttons);
    }
    
    public MessageBox(Window ownerWindow,String message,String title)
    {
        this(ownerWindow,message,title,BUTTONS_OK);
    }
    
    public MessageBox(Window ownerWindow,String message,String title,Buttons buttons)
    {
        this(ownerWindow,message,title,getMask(buttons));
    }
    
    public MessageBox(Window ownerWindow,String message,String title,int buttons)
    {
        this(ownerWindow,message,title,buttons,false);
    }
    
    public MessageBox(Window ownerWindow,String message,String title,final int buttons,boolean showPromptTextBox)
    {
        setTitle(title);
        if (ownerWindow!=null)
            initOwner(ownerWindow);
        initModality(Modality.WINDOW_MODAL);
        initStyle(StageStyle.UTILITY);
        setResizable(false);
        
        Label messageLabel = new Label(message);
        VBox mainBox = new VBox(5);
        mainBox.setAlignment(Pos.CENTER);
        mainBox.getChildren().add(messageLabel);
        
        if (showPromptTextBox)
        {
            textField = new TextField();
            textField.setOnKeyPressed(new EventHandler<KeyEvent>()
                {
                    public void handle(KeyEvent event)
                    {
                        if (event.getCode()==KeyCode.ENTER)
                        {
                            dialogResult = (buttons & BUTTONS_OK)==0 ? ((buttons & BUTTONS_YES)==0 ? DialogResult.OK : DialogResult.YES) : DialogResult.OK;
                            close();
                        }
                    }
                });
            mainBox.getChildren().add(textField);
        }
        
        HBox buttonBox = new HBox(5);
        buttonBox.setAlignment(Pos.CENTER);
        if ((buttons & BUTTONS_YES)!=0)
            addButton("Yes",DialogResult.YES,buttonBox);
        if ((buttons & BUTTONS_NO)!=0)
            addButton("No",DialogResult.NO,buttonBox);
        if ((buttons & BUTTONS_OK)!=0)
            addButton("OK",DialogResult.OK,buttonBox);
        if ((buttons & BUTTONS_CANCEL)!=0)
            addButton("Cancel",DialogResult.CANCEL,buttonBox);
        mainBox.getChildren().add(buttonBox);
        
        FlowPane pane = new FlowPane(5,5);
        pane.setAlignment(Pos.CENTER);
        pane.getChildren().add(mainBox);
        FlowPane.setMargin(mainBox,new Insets(10));
        setScene(new Scene(pane,new Color(0.95,0.95,0.95,1)));
        sizeToScene();
    }
    
    private void addButton(String text,final DialogResult result,Pane pane)
    {
        Button button = new Button(text);
        button.setOnAction(new EventHandler<ActionEvent>()
            {
                public void handle(ActionEvent event)
                {
                    dialogResult = result;
                    close();
                }
            });
        pane.getChildren().add(button);
    }
    
    public void setTextFieldText(String textFieldText)
    {
        textField.setText(textFieldText);
    }
    
    public String getTextFieldText()
    {
        return textField.getText();
    }
    
    public DialogResult getDialogResult()
    {
        return dialogResult;
    }
}
