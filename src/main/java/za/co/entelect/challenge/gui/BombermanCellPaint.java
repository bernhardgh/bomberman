package za.co.entelect.challenge.gui;

import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Paint;
import javafx.scene.paint.RadialGradient;
import javafx.scene.paint.Stop;
import za.co.entelect.challenge.model.bomberman.BombermanCell;

public class BombermanCellPaint implements CellPaint<BombermanCell>
{
    @Override
    public Paint getClearPaint()
    {
        return new Color(0.8, 0.8, 0.8, 1);
    }

    @Override
    public Paint getPaintForCell(BombermanCell cell)
    {
        switch (cell.getCellType())
        {
            case CLEAR: return getClearPaint();
            case INDESTRUCTIBLE_WALL: return Color.BLACK;
            case DESTRUCTIBLE_WALL: return new Color(0.4, 0.4, 0.4, 1);
            case PLAYER: {
                switch (cell.getCellOwner().getPlayerKey())
                {
                    case 'A': return makeRadialGradient(new Color(1, 0.6, 0.6, 1), Color.RED);
                    case 'B': return makeRadialGradient(new Color(0.6, 1, 0.6, 1), Color.GREEN);
                    case 'C': return makeRadialGradient(new Color(0.6, 0.6, 1, 1), Color.BLUE);
                    default: return makeRadialGradient(new Color(1, 1, 0.6, 1), Color.YELLOW);
                }
            }
            case BOMB_EXPLOSION: return makeRadialGradient(Color.YELLOW, Color.CRIMSON); 
            case BOMB_RADIUS_POWER_UP: return makeRadialGradient(Color.CYAN, Color.DARKCYAN);
            case BOMB_BAG_POWER_UP: return makeLinearGradient(Color.MAGENTA, Color.DARKMAGENTA);
            case SUPER_POWER_UP: return Color.DARKGREEN;
        }
        return null;
    }
    
    
    private static Paint makeLinearGradient(Color color1, Color color2)
    {
        return new LinearGradient(0, 0, 0.35, 1, true, CycleMethod.NO_CYCLE, new Stop(0, color1), new Stop(1,color2));
    }
    
    private static Paint makeRadialGradient(Color color1, Color color2)
    {
        return new RadialGradient(0, 0, 0.35, 0.35, 1, true, CycleMethod.REFLECT, new Stop(0, color1), new Stop(1,color2));
    }
}
