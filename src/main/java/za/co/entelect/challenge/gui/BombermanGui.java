package za.co.entelect.challenge.gui;

import za.co.entelect.challenge.logging.Logger;
import za.co.entelect.challenge.logging.Logger.LogLevel;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.stage.Stage;

public class BombermanGui extends Application
{
    public static void main(String[] args)
    {
        Logger.getInstance().setLogLevel(LogLevel.INFO);
        
        launch(args);
    }

    public void start(Stage stage) throws Exception
    {
        Pane pane = (Pane) FXMLLoader.load(BombermanController.class.getClassLoader().getResource("BombermanFrontend.fxml"));
        stage.setScene(new Scene(pane, new LinearGradient(0, 0, 0, 1, true, CycleMethod.NO_CYCLE, new Stop(0, new Color(0.95, 0.95, 0.95, 1)), new Stop(1, new Color(0.85, 0.85, 0.85, 1)))));
        stage.setTitle("Bomberman");
        stage.show();
    }
}
