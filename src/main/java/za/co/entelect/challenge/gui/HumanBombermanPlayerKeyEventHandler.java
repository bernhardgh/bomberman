package za.co.entelect.challenge.gui;

import za.co.entelect.challenge.model.bomberman.BombermanMove;
import za.co.entelect.challenge.model.bomberman.players.HumanBombermanPlayer;
import javafx.event.EventHandler;
import javafx.scene.input.KeyEvent;

public class HumanBombermanPlayerKeyEventHandler implements EventHandler<KeyEvent>
{
    private HumanBombermanPlayer player;
    
    public HumanBombermanPlayerKeyEventHandler(HumanBombermanPlayer player)
    {
        this.player = player;
    }
    
    @Override
    public void handle(KeyEvent event)
    {
        switch (event.getCode())
        {
            case UP : player.setPreliminaryMove(BombermanMove.UP); break;
            case DOWN : player.setPreliminaryMove(BombermanMove.DOWN); break;
            case LEFT : player.setPreliminaryMove(BombermanMove.LEFT); break;
            case RIGHT : player.setPreliminaryMove(BombermanMove.RIGHT); break;
            case Z : player.setPreliminaryMove(BombermanMove.PLACE_BOMB); break;
            case X : player.setPreliminaryMove(BombermanMove.TRIGGER_BOMB); break;
            case C : player.setPreliminaryMove(BombermanMove.DO_NOTHING); break;
            default : break;
        }
    }
}
