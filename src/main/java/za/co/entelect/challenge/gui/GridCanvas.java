package za.co.entelect.challenge.gui;

import za.co.entelect.challenge.model.Cell;
import za.co.entelect.challenge.model.Grid;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;

public class GridCanvas<CellType extends Cell> extends Canvas
{
    private Grid<CellType> grid;
    private CellPaint<CellType> cellPaint;
    
    public GridCanvas(CellPaint<CellType> cellPaint)
    {
        this.cellPaint = cellPaint;
        SizeChangeListener changeListener = new SizeChangeListener();
        widthProperty().addListener(changeListener);
        heightProperty().addListener(changeListener);
    }
    
    public Grid<CellType> getGrid()
    {
        return grid;
    }
    
    public void setGrid(Grid<CellType> newGrid)
    {
        grid = newGrid;
        redraw();
    }
    
    public void redraw()
    {
        GraphicsContext gc = getGraphicsContext2D();
        gc.clearRect(0,0,getWidth(),getHeight());
        
        double cellSize,leftOffset = 0,topOffset = 0;
        if ((double) getWidth() / getHeight()>=(double) grid.getWidth() / grid.getHeight())
        {
            cellSize = (double) getHeight() / grid.getHeight();
            leftOffset = (getWidth() - grid.getWidth()*cellSize) / 2;
        }
        else
        {
            cellSize = (double) getWidth() / grid.getWidth();
            topOffset = (getHeight() - grid.getHeight()*cellSize) / 2;
        }
        
        // Draw grid.
        gc.setFill(cellPaint.getClearPaint());
        gc.fillRect(leftOffset,topOffset,grid.getWidth() * cellSize,grid.getHeight() * cellSize);
        gc.setLineWidth(1);
        gc.setStroke(Color.BLACK);
        for (int r=1; r<grid.getHeight(); r++)
            gc.strokeLine(leftOffset,topOffset + r*cellSize,getWidth() - leftOffset,topOffset + r*cellSize);
        for (int c=1; c<grid.getWidth(); c++)
            gc.strokeLine(leftOffset + c*cellSize,topOffset,leftOffset + c*cellSize,getHeight() - topOffset);
        
        boolean[][] cellDrawn = new boolean[grid.getHeight()][grid.getWidth()];
        for (int r=0; r<grid.getHeight(); r++)
            for (int c=0; c<grid.getWidth(); c++)
                cellDrawn[r][c] = false;
        // Draw cells.
        for (int r=0; r<grid.getHeight(); r++)
            for (int c=0; c<grid.getWidth(); c++)
            {
                double x = leftOffset + 1 + c*cellSize, y = topOffset + 1 + r*cellSize;
                CellType cell = grid.getCell(r,c);
                
                if (!cellDrawn[r][c])
                {
                    Paint paintForCell = cellPaint.getPaintForCell(cell);
                    if (! paintForCell.equals(cellPaint.getClearPaint()))
                    {
                        int lastSameRow;
                        for (lastSameRow=r; lastSameRow<grid.getHeight() - 1; lastSameRow++)
                        {
                            if (cellDrawn[lastSameRow + 1][c])
                                break;
                            CellType otherCell = grid.getCell(lastSameRow + 1,c);
                            if (! cellPaint.getPaintForCell(otherCell).equals(paintForCell))
                                break;
                        }
                        int lastSameCol;
                        OUTER:
                        for (lastSameCol=c; lastSameCol<grid.getWidth() - 1; lastSameCol++)
                            for (int columnRow=r; columnRow<=lastSameRow; columnRow++)
                            {
                                CellType otherCell = grid.getCell(columnRow,lastSameCol + 1);
                                if (! cellPaint.getPaintForCell(otherCell).equals(paintForCell))
                                    break OUTER;
                            }
                        int colCount = lastSameCol - c + 1,rowCount = lastSameRow - r + 1;
                        double width = colCount*cellSize - 1,height = rowCount*cellSize - 1;
                        if ((c==0) || (cellPaint.getPaintForCell(grid.getCell(r,c - 1)).equals(paintForCell)))
                        {
                            x-=1;
                            width+=1;
                        }
                        if ((r==0) || (cellPaint.getPaintForCell(grid.getCell(r - 1,c)).equals(paintForCell)))
                        {
                            y-=1;
                            height+=1;
                        }
                        gc.setFill(paintForCell);
                        gc.fillRect(x,y,width,height);
                        
                        for (int drawnRow=r; drawnRow<=lastSameRow; drawnRow++)
                            for (int drawnCol=c; drawnCol<=lastSameCol; drawnCol++)
                                cellDrawn[drawnRow][drawnCol] = true;
                    }
                }

                if (cell.getDisplayNumber() > 0)
                {
                    gc.setFill(Color.BLACK);
                    gc.fillText(Integer.toString(cell.getDisplayNumber()), x + cellSize/2 - 5, y + cellSize/2 + 4);
                }
            }
    }
    
    
    private class SizeChangeListener implements ChangeListener<Number>
    {
        public void changed(ObservableValue<? extends Number> observable,Number oldValue,Number newValue)
        {
            redraw();
        }
    }
}
