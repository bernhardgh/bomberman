package za.co.entelect.challenge.model.bomberman.players.helpers;

import za.co.entelect.challenge.model.bomberman.BombermanState;
import za.co.entelect.challenge.util.ComputedHash;

public class BombermanShortestDistanceLookupCache
{
    private static BombermanShortestDistanceLookupCache instance = null;
    
    public synchronized static BombermanShortestDistanceLookupCache getInstance()
    {
        if (instance == null)
            instance = new BombermanShortestDistanceLookupCache();
        return instance;
    }
    
    private ComputedHash currentComputedHash = null;
    private int currentOriginRow, currentOriginColumn;
    private BombermanShortestDistanceLookup currentLookup;
    
    private BombermanShortestDistanceLookupCache() {}
    
    public synchronized BombermanShortestDistanceLookup getLookup(BombermanState gameState, int originRow, int originColumn)
    {
        ComputedHash stateComputedHash = gameState.getComputedHash();
        if ((currentComputedHash != null) && (currentOriginRow == originRow) && (currentOriginColumn == originColumn) && (currentComputedHash.equals(stateComputedHash)))
            return currentLookup;
        
        currentComputedHash = stateComputedHash;
        currentOriginRow = originRow;
        currentOriginColumn = originColumn;
        return currentLookup = new BombermanShortestDistanceLookup(gameState.getGrid(), originRow, originColumn);
    }
}
