package za.co.entelect.challenge.model.bomberman.players.helpers;

import za.co.entelect.challenge.model.bomberman.BombermanCell;
import za.co.entelect.challenge.model.bomberman.BombermanGrid;
import za.co.entelect.challenge.model.bomberman.BombermanMove;
import za.co.entelect.challenge.model.bomberman.BombermanPlayer;
import za.co.entelect.challenge.model.bomberman.BombermanCell.CellType;

public class BombermanMoveHelpers
{
    public static boolean movesAreOpposite(BombermanMove move1, BombermanMove move2)
    {
        if (move1 == move2)
            return false;
        
        return ((moveIsHorizontal(move1)) && (moveIsHorizontal(move2))) || ((moveIsVertical(move1)) && (moveIsVertical(move2)));
    }
    
    private static boolean moveIsHorizontal(BombermanMove move)
    {
        return (move == BombermanMove.LEFT) || (move == BombermanMove.RIGHT);
    }
    
    private static boolean moveIsVertical(BombermanMove move)
    {
        return (move == BombermanMove.UP) || (move == BombermanMove.DOWN);
    }
    
    public static boolean isMovementMove(BombermanMove move)
    {
        return (move == BombermanMove.UP) || (move == BombermanMove.LEFT) || (move == BombermanMove.RIGHT) || (move == BombermanMove.DOWN);
    }
    
    public static BombermanMove getOppositeDirectionMove(BombermanMove move)
    {
        switch (move)
        {
            case UP: return BombermanMove.DOWN;
            case LEFT: return BombermanMove.RIGHT;
            case RIGHT: return BombermanMove.LEFT;
            case DOWN: return BombermanMove.UP;
            default: return move;
        }
    }
    
    public static boolean bombTriggerIsSuicide(BombermanPlayer player, BombermanGrid grid)
    {
        return bombTriggerIsSuicide(player, grid, 0);
    }
    
    public static boolean bombTriggerIsSuicide(BombermanPlayer player, BombermanGrid grid, int futureStepCount)
    {
        int playerRow = player.getRow(), playerColumn = player.getColumn();
        BombermanCell bombToTrigger = player.getBombToTrigger();
        int bombRow = bombToTrigger.getRow(), bombColumn = bombToTrigger.getColumn();
        if (playerRow == bombRow)
        {
            if (playerColumn == bombColumn)
            {
                int movesToEscapeFromBomb = isNextToInnerIndestructibleWall(bombRow, bombColumn) ? 2 : 3;
                return (futureStepCount < movesToEscapeFromBomb) && (futureStepCount <= bombToTrigger.getBombRadius());
            }
            if ((bombRow % 2 == 0) || (Math.abs(playerColumn - bombColumn) + futureStepCount > bombToTrigger.getBombRadius()))
                return false;
            boolean isNextToWall = playerColumn % 2 == 0;
            int movesToDodge = isNextToWall ? 2 : 1;
            if (futureStepCount >= movesToDodge)
                return false;
            int direction = bombColumn < playerColumn ? 1 : -1;
            for (int column = bombColumn + direction; column != playerColumn; column += direction)
            {
                BombermanCell cell = grid.getCell(playerRow, column);
                CellType cellType = cell.getCellType();
                if ((cellType == CellType.DESTRUCTIBLE_WALL) || ((cell.hasBomb()) && (Math.abs(playerColumn - column) + futureStepCount > cell.getBombRadius())))
                    return false;
            }
            return true;
        }
        
        if (playerColumn == bombColumn)
        {
            if ((bombColumn % 2 == 0) || (Math.abs(playerRow - bombRow) + futureStepCount > bombToTrigger.getBombRadius()))
                return false;
            boolean isNextToWall = playerRow % 2 == 0;
            int movesToDodge = isNextToWall ? 2 : 1;
            if (futureStepCount >= movesToDodge)
                return false;
            int direction = bombRow < playerRow ? 1 : -1;
            for (int row = bombRow + direction; row != playerRow; row += direction)
            {
                BombermanCell cell = grid.getCell(row, playerColumn);
                CellType cellType = cell.getCellType();
                if ((cellType == CellType.DESTRUCTIBLE_WALL) || ((cell.hasBomb()) && (Math.abs(playerRow - row) + futureStepCount > cell.getBombRadius())))
                    return false;
            }
            return true;
        }
        
        return false;
    }
    
    private static boolean isNextToInnerIndestructibleWall(int row, int column)
    {
        return (row % 2 == 0) || (column % 2 == 0);
    }
}
