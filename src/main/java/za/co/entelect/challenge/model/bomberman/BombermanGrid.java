package za.co.entelect.challenge.model.bomberman;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import za.co.entelect.challenge.model.GameGrid;
import za.co.entelect.challenge.model.bomberman.BombermanCell.CellType;

public class BombermanGrid extends GameGrid<BombermanCell>
{
    private static final long serialVersionUID = 7927883298648974380L;
    
    private int usableCellCount;
    private List<PowerUp> hiddenPowerUps = null;

    public BombermanGrid(int height, int width)
    {
        super(height, width, new BombermanCell());
        
        for (int row = 0; row < height; row++)
        {
            getCell(row, 0).setCell(CellType.INDESTRUCTIBLE_WALL);
            getCell(row, width - 1).setCell(CellType.INDESTRUCTIBLE_WALL);
        }
        for (int column = 0; column < width; column++)
        {
            getCell(0, column).setCell(CellType.INDESTRUCTIBLE_WALL);
            getCell(height - 1, column).setCell(CellType.INDESTRUCTIBLE_WALL);
        }

        int centerRow = height / 2, centerColumn = width / 2;
        for (int row = 2; row < height - 2; row += 2)
            for (int column = 2; column < width - 2; column += 2)
                if ((row != centerRow) || (column != centerColumn))
                    getCell(row, column).setCell(CellType.INDESTRUCTIBLE_WALL);
        
        usableCellCount = (3*getHeight()*getWidth() - 5*(getWidth() + getHeight()) + 11) / 4;
    }
    
    public int getUsableCellCount()
    {
        return usableCellCount;
    }
    
    public boolean addHiddenPowerUp(PowerUp powerUp)
    {
        if (hiddenPowerUps == null)
            hiddenPowerUps = new LinkedList<PowerUp>();
        
        if ((getCell(powerUp.getRow(), powerUp.getColumn()).getCellType() != CellType.DESTRUCTIBLE_WALL) || (hiddenPowerUps.stream().anyMatch(p -> (p.getRow() == powerUp.getRow()) && (p.getColumn() == powerUp.getColumn()))))
            return false;
        
        hiddenPowerUps.add(powerUp);
        return true;
    }
    
    public void restoreHiddenPowerUp(PowerUp powerUp)
    {
        if (hiddenPowerUps != null)
            hiddenPowerUps.add(powerUp);
    }
    
    public List<PowerUp> getHiddenPowerUps()
    {
        return hiddenPowerUps;
    }
    
    public PowerUp retrieveHiddenPowerUp(BombermanCell cell)
    {
        if (hiddenPowerUps == null)
            return null;
        
        Iterator<PowerUp> it = hiddenPowerUps.iterator();
        while (it.hasNext())
        {
            PowerUp nextPowerUp = it.next();
            if ((nextPowerUp.getRow() == cell.getRow()) && (nextPowerUp.getColumn() == cell.getColumn()))
            {
                it.remove();
                return nextPowerUp;
            }
        }
        return null;
    }
    
    @Override
    public Object clone()
    {
        BombermanGrid grid = (BombermanGrid)super.clone();
        grid.hiddenPowerUps = null;
        return grid;
    }
}
