package za.co.entelect.challenge.model;

import java.io.Serializable;

public abstract class Cell implements Cloneable, Serializable
{
    private static final long serialVersionUID = -8435442360893737834L;
    
    private int row, column;
    
    public Cell(int row, int column)
    {
        setRowAndColumn(row,column);
    }
    
    public Cell()
    {
        this(0, 0);
    }
    
    public int getRow()
    {
        return row;
    }
    
    public int getColumn()
    {
        return column;
    }
    
    public void setRowAndColumn(int newRow,int newColumn)
    {
        row = newRow;
        column = newColumn;
    }
    
    public int getDisplayNumber()
    {
        return 0;
    }
    
    @Override
    public int hashCode()
    {
        return row*21 + column;
    }
    
    @Override
    public boolean equals(Object other)
    {
        if (! (other instanceof Cell))
            return false;
        Cell otherCell = (Cell) other;
        return (otherCell.row == row) && (otherCell.column == column);
    }
    
    @Override
    public Object clone()
    {
        try
        {
            return super.clone();
        } 
        catch (CloneNotSupportedException e) {}
        return null;
    }
    
    @Override
    public String toString()
    {
        return "(" + row + ", " + column + ")";
    }

    public abstract char getCharacterCode();
}
