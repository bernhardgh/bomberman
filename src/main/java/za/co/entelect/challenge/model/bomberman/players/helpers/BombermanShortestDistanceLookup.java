package za.co.entelect.challenge.model.bomberman.players.helpers;

import java.util.LinkedList;
import java.util.Queue;

import za.co.entelect.challenge.model.bomberman.BombermanCell;
import za.co.entelect.challenge.model.bomberman.BombermanCell.CellType;
import za.co.entelect.challenge.model.bomberman.BombermanGrid;

public class BombermanShortestDistanceLookup
{
    private static final int[][] AROUND = new int[][] { { -1, 0 }, { 0, -1 }, { 1, 0 }, { 0, 1 } };
    
    private int[][] distances;
    
    public BombermanShortestDistanceLookup(BombermanGrid grid, int originRow, int originColumn)
    {
        distances = computeDistances(grid, originRow, originColumn);
    }
    
    public int getDistance(int row, int column)
    {
        return distances[row][column];
    }
    
    
    private static int[][] computeDistances(BombermanGrid grid, int originRow, int originColumn)
    {
        int height = grid.getHeight(), width = grid.getWidth();
        int[][] distances = new int[height][width];
        for (int row = 0; row < height; row++)
            for (int column = 0; column < width; column++)
                distances[row][column] = Integer.MAX_VALUE;
        Queue<DistanceEntry> queue = new LinkedList<DistanceEntry>();
        distances[originRow][originColumn] = 0;
        queue.offer(new DistanceEntry(originRow, originColumn));
        
        for (int currentDistance = 1; !queue.isEmpty(); currentDistance++)
        {
            int entryCount = queue.size();
            for (int i = 0; i < entryCount; i++)
            {
                DistanceEntry entry = queue.poll();
                for (int[] nextNeighbour : AROUND)
                {
                    int nextRow = entry.getRow() + nextNeighbour[0], nextColumn = entry.getColumn() + nextNeighbour[1];
                    if (distances[nextRow][nextColumn] != Integer.MAX_VALUE)
                        continue;
                    BombermanCell cell = grid.getCell(nextRow, nextColumn);
                    CellType cellType = cell.getCellType();
                    if ((cellType == CellType.CLEAR) || (cellType == CellType.BOMB_EXPLOSION) || (cellType == CellType.PLAYER) || (cell.hasPowerUp()))
                    {
                        distances[nextRow][nextColumn] = currentDistance;
                        queue.offer(new DistanceEntry(nextRow, nextColumn));
                    }
                }
            }
        }
        
        return distances;
    }
    
    private static class DistanceEntry
    {
        private int row, column;
        
        public DistanceEntry(int row, int column)
        {
            this.row = row;
            this.column = column;
        }
        
        public int getRow()
        {
            return row;
        }
        
        public int getColumn()
        {
            return column;
        }
    }
}
