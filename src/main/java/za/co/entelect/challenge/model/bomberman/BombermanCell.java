package za.co.entelect.challenge.model.bomberman;

import java.util.ArrayList;
import java.util.List;

import za.co.entelect.challenge.model.Cell;
import za.co.entelect.challenge.model.Player;
import za.co.entelect.challenge.model.bomberman.moveinfo.BombExplodeMoveEvent;
import za.co.entelect.challenge.model.bomberman.moveinfo.CellChangeMoveEvent;
import za.co.entelect.challenge.model.bomberman.moveinfo.MoveEvent;
import za.co.entelect.challenge.model.bomberman.moveinfo.PlantBombMoveEvent;
import za.co.entelect.challenge.model.bomberman.moveinfo.PlayerCellChangeMoveEvent;
import za.co.entelect.challenge.model.bomberman.moveinfo.BombCountdownMoveEvent;
import za.co.entelect.challenge.model.bomberman.moveinfo.GrantBombMoveEvent;
import za.co.entelect.challenge.util.IntMath;
import za.co.entelect.challenge.util.ObjectCode;
import za.co.entelect.challenge.util.ObjectCodeComponent;

public class BombermanCell extends Cell
{
    private static final long serialVersionUID = -7261505001377803814L;
    
    public enum CellType { CLEAR, INDESTRUCTIBLE_WALL, DESTRUCTIBLE_WALL, PLAYER, BOMB_EXPLOSION,
        BOMB_RADIUS_POWER_UP, BOMB_BAG_POWER_UP, SUPER_POWER_UP
    }
    
    private CellType cellType = CellType.CLEAR;
    private BombermanPlayer cellOwner = null;
    private int bombCountdown = 0;
    private int bombRadius = 0;
    private ArrayList<Character> playersVisited = new ArrayList<Character>(4);
    
    private ObjectCodeComponent gameStateCodeComponent = null;
    private int minimumPlayerKey;
    
    public transient int tag = 0;
    
    public BombermanCell(int row, int column, CellType cellType, BombermanPlayer cellOwner)
    {
        super(row, column);
        this.cellType = cellType;
        this.cellOwner = cellOwner;
    }
    
    public BombermanCell()
    {
        this(0, 0, CellType.CLEAR, null);
    }
    
    public void initializeCodes(ObjectCode gameStateCode, char minimumPlayerKey)
    {
        gameStateCodeComponent = new ObjectCodeComponent(gameStateCode);
        this.minimumPlayerKey = (int)minimumPlayerKey;
        
        refreshCodes();
    }
    
    public void resetGameStateCode(ObjectCode gameStateCode)
    {
        if (gameStateCodeComponent != null)
            gameStateCodeComponent.setObjectCode(gameStateCode);
    }
    
    @Override
    public int getDisplayNumber()
    {
        return bombCountdown;
    }
    
    public boolean isOpenToMoveOn()
    {
        return ((cellType == CellType.CLEAR) || (hasPowerUp())) && (!hasBomb());
    }
    
    public List<Character> getPlayersVisited()
    {
        return playersVisited;
    }
    
    public boolean wasVisitedByPlayer(Player player)
    {
        return playersVisited.contains(player.getPlayerKey());
    }
    
    public boolean hasPowerUp()
    {
        return (cellType == CellType.BOMB_RADIUS_POWER_UP) || (cellType == CellType.BOMB_BAG_POWER_UP) || (cellType == CellType.SUPER_POWER_UP);
    }
    
    public CellType getCellType()
    {
        return cellType;
    }
    
    public BombermanPlayer getCellOwner()
    {
        return cellOwner;
    }
    
    public void setCell(CellType newCellType, BombermanPlayer newCellOwner)
    {
        cellType = newCellType;
        cellOwner = newCellOwner;
        
        refreshCodes();
    }
    
    public void setCell(CellType newCellType)
    {
        setCell(newCellType, null);
    }
    
    public MoveEvent performCellChange(BombermanPlayer newCellOwner)
    {
        boolean isNewVisit = !wasVisitedByPlayer(newCellOwner);
        MoveEvent moveEvent = new PlayerCellChangeMoveEvent(this, newCellOwner, isNewVisit);
        
        switch (cellType)
        {
            case BOMB_RADIUS_POWER_UP: newCellOwner.increaseBombRadius(); break;
            case BOMB_BAG_POWER_UP: newCellOwner.increaseBombBagSize(); break;
            case SUPER_POWER_UP: {
                newCellOwner.increaseBombRadius();
                newCellOwner.increaseBombBagSize();
                newCellOwner.addToScore(50);
                break;
            }
            default: break;
        }
        
        newCellOwner.setRowAndColumn(getRow(), getColumn());

        if (isNewVisit)
        {
            playersVisited.add(newCellOwner.getPlayerKey());
            newCellOwner.incrementGridCoverage();
        }
        
        setCell(CellType.PLAYER, newCellOwner);
        
        return moveEvent;
    }
    
    public CellChangeMoveEvent performCellChange(CellType newCellType)
    {
        return performCellChange(newCellType, hasBomb());
    }
    
    public CellChangeMoveEvent performCellChange(CellType newCellType, boolean retainCellOwner)
    {
        CellChangeMoveEvent moveEvent = new CellChangeMoveEvent(this);
        
        if (hasPowerUp())
            return moveEvent;

        cellType = newCellType;
        if (!retainCellOwner)
            cellOwner = null;
        
        refreshCodes();
        
        return moveEvent;
    }
    
    public void restoreCellOwner(BombermanPlayer owner)
    {
        cellOwner = owner;
        refreshCodes();
    }
    
    public boolean hasBomb()
    {
        return bombRadius > 0;
    }
    
    public int getBombCountdown()
    {
        return bombCountdown;
    }
    
    public void setBombCountdown(int newBombCountdown)
    {
        bombCountdown = newBombCountdown;
        refreshCodes();
    }
    
    public int getBombRadius()
    {
        return bombRadius;
    }
    
    public void setBomb(int bombCountdown, int bombRadius, BombermanPlayer player)
    {
        this.bombCountdown = bombCountdown;
        this.bombRadius = bombRadius;
        cellOwner = player;
        
        refreshCodes();
    }
    
    public MoveEvent plantBomb()
    {
        cellOwner.removeAndDenyBomb(this);
        bombCountdown = cellOwner.getBombTimer();
        bombRadius = cellOwner.getBombRadius();
        
        refreshCodes();
        
        return new PlantBombMoveEvent(this);
    }
    
    public MoveEvent triggerBomb()
    {
        MoveEvent moveEvent = new BombCountdownMoveEvent(this);
        bombCountdown = 1;
        
        refreshCodes();
        
        return moveEvent;
    }
    
    public MoveEvent tickBomb()
    {
        MoveEvent moveEvent = new BombCountdownMoveEvent(this);
        if ((hasBomb()) && (bombCountdown != 0))
        {
            bombCountdown--;
            refreshCodes();
        }
        return moveEvent;
    }
    
    public boolean isBombExploding()
    {
        return (hasBomb()) && (bombCountdown == 0);
    }
    
    public MoveEvent setBombDidExplode()
    {
        MoveEvent moveEvent = new BombExplodeMoveEvent(this);
        
        bombRadius = 0;
        bombCountdown = 0;
        cellOwner.returnBomb(this);
        
        refreshCodes();
        
        return moveEvent;
    }
    
    public MoveEvent grantBomb()
    {
        cellOwner.grantBomb();
        return new GrantBombMoveEvent(cellOwner);
    }
    
    private void refreshCodes()
    {
        if (gameStateCodeComponent != null)
            gameStateCodeComponent.refreshCodes(computeCode());
    }
    
    private int computeCode()
    {
        int visitedCode = 0;
        int playerCount = playersVisited.size();
        for (int i = 0; i < playerCount; i++)
            visitedCode |= (1 << ((int)playersVisited.get(i) - minimumPlayerKey));

        int cellTypeCode = cellOwner == null ? cellType.ordinal() : 8 + ((int)cellOwner.getPlayerKey() - minimumPlayerKey);
        
        return ((IntMath.logBase2(bombRadius)*16 + visitedCode)*12 + cellTypeCode)*11 + bombCountdown;
    }
    
    @Override
    public char getCharacterCode()
    {
        switch (cellType)
        {
            case CLEAR: return hasBomb() ? (char)('0' + bombCountdown) : ' ';
            case INDESTRUCTIBLE_WALL: return '#';
            case DESTRUCTIBLE_WALL: return '+';
            case PLAYER: return hasBomb() ? Character.toLowerCase(cellOwner.getPlayerKey()) : cellOwner.getPlayerKey();
            case BOMB_EXPLOSION: return '*';
            case BOMB_RADIUS_POWER_UP: return '!';
            case BOMB_BAG_POWER_UP: return '&';
            case SUPER_POWER_UP: return '$';
        }
        throw new RuntimeException("Unsupported CellType");
    }
    
    @Override
    public Object clone()
    {
        BombermanCell copy = (BombermanCell)super.clone();
        copy.playersVisited = new ArrayList<Character>(4);
        copy.playersVisited.addAll(playersVisited);
        
        if (gameStateCodeComponent != null)
            copy.gameStateCodeComponent = (ObjectCodeComponent)gameStateCodeComponent.clone();
        
        return copy;
    }
}
