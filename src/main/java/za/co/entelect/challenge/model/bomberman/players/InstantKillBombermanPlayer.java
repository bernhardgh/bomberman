package za.co.entelect.challenge.model.bomberman.players;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import za.co.entelect.challenge.dto.DodgeCounts;
import za.co.entelect.challenge.logging.Logger;
import za.co.entelect.challenge.logging.Logger.LogLevel;
import za.co.entelect.challenge.model.GameEngine;
import za.co.entelect.challenge.model.GamePlayer;
import za.co.entelect.challenge.model.GameState;
import za.co.entelect.challenge.model.Move;
import za.co.entelect.challenge.model.MoveInfo;
import za.co.entelect.challenge.model.bomberman.BombermanCell;
import za.co.entelect.challenge.model.bomberman.BombermanGrid;
import za.co.entelect.challenge.model.bomberman.BombermanMove;
import za.co.entelect.challenge.model.bomberman.BombermanPlayer;
import za.co.entelect.challenge.model.bomberman.BombermanState;
import za.co.entelect.challenge.model.bomberman.players.helpers.BombermanCellHelpers;

public class InstantKillBombermanPlayer extends GamePlayer<BombermanCell>
{
    public static final String DODGE_COUNTS_FILE_PATH = "dodgeCounts";
    public static final int DODGE_COUNT_THRESHOLD = 2;

    private GamePlayer<BombermanCell> innerPlayer;
    
    public InstantKillBombermanPlayer(GamePlayer<BombermanCell> innerPlayer)
    {
        super(innerPlayer.getGameEngine(), innerPlayer.getPlayer());
        this.innerPlayer = innerPlayer;
    }

    @Override
    public Move getPreliminaryMove()
    {
        if (super.getPreliminaryMove() == null)
            return innerPlayer.getPreliminaryMove();
        return super.getPreliminaryMove();
    }

    @Override
    public Move getNextMove(GameState<BombermanCell> gameState) throws InterruptedException
    {
        BombermanState bombermanState = (BombermanState) gameState;
        BombermanPlayer player = bombermanState.getPlayer(((BombermanPlayer) getPlayer()).getPlayerKey());
        if (!player.isAlive())
            return BombermanMove.DO_NOTHING;

        Move move = shouldTriggerBomb(bombermanState) ? BombermanMove.TRIGGER_BOMB : innerPlayer.getNextMove(gameState);
        setPreliminaryMove(move);
        return move;
    }

    private boolean shouldTriggerBomb(BombermanState gameState)
    {
        GameEngine<BombermanCell> gameEngine = getGameEngine();
        if (!gameEngine.isMoveValid(gameState, getPlayer(), BombermanMove.TRIGGER_BOMB))
            return false;

        gameState = (BombermanState) gameState.clone();
        BombermanPlayer player = gameState.getPlayer(((BombermanPlayer) getPlayer()).getPlayerKey());

        ArrayList<BombermanPlayer> alivePlayers;
        {
            MoveInfo moveInfo = gameEngine.advanceRound(gameState);
            if ((!player.isAlive()) || (BombermanCellHelpers.isInBombThreat(player, gameState)))
                return false;

            alivePlayers = new ArrayList<BombermanPlayer>(gameState.getPlayers().length - 1);
            for (BombermanPlayer nextPlayer : gameState.getPlayers())
                if ((nextPlayer != player) && (nextPlayer.isAlive()))
                    alivePlayers.add(nextPlayer);
            gameEngine.undo(moveInfo);
        }

        gameEngine.applyMove(gameState, player, BombermanMove.TRIGGER_BOMB);
        gameEngine.advanceRound(gameState);
        if (!player.isAlive())
            return false;

        DodgeCounts dodgeCounts = new DodgeCounts(gameState.getPlayers());
        File dodgeCountsFile = new File(DODGE_COUNTS_FILE_PATH);
        if (dodgeCountsFile.exists())
        {
            try (FileInputStream stream = new FileInputStream(dodgeCountsFile))
            {
                dodgeCounts.loadCounts(stream);
            }
            catch (IOException e)
            {
                Logger.getInstance().log(LogLevel.WARNING, "Reading dodge-counts file failed with ", e);
            }
        }

        boolean hasDyingPlayers = false, hasDodgingPlayers = false;
        for (BombermanPlayer nextPlayer : alivePlayers)
            if (!nextPlayer.isAlive())
            {
                if (!canDodge(gameState, nextPlayer))
                    hasDyingPlayers = true;
                else if (nextPlayer.getDodgeCount() < DODGE_COUNT_THRESHOLD)
                {
                    hasDyingPlayers = true;
                    hasDodgingPlayers = true;
                    nextPlayer.incrementDodgeCount();
                }
            }
        if (hasDodgingPlayers)
        {
            try (FileOutputStream stream = new FileOutputStream(dodgeCountsFile))
            {
                dodgeCounts.saveCounts(stream);
            }
            catch (IOException e)
            {
                Logger.getInstance().log(LogLevel.WARNING, "Writing dodge-counts file failed with ", e);
            }
        }

        return hasDyingPlayers;
    }

    private static boolean canDodge(BombermanState gameState, BombermanPlayer player)
    {
        int playerRow = player.getRow(), playerColumn = player.getColumn();
        BombermanGrid grid = gameState.getGrid();
        return (grid.getCell(playerRow - 1, playerColumn).isOpenToMoveOn()) || (grid.getCell(playerRow + 1, playerColumn).isOpenToMoveOn()) || (grid.getCell(playerRow, playerColumn - 1).isOpenToMoveOn()) || (grid.getCell(playerRow, playerColumn + 1).isOpenToMoveOn());
    }
    
    
    public static void resetDodgeCountsFile()
    {
        (new File(DODGE_COUNTS_FILE_PATH)).delete();
    }
}
