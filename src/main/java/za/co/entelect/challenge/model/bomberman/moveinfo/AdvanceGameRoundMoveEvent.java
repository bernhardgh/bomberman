package za.co.entelect.challenge.model.bomberman.moveinfo;

import za.co.entelect.challenge.model.bomberman.BombermanState;

public class AdvanceGameRoundMoveEvent implements MoveEvent
{
    private BombermanState gameState;
    
    public AdvanceGameRoundMoveEvent(BombermanState gameState)
    {
        this.gameState = gameState;
    }

    @Override
    public void undo()
    {
        gameState.decrementGameRound();
    }
}
