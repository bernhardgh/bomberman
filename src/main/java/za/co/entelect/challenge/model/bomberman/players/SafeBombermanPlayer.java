package za.co.entelect.challenge.model.bomberman.players;

import za.co.entelect.challenge.model.GamePlayer;
import za.co.entelect.challenge.model.GameState;
import za.co.entelect.challenge.model.Move;
import za.co.entelect.challenge.model.Player;
import za.co.entelect.challenge.model.bomberman.BombermanCell;
import za.co.entelect.challenge.model.bomberman.BombermanEngine;
import za.co.entelect.challenge.model.bomberman.BombermanMove;
import za.co.entelect.challenge.model.bomberman.BombermanPlayer;
import za.co.entelect.challenge.model.bomberman.BombermanState;
import za.co.entelect.challenge.model.bomberman.players.helpers.BombermanCellHelpers;

public class SafeBombermanPlayer extends GamePlayer<BombermanCell>
{
    public SafeBombermanPlayer(Player player)
    {
        super(new BombermanEngine(), player);
    }

    @Override
    public Move getNextMove(GameState<BombermanCell> gameState) throws InterruptedException
    {
        BombermanState bombermanState = (BombermanState)gameState;
        BombermanPlayer player = bombermanState.getPlayer(getPlayer().getPlayerKey());
        
        for (BombermanMove nextMove : BombermanMove.values())
        {
            if ((getGameEngine().isMoveValid(gameState, player, nextMove)) 
                    && (!BombermanCellHelpers.isInBombThreat(BombermanCellHelpers.getDestinationCell(bombermanState.getGrid(), player, nextMove), bombermanState, nextMove == BombermanMove.TRIGGER_BOMB)))
                return nextMove;
        }
        
        return BombermanMove.DO_NOTHING;
    }
}
