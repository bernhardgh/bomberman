package za.co.entelect.challenge.model;

public abstract class GameEngine<CellType extends Cell>
{
    public abstract MoveInfo advanceRound(GameState<CellType> gameState);
    public abstract boolean isMoveValid(GameState<CellType> gameState, Player player, Move move);
    public abstract MoveInfo applyMove(GameState<CellType> gameState, Player player, Move moves) throws InvalidMoveException;
    public abstract boolean playerDidWin(GameState<CellType> gameState, Player player);
    
    public MoveInfo advanceRound(GameState<CellType> gameState, Move... playerMoves) throws InvalidMoveException
    {
        MoveInfo moveInfo = null;
        Player[] players = gameState.getPlayers();
        for (int i = 0; i < playerMoves.length; i++)
            moveInfo = concatenate(moveInfo, applyMove(gameState, players[i], playerMoves[i]));
        return concatenate(moveInfo, advanceRound(gameState));
    }
    
    public void undo(MoveInfo moveInfo)
    {
        moveInfo.undo();
    }
    
    public boolean gameDidEnd(GameState<CellType> gameState)
    {
        for (Player nextPlayer : gameState.getPlayers())
            if (playerDidWin(gameState, nextPlayer))
                return true;
        return false;
    }
    
    private static MoveInfo concatenate(MoveInfo one, MoveInfo two)
    {
        if (one == null)
            return two;
        
        one.concatenate(two);
        return one;
    }
}
