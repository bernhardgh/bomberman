package za.co.entelect.challenge.model.bomberman.moveinfo;

import za.co.entelect.challenge.model.bomberman.BombermanPlayer;

public class PlayerDiedMoveEvent implements MoveEvent
{
    private BombermanPlayer player;
    
    public PlayerDiedMoveEvent(BombermanPlayer player)
    {
        this.player = player;
    }

    @Override
    public void undo()
    {
        player.setIsAlive(true);
    }
}
