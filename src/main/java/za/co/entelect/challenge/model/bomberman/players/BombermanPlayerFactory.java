package za.co.entelect.challenge.model.bomberman.players;

import za.co.entelect.challenge.model.GamePlayer;
import za.co.entelect.challenge.model.Player;
import za.co.entelect.challenge.model.TimeLimitedGamePlayer;
import za.co.entelect.challenge.model.bomberman.BombermanCell;
import za.co.entelect.challenge.model.bomberman.heuristics.BombermanHeuristic;
import za.co.entelect.challenge.model.bomberman.heuristics.TermBombermanHeuristic;
import za.co.entelect.challenge.model.bomberman.players.DFSBombermanPlayer.PlayerSelection;

public abstract class BombermanPlayerFactory
{
    private static BombermanPlayerFactory[] playerFactories = null;
    
    private String name;
    
    private BombermanPlayerFactory(String name)
    {
        this.name = name;
    }
    
    public String getName()
    {
        return name;
    }
    
    public boolean shouldDisplayOnGui()
    {
        return true;
    }
    
    public String toString()
    {
        return name;
    }
    
    public abstract GamePlayer<BombermanCell> createPlayer(Player player, long timeLimit);
    
    
    public static BombermanPlayerFactory[] getPlayerFactories()
    {
        if (playerFactories == null)
        {
            playerFactories = new BombermanPlayerFactory[] {
                    new BombermanPlayerFactory("Lazy")
                    {
                        @Override
                        public GamePlayer<BombermanCell> createPlayer(Player player, long timeLimit)
                        {
                            return new SafeBombermanPlayer(player);
                        }
                    },
                    new BombermanPlayerFactory("Human")
                    {
                        @Override
                        public GamePlayer<BombermanCell> createPlayer(Player player, long timeLimit)
                        {
                            return new HumanBombermanPlayer(player);
                        }
                    },
                    new BombermanPlayerFactory("Beam")
                    {
                        @Override
                        public GamePlayer<BombermanCell> createPlayer(Player player, long timeLimit)
                        {
                            return makeTimeLimited(new BeamBombermanPlayer(player,
                                    new TermBombermanHeuristic(true)), 
                                    timeLimit);
                        }
                    },
                    new BombermanPlayerFactory("DFS")
                    {
                        @Override
                        public GamePlayer<BombermanCell> createPlayer(Player player, long timeLimit)
                        {
                            return makeTimeLimited(new DFSBombermanPlayer(player,
                                    PlayerSelection.NEAR_PLAYERS,
                                    new TermBombermanHeuristic()), 
                                    timeLimit);
                        }
                    },
                    new BombermanPlayerFactory("Multi-DFS")
                    {
                        @Override
                        public GamePlayer<BombermanCell> createPlayer(Player player, long timeLimit)
                        {
                            return makeTimeLimited(new MultiThreadedDFSBombermanPlayer(player,
                                    PlayerSelection.NEAR_PLAYERS,
                                    new TermBombermanHeuristic()),
                                    timeLimit);
                        }
                        
                        @Override
                        public boolean shouldDisplayOnGui()
                        {
                            return false;
                        }
                    },
                    new BombermanPlayerFactory("DFS-Nearest")
                    {
                        @Override
                        public GamePlayer<BombermanCell> createPlayer(Player player, long timeLimit)
                        {
                            return makeTimeLimited(new DFSBombermanPlayer(player,
                                    PlayerSelection.NEAREST_PLAYER,
                                    new TermBombermanHeuristic()), 
                                    timeLimit);
                        }
                    },
                    new BombermanPlayerFactory("Multi-DFS-Nearest")
                    {
                        @Override
                        public GamePlayer<BombermanCell> createPlayer(Player player, long timeLimit)
                        {
                            return makeTimeLimited(new MultiThreadedDFSBombermanPlayer(player,
                                    PlayerSelection.NEAREST_PLAYER,
                                    new TermBombermanHeuristic()), 
                                    timeLimit);
                        }
                        
                        @Override
                        public boolean shouldDisplayOnGui()
                        {
                            return false;
                        }
                    },
                    new BombermanPlayerFactory("AlphaBeta")
                    {
                        @Override
                        public GamePlayer<BombermanCell> createPlayer(Player player, long timeLimit)
                        {
                            return makeTimeLimited(new AlphaBetaBombermanPlayer(player,
                                    new TermBombermanHeuristic()), 
                                    timeLimit);
                        }
                    },
                    new BombermanPlayerFactory("Multi-AlphaBeta")
                    {
                        @Override
                        public GamePlayer<BombermanCell> createPlayer(Player player, long timeLimit)
                        {
                            return makeTimeLimited(new MultiThreadedAlphaBetaBombermanPlayer(player,
                                    new TermBombermanHeuristic()), 
                                    timeLimit);
                        }
                        
                        @Override
                        public boolean shouldDisplayOnGui()
                        {
                            return false;
                        }
                    },
                    new BombermanPlayerFactory("Search")
                    {
                        @Override
                        public GamePlayer<BombermanCell> createPlayer(Player player, long timeLimit)
                        {
                            BombermanHeuristic heuristic = new TermBombermanHeuristic();
                            return makeTimeLimited(new InstantKillBombermanPlayer(
                                    new HybridSearchBombermanPlayer(
                                            new BeamBombermanPlayer(player, new TermBombermanHeuristic(true)),
                                            new HybridAdversarialSearchBombermanPlayer(
                                                    new MultiThreadedAlphaBetaBombermanPlayer(player, heuristic),
                                                    new MultiThreadedDFSBombermanPlayer(player, new DFSBombermanPlayer.NearPlayersSelectionStrategyFactory(HybridAdversarialSearchBombermanPlayer.DEFAULT_PLAYER_DISTANCE_THRESHOLD), heuristic)))),
                                    timeLimit);
                        }
                    },
                    new BombermanPlayerFactory("Replay")
                    {
                        @Override
                        public GamePlayer<BombermanCell> createPlayer(Player player, long timeLimit)
                        {
                            return new ReplayBombermanPlayer(player);
                        }
                    }
            };
        }
        return playerFactories;
    }
    
    public static GamePlayer<BombermanCell> createPlayer(String name, Player player, long timeLimit)
    {
        for (BombermanPlayerFactory nextFactory : getPlayerFactories())
            if (nextFactory.name.equals(name))
                return nextFactory.createPlayer(player, timeLimit);
        
        throw new IllegalArgumentException("No factory for name " + name + " was found.");
    }
    
    
    private static GamePlayer<BombermanCell> makeTimeLimited(GamePlayer<BombermanCell> gamePlayer, long timeLimit)
    {
        return timeLimit == -1 ? gamePlayer : new TimeLimitedGamePlayer<BombermanCell>(gamePlayer, new SafeBombermanPlayer(gamePlayer.getPlayer()), Math.max(0, timeLimit - 200), timeLimit);
    }
}
