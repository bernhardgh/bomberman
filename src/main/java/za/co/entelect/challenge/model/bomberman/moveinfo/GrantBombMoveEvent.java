package za.co.entelect.challenge.model.bomberman.moveinfo;

import za.co.entelect.challenge.model.bomberman.BombermanPlayer;

public class GrantBombMoveEvent implements MoveEvent
{
    private BombermanPlayer cellOwner;
    
    public GrantBombMoveEvent(BombermanPlayer cellOwner)
    {
        this.cellOwner = cellOwner;
    }
    
    @Override
    public void undo()
    {
        cellOwner.denyBomb();
    }
}
