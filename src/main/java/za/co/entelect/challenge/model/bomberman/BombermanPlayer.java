package za.co.entelect.challenge.model.bomberman;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;

import za.co.entelect.challenge.model.Player;
import za.co.entelect.challenge.util.IntMath;
import za.co.entelect.challenge.util.ObjectCode;
import za.co.entelect.challenge.util.ObjectCodeComponent;

public class BombermanPlayer extends Player
{
    private static final long serialVersionUID = 7170922823254666788L;
    
    private static final int COMPLETE_COVERAGE_SCORE = 100;
    
    private int row, column;
    private int baseScore = 0;
    private int gridCoverage = 0, usableCellCount;
    private boolean isAlive = true;
    private int bombBagSize = 1, availableBombCount = 1;
    private int bombRadius = 1;
    private ArrayList<BombermanCell> cellsWithBombs = new ArrayList<BombermanCell>(4);
    
    private transient ProcessingInfo processingInfo = new ProcessingInfo();
    private transient boolean isActive = true, isMainPlayer = false;
    private transient int dodgeCount = 0;

    private ObjectCodeComponent gameStateCodeComponent = null;
    
    public BombermanPlayer(char playerKey)
    {
        super(playerKey);
    }
    
    public void initializeCodes(ObjectCode gameStateCode)
    {
        gameStateCodeComponent = new ObjectCodeComponent(gameStateCode);
        refreshCodes();
    }
    
    public void resetGameStateCode(ObjectCode gameStateCode)
    {
        if (gameStateCodeComponent != null)
            gameStateCodeComponent.setObjectCode(gameStateCode);
    }
    
    public boolean isAlive()
    {
        return isAlive;
    }
    
    public void setIsAlive(boolean flag)
    {
        isAlive = flag;
    }
    
    public int getRow()
    {
        return row;
    }
    
    public int getColumn()
    {
        return column;
    }
    
    public void setRowAndColumn(int row, int column)
    {
        this.row = row;
        this.column = column;
    }
    
    public void setUsableCellCount(int usableCellCount)
    {
        this.usableCellCount = usableCellCount;
    }
    
    public int getGridCoverage()
    {
        return gridCoverage;
    }
    
    public void setGridCoverage(int gridCoverage)
    {
        this.gridCoverage = gridCoverage;
    }
    
    public void incrementGridCoverage()
    {
        gridCoverage++;
    }
    
    public void decrementGridCoverage()
    {
        gridCoverage--;
    }
    
    public int getScore()
    {
        return baseScore + getCoverageScoreInt();
    }
    
    public void setScore(int score)
    {
        setBaseScore(score - getCoverageScoreInt());
    }
    
    public double getScoreDouble()
    {
        return baseScore + getCoverageScoreDouble();
    }
    
    public int getBaseScore()
    {
        return baseScore;
    }
    
    public void setBaseScore(int newBaseScore)
    {
        baseScore = newBaseScore;
        refreshCodes();
    }
    
    private int getCoverageScoreInt()
    {
        return gridCoverage * COMPLETE_COVERAGE_SCORE / usableCellCount;
    }
    
    private double getCoverageScoreDouble()
    {
        return (double)gridCoverage * COMPLETE_COVERAGE_SCORE / usableCellCount;
    }
    
    public void addToScore(int pointsToAdd)
    {
        baseScore += pointsToAdd;
        refreshCodes();
    }
    
    public int getAvailableBombCount()
    {
        return availableBombCount;
    }
    
    public int getBombBagSize()
    {
        return bombBagSize;
    }
    
    public void setBombCounts(int count)
    {
        setBombCounts(count, count);
    }
    
    public void setBombCounts(int availableBombCount, int bombBagSize)
    {
        this.availableBombCount = availableBombCount;
        this.bombBagSize = bombBagSize;
        refreshCodes();
    }
    
    public void increaseBombBagSize()
    {
        bombBagSize++;
        availableBombCount++;
        refreshCodes();
    }
    
    public List<BombermanCell> getCellsWithBombs()
    {
        return cellsWithBombs;
    } 
    
    public void removeAndDenyBomb(BombermanCell plantedCell)
    {
        cellsWithBombs.add(plantedCell);
        availableBombCount--;
    }
    
    public void removeBomb(BombermanCell plantedCell, int bombIndex)
    {
        cellsWithBombs.add(bombIndex, plantedCell);
    }
    
    public void returnBomb(BombermanCell plantedCell)
    {
        cellsWithBombs.remove(plantedCell);
    }
    
    public void grantBomb()
    {
        availableBombCount++;
    }
    
    public void denyBomb()
    {
        availableBombCount--;
    }
    
    public int getBombIndex(BombermanCell bombCell)
    {
        return cellsWithBombs.indexOf(bombCell);
    }
    
    public int getBombRadius()
    {
        return bombRadius;
    }
    
    public void setBombRadius(int newBombRadius)
    {
        bombRadius = newBombRadius;
        refreshCodes();
    }
    
    public void increaseBombRadius()
    {
        bombRadius *= 2;
        refreshCodes();
    }
    
    public int getBombTimer()
    {
        return Math.min(10, bombBagSize*3 + 1);
    }
    
    public boolean hasBombToTrigger()
    {
        return (!cellsWithBombs.isEmpty()) && (cellsWithBombs.get(cellsWithBombs.size() - 1).getBombCountdown() > 1);
    }
    
    public BombermanCell getBombToTrigger()
    {
        BombermanCell cellToTrigger = cellsWithBombs.get(0);
        return cellToTrigger.getBombCountdown() > 1 ? cellToTrigger : cellsWithBombs.get(1);
    }
    
    public ProcessingInfo getProcessingInfo()
    {
        return processingInfo;
    }
    
    public boolean isActive()
    {
        return isActive;
    }
    
    public void setIsActive(boolean flag)
    {
        isActive = flag;
    }
    
    public boolean isMainPlayer()
    {
        return isMainPlayer;
    }
    
    public void setMainPlayer()
    {
        isMainPlayer = true;
    }
    
    public int getDodgeCount()
    {
        return dodgeCount;
    }
    
    public void setDodgeCount(int newDodgeCount)
    {
        dodgeCount = newDodgeCount;
    }
    
    public void incrementDodgeCount()
    {
        dodgeCount++;
    }
    
    private void refreshCodes()
    {
        if (gameStateCodeComponent != null)
            gameStateCodeComponent.refreshCodes(computeCode());
    }
    
    private int computeCode()
    {
        return (baseScore*17 + IntMath.logBase2(bombRadius))*9 + bombBagSize;
    }
    
    @Override
    public Object clone()
    {
        BombermanPlayer copy = (BombermanPlayer)super.clone();
        copy.cellsWithBombs = new ArrayList<BombermanCell>(cellsWithBombs);
        copy.processingInfo = new ProcessingInfo();
        
        if (gameStateCodeComponent != null)
            copy.gameStateCodeComponent = (ObjectCodeComponent)gameStateCodeComponent.clone();
        
        return copy;
    }
    
    private void readObject(ObjectInputStream in) throws ClassNotFoundException, IOException
    {
        in.defaultReadObject();
        if (bombBagSize == 0)
            bombBagSize = availableBombCount + cellsWithBombs.size();
        
        processingInfo = new ProcessingInfo();
        isActive = true;
    }
    
    
    public class ProcessingInfo
    {
        private boolean isPartOfChain;
        private int pointsEarned;
        private boolean wasKilled;
        
        public ProcessingInfo()
        {
            resetInfo();
        }
        
        public void resetInfo()
        {
            isPartOfChain = false;
            pointsEarned = 0;
            wasKilled = false;
        }
        
        public boolean isPartOfChain()
        {
            return isPartOfChain;
        }
        
        public void setIsPartOfChain()
        {
            setIsPartOfChain(true);
        }
        
        public void setIsPartOfChain(boolean flag)
        {
            isPartOfChain = flag;
        }
        
        public int getPointsEarned()
        {
            return pointsEarned;
        }
        
        public void addPointsEarned(int points)
        {
            pointsEarned += points;
        }
        
        public boolean wasKilled()
        {
            return wasKilled;
        }
        
        public void setWasKilled()
        {
            wasKilled = true;
        }
    }
}
