package za.co.entelect.challenge.model.bomberman.moveinfo;

import za.co.entelect.challenge.model.bomberman.BombermanCell;

public class BombCountdownMoveEvent implements MoveEvent
{
    private BombermanCell cell;
    private int oldBombCountdown;
    
    public BombCountdownMoveEvent(BombermanCell cell)
    {
        this.cell = cell;
        oldBombCountdown = cell.getBombCountdown();
    }

    @Override
    public void undo()
    {
        cell.setBombCountdown(oldBombCountdown);
    }
}
