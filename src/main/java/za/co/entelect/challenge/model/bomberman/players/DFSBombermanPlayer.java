package za.co.entelect.challenge.model.bomberman.players;

import java.util.ArrayList;
import java.util.Map;

import za.co.entelect.challenge.logging.Logger;
import za.co.entelect.challenge.logging.Logger.LogLevel;
import za.co.entelect.challenge.model.GamePlayer;
import za.co.entelect.challenge.model.GameState;
import za.co.entelect.challenge.model.Move;
import za.co.entelect.challenge.model.Player;
import za.co.entelect.challenge.model.bomberman.BombermanCell;
import za.co.entelect.challenge.model.bomberman.BombermanEngine;
import za.co.entelect.challenge.model.bomberman.BombermanGrid;
import za.co.entelect.challenge.model.bomberman.BombermanMove;
import za.co.entelect.challenge.model.bomberman.BombermanPlayer;
import za.co.entelect.challenge.model.bomberman.BombermanState;
import za.co.entelect.challenge.model.bomberman.heuristics.BombermanHeuristic;
import za.co.entelect.challenge.model.bomberman.moveinfo.BombermanMoveInfo;
import za.co.entelect.challenge.model.bomberman.players.SearchController.CacheEntry;
import za.co.entelect.challenge.model.bomberman.players.helpers.BombermanCellAssessor;
import za.co.entelect.challenge.model.bomberman.players.helpers.BombermanCellHelpers;
import za.co.entelect.challenge.model.bomberman.players.helpers.BombermanMoveHelpers;
import za.co.entelect.challenge.model.bomberman.players.helpers.BombermanShortestDistanceLookup;
import za.co.entelect.challenge.model.bomberman.players.helpers.BombermanShortestDistanceLookupCache;
import za.co.entelect.challenge.util.ComputedHash;

public class DFSBombermanPlayer extends GamePlayer<BombermanCell>
{
    private static final BombermanMove[] ORDERED_MOVES_WALL_IN_RANGE = new BombermanMove[] { 
        BombermanMove.PLACE_BOMB, BombermanMove.UP, BombermanMove.RIGHT, BombermanMove.DOWN, BombermanMove.LEFT, BombermanMove.TRIGGER_BOMB, BombermanMove.DO_NOTHING
    };
    private static final BombermanMove[] ORDERED_MOVES_NO_WALL_IN_RANGE = new BombermanMove[] { 
        BombermanMove.UP, BombermanMove.RIGHT, BombermanMove.DOWN, BombermanMove.LEFT, BombermanMove.TRIGGER_BOMB, BombermanMove.DO_NOTHING, BombermanMove.PLACE_BOMB
    };
    private static final BombermanMove[] ORDERED_MOVES_WALL_SINGLE_PLAYER = ORDERED_MOVES_WALL_IN_RANGE;
    
    private static final Logger logger = Logger.getInstance();
    
    public enum PlayerSelection { NEAREST_PLAYER, NEAR_PLAYERS };
    
    private PlayerSelectionStrategyFactory opponentPlayerSelectionStrategyFactory;
    private BombermanHeuristic heuristic;
    private BombermanCellAssessor assessor;
    
    private SearchController searchController;
    private SearchController.PlayerSearchPly mainPlayerPly;
    private int nodeCount;
    private boolean didCutOffSearch;

    public DFSBombermanPlayer(Player player, PlayerSelection opponentPlayerSelection, BombermanHeuristic heuristic)
    {
        this(player, opponentPlayerSelection == PlayerSelection.NEAREST_PLAYER ? new NearestPlayerSelectionStrategyFactory() : new NearPlayersSelectionStrategyFactory(), heuristic);
    }
    
    public DFSBombermanPlayer(Player player, PlayerSelectionStrategyFactory opponentPlayerSelectionStrategyFactory, BombermanHeuristic heuristic)
    {
        super(new BombermanEngine(), player);
        this.opponentPlayerSelectionStrategyFactory = opponentPlayerSelectionStrategyFactory;
        this.heuristic = heuristic;
    }
    
    @Override
    public Move getNextMove(GameState<BombermanCell> gameState) throws InterruptedException
    {
        BombermanMove immediateMove = initializeSearch((BombermanState)gameState.clone());
        if (immediateMove != null)
            return immediateMove;
        int depthReached = 0;
        
        try
        {
            for (int maxDepth = 5 - searchController.getSearchPlies().length; ; maxDepth++)
            {
                logger.log(LogLevel.TRACE, "=== Depth Limit %d ===", maxDepth);
                
                SearchResult result = computeMove(maxDepth);
                setPreliminaryMove(result.getMove());
                
                depthReached = maxDepth;
                if (!result.hasMoreDepthToSearch())
                    break;
                
                Thread.sleep(0);
            }
        }
        finally
        {
            logger.log(LogLevel.INFO, "==== Depth reached: %d ====", depthReached);
            logger.log(LogLevel.INFO, "==== Node count: %d ====", nodeCount);
        }
        
        return getPreliminaryMove();
    }
    
    protected int getNodeCount()
    {
        return nodeCount;
    }
    
    protected BombermanMove initializeSearch(BombermanState bombermanState)
    {
        BombermanPlayer player = bombermanState.getPlayer(((BombermanPlayer)getPlayer()).getPlayerKey());
        if (!player.isAlive())
            return BombermanMove.DO_NOTHING;
        
        player.setMainPlayer();
        player.setIsActive(true);
        ArrayList<SearchController.SearchPly> searchPlies = new ArrayList<>(bombermanState.getPlayers().length + 1);
        boolean hasInactivePlayers = false;
        {
            PlayerSelectionStrategy selector = opponentPlayerSelectionStrategyFactory.create(bombermanState, player, bombermanState.getPlayers());
            for (BombermanPlayer nextPlayer : bombermanState.getPlayers())
                if (nextPlayer != player)
                {
                    boolean playerActive = (nextPlayer.isAlive()) && (searchPlies.size() < 2) && (selector.selectPlayer(nextPlayer));
                    nextPlayer.setIsActive(playerActive);
                    if (playerActive)
                        searchPlies.add(SearchController.SearchPly.getPlayerPly(nextPlayer));
                    else
                        hasInactivePlayers = true;
                }
        }
        mainPlayerPly = SearchController.SearchPly.getPlayerPly(player);
        searchPlies.add(mainPlayerPly);
        
        logger.log(LogLevel.INFO, "==== Search ply count: %d ====", searchPlies.size());
        searchPlies.add(SearchController.SearchPly.getAdvanceRoundPly());
        
        searchController = new SearchController((BombermanEngine)getGameEngine(), bombermanState, player,
                searchPlies.toArray(new SearchController.SearchPly[0]));
        
        assessor = hasInactivePlayers ? new BombermanCellAssessor() : null;
        nodeCount = 0;
        return null;
    }
    
    protected SearchResult computeMove(int depthLimit) throws InterruptedException
    {
        didCutOffSearch = false;
        searchController.resetSearchPlies();
        Score score = computeScore(depthLimit);
        return new SearchResult(mainPlayerPly.getBestMove(), (didCutOffSearch) && (depthLimit < 50) && (!score.hasWinner()));
    }
    
    private Score computeScore(int depthLimit) throws InterruptedException
    {
        return searchController.getCurrentPly() instanceof SearchController.PlayerSearchPly
                ? computeScorePlayer(depthLimit)
                : computeScoreAdvanceRound(depthLimit);
    }
    
    private Score computeScorePlayer(int depthLimit) throws InterruptedException
    {
        BombermanEngine gameEngine = searchController.getGameEngine();
        BombermanState gameState = searchController.getGameState();
        
        int plyIndex = searchController.getCurrentSearchPlyIndex();
        SearchController.SearchPly[] plies = searchController.getSearchPlies();
        int playerCount = plies.length - 1;
        SearchController.PlayerSearchPly ply = (SearchController.PlayerSearchPly)plies[plyIndex];

        Map<ComputedHash, CacheEntry> scoreCache = null;
        ComputedHash gameStateHash = null;
        if (depthLimit >= 3)
        {
            scoreCache = ply.getScoreCache();
            gameStateHash = gameState.getComputedHash();
            CacheEntry cacheEntry = scoreCache.get(gameStateHash);
            
            if ((cacheEntry != null) && (cacheEntry.getDepth() >= depthLimit))
            {
                Score cachedScore = cacheEntry.getScore();
                if (cacheEntry.getDepth() > depthLimit)
                    cachedScore = cachedScore.compound(cacheEntry.getDepth() - depthLimit);
                
                switch (cacheEntry.getEntryType())
                {
                    case EXACT: return cachedScore;
                    case LOWER_BOUND: {
                        if (!isPotentialScoreForSomeOtherPlayer(cachedScore, plyIndex, plies))
                            return cachedScore;
                        break;
                    }
                    case UPPER_BOUND: {
                        if ((ply.getAlpha() != null) && (!cachedScore.isLarger(ply.getAlpha(), plyIndex)))
                            return cachedScore;
                        break;
                    }
                }
            }
        }
        
        BombermanPlayer player = ply.getPlayer();
        int playerPowerUpSum = player.getBombBagSize() + player.getBombRadius();
        BombermanMove lastMove = ply.getLastMove();
        boolean didGetPowerUp = ply.didGetPowerUp();
        boolean hasBombsOrExplosions = gameState.hasBombsOrExplosions();
        
        Score initialAlpha = ply.getAlpha();
        Score alpha = initialAlpha;
        BombermanMove bestMove = null;
        boolean scoreIsExact = false;
        for (BombermanMove nextMove : getOrderedMoves(gameState.getGrid(), player, playerCount))
            if (((didGetPowerUp) || (!BombermanMoveHelpers.movesAreOpposite(lastMove, nextMove)))
                    && ((nextMove != BombermanMove.DO_NOTHING) || (!player.isAlive()) || (hasBombsOrExplosions))
                    && (gameEngine.isMoveValid(gameState, player, nextMove))
                    && ((nextMove != BombermanMove.TRIGGER_BOMB) || (!(new BombermanCellAssessor()).bombTriggerIsSuicide(player, gameState.getGrid()))))
            {
                BombermanMoveInfo playerMoveInfo = gameEngine.applyMove(gameState, player, nextMove);
                ply.setLastMove(nextMove);
                ply.setDidGetPowerUp(player.getBombBagSize() + player.getBombRadius() > playerPowerUpSum);
                BombermanPlayer threatPlayer = assessor == null ? null : assessor.getThreatForLocation(player.getRow(), player.getColumn(), gameState, searchController.getCurrentPlyDepth() / plies.length);
                BombermanMoveInfo threatMoveInfo = threatPlayer == null ? null : gameEngine.applyMove(gameState, threatPlayer, BombermanMove.TRIGGER_BOMB);
                searchController.nextPly();
                Score moveScore = computeScore(depthLimit);
                searchController.previousPly();
                if (threatMoveInfo != null)
                    gameEngine.undo(threatMoveInfo);
                gameEngine.undo(playerMoveInfo);

                if (searchController.getCurrentPlyDepth() == 0)
                    logger.log(LogLevel.TRACE, "%s (%d): %s", nextMove, depthLimit, moveScore);
                
                if ((playerCount != 1) && (!isPotentialScoreForSomeOtherPlayer(moveScore, plyIndex, plies)))
                {
                    if (gameStateHash != null)
                        scoreCache.put(gameStateHash, new CacheEntry(moveScore, depthLimit, CacheEntry.EntryType.LOWER_BOUND));
                    
                    ply.setAlpha(initialAlpha);
                    ply.setLastMove(lastMove);
                    ply.setDidGetPowerUp(didGetPowerUp);
                    return moveScore;
                }

                if ((alpha == null) || (moveScore.isLarger(alpha, plyIndex)))
                {
                    ply.setAlpha(alpha = moveScore);
                    scoreIsExact = true;
                    bestMove = nextMove;
                }
            }
        
        if (gameStateHash != null)
            scoreCache.put(gameStateHash, new CacheEntry(alpha, depthLimit, scoreIsExact ? CacheEntry.EntryType.EXACT : CacheEntry.EntryType.UPPER_BOUND));

        ply.setAlpha(initialAlpha);
        ply.setLastMove(lastMove);
        ply.setDidGetPowerUp(didGetPowerUp);
        ply.setBestMove(bestMove);
        return alpha;
    }
    
    private static boolean isPotentialScoreForSomeOtherPlayer(Score score, int plyIndex, SearchController.SearchPly[] plies)
    {
        int playerCount = plies.length - 1;
        for (int otherPlyIndex = 0; otherPlyIndex < playerCount; otherPlyIndex++)
            if (otherPlyIndex != plyIndex)
            {
                Score otherPlayerAlpha = ((SearchController.PlayerSearchPly)plies[otherPlyIndex]).getAlpha();
                if ((otherPlayerAlpha == null) || (score.isLarger(otherPlayerAlpha, otherPlyIndex)))
                    return true;
            }
        return false;
    }
    
    private static BombermanMove[] getOrderedMoves(BombermanGrid grid, BombermanPlayer player, int playerCount)
    {
        return playerCount == 1 
                ? ORDERED_MOVES_WALL_SINGLE_PLAYER
                : (BombermanCellHelpers.hasWallInRange(grid, player) ? ORDERED_MOVES_WALL_IN_RANGE : ORDERED_MOVES_NO_WALL_IN_RANGE);
    }
    
    private Score computeScoreAdvanceRound(int depthLimit) throws InterruptedException
    {
        nodeCount++;
        
        BombermanEngine gameEngine = searchController.getGameEngine();
        BombermanState gameState = searchController.getGameState();
        BombermanMoveInfo roundMoveInfo = gameEngine.advanceRound(gameState);

        Score score;
        boolean gameDidEnd = gameEngine.gameDidEnd(gameState);
        if ((depthLimit == 0) || (!searchController.getMainPlayer().isAlive()) || (gameDidEnd))
        {
            if (depthLimit == 0)
                didCutOffSearch = true;
            score = new Score(
                    getPlayerEvaluations(gameState, searchController.getSearchPlies())
                    , gameDidEnd ? getWinnerPlyIndex(gameEngine, gameState, searchController.getSearchPlies()) : -1
                    , searchController.getCurrentIntermediateScores());
        }
        else
        {
            searchController.pushIntermediateScores(getPlayerEvaluations(gameState, searchController.getSearchPlies()));
            searchController.nextPly();
            score = computeScore(depthLimit - 1);
            searchController.previousPly();
            searchController.popIntermediateScores();
        }
        
        if (depthLimit > 6 - searchController.getSearchPlies().length)
            Thread.sleep(0);
        
        gameEngine.undo(roundMoveInfo);
        
        return score;
    }
    
    private double[] getPlayerEvaluations(BombermanState gameState, SearchController.SearchPly[] searchPlies)
    {
        double[] evaluations = new double[searchPlies.length - 1];
        for (int i = 0; i < evaluations.length; i++)
            evaluations[i] = heuristic.evaluate(gameState, ((SearchController.PlayerSearchPly)searchPlies[i]).getPlayer());
        return evaluations;
    }
    
    private static int getWinnerPlyIndex(BombermanEngine gameEngine, BombermanState gameState, SearchController.SearchPly[] searchPlies)
    {
        for (int i = 0; i < searchPlies.length - 1; i++)
            if (gameEngine.playerDidWin(gameState, ((SearchController.PlayerSearchPly)searchPlies[i]).getPlayer()))
                return i;
        return -1;
    }
    
    
    protected static class SearchResult
    {
        private BombermanMove move;
        private boolean hasMoreDepthToSearch;
        
        private SearchResult(BombermanMove move, boolean hasMoreDepthToSearch)
        {
            this.move = move;
            this.hasMoreDepthToSearch = hasMoreDepthToSearch;
        }
        
        public BombermanMove getMove()
        {
            return move;
        }
        
        public boolean hasMoreDepthToSearch()
        {
            return hasMoreDepthToSearch;
        }
    }
    
    
    public static interface PlayerSelectionStrategyFactory
    {
        public PlayerSelectionStrategy create(BombermanState gameState, BombermanPlayer mainPlayer, BombermanPlayer[] allPlayers);
    }
    
    public static class NearestPlayerSelectionStrategyFactory implements PlayerSelectionStrategyFactory
    {
        @Override
        public PlayerSelectionStrategy create(BombermanState gameState, BombermanPlayer mainPlayer, BombermanPlayer[] allPlayers)
        {
            return new NearestPlayerSelectionStrategy(gameState, mainPlayer, allPlayers);
        }
    }
    
    public static class NearPlayersSelectionStrategyFactory implements PlayerSelectionStrategyFactory
    {
        private int distanceThreshold;
        
        public NearPlayersSelectionStrategyFactory()
        {
            this(NearPlayersSelectionStrategy.DEFAULT_DISTANCE_THRESHOLD);
        }
        
        public NearPlayersSelectionStrategyFactory(int distanceThreshold)
        {
            this.distanceThreshold = distanceThreshold;
        }
        
        @Override
        public PlayerSelectionStrategy create(BombermanState gameState, BombermanPlayer mainPlayer, BombermanPlayer[] allPlayers)
        {
            return new NearPlayersSelectionStrategy(gameState, mainPlayer, distanceThreshold);
        }
    }
    
    
    private static interface PlayerSelectionStrategy
    {
        public boolean selectPlayer(BombermanPlayer player);
    }
    
    private static class NearestPlayerSelectionStrategy implements PlayerSelectionStrategy
    {
        private BombermanPlayer nearestPlayer = null;
        
        public NearestPlayerSelectionStrategy(BombermanState gameState, BombermanPlayer mainPlayer, BombermanPlayer[] allPlayers)
        {
            BombermanShortestDistanceLookup distanceLookup = BombermanShortestDistanceLookupCache.getInstance().getLookup(gameState, mainPlayer.getRow(), mainPlayer.getColumn());
            int minimumDistance = Integer.MAX_VALUE;
            for (BombermanPlayer nextPlayer : allPlayers)
                if (nextPlayer != mainPlayer)
                {
                    int distance = distanceLookup.getDistance(nextPlayer.getRow(), nextPlayer.getColumn());
                    if (distance < minimumDistance)
                    {
                        minimumDistance = distance;
                        nearestPlayer = nextPlayer;
                    }
                }
            
            if (nearestPlayer == null)
            {
                for (BombermanPlayer nextPlayer : allPlayers)
                    if (nextPlayer != mainPlayer)
                    {
                        int distance = Math.abs(nextPlayer.getRow() - mainPlayer.getRow()) + Math.abs(nextPlayer.getColumn() - mainPlayer.getColumn());
                        if (distance < minimumDistance)
                        {
                            minimumDistance = distance;
                            nearestPlayer = nextPlayer;
                        }
                    }
            }
        }
        
        @Override
        public boolean selectPlayer(BombermanPlayer player)
        {
            return player == nearestPlayer;
        }
    }
    
    private static class NearPlayersSelectionStrategy implements PlayerSelectionStrategy
    {
        protected static final int DEFAULT_DISTANCE_THRESHOLD = 8;
        
        private BombermanShortestDistanceLookup distanceLookup;
        private int distanceThreshold;

        public NearPlayersSelectionStrategy(BombermanState gameState, BombermanPlayer mainPlayer, int distanceThreshold)
        {
            distanceLookup = BombermanShortestDistanceLookupCache.getInstance().getLookup(gameState, mainPlayer.getRow(), mainPlayer.getColumn());
            this.distanceThreshold = distanceThreshold;
        }
        
        @Override
        public boolean selectPlayer(BombermanPlayer player)
        {
            return distanceLookup.getDistance(player.getRow(), player.getColumn()) <= distanceThreshold;
        }
    }
}
