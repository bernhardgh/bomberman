package za.co.entelect.challenge.model.bomberman.moveinfo;

import za.co.entelect.challenge.model.bomberman.BombermanCell;
import za.co.entelect.challenge.model.bomberman.BombermanPlayer;

public class BombExplodeMoveEvent implements MoveEvent
{
    private BombermanCell cell;
    private int oldBombRadius, oldBombCountdown;
    private BombermanPlayer cellOwner;
    private int bombIndex;
    
    public BombExplodeMoveEvent(BombermanCell cell)
    {
        this.cell = cell;
        oldBombRadius = cell.getBombRadius();
        oldBombCountdown = cell.getBombCountdown();
        cellOwner = cell.getCellOwner();
        bombIndex = cellOwner.getBombIndex(cell);
    }

    @Override
    public void undo()
    {
        cell.setBomb(oldBombCountdown, oldBombRadius, cellOwner);
        cellOwner.removeBomb(cell, bombIndex);
    }
}
