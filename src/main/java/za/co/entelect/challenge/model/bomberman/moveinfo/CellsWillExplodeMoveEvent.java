package za.co.entelect.challenge.model.bomberman.moveinfo;

import za.co.entelect.challenge.model.bomberman.BombermanState;

public class CellsWillExplodeMoveEvent implements MoveEvent
{
    private BombermanState gameState;
    
    public CellsWillExplodeMoveEvent(BombermanState gameState)
    {
        this.gameState = gameState;
    }

    @Override
    public void undo()
    {
        gameState.getExplodingCells().clear();
    }
}
