package za.co.entelect.challenge.model.bomberman.heuristics;

import za.co.entelect.challenge.model.bomberman.BombermanPlayer;
import za.co.entelect.challenge.model.bomberman.BombermanState;

public abstract class BombermanHeuristic
{
    protected final double getBaseScore(BombermanState gameState, BombermanPlayer player)
    {
        double score = player.getScoreDouble();
        if ((player.isMainPlayer()) && (!player.isAlive()))
            score -= gameState.getPlayerKillPoints() * gameState.getPlayers().length;
        return score;
    }
    
    public abstract double evaluate(BombermanState gameState, BombermanPlayer player);
}
