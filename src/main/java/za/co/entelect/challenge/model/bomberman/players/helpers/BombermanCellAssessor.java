package za.co.entelect.challenge.model.bomberman.players.helpers;

import java.util.ArrayList;

import za.co.entelect.challenge.model.bomberman.BombermanCell;
import za.co.entelect.challenge.model.bomberman.BombermanEngine;
import za.co.entelect.challenge.model.bomberman.BombermanGrid;
import za.co.entelect.challenge.model.bomberman.BombermanCell.CellType;
import za.co.entelect.challenge.model.bomberman.BombermanPlayer;
import za.co.entelect.challenge.model.bomberman.BombermanState;

public class BombermanCellAssessor
{
    private static final int CELL_NOT_PROCESSED = 0, CELL_PROCESSED = 1;
    
    private ArrayList<BombermanCell> threatCells = new ArrayList<BombermanCell>(3);
    private ArrayList<BombermanCell> processedCells = new ArrayList<BombermanCell>();
    
    public boolean bombTriggerIsSuicide(BombermanPlayer player, BombermanGrid grid)
    {
        threatCells.clear();
        threatCells.add(player.getBombToTrigger());
        return getThreatForLocationAndRestoreCellsState(player.getRow(), player.getColumn(), grid) == player;
    }
    
    public BombermanPlayer getThreatForLocation(int row, int column, BombermanState gameState, int futureStepCount)
    {
        BombermanGrid grid = gameState.getGrid();
        threatCells.clear();
        for (BombermanPlayer nextPlayer : gameState.getPlayers())
            if ((!nextPlayer.isActive()) && (nextPlayer.hasBombToTrigger()) && (!BombermanMoveHelpers.bombTriggerIsSuicide(nextPlayer, grid, futureStepCount)))
                threatCells.add(nextPlayer.getBombToTrigger());
        return threatCells.isEmpty() ? null : getThreatForLocationAndRestoreCellsState(row, column, grid);
    }
    
    private BombermanPlayer getThreatForLocationAndRestoreCellsState(int row, int column, BombermanGrid grid)
    {
        processedCells.clear();
        try
        {
            return getThreatForLocation(row, column, grid);
        }
        finally
        {
            for (BombermanCell nextCell : processedCells)
                nextCell.tag = CELL_NOT_PROCESSED;
        }
    }
    
    private BombermanPlayer getThreatForLocation(int row, int column, BombermanGrid grid)
    {
        BombermanCell cell = grid.getCell(row, column);
        if ((cell.hasBomb()) && (threatCells.contains(cell)))
            return cell.getCellOwner();
        
        cell.tag = CELL_PROCESSED;
        processedCells.add(cell);
        
        for (int[] direction : BombermanEngine.DIRECTIONS)
        {
            int currentRow = row, currentColumn = column;
            int rowDirection = direction[0], columnDirection = direction[1];
            for (int distance = 1; ; distance++)
            {
                currentRow += rowDirection;
                currentColumn += columnDirection;
                
                cell = grid.getCell(currentRow, currentColumn);
                if ((cell.tag == CELL_PROCESSED) || (cell.getCellType() == CellType.INDESTRUCTIBLE_WALL) || (cell.getCellType() == CellType.DESTRUCTIBLE_WALL))
                    break;
                
                if (cell.hasBomb())
                {
                    if (distance <= cell.getBombRadius())
                    {
                        BombermanPlayer threatPlayer = getThreatForLocation(currentRow, currentColumn, grid);
                        if (threatPlayer != null)
                            return threatPlayer;
                    }
                    break;
                }
            }
        }
        
        return null;
    }
}
