package za.co.entelect.challenge.model.bomberman.moveinfo;

import za.co.entelect.challenge.model.bomberman.BombermanCell;
import za.co.entelect.challenge.model.bomberman.BombermanCell.CellType;
import za.co.entelect.challenge.model.bomberman.PowerUp.PowerUpType;
import za.co.entelect.challenge.model.bomberman.BombermanPlayer;
import za.co.entelect.challenge.model.bomberman.BombermanState;
import za.co.entelect.challenge.model.bomberman.PowerUp;

public class CellChangeMoveEvent implements MoveEvent
{
    protected BombermanCell cell;
    private CellType oldCellType;
    private BombermanPlayer oldOwner;
    
    private BombermanState gameStateOfExplodedCell = null;
    
    public CellChangeMoveEvent(BombermanCell cell)
    {
        this.cell = cell;
        this.oldCellType = cell.getCellType();
        this.oldOwner = cell.getCellOwner();
    }
    
    public void setIsExplodedCell(BombermanState gameStateOfExplodedCell)
    {
        this.gameStateOfExplodedCell = gameStateOfExplodedCell;
    }
    
    @Override
    public void undo()
    {
        if (gameStateOfExplodedCell != null)
        {
            gameStateOfExplodedCell.getExplodingCells().add(cell);
            
            if (cell.hasPowerUp())
            {
                PowerUp powerUp = new PowerUp(cell.getRow(), cell.getColumn(), cell.getCellType() == CellType.BOMB_BAG_POWER_UP ? PowerUpType.BOMB_BAG_POWER_UP : PowerUpType.BOMB_RADIUS_POWER_UP);
                gameStateOfExplodedCell.getGrid().restoreHiddenPowerUp(powerUp);
            }
        }
        
        cell.setCell(oldCellType, oldOwner);
    }
}
