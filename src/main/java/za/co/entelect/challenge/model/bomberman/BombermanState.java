package za.co.entelect.challenge.model.bomberman;

import java.util.ArrayList;
import java.util.List;

import za.co.entelect.challenge.model.GameState;
import za.co.entelect.challenge.model.bomberman.BombermanCell.CellType;
import za.co.entelect.challenge.util.ComputedHash;
import za.co.entelect.challenge.util.ObjectCode;

public class BombermanState extends GameState<BombermanCell>
{
    private static final long serialVersionUID = -268617458882630778L;
    
    private BombermanGrid grid;
    private BombermanPlayer[] players;
    private int gameRound = 0;
    private int maximumGameRounds;
    private int playerKillPoints;
    
    private List<BombermanCell> explodingCells = new ArrayList<BombermanCell>();
    
    private ObjectCode code = new ObjectCode();
    
    public BombermanState(BombermanGrid grid, BombermanPlayer... players)
    {
        this.grid = grid;
        this.players = players;
        
        maximumGameRounds = grid.getWidth() * grid.getHeight();
        
        int usableCellCount = grid.getUsableCellCount();
        for (BombermanPlayer nextPlayer : players)
            nextPlayer.setUsableCellCount(usableCellCount);
        
        int height = grid.getHeight(), width = grid.getWidth();
        for (int row = 0; row < height; row++)
            for (int column = 0; column < width; column++)
            {
                BombermanCell cell = grid.getCell(row, column);
                if (cell.getCellType() == CellType.BOMB_EXPLOSION)
                    explodingCells.add(cell);
            }
    }
    
    public void initializeCodeIntegration()
    {
        char minimumPlayerKey = players[0].getPlayerKey();
                
        for (BombermanPlayer nextPlayer : players)
        {
            nextPlayer.initializeCodes(code);
            
            if (nextPlayer.getPlayerKey() < minimumPlayerKey)
                minimumPlayerKey = nextPlayer.getPlayerKey();
        }
        
        int height = grid.getHeight(), width = grid.getWidth();
        for (int row = 1; row < height - 1; row++)
            for (int column = 1; column < width - 1; column++)
            {
                BombermanCell cell = grid.getCell(row, column);
                if (cell.getCellType() != CellType.INDESTRUCTIBLE_WALL)
                    cell.initializeCodes(code, minimumPlayerKey);
            }
    }
    
    public ComputedHash getComputedHash()
    {
        return code.getComputedHash();
    }
    
    @Override
    public BombermanGrid getGrid()
    {
        return grid;
    }
    
    @Override
    public BombermanPlayer[] getPlayers()
    {
        return players;
    }

    @Override
    public BombermanPlayer getPlayer(char playerKey)
    {
        return (BombermanPlayer)super.getPlayer(playerKey);
    }
    
    public boolean hasBombsOrExplosions()
    {
        if (!explodingCells.isEmpty())
            return true;
        
        for (BombermanPlayer nextPlayer : players)
            if (!nextPlayer.getCellsWithBombs().isEmpty())
                return true;
        return false;
    }
    
    public int getGameRound()
    {
        return gameRound;
    }
    
    public void setGameRound(int newGameRound)
    {
        gameRound = newGameRound;
    }
    
    public void incrementGameRound()
    {
        gameRound++;
    }
    
    public void decrementGameRound()
    {
        gameRound--;
    }
    
    public int getMaximumGameRounds()
    {
        return maximumGameRounds;
    }

    public int getPlayerKillPoints()
    {
        return playerKillPoints;
    }
    
    public void setPlayerKillPoints(int playerKillPoints)
    {
        this.playerKillPoints = playerKillPoints;
    }
    
    public List<BombermanCell> getExplodingCells()
    {
        return explodingCells;
    }
    
    @Override
    public int hashCode()
    {
        return code.hashCode();
    }
    
    @Override
    public boolean equals(Object other)
    {
        if (!(other instanceof BombermanState))
            return false;
        return code.equals(((BombermanState)other).code);
    }
    
    @Override
    public Object clone()
    {
        BombermanState copy = (BombermanState)super.clone();
        copy.grid = (BombermanGrid)grid.clone();
        
        copy.players = new BombermanPlayer[players.length];
        for (int i = 0; i < players.length; i++)
            copy.players[i] = (BombermanPlayer)players[i].clone();
        
        copy.explodingCells = new ArrayList<BombermanCell>();
        for (BombermanCell nextCell : explodingCells)
            copy.explodingCells.add(copy.grid.getCell(nextCell.getRow(), nextCell.getColumn()));
        
        copy.code = (ObjectCode)code.clone();
        copy.fixCellReferences();
        copy.fixPlayerReferences();
        
        return copy;
    }
    
    private void fixCellReferences()
    {
        int height = grid.getHeight(), width = grid.getWidth();
        for (int row = 0; row < height; row++)
            for (int column = 0; column < width; column++)
            {
                BombermanCell cell = grid.getCell(row, column);
                cell.resetGameStateCode(code);
                
                BombermanPlayer cellOwner = cell.getCellOwner();
                if (cellOwner != null)
                    cell.setCell(cell.getCellType(), (BombermanPlayer)getPlayer(cellOwner.getPlayerKey()));
            }
    }
    
    private void fixPlayerReferences()
    {
        for (BombermanPlayer nextPlayer : players)
        {
            List<BombermanCell> cells = nextPlayer.getCellsWithBombs();
            for (int i = 0; i < cells.size(); i++)
            {
                BombermanCell referenceCell = cells.get(i);
                nextPlayer.getCellsWithBombs().set(i, grid.getCell(referenceCell.getRow(), referenceCell.getColumn()));
            }
            
            nextPlayer.resetGameStateCode(code);
        }
    }
}
