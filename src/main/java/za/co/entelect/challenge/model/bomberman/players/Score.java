package za.co.entelect.challenge.model.bomberman.players;

import java.util.AbstractList;
import java.util.Arrays;

public class Score
{
    private static final double EPSILON = 1E-5;
    private static final double DECAY_FACTOR = 0.95;
    
    private PlayerScore[] playerScores;
    
    private Score() {}
    
    public Score(double[] scores, int winnerPlyIndex, AbstractList<double[]> intermediateScoresList)
    {
        double compoundedDenominator = 1;
        double[] compoundedEnumerators = new double[scores.length];
        for (int i = 0; i < compoundedEnumerators.length; i++)
            compoundedEnumerators[i] = scores[i];
        
        for (int scoresIndex = intermediateScoresList.size() - 1; scoresIndex >= 0; scoresIndex--)
        {
            compoundedDenominator = 1 + compoundedDenominator*DECAY_FACTOR;

            double[] intermediateScores = intermediateScoresList.get(scoresIndex);
            for (int i = 0; i < compoundedEnumerators.length; i++)
                compoundedEnumerators[i] = intermediateScores[i] + compoundedEnumerators[i]*DECAY_FACTOR;
        }
        
        playerScores = new PlayerScore[scores.length];
        for (int i = 0; i < scores.length; i++)
        {
            double leafScore = scores[i];
            double resolvedLeafScore = leafScore;
            for (int j = 0; j < scores.length; j++)
                if (i != j)
                    resolvedLeafScore -= scores[j];

            double resolvedCompoundedEnumerator = compoundedEnumerators[i];
            for (int j = 0; j < compoundedEnumerators.length; j++)
                if (i != j)
                    resolvedCompoundedEnumerator -= compoundedEnumerators[j];
            
            playerScores[i] = new PlayerScore(i == winnerPlyIndex, resolvedLeafScore, resolvedCompoundedEnumerator, compoundedDenominator);
        }
    }
    
    public boolean isLarger(Score other, int plyIndex)
    {
        return playerScores[plyIndex].isLarger(other.playerScores[plyIndex]);
    }
    
    public boolean hasWinner()
    {
        for (PlayerScore nextPlayerScore : playerScores)
            if (nextPlayerScore.isWinner())
                return true;
        return false;
    }
    
    public Score compound(int count)
    {
        Score compounded = new Score();
        compounded.playerScores = new PlayerScore[playerScores.length];
        for (int i = 0; i < playerScores.length; i++)
            compounded.playerScores[i] = playerScores[i].compound(count);
        return compounded;
    }
    
    @Override
    public String toString()
    {
        return "Score[playerScores=" + Arrays.toString(playerScores) + ']';
    }
    
    
    private static class PlayerScore
    {
        private boolean isWinner;
        private double resolvedLeafScore, resolvedCompoundedEnumerator;
        private double compoundedDenominator;
        
        public PlayerScore(boolean isWinner, double resolvedLeafScore, double resolvedCompoundedScore, double compoundedDenominator)
        {
            this.isWinner = isWinner;
            this.resolvedLeafScore = resolvedLeafScore;
            this.resolvedCompoundedEnumerator = resolvedCompoundedScore;
            this.compoundedDenominator = compoundedDenominator;
        }
        
        public boolean isWinner()
        {
            return isWinner;
        }
        
        public boolean isLarger(PlayerScore other)
        {
            if ((isWinner) || (other.isWinner))
            {
                if (!other.isWinner)
                    return true;
                if (!isWinner)
                    return false;
                return resolvedCompoundedEnumerator / compoundedDenominator > other.resolvedCompoundedEnumerator / other.compoundedDenominator;
            }
            
            return (Math.abs(resolvedLeafScore - other.resolvedLeafScore) < EPSILON) 
                    ? resolvedCompoundedEnumerator / compoundedDenominator > other.resolvedCompoundedEnumerator / other.compoundedDenominator 
                    : resolvedLeafScore > other.resolvedLeafScore;
        }
        
        public PlayerScore compound(int count)
        {
            double worstCaseInitialScore = Math.min(0, resolvedCompoundedEnumerator);
            double newResolvedCompoundedEnumerator = resolvedCompoundedEnumerator;
            double newCompoundedDenominator = compoundedDenominator;
            for (int i = 0; i < count; i++)
            {
                newResolvedCompoundedEnumerator = worstCaseInitialScore + newResolvedCompoundedEnumerator*DECAY_FACTOR;
                newCompoundedDenominator = 1 + newCompoundedDenominator*DECAY_FACTOR;
            }
            
            return new PlayerScore(isWinner, resolvedLeafScore, newResolvedCompoundedEnumerator, newCompoundedDenominator);
        }
        
        @Override
        public String toString()
        {
            return "[" + (isWinner ? "isWinner; " : "") + resolvedLeafScore + "; " + (resolvedCompoundedEnumerator / compoundedDenominator) + "]";
        }
    }
}
