package za.co.entelect.challenge.model;

import java.io.Serializable;
import java.lang.reflect.Array;

public class Grid<CellType extends Cell> implements Cloneable, Serializable
{
    private static final long serialVersionUID = -269665610693661207L;
    
    private CellType[][] board;
    private int height, width;
    
    @SuppressWarnings("unchecked")
    public Grid(int height, int width, CellType clearCell)
    {
        board = (CellType[][]) Array.newInstance(clearCell.getClass(), height, width);
        for (int r = 0; r < height; r++)
            for (int c = 0; c < width; c++)
            {
                CellType cell = (CellType) clearCell.clone();
                cell.setRowAndColumn(r, c);
                board[r][c] = cell;
            }
        this.height = height;
        this.width = width;
    }
    
    public int getHeight()
    {
        return height;
    }
    
    public int getWidth()
    {
        return width;
    }
    
    public CellType getCell(int row, int col)
    {
        return board[row][col];
    }
    
    @SuppressWarnings("unchecked")
    public Object clone()
    {
        try
        {
            Grid<CellType> clone = (Grid<CellType>)super.clone();
            clone.board = (CellType[][])board.clone();
            for (int r = 0; r < height; r++)
            {
                clone.board[r] = (CellType[])board[r].clone();
                for (int c = 0; c < width; c++)
                    clone.board[r][c] = (CellType)board[r][c].clone();
            }
            return clone;
        } 
        catch (CloneNotSupportedException e) {}
        return null;
    }
    
    public String toString()
    {
        StringBuilder sb = new StringBuilder(height * (width + 1));
        for (int r = 0; r < height; r++)
        {
            for (int c = 0; c < width; c++)
                sb.append(board[r][c].getCharacterCode());
            sb.append('\n');
        }
        return sb.toString();
    }
}
