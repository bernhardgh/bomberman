package za.co.entelect.challenge.model.bomberman.players;

import java.util.Map;

import za.co.entelect.challenge.logging.Logger;
import za.co.entelect.challenge.logging.Logger.LogLevel;
import za.co.entelect.challenge.model.GameEngine;
import za.co.entelect.challenge.model.GamePlayer;
import za.co.entelect.challenge.model.GameState;
import za.co.entelect.challenge.model.Move;
import za.co.entelect.challenge.model.MoveInfo;
import za.co.entelect.challenge.model.Player;
import za.co.entelect.challenge.model.bomberman.BombermanCell;
import za.co.entelect.challenge.model.bomberman.BombermanEngine;
import za.co.entelect.challenge.model.bomberman.BombermanGrid;
import za.co.entelect.challenge.model.bomberman.BombermanMove;
import za.co.entelect.challenge.model.bomberman.BombermanPlayer;
import za.co.entelect.challenge.model.bomberman.BombermanState;
import za.co.entelect.challenge.model.bomberman.BombermanCell.CellType;
import za.co.entelect.challenge.model.bomberman.heuristics.BombermanHeuristic;
import za.co.entelect.challenge.model.bomberman.players.helpers.BombermanCellAssessor;
import za.co.entelect.challenge.model.bomberman.players.helpers.BombermanCellHelpers;
import za.co.entelect.challenge.model.bomberman.players.helpers.BombermanMoveHelpers;
import za.co.entelect.challenge.model.bomberman.players.helpers.BombermanShortestDistanceLookup;
import za.co.entelect.challenge.model.bomberman.players.helpers.BombermanShortestDistanceLookupCache;
import za.co.entelect.challenge.util.ComputedHash;
import za.co.entelect.challenge.util.FixedSizeHashMap;

public class AlphaBetaBombermanPlayer extends GamePlayer<BombermanCell>
{
    private static final double NO_APIRATION_SCORE = Double.NEGATIVE_INFINITY;
    private static final double ASPIRATION_EPSILON = 5.8;
    
    private static final double MOVE_STEP_EPSILON = 1E-8;
    
    private static final BombermanMove[] ORDERED_MOVES_WALL_IN_RANGE = new BombermanMove[] { 
        BombermanMove.PLACE_BOMB, BombermanMove.UP, BombermanMove.RIGHT, BombermanMove.DOWN, BombermanMove.LEFT, BombermanMove.TRIGGER_BOMB, BombermanMove.DO_NOTHING
    };
    private static final BombermanMove[] ORDERED_MOVES_NO_WALL_IN_RANGE = new BombermanMove[] { 
        BombermanMove.UP, BombermanMove.RIGHT, BombermanMove.DOWN, BombermanMove.LEFT, BombermanMove.TRIGGER_BOMB, BombermanMove.DO_NOTHING, BombermanMove.PLACE_BOMB
    };
    private static final BombermanMove[] ORDERED_MOVES_WAIT = new BombermanMove[] {
        BombermanMove.DO_NOTHING, BombermanMove.TRIGGER_BOMB, BombermanMove.UP, BombermanMove.RIGHT, BombermanMove.DOWN, BombermanMove.LEFT 
    };

    private static final Logger logger = Logger.getInstance();

    private BombermanState gameState;
    private BombermanPlayer player, opponent;
    private BombermanHeuristic heuristic;
    private BombermanCellAssessor assessor;
    private Map<ComputedHash, CacheEntry> playerScoreCache = new FixedSizeHashMap<>(100000), opponentScoreCache = new FixedSizeHashMap<>(100000);
    private int nodeCount;
    private boolean didCutOffSearch;
    
    public AlphaBetaBombermanPlayer(Player player, BombermanHeuristic heuristic)
    {
        super(new BombermanEngine(), player);
        this.heuristic = heuristic;
    }

    @Override
    public Move getNextMove(GameState<BombermanCell> gameState) throws InterruptedException
    {
        BombermanMove immediateMove = initializeSearch((BombermanState)gameState.clone());
        if (immediateMove != null)
            return immediateMove;

        int depthReached = 0;
        try
        {
            double previousScore = NO_APIRATION_SCORE;
            for (int maxDepth = 4; ; maxDepth++)
            {
                logger.log(LogLevel.TRACE, "=== Depth Limit %d ===", maxDepth);
                
                SearchResult result = computeMove(maxDepth, previousScore);
                setPreliminaryMove(result.getMove());
                previousScore = result.getScore();
                
                depthReached = maxDepth;
                if (!result.hasMoreDepthToSearch())
                    break;
                
                Thread.sleep(0);
            }
        }
        finally
        {
            logger.log(LogLevel.INFO, "==== Depth reached: %d ====", depthReached);
            logger.log(LogLevel.INFO, "==== Node count: %d ====", nodeCount);
        }
        
        return getPreliminaryMove();
    }
    
    protected int getNodeCount()
    {
        return nodeCount;
    }

    protected BombermanMove initializeSearch(BombermanState startState)
    {
        gameState = startState;
        player = gameState.getPlayer(((BombermanPlayer)getPlayer()).getPlayerKey());
        if (!player.isAlive())
            return BombermanMove.DO_NOTHING;
        
        player.setMainPlayer();
        player.setIsActive(true);

        opponent = getNearestPlayer(gameState, player);
        if ((opponent == null) && (getGameEngine().playerDidWin(gameState, player)))
            return BombermanMove.DO_NOTHING;
        opponent.setIsActive(true);

        boolean hasInactivePlayers = false;
        for (BombermanPlayer nextPlayer : gameState.getPlayers())
            if ((nextPlayer != player) && (nextPlayer != opponent) && (nextPlayer.isAlive()))
            {
                nextPlayer.setIsActive(false);
                hasInactivePlayers = true;
            }
        assessor = hasInactivePlayers ? new BombermanCellAssessor() : null;
        
        playerScoreCache.clear();
        opponentScoreCache.clear();
        
        nodeCount = 0;
        return null;
    }

    protected SearchResult computeMove(int depthLimit) throws InterruptedException
    {
        return computeMove(depthLimit, NO_APIRATION_SCORE);
    }

    protected SearchResult computeMove(int depthLimit, double previousScore) throws InterruptedException
    {
        didCutOffSearch = false;
        PlayerPly playerPly = new PlayerPly(player), opponentPly = new PlayerPly(opponent);
        double score;
        if (previousScore == NO_APIRATION_SCORE)
            score = computeScore(playerPly, opponentPly, depthLimit, 0);
        else
        {
            // Aspiration search.
            double alpha = previousScore - ASPIRATION_EPSILON, beta = previousScore + ASPIRATION_EPSILON;
            while (true)
            {
                score = computeScore(playerPly, opponentPly, alpha, beta, depthLimit, 0);
                if (score <= alpha) // Fail-low
                    alpha = Double.NEGATIVE_INFINITY;
                else if (score >= beta) // Fail-high
                    beta = Double.POSITIVE_INFINITY;
                else
                    break;
            }
        }
        
        return new SearchResult(playerPly.getBestMove(), score, (didCutOffSearch) && (depthLimit < 50) && (Math.abs(score) <= 1000));
    }
    
    private double computeScore(PlayerPly playerPly, PlayerPly opponentPly, int depthLimit, int currentPlyDepth) throws InterruptedException
    {
        return computeScore(playerPly, opponentPly, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, depthLimit, currentPlyDepth);
    }
    
    private double computeScore(PlayerPly playerPly, PlayerPly opponentPly, double alpha, double beta, int depthLimit, int currentPlyDepth) throws InterruptedException
    {
        BombermanPlayer player = playerPly.getPlayer(), opponent = opponentPly.getPlayer();
        boolean isNextRoundPly = !player.isMainPlayer();
        GameEngine<BombermanCell> gameEngine = getGameEngine();
        
        if (!isNextRoundPly)
        {
            boolean gameDidEnd = gameEngine.gameDidEnd(gameState);
            if ((depthLimit == 0) || (gameDidEnd) || (!player.isAlive()))
            {
                if (depthLimit == 0)
                    didCutOffSearch = true;
                return getEvaluation(player, opponent, gameDidEnd);
            }
        }

        Map<ComputedHash, CacheEntry> scoreCache = null;
        ComputedHash gameStateHash = null;
        boolean isRoot = currentPlyDepth == 0;
        if ((!isRoot) && (depthLimit >= 3))
        {
            scoreCache = player.isMainPlayer() ? playerScoreCache : opponentScoreCache;
            gameStateHash = gameState.getComputedHash();
            CacheEntry cacheEntry = scoreCache.get(gameStateHash);
            if ((cacheEntry != null) && (cacheEntry.getDepth() >= depthLimit))
            {
                double cachedScore = cacheEntry.getScore();
                if ((cacheEntry.getEntryType() == CacheEntry.EntryType.EXACT) ||
                        ((cacheEntry.getEntryType() == CacheEntry.EntryType.LOWER_BOUND) && (cachedScore >= beta)) ||
                        ((cacheEntry.getEntryType() == CacheEntry.EntryType.UPPER_BOUND) && (cachedScore <= alpha)))
                    return cachedScore;
            }
        }
        
        if (currentPlyDepth <= 2)
            playerPly.setDeeperBestMove(null);

        int playerPowerUpSum = player.getBombBagSize() + player.getBombRadius();
        BombermanMove lastMove = playerPly.getLastMove();
        boolean didGetPowerUp = playerPly.didGetPowerUp();
        boolean hasBombsOrExplosions = gameState.hasBombsOrExplosions();
        
        int nextDepthLimit = isNextRoundPly ? depthLimit - 1 : depthLimit;
        int nextPlyDepth = currentPlyDepth + 1;
        boolean scoreIsExact = false;
        BombermanMove bestMove = null;
        for (BombermanMove nextMove : getOrderedMoves(gameState.getGrid(), player))
            if (((didGetPowerUp) || (!BombermanMoveHelpers.movesAreOpposite(lastMove, nextMove)))
                    && ((nextMove != BombermanMove.DO_NOTHING) || (!player.isAlive()) || (hasBombsOrExplosions))
                    && ((nextMove != BombermanMove.TRIGGER_BOMB) || ((player.hasBombToTrigger()) && (!(new BombermanCellAssessor()).bombTriggerIsSuicide(player, gameState.getGrid())))))
            {
                boolean moveValid = gameEngine.isMoveValid(gameState, player, nextMove);
                boolean opponentIsClashing = false;
                if ((isNextRoundPly) && (!moveValid) && (player.isAlive()) && (BombermanMoveHelpers.isMovementMove(nextMove)))
                {
                    BombermanCell destinationCell = BombermanCellHelpers.getDestinationCell(gameState.getGrid(), player, nextMove);
                    if ((destinationCell.getCellType() == CellType.PLAYER) && (destinationCell.getCellOwner() == opponent) && (BombermanMoveHelpers.isMovementMove(opponentPly.getLastMove())))
                        opponentIsClashing = true;
                }
                
                if ((!moveValid) && (!opponentIsClashing))
                    continue;
                
                MoveInfo moveInfo = opponentIsClashing ? null : gameEngine.applyMove(gameState, player, nextMove);
                BombermanPlayer threatPlayer = assessor == null ? null : assessor.getThreatForLocation(player.getRow(), player.getColumn(), gameState, currentPlyDepth / 2);
                MoveInfo threatMoveInfo = threatPlayer == null ? null : gameEngine.applyMove(gameState, threatPlayer, BombermanMove.TRIGGER_BOMB);
                MoveInfo advanceRoundInfo = null;
                if (isNextRoundPly)
                {
                    advanceRoundInfo = gameEngine.advanceRound(gameState);
                    nodeCount++;
                }
                playerPly.setLastMove(moveInfo == null ? BombermanMove.DO_NOTHING : nextMove);
                playerPly.setDidGetPowerUp(player.getBombBagSize() + player.getBombRadius() > playerPowerUpSum);
                double moveScore = opponentIsClashing
                        ? -computeScore(opponentPly, playerPly, nextDepthLimit, nextPlyDepth)
                        : -computeScore(opponentPly, playerPly, -beta, -alpha, nextDepthLimit, nextPlyDepth);
                if (advanceRoundInfo != null)
                    gameEngine.undo(advanceRoundInfo);
                if (threatMoveInfo != null)
                    gameEngine.undo(threatMoveInfo);
                if (moveInfo != null)
                    gameEngine.undo(moveInfo);
                
                if (opponentIsClashing)
                {
                    MoveInfo opponentMoveInfo = ((BombermanEngine)gameEngine).applyMove(gameState, opponent, BombermanMoveHelpers.getOppositeDirectionMove(opponentPly.getLastMove()), false);
                    BombermanMove opponentLastMove = opponentPly.getLastMove();
                    opponentPly.setLastMove(BombermanMove.DO_NOTHING);
                    moveInfo = gameEngine.applyMove(gameState, player, nextMove);
                    threatPlayer = assessor == null ? null : assessor.getThreatForLocation(player.getRow(), player.getColumn(), gameState, currentPlyDepth / 2);
                    threatMoveInfo = threatPlayer == null ? null : gameEngine.applyMove(gameState, threatPlayer, BombermanMove.TRIGGER_BOMB);
                    advanceRoundInfo = gameEngine.advanceRound(gameState);
                    playerPly.setLastMove(nextMove);
                    playerPly.setDidGetPowerUp(player.getBombBagSize() + player.getBombRadius() > playerPowerUpSum);
                    double newAlpha = 2*alpha - moveScore, newBeta = 2*beta - moveScore;
                    moveScore = (moveScore - computeScore(opponentPly, playerPly, -newBeta, -newAlpha, nextDepthLimit, nextPlyDepth)) / 2;
                    gameEngine.undo(advanceRoundInfo);
                    if (threatMoveInfo != null)
                        gameEngine.undo(threatMoveInfo);
                    gameEngine.undo(moveInfo);
                    gameEngine.undo(opponentMoveInfo);
                    opponentPly.setLastMove(opponentLastMove);
                }
                
                if (isRoot)
                    logger.log(LogLevel.TRACE, "%s (%d): %f", nextMove, depthLimit, moveScore);
                
                if (moveScore >= beta)
                {
                    if (gameStateHash != null)
                        scoreCache.put(gameStateHash, new CacheEntry(moveScore, depthLimit, CacheEntry.EntryType.LOWER_BOUND));
                    playerPly.setLastMove(lastMove);
                    playerPly.setDidGetPowerUp(didGetPowerUp);
                    if (isRoot)
                        playerPly.setBestMove(bestMove);
                    else if ((currentPlyDepth == 2) && (bestMove != null))
                        playerPly.setDeeperBestMove(bestMove);
                    return moveScore;
                }
                boolean setMoveAsBest = isRoot ? (bestMove == null ? (moveScore >= alpha) : (moveScore > alpha + MOVE_STEP_EPSILON)) : (moveScore > alpha);
                if ((isRoot) && (setMoveAsBest) && (nextMove == BombermanMove.DO_NOTHING) && (bestMove != null) && (moveScore < alpha + 200))
                {
                    BombermanMove deeperBestMove = opponentPly.getDeeperBestMove();
                    if ((deeperBestMove != null) && (deeperBestMove != BombermanMove.PLACE_BOMB) && (BombermanCellHelpers.getDestinationCell(gameState.getGrid(), player, deeperBestMove).getCellType() != CellType.BOMB_EXPLOSION))
                        setMoveAsBest = false;
                }
                if (setMoveAsBest)
                {
                    alpha = moveScore;
                    bestMove = nextMove;
                    scoreIsExact = true;
                    
                    if ((currentPlyDepth == 1) && (opponentPly.getDeeperBestMove() != null))
                        playerPly.setDeeperBestMove(opponentPly.getDeeperBestMove());
                }
            }
        
        if ((isNextRoundPly) && (depthLimit > 3))
            Thread.sleep(0);

        if (gameStateHash != null)
            scoreCache.put(gameStateHash, new CacheEntry(alpha, depthLimit, scoreIsExact ? CacheEntry.EntryType.EXACT : CacheEntry.EntryType.UPPER_BOUND));
        
        playerPly.setLastMove(lastMove);
        playerPly.setDidGetPowerUp(didGetPowerUp);
        if (isRoot)
            playerPly.setBestMove(bestMove);
        else if ((currentPlyDepth == 2) && (bestMove != null))
            playerPly.setDeeperBestMove(bestMove);
        return alpha;
    }
    
    private double getEvaluation(BombermanPlayer player, BombermanPlayer opponent, boolean gameDidEnd)
    {
        double evaluation = heuristic.evaluate(gameState, player) - heuristic.evaluate(gameState, opponent);
        
        if (gameDidEnd)
        {
            GameEngine<BombermanCell> gameEngine = getGameEngine();
            if (gameEngine.playerDidWin(gameState, player))
                evaluation += 1000000;
            if (gameEngine.playerDidWin(gameState, opponent))
                evaluation -= 1000000;
        }
        return evaluation;
    }
    
    private static BombermanMove[] getOrderedMoves(BombermanGrid grid, BombermanPlayer player)
    {
        if ((player.getAvailableBombCount() == 0) && (player.getCellsWithBombs().size() < player.getBombBagSize()))
            return ORDERED_MOVES_WAIT;
        return BombermanCellHelpers.hasWallInRange(grid, player) ? ORDERED_MOVES_WALL_IN_RANGE : ORDERED_MOVES_NO_WALL_IN_RANGE;
    }
    
    
    private static BombermanPlayer getNearestPlayer(BombermanState gameState, BombermanPlayer player)
    {
        BombermanShortestDistanceLookup lookup = BombermanShortestDistanceLookupCache.getInstance().getLookup(gameState, player.getRow(), player.getColumn());
        int closestDistance = Integer.MAX_VALUE;
        BombermanPlayer nearestPlayer = null;
        for (BombermanPlayer nextPlayer : gameState.getPlayers())
            if ((nextPlayer != player) && (nextPlayer.isAlive()))
            {
                int distance = lookup.getDistance(nextPlayer.getRow(), nextPlayer.getColumn());
                if (distance < closestDistance)
                {
                    nearestPlayer = nextPlayer;
                    closestDistance = distance;
                }
            }
        
        if (nearestPlayer == null)
        {
            for (BombermanPlayer nextPlayer : gameState.getPlayers())
                if ((nextPlayer != player) && (nextPlayer.isAlive()))
                {
                    int distance = Math.abs(nextPlayer.getRow() - player.getRow()) + Math.abs(nextPlayer.getColumn() - player.getColumn());
                    if (distance < closestDistance)
                    {
                        nearestPlayer = nextPlayer;
                        closestDistance = distance;
                    }
                }
        }
        
        return nearestPlayer;
    }
    
    
    protected static class SearchResult
    {
        private BombermanMove move;
        private double score;
        private boolean hasMoreDepthToSearch;
        
        private SearchResult(BombermanMove move, double score, boolean hasMoreDepthToSearch)
        {
            this.move = move;
            this.score = score;
            this.hasMoreDepthToSearch = hasMoreDepthToSearch;
        }
        
        public BombermanMove getMove()
        {
            return move;
        }
        
        public double getScore()
        {
            return score;
        }
        
        public boolean hasMoreDepthToSearch()
        {
            return hasMoreDepthToSearch;
        }
    }
    
    
    private static class CacheEntry
    {
        public enum EntryType { EXACT, LOWER_BOUND, UPPER_BOUND };
        
        private double score;
        private int depth;
        private EntryType entryType;
        
        public CacheEntry(double score, int depth, EntryType entryType)
        {
            this.score = score;
            this.depth = depth;
            this.entryType = entryType;
        }
        
        public double getScore()
        {
            return score;
        }
        
        public int getDepth()
        {
            return depth;
        }
        
        public EntryType getEntryType()
        {
            return entryType;
        }
    }
    
    
    private static class PlayerPly
    {
        private BombermanPlayer player;
        private BombermanMove bestMove, deeperBestMove;
        private BombermanMove lastMove = null;
        private boolean didGetPowerUp = false;
        
        public PlayerPly(BombermanPlayer player)
        {
            this.player = player;
        }
        
        public BombermanPlayer getPlayer()
        {
            return player;
        }
        
        public void setBestMove(BombermanMove newBestMove)
        {
            bestMove = newBestMove;
        }
        
        public BombermanMove getBestMove()
        {
            return bestMove;
        }
        
        public void setDeeperBestMove(BombermanMove newBestMove)
        {
            deeperBestMove = newBestMove;
        }
        
        public BombermanMove getDeeperBestMove()
        {
            return deeperBestMove;
        }
        
        public void setLastMove(BombermanMove lastMove)
        {
            this.lastMove = lastMove;
        }
        
        public BombermanMove getLastMove()
        {
            return lastMove;
        }
        
        public void setDidGetPowerUp(boolean flag)
        {
            didGetPowerUp = flag;
        }
        
        public boolean didGetPowerUp()
        {
            return didGetPowerUp;
        }
    }
}
