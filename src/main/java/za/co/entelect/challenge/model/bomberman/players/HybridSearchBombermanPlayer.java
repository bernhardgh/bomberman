package za.co.entelect.challenge.model.bomberman.players;

import za.co.entelect.challenge.model.GamePlayer;
import za.co.entelect.challenge.model.GameState;
import za.co.entelect.challenge.model.HybridGamePlayer;
import za.co.entelect.challenge.model.bomberman.BombermanCell;
import za.co.entelect.challenge.model.bomberman.BombermanCell.CellType;
import za.co.entelect.challenge.model.bomberman.players.helpers.BombermanShortestDistanceLookup;
import za.co.entelect.challenge.model.bomberman.players.helpers.BombermanShortestDistanceLookupCache;
import za.co.entelect.challenge.model.bomberman.BombermanGrid;
import za.co.entelect.challenge.model.bomberman.BombermanPlayer;
import za.co.entelect.challenge.model.bomberman.BombermanState;

public class HybridSearchBombermanPlayer extends HybridGamePlayer<BombermanCell>
{
    private static final int DEFAULT_PLAYER_DISTANCE_THRESHOLD = 7;
    private static final int DESTRUCTIBLE_WALL_COUNT_THRESHOLD = 8;
    
    private int playerDistanceThreshold = DEFAULT_PLAYER_DISTANCE_THRESHOLD;
    
    public HybridSearchBombermanPlayer(GamePlayer<BombermanCell> singlePlySearchGamePlayer, GamePlayer<BombermanCell> multiPlySearchGamePlayer)
    {
        super(singlePlySearchGamePlayer, multiPlySearchGamePlayer);
    }
    
    public int getPlayerDistanceThreshold()
    {
        return playerDistanceThreshold;
    }
    
    public void setPlayerDistanceThreshold(int newThreshold)
    {
        playerDistanceThreshold = newThreshold;
    }

    @Override
    protected boolean usePrimaryPlayer(GameState<BombermanCell> gameState)
    {
        BombermanState bombermanState = (BombermanState)gameState;
        BombermanPlayer player = (BombermanPlayer)getPlayer();
        BombermanShortestDistanceLookup lookup = BombermanShortestDistanceLookupCache.getInstance().getLookup(bombermanState, player.getRow(), player.getColumn());
        for (BombermanPlayer nextPlayer : bombermanState.getPlayers())
            if ((nextPlayer != player) && (nextPlayer.isAlive()) && (lookup.getDistance(nextPlayer.getRow(), nextPlayer.getColumn()) <= playerDistanceThreshold))
                return false;
        
        int destructibleWallCount = 0;
        BombermanGrid grid = bombermanState.getGrid();
        int width = grid.getWidth(), height = grid.getHeight();
        for (int row = 1; row < height - 1; row++)
            for (int column = 1; column < width - 1; column++)
                if (grid.getCell(row, column).getCellType() == CellType.DESTRUCTIBLE_WALL)
                    destructibleWallCount++;
        
        return destructibleWallCount > DESTRUCTIBLE_WALL_COUNT_THRESHOLD;
    }
}
