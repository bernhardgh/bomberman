package za.co.entelect.challenge.model.bomberman.players;

import za.co.entelect.challenge.model.GamePlayer;
import za.co.entelect.challenge.model.GameState;
import za.co.entelect.challenge.model.Move;
import za.co.entelect.challenge.model.Player;
import za.co.entelect.challenge.model.bomberman.BombermanCell;
import za.co.entelect.challenge.model.bomberman.BombermanEngine;
import za.co.entelect.challenge.model.bomberman.BombermanMove;
import za.co.entelect.challenge.model.bomberman.BombermanPlayer;

public class HumanBombermanPlayer extends GamePlayer<BombermanCell>
{
    public HumanBombermanPlayer(Player player)
    {
        super(new BombermanEngine(), player);
    }
    
    @Override
    public boolean isHumanPlayer()
    {
        return true;
    }

    @Override
    public Move getNextMove(GameState<BombermanCell> gameState) throws InterruptedException
    {
        if (!((BombermanPlayer)getPlayer()).isAlive())
            return BombermanMove.DO_NOTHING;
        
        while ((getPreliminaryMove() == null) || (!getGameEngine().isMoveValid(gameState, getPlayer(), getPreliminaryMove())))
        {
            synchronized(this)
            {
                wait();
            }
        }
        Move moveToReturn = getPreliminaryMove();
        clearPreliminaryMove();
        return moveToReturn;
    }
    
    @Override
    public void setPreliminaryMove(Move move)
    {
        super.setPreliminaryMove(move);
        
        (new Thread() {
            public void run()
            {
                try
                {
                    // This method is called when a key-event is handled by the UI-thread. Wait a while to give all the key event handlers enough
                    // time to finish handling the event before the next player will move. In this way we prevent that the same key event is
                    // processed on the next player.
                    Thread.sleep(100);
                    synchronized(HumanBombermanPlayer.this)
                    {
                        HumanBombermanPlayer.this.notify();
                    }
                } 
                catch (InterruptedException e) {}
            }
        }).start();
    }
}
