package za.co.entelect.challenge.model.bomberman.moveinfo;

import java.util.List;

import za.co.entelect.challenge.model.MoveInfo;

public class BombermanMoveInfo implements MoveInfo
{
    public static final BombermanMoveInfo EMPTY = new BombermanMoveInfo(); 
    
    private List<MoveEvent> moveEvents = null;
    
    private BombermanMoveInfo() {}
    
    public BombermanMoveInfo(List<MoveEvent> moveEvents)
    {
        this.moveEvents = moveEvents;
    }
    
    public void addMoveEvent(MoveEvent moveEvent)
    {
        if (moveEvent != null)
            moveEvents.add(moveEvent);
    }

    @Override
    public BombermanMoveInfo concatenate(MoveInfo other)
    {
        BombermanMoveInfo otherBombermanMoveInfo = (BombermanMoveInfo)other;
        if (moveEvents == null)
            return otherBombermanMoveInfo;
        
        if (otherBombermanMoveInfo.moveEvents != null)
            moveEvents.addAll(otherBombermanMoveInfo.moveEvents);
        
        return this;
    }

    @Override
    public void undo()
    {
        if (moveEvents != null)
        {
            for (int i = moveEvents.size() - 1; i >= 0; i--)
                moveEvents.get(i).undo();
        }
    }
}
