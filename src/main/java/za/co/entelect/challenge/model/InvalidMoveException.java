package za.co.entelect.challenge.model;

public class InvalidMoveException extends RuntimeException
{
    private static final long serialVersionUID = -5586640111152068477L;

    public InvalidMoveException(String message)
    {
        super(message);
    }
}
