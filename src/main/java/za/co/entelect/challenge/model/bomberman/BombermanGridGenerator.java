package za.co.entelect.challenge.model.bomberman;

import java.util.Random;

import za.co.entelect.challenge.model.bomberman.BombermanCell.CellType;
import za.co.entelect.challenge.model.bomberman.PowerUp.PowerUpType;

public class BombermanGridGenerator
{
    private static final Random generator = new Random();
    
    private int playerCount;
    private BombermanGrid grid;
    
    public BombermanGridGenerator(int height, int width, BombermanPlayer... players)
    {
        playerCount = players.length;
        grid = new BombermanGrid(height, width);

        placePlayers(players);
        placeWallAroundPlayers(players);
        placeDestructibleWalls(players);
        placeSuperPowerUp();
        addPowerUps(players.length);
    }
    
    public BombermanGrid getBombermanGrid()
    {
        return grid;
    }
    
    public int getPlayerKillPoints()
    {
        int destructibleWallCount = 0;
        for (int row = 1; row < grid.getHeight() - 1; row++)
            for (int column = 1; column < grid.getWidth() - 1; column++)
                if (grid.getCell(row, column).getCellType() == CellType.DESTRUCTIBLE_WALL)
                    destructibleWallCount++;
        
        return (100 + destructibleWallCount*10) / playerCount;
    }

    private void placePlayers(BombermanPlayer[] players)
    {
        for (int i = 0; i < players.length; i++)
        {
            int row = (i == 0) || (i == 3) ? 1 : grid.getHeight() - 2;
            int column = i % 2 == 0 ? 1 : grid.getWidth() - 2;
            grid.getCell(row, column).performCellChange(players[i]);
        }
    }
    
    private void placeWallAroundPlayers(BombermanPlayer[] players)
    {
        for (int i = 0; i < players.length; i++)
            for (int rowDelta = -3; rowDelta <= 3; rowDelta += 3)
                for (int columnDelta = -3; columnDelta <= 3; columnDelta += 3)
                    if (Math.abs(rowDelta - columnDelta) == 3)
                    {
                        int wallRow = players[i].getRow() + rowDelta, wallColumn = players[i].getColumn() + columnDelta;
                        if ((wallRow >= 0) && (wallRow < grid.getHeight()) && (wallColumn >= 0) && (wallColumn < grid.getWidth()))
                            grid.getCell(wallRow, wallColumn).setCell(CellType.DESTRUCTIBLE_WALL);
                    }
    }
    
    private void placeDestructibleWalls(BombermanPlayer[] players)
    {
        int quadrantWidth = grid.getWidth()/2 + 1, quadrantHeight = grid.getHeight()/2 + 1;
        for (int row = 1; row < quadrantWidth; row++)
        {
            NEXT_CELL:
            for (int column = 1; column < quadrantHeight; column++)
                if ((grid.getCell(row, column).getCellType() == CellType.CLEAR) && (generator.nextInt(100) < 35))
                {
                    for (BombermanPlayer nextPlayer : players)
                        if (isInSafeZone(row, column, nextPlayer))
                            break NEXT_CELL;
                    
                    grid.getCell(row, column).setCell(CellType.DESTRUCTIBLE_WALL);
                    grid.getCell(row, grid.getWidth() - 1 - column).setCell(CellType.DESTRUCTIBLE_WALL);
                    grid.getCell(grid.getHeight() - 1 - row, column).setCell(CellType.DESTRUCTIBLE_WALL);
                    grid.getCell(grid.getHeight() - 1 - row, grid.getWidth() - 1 - column).setCell(CellType.DESTRUCTIBLE_WALL);
                }
        }
    }
    
    private static boolean isInSafeZone(int row, int column, BombermanPlayer player)
    {
        return ((player.getRow() == row) && (Math.abs(player.getColumn() - column) <= 1)) || ((player.getColumn() == column) && (Math.abs(player.getRow() - row) <= 1));
    }
    
    private void placeSuperPowerUp()
    {
        int centerRow = grid.getHeight() / 2, centerColumn = grid.getWidth() / 2;
        grid.getCell(centerRow, centerColumn).setCell(CellType.SUPER_POWER_UP);
        for (int row = centerRow - 2; row <= centerRow + 2; row++)
            for (int column = centerColumn - 2; column <= centerColumn + 2; column++)
            {
                BombermanCell cell = grid.getCell(row, column);
                if (cell.getCellType() == CellType.CLEAR)
                    cell.setCell(CellType.DESTRUCTIBLE_WALL);
            }
    }
    
    private void addPowerUps(int playerCount)
    {
        int bombBagPowerUpCountPerQuadrant = playerCount / 2;
        int bombRadiusPowerUpCountPerQuadrant = playerCount;
        int quadrantHeight = grid.getHeight() / 2, quadrantWidth = grid.getWidth() / 2;
        
        addPowerUps(0, quadrantHeight, 0, quadrantWidth, bombBagPowerUpCountPerQuadrant, bombRadiusPowerUpCountPerQuadrant);
        addPowerUps(quadrantHeight, grid.getHeight(), 0, quadrantWidth, bombBagPowerUpCountPerQuadrant, bombRadiusPowerUpCountPerQuadrant);
        addPowerUps(0, quadrantHeight, quadrantWidth, grid.getWidth(), bombBagPowerUpCountPerQuadrant, bombRadiusPowerUpCountPerQuadrant);
        addPowerUps(quadrantHeight, grid.getHeight(), quadrantWidth, grid.getWidth(), bombBagPowerUpCountPerQuadrant, bombRadiusPowerUpCountPerQuadrant);
    }
    
    private void addPowerUps(int startRow, int endRow, int startColumn, int endColumn, int bombBagPowerUpCount, int bombRadiusPowerUpCount)
    {
        for (int i = 0; i < bombBagPowerUpCount; i++)
            addPowerUp(startRow, endRow, startColumn, endColumn, PowerUpType.BOMB_BAG_POWER_UP);
        for (int i = 0; i < bombRadiusPowerUpCount; i++)
            addPowerUp(startRow, endRow, startColumn, endColumn, PowerUpType.BOMB_RADIUS_POWER_UP);
    }
    
    private void addPowerUp(int startRow, int endRow, int startColumn, int endColumn, PowerUpType powerUpType)
    {
        while (!grid.addHiddenPowerUp(new PowerUp(startRow + generator.nextInt(endRow - startRow), startColumn + generator.nextInt(endColumn - startColumn), powerUpType)))
            ;
    }
}
