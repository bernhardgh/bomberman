package za.co.entelect.challenge.model.bomberman.heuristics;

import java.util.List;

import za.co.entelect.challenge.model.bomberman.BombermanCell;
import za.co.entelect.challenge.model.bomberman.BombermanCell.CellType;
import za.co.entelect.challenge.model.bomberman.BombermanEngine;
import za.co.entelect.challenge.model.bomberman.BombermanGrid;
import za.co.entelect.challenge.model.bomberman.BombermanPlayer;
import za.co.entelect.challenge.model.bomberman.BombermanState;
import za.co.entelect.challenge.model.bomberman.players.helpers.BombermanCellHelpers;

public class TermBombermanHeuristic extends BombermanHeuristic
{
    private static final int BAG_SIZE_FACTOR = -6;
    private static final double BOMB_RADIUS_FACTOR = 60;
    private static final int RADIUS_LIMIT = 18;
    private static final int WALL_FACTOR = 8;
    private static final int BOMB_THREAT_PENALTY = 5;
    private static final int TRAPPED_PENALTY = 50;

    public static final double DEFAULT_AREA_PROSPERITY_TERM_FACTOR = 1.9;
    public static final int DEFAULT_AREA_PROSPERITY_RADIUS = 5;
    public static final double DEFAULT_AREA_PROSPERITY_DISTANCE_FACTOR = 0.95;
    
    private double areaProsperityTermFactor = 0;
    private int areaProsperityRadius;
    private double[] distanceFactors;
    
    public TermBombermanHeuristic() {}
    
    public TermBombermanHeuristic(boolean includeAreaProsperityTerm)
    {
        this(includeAreaProsperityTerm ? DEFAULT_AREA_PROSPERITY_TERM_FACTOR : 0);
    }
    
    public TermBombermanHeuristic(double areaProsperityTermFactor)
    {
        this(areaProsperityTermFactor, DEFAULT_AREA_PROSPERITY_RADIUS);
    }
    
    public TermBombermanHeuristic(double areaProsperityTermFactor, int areaProsperityRadius)
    {
        this(areaProsperityTermFactor, areaProsperityRadius, DEFAULT_AREA_PROSPERITY_DISTANCE_FACTOR);
    }
    
    public TermBombermanHeuristic(double areaProsperityTermFactor, int areaProsperityRadius, double areaProsperityDistanceFactor)
    {
        this.areaProsperityTermFactor = areaProsperityTermFactor;
        this.areaProsperityRadius = areaProsperityRadius;
        
        distanceFactors = new double[areaProsperityRadius + 1];
        double factor = 1;
        for (int i = 1; i <= areaProsperityRadius; i++, factor *= areaProsperityDistanceFactor)
            distanceFactors[i] = factor;
    }
    
    @Override
    public double evaluate(BombermanState gameState, BombermanPlayer player)
    {
        double eval = getBaseScore(gameState, player);
        eval += BAG_SIZE_FACTOR * player.getBombBagSize();
        eval -= BOMB_RADIUS_FACTOR / Math.min(RADIUS_LIMIT, player.getBombRadius());
        eval += WALL_FACTOR * getDestroyedWallsCount(player, gameState);
        if (BombermanCellHelpers.isInBombThreat(player, gameState))
        	eval -= BOMB_THREAT_PENALTY;
        BombermanGrid grid = gameState.getGrid();
        if (isTrapped(player, grid))
            eval -= TRAPPED_PENALTY;
        if (areaProsperityTermFactor != 0)
            eval += areaProsperityTermFactor * getAreaProsperityTerm(player, grid);
        return eval;
    }
    
    private static int getDestroyedWallsCount(BombermanPlayer player, BombermanState gameState)
    {
        BombermanGrid grid = gameState.getGrid();
        int count = 0;
        List<BombermanCell> cellsWithBombs = player.getCellsWithBombs();
        int bombCount = cellsWithBombs.size();
        for (int i = 0; i < bombCount; i++)
        {
            BombermanCell nextBombCell = cellsWithBombs.get(i);
            for (int[] direction : BombermanEngine.DIRECTIONS)
            {
                int rowDirection = direction[0], columnDirection = direction[1];
                int row = nextBombCell.getRow(), column = nextBombCell.getColumn();
                int radius = nextBombCell.getBombRadius();
                for (int j = 0; j < radius; j++)
                {
                    row += rowDirection;
                    column += columnDirection;
                    BombermanCell cell = grid.getCell(row, column);
                    CellType cellType = cell.getCellType();
                    
                    if (cellType == CellType.DESTRUCTIBLE_WALL)
                    {
                        count++;
                        break;
                    }
                    else if ((cellType == CellType.INDESTRUCTIBLE_WALL) || (cell.hasBomb()))
                        break;
                }
            }
        }
        return count;
    }
    
    private static boolean isTrapped(BombermanPlayer player, BombermanGrid grid)
    {
        int row = player.getRow(), column = player.getColumn();
        for (int[] direction : BombermanEngine.DIRECTIONS)
        {
            BombermanCell cell = grid.getCell(row + direction[0], column + direction[1]);
            if ((cell.isOpenToMoveOn()) || (cell.getCellType() == CellType.BOMB_EXPLOSION))
                return false;
        }
        return true;
    }
    
    private double getAreaProsperityTerm(BombermanPlayer player, BombermanGrid grid)
    {
        double value = 0;
        int gridHeight = grid.getHeight(), gridWidth = grid.getWidth();
        int playerRow = player.getRow(), playerColumn = player.getColumn();
        for (int diagonal1 = 0; diagonal1 <= areaProsperityRadius; diagonal1++)
            for (int diagonal2 = 0; diagonal2 <= areaProsperityRadius; diagonal2++)
            {
                int row = playerRow - diagonal2 + diagonal1;
                if ((row <= 0) || (row >= gridHeight))
                    continue;
                int column = playerColumn - areaProsperityRadius + diagonal2 + diagonal1;
                if ((column <= 0) || (column >= gridWidth))
                    continue;
                
                int cellValue;
                switch (grid.getCell(row, column).getCellType())
                {
                    case DESTRUCTIBLE_WALL: cellValue = 1; break;
                    case SUPER_POWER_UP: cellValue = 5; break;
                    default: cellValue = 0;
                }
                if (cellValue != 0)
                {
                    int distance = Math.abs(row - playerRow) + Math.abs(column - playerColumn);
                    value += distanceFactors[distance] * cellValue;
                }
            }
        
        return value;
    }
}
