package za.co.entelect.challenge.model;

public abstract class GameGrid<CellType extends Cell> extends Grid<CellType>
{
    private static final long serialVersionUID = 6759824070452342639L;

    public GameGrid(int height, int width, CellType clearCell)
	{
		super(height, width, clearCell);
	}
}
