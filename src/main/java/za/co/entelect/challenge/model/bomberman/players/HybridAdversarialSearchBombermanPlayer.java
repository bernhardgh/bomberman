package za.co.entelect.challenge.model.bomberman.players;

import za.co.entelect.challenge.model.GamePlayer;
import za.co.entelect.challenge.model.GameState;
import za.co.entelect.challenge.model.HybridGamePlayer;
import za.co.entelect.challenge.model.bomberman.BombermanCell;
import za.co.entelect.challenge.model.bomberman.BombermanPlayer;
import za.co.entelect.challenge.model.bomberman.BombermanState;
import za.co.entelect.challenge.model.bomberman.players.helpers.BombermanShortestDistanceLookup;
import za.co.entelect.challenge.model.bomberman.players.helpers.BombermanShortestDistanceLookupCache;

public class HybridAdversarialSearchBombermanPlayer extends HybridGamePlayer<BombermanCell>
{
    public static final int DEFAULT_PLAYER_DISTANCE_THRESHOLD = 4;
    
    private int playerDistanceThreshold = DEFAULT_PLAYER_DISTANCE_THRESHOLD;
    
    public HybridAdversarialSearchBombermanPlayer(GamePlayer<BombermanCell> twoAgentGamePlayer, GamePlayer<BombermanCell> multiAgentGamePlayer)
    {
        super(twoAgentGamePlayer, multiAgentGamePlayer);
    }
    
    public int getPlayerDistanceThreshold()
    {
        return playerDistanceThreshold;
    }
    
    public void setPlayerDistanceThreshold(int newThreshold)
    {
        playerDistanceThreshold = newThreshold;
    }

    @Override
    protected boolean usePrimaryPlayer(GameState<BombermanCell> gameState)
    {
        int nearPlayerCount = 0;
        BombermanState bombermanState = (BombermanState)gameState;
        BombermanPlayer player = (BombermanPlayer)getPlayer();
        BombermanShortestDistanceLookup lookup = BombermanShortestDistanceLookupCache.getInstance().getLookup(bombermanState, player.getRow(), player.getColumn());
        for (BombermanPlayer nextPlayer : bombermanState.getPlayers())
            if ((nextPlayer != player) && (nextPlayer.isAlive()) && (lookup.getDistance(nextPlayer.getRow(), nextPlayer.getColumn()) <= playerDistanceThreshold))
                nearPlayerCount++;
        
        return nearPlayerCount <= 1;
    }
}
