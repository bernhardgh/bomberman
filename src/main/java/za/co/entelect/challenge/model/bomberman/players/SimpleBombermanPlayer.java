package za.co.entelect.challenge.model.bomberman.players;

import java.util.Random;

import za.co.entelect.challenge.model.GameEngine;
import za.co.entelect.challenge.model.GamePlayer;
import za.co.entelect.challenge.model.GameState;
import za.co.entelect.challenge.model.Move;
import za.co.entelect.challenge.model.Player;
import za.co.entelect.challenge.model.bomberman.BombermanCell;
import za.co.entelect.challenge.model.bomberman.BombermanEngine;
import za.co.entelect.challenge.model.bomberman.BombermanMove;
import za.co.entelect.challenge.model.bomberman.BombermanPlayer;
import za.co.entelect.challenge.model.bomberman.BombermanState;
import za.co.entelect.challenge.model.bomberman.players.helpers.BombermanCellHelpers;

public class SimpleBombermanPlayer extends GamePlayer<BombermanCell>
{
    private static final int NUMBER_OF_MOVES = BombermanMove.values().length;
    private static final Random generator = new Random();
    
    private boolean[] triedMove = new boolean[NUMBER_OF_MOVES];
    
    public SimpleBombermanPlayer(Player player)
    {
        super(new BombermanEngine(), player);
    }

    @Override
    public Move getNextMove(GameState<BombermanCell> gameState)
    {
        GameEngine<BombermanCell> gameEngine = getGameEngine();
        BombermanState bombermanState = (BombermanState)gameState;
        BombermanPlayer player = (BombermanPlayer)getPlayer();
        boolean hasBombs = bombermanState.hasBombsOrExplosions();

        for (int i = 0; i < triedMove.length; i++)
            triedMove[i] = false;
        
        for (int movesTriedCount = 0; movesTriedCount < NUMBER_OF_MOVES; movesTriedCount++)
        {
            int moveIndex;
            do
                moveIndex = generator.nextInt(NUMBER_OF_MOVES);
            while (triedMove[moveIndex]);
            
            BombermanMove move = BombermanMove.values()[moveIndex];
            if (((move != BombermanMove.DO_NOTHING) || (hasBombs)) 
                    && (gameEngine.isMoveValid(bombermanState, player, move))
                    && (!BombermanCellHelpers.isInBombThreat(BombermanCellHelpers.getDestinationCell(bombermanState.getGrid(), player, move), bombermanState, move == BombermanMove.TRIGGER_BOMB)))
            {
                return move;
            }
            
            triedMove[moveIndex] = true;
        }
        
        BombermanMove move;
        do
            move = BombermanMove.values()[generator.nextInt(BombermanMove.values().length)];
        while (!getGameEngine().isMoveValid(gameState, getPlayer(), move));
        return move;
    }
}
