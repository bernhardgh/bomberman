package za.co.entelect.challenge.model;

public interface MoveInfo
{
    public MoveInfo concatenate(MoveInfo other);
    public void undo();
}
