package za.co.entelect.challenge.model.bomberman.players;

import za.co.entelect.challenge.logging.Logger;
import za.co.entelect.challenge.logging.Logger.LogLevel;
import za.co.entelect.challenge.model.GamePlayer;
import za.co.entelect.challenge.model.GameState;
import za.co.entelect.challenge.model.Move;
import za.co.entelect.challenge.model.Player;
import za.co.entelect.challenge.model.SearchThreadCoordinator;
import za.co.entelect.challenge.model.bomberman.BombermanCell;
import za.co.entelect.challenge.model.bomberman.BombermanMove;
import za.co.entelect.challenge.model.bomberman.BombermanState;
import za.co.entelect.challenge.model.bomberman.heuristics.BombermanHeuristic;

public class MultiThreadedAlphaBetaBombermanPlayer extends GamePlayer<BombermanCell>
{
    private static final Logger logger = Logger.getInstance();
    
    private BombermanHeuristic heuristic;
    
    public MultiThreadedAlphaBetaBombermanPlayer(Player player, BombermanHeuristic heuristic)
    {
        super(null, player);
        this.heuristic = heuristic;
    }

    @Override
    public Move getNextMove(GameState<BombermanCell> gameState) throws InterruptedException
    {
        MySearchThreadCoordinator searchThreadCoordinator = new MySearchThreadCoordinator();
        try
        {
            return searchThreadCoordinator.searchMove(gameState);
        }
        finally
        {
            if (logger.getLogLevel().ordinal() >= LogLevel.INFO.ordinal())
            {
                logger.log(LogLevel.INFO, "==== Depth reached: %d ====", searchThreadCoordinator.getCurrentMoveMaxDepth());
                int nodeCount = 0;
                for (Runnable nextRunnable : searchThreadCoordinator.getRunnables())
                    nodeCount += ((SearchRunnable)nextRunnable).getNodeCount();
                logger.log(LogLevel.INFO, "==== Node count: %d ====", nodeCount);
            }
        }
    }
    
    
    private class MySearchThreadCoordinator extends SearchThreadCoordinator<BombermanCell>
    {
        public MySearchThreadCoordinator()
        {
            super(MultiThreadedAlphaBetaBombermanPlayer.this, 4);
        }

        @Override
        protected Runnable createSearchRunnable(GameState<BombermanCell> gameState)
        {
            return new SearchRunnable(this, (BombermanState)gameState);
        }
    }
    
    
    private class SearchRunnable extends AlphaBetaBombermanPlayer implements Runnable
    {
        private MySearchThreadCoordinator searchThreadCoordinator;
        private BombermanState gameState;
        
        public SearchRunnable(MySearchThreadCoordinator searchThreadCoordinator, BombermanState gameState)
        {
            super(MultiThreadedAlphaBetaBombermanPlayer.this.getPlayer(), heuristic);
            this.searchThreadCoordinator = searchThreadCoordinator;
            this.gameState = gameState;
        }
        
        @Override
        public void run()
        {
            try
            {
                getNextMove(gameState);
            }
            catch (InterruptedException e) {}
        }
        
        @Override
        public Move getNextMove(GameState<BombermanCell> gameState) throws InterruptedException
        {
            BombermanMove immediateMove = initializeSearch((BombermanState)gameState);
            if (immediateMove != null)
            {
                searchThreadCoordinator.perceiveSearchResult(0, immediateMove, false);
                return immediateMove;
            }
            
            BombermanMove bestMove = null;
            while (searchThreadCoordinator.isSearching())
            {
                int nextMaxDepth = searchThreadCoordinator.nextMaxDepth();
                SearchResult result = searchThreadCoordinator.hasCurrentScore() 
                        ? computeMove(nextMaxDepth, searchThreadCoordinator.getCurrentScore()) : computeMove(nextMaxDepth);
                searchThreadCoordinator.perceiveSearchResult(nextMaxDepth, bestMove = result.getMove(), result.getScore(), result.hasMoreDepthToSearch());
            }
            return bestMove;
        }
    }
}
