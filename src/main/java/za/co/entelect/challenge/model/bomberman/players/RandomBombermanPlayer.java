package za.co.entelect.challenge.model.bomberman.players;

import java.util.Random;

import za.co.entelect.challenge.model.GamePlayer;
import za.co.entelect.challenge.model.GameState;
import za.co.entelect.challenge.model.Move;
import za.co.entelect.challenge.model.Player;
import za.co.entelect.challenge.model.bomberman.BombermanCell;
import za.co.entelect.challenge.model.bomberman.BombermanEngine;
import za.co.entelect.challenge.model.bomberman.BombermanMove;

public class RandomBombermanPlayer extends GamePlayer<BombermanCell>
{
    private static final Random generator = new Random();
    
    public RandomBombermanPlayer(Player player)
    {
        super(new BombermanEngine(), player);
    }

    @Override
    public Move getNextMove(GameState<BombermanCell> gameState)
    {
        BombermanMove move;
        do
            move = BombermanMove.values()[generator.nextInt(BombermanMove.values().length)];
        while (!getGameEngine().isMoveValid(gameState, getPlayer(), move));
        
        return move;
    }
}
