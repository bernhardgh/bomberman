package za.co.entelect.challenge.model;

import java.io.Serializable;

public abstract class GameState<CellType extends Cell> implements Cloneable, Serializable
{
    private static final long serialVersionUID = -5262441152646404059L;

    public abstract GameGrid<CellType> getGrid();
    public abstract Player[] getPlayers();
    
    public Player getPlayer(char playerKey)
    {
        for (Player nextPlayer : getPlayers())
            if (nextPlayer.getPlayerKey() == playerKey)
                return nextPlayer;
        return null;
    }
    
    public Object clone()
    {
        try
        {
            return super.clone();
        }
        catch (CloneNotSupportedException e) {}
        
        return null;
    }
}
