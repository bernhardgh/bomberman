package za.co.entelect.challenge.model.bomberman.players;

import za.co.entelect.challenge.model.GamePlayer;
import za.co.entelect.challenge.model.GameState;
import za.co.entelect.challenge.model.Move;
import za.co.entelect.challenge.model.Player;
import za.co.entelect.challenge.model.bomberman.BombermanCell;
import za.co.entelect.challenge.model.bomberman.BombermanMove;

public class LazyBombermanPlayer extends GamePlayer<BombermanCell>
{
    public LazyBombermanPlayer(Player player)
    {
        super(null, player);
    }

    @Override
    public Move getNextMove(GameState<BombermanCell> gameState) throws InterruptedException
    {
        return BombermanMove.DO_NOTHING;
    }
}
