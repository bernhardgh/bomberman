package za.co.entelect.challenge.model.bomberman;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import za.co.entelect.challenge.logging.Logger;
import za.co.entelect.challenge.logging.Logger.LogLevel;
import za.co.entelect.challenge.model.GameEngine;
import za.co.entelect.challenge.model.GameState;
import za.co.entelect.challenge.model.Move;
import za.co.entelect.challenge.model.Player;
import za.co.entelect.challenge.model.bomberman.BombermanCell.CellType;
import za.co.entelect.challenge.model.bomberman.BombermanPlayer.ProcessingInfo;
import za.co.entelect.challenge.model.bomberman.PowerUp.PowerUpType;
import za.co.entelect.challenge.model.bomberman.moveinfo.AdvanceGameRoundMoveEvent;
import za.co.entelect.challenge.model.bomberman.moveinfo.BombermanMoveInfo;
import za.co.entelect.challenge.model.bomberman.moveinfo.CellChangeMoveEvent;
import za.co.entelect.challenge.model.bomberman.moveinfo.CellsWillExplodeMoveEvent;
import za.co.entelect.challenge.model.bomberman.moveinfo.MoveEvent;
import za.co.entelect.challenge.model.bomberman.moveinfo.PlayerDiedMoveEvent;
import za.co.entelect.challenge.model.bomberman.moveinfo.PlayerScoreChangedMoveEvent;
import za.co.entelect.challenge.model.bomberman.players.helpers.BombermanCellHelpers;

public class BombermanEngine extends GameEngine<BombermanCell>
{
    public static final int[][] DIRECTIONS = new int[][]{ { -1, 0 }, { 1, 0 }, { 0, -1 }, { 0, 1 } };
    private static final Comparator<BombermanCell> EXPLODING_BOMB_CELL_COMPARATOR = new ReverseBombRadiusComparator();
    
    private static final int BOMB_NOT_PROCESSED = 0, BOMB_DID_EXPLODE = 1;
    
    private ArrayList<BombermanCell> cellsWithExplodingBombs = new ArrayList<BombermanCell>();
    
    @Override
    public boolean isMoveValid(GameState<BombermanCell> gameState, Player player, Move move)
    {
        if (move == BombermanMove.DO_NOTHING)
            return true;

        BombermanPlayer bombermanPlayer = (BombermanPlayer)player;
        if (!bombermanPlayer.isAlive())
            return false;
        
        if (move == BombermanMove.TRIGGER_BOMB)
            return bombermanPlayer.hasBombToTrigger();

        BombermanState bombermanState = (BombermanState)gameState;
        if (move == BombermanMove.PLACE_BOMB)
            return (bombermanPlayer.getAvailableBombCount() != 0) && (!bombermanState.getGrid().getCell(bombermanPlayer.getRow(), bombermanPlayer.getColumn()).hasBomb());
        
        BombermanCell nextCell = BombermanCellHelpers.getDestinationCell(bombermanState.getGrid(), bombermanPlayer, (BombermanMove)move);
        if (!nextCell.isOpenToMoveOn())
            return false;
        
        return (!nextCell.hasPowerUp()) || (!bombermanState.getExplodingCells().contains(nextCell));
    }
    
    @Override
    public BombermanMoveInfo advanceRound(GameState<BombermanCell> gameState, Move... playerMoves)
    {
        return (BombermanMoveInfo)super.advanceRound(gameState, playerMoves);
    }
    
    @Override
    public BombermanMoveInfo advanceRound(GameState<BombermanCell> gameState)
    {
        BombermanMoveInfo moveInfo = new BombermanMoveInfo(new ArrayList<MoveEvent>(20));
        
        BombermanState bombermanState = (BombermanState)gameState;
        moveInfo.addMoveEvent(new AdvanceGameRoundMoveEvent(bombermanState));
        bombermanState.incrementGameRound();
        
        clearExplosions(bombermanState, moveInfo);
        explodeBombs(bombermanState, moveInfo);
        
        return moveInfo;
    }
    
    private void clearExplosions(BombermanState gameState, BombermanMoveInfo moveInfo)
    {
        BombermanGrid grid = gameState.getGrid();
        List<BombermanCell> explodingCells = gameState.getExplodingCells();
        int count = explodingCells.size();
        for (int i = 0; i < count; i++)
        {
            BombermanCell nextCell = explodingCells.get(i);
            CellType newCellType;
            if (nextCell.getCellType() == CellType.BOMB_EXPLOSION)
            {
                PowerUp powerUp = grid.retrieveHiddenPowerUp(nextCell);
                newCellType = powerUp == null ? CellType.CLEAR : (powerUp.getPowerUpType() == PowerUpType.BOMB_BAG_POWER_UP ? CellType.BOMB_BAG_POWER_UP : CellType.BOMB_RADIUS_POWER_UP);
            }
            else
                newCellType = nextCell.getCellType();

            if (nextCell.getCellOwner() != null)
                moveInfo.addMoveEvent(nextCell.grantBomb());
            
            CellChangeMoveEvent moveEvent = nextCell.performCellChange(newCellType);
            moveEvent.setIsExplodedCell(gameState);
            moveInfo.addMoveEvent(moveEvent);
        }
    }
    
    private void explodeBombs(BombermanState gameState, BombermanMoveInfo moveInfo)
    {
        cellsWithExplodingBombs.clear();
        for (BombermanPlayer nextPlayer : gameState.getPlayers())
        {
            nextPlayer.getProcessingInfo().resetInfo();
            List<BombermanCell> cellsWithBombs = nextPlayer.getCellsWithBombs();
            int count = cellsWithBombs.size();
            for (int i = 0; i < count; i++)
            {
                BombermanCell nextCellWithBomb = cellsWithBombs.get(i);
                moveInfo.addMoveEvent(nextCellWithBomb.tickBomb());
                if (nextCellWithBomb.isBombExploding())
                    cellsWithExplodingBombs.add(nextCellWithBomb);
            }
        }

        List<BombermanCell> explodingCells = gameState.getExplodingCells();
        explodingCells.clear();
        if (!cellsWithExplodingBombs.isEmpty())
        {
            moveInfo.addMoveEvent(new CellsWillExplodeMoveEvent(gameState));
            cellsWithExplodingBombs.sort(EXPLODING_BOMB_CELL_COMPARATOR);
            int count = cellsWithExplodingBombs.size();
            for (int i = 0; i < count; i++)
            {
                BombermanCell nextExplodingCell = cellsWithExplodingBombs.get(i);
                if (nextExplodingCell.isBombExploding())
                {
                    int chainScore = explode(gameState, nextExplodingCell, moveInfo);
                    
                    int explodingCellsCount = explodingCells.size();
                    for (int j = 0; j < explodingCellsCount; j++)
                        explodingCells.get(j).tag = BOMB_NOT_PROCESSED;
                    
                    for (BombermanPlayer player : gameState.getPlayers())
                    {
                        ProcessingInfo playerInfo = player.getProcessingInfo();
                        if (playerInfo.isPartOfChain())
                        {
                            playerInfo.addPointsEarned(chainScore);
                            playerInfo.setIsPartOfChain(false);
                        }
                    }
                }
            }
            
            for (BombermanPlayer nextPlayer : gameState.getPlayers())
            {
                moveInfo.addMoveEvent(new PlayerScoreChangedMoveEvent(nextPlayer));
                ProcessingInfo playerInfo = nextPlayer.getProcessingInfo();
                if (playerInfo.wasKilled())
                {
                    if (nextPlayer.isActive())
                    {
                        moveInfo.addMoveEvent(new PlayerDiedMoveEvent(nextPlayer));
                        nextPlayer.setIsAlive(false);
                        nextPlayer.addToScore(-gameState.getPlayerKillPoints());
                    }
                }
                else
                    nextPlayer.addToScore(playerInfo.getPointsEarned());
            }
            
            count = explodingCells.size();
            for (int i = 0; i < count; i++)
                moveInfo.addMoveEvent(explodingCells.get(i).performCellChange(CellType.BOMB_EXPLOSION, true));
        }
    }
    
    private int explode(BombermanState gameState, BombermanCell explodingBombCell, BombermanMoveInfo moveInfo)
    {
        int bombRadius = explodingBombCell.getBombRadius();
        int chainScore = explodeCell(gameState, explodingBombCell, moveInfo);
        
        for (int[] direction : DIRECTIONS)
        {
            int rowDirection = direction[0], columnDirection = direction[1];
            int currentRow = explodingBombCell.getRow(), currentColumn = explodingBombCell.getColumn();
            for (int i = 0; i < bombRadius; i++)
            {
                currentRow += rowDirection;
                currentColumn += columnDirection;
                BombermanCell nextCell = gameState.getGrid().getCell(currentRow, currentColumn);
                CellType cellType = nextCell.getCellType();
                
                if ((cellType == CellType.INDESTRUCTIBLE_WALL) || (nextCell.tag == BOMB_DID_EXPLODE))
                    break;
                
                if (nextCell.hasBomb())
                {
                    chainScore += explode(gameState, nextCell, moveInfo);
                    break;
                }
                
                chainScore += explodeCell(gameState, nextCell, moveInfo);
                if (cellType == CellType.DESTRUCTIBLE_WALL)
                    break;
            }
        }
        
        return chainScore;
    }
    
    private int explodeCell(BombermanState gameState, BombermanCell explosionChainCell, BombermanMoveInfo moveInfo)
    {
        List<BombermanCell> explodingCells = gameState.getExplodingCells();
        if (!explodingCells.contains(explosionChainCell))
            explodingCells.add(explosionChainCell);
        
        int chainScore = 0;
        CellType cellType = explosionChainCell.getCellType();
        if (cellType == CellType.DESTRUCTIBLE_WALL)
            chainScore = 10;
        else if (cellType == CellType.PLAYER)
        {
            BombermanPlayer cellOwner = explosionChainCell.getCellOwner();
            if (cellOwner != null)
            {
                cellOwner.getProcessingInfo().setWasKilled();
                if (cellOwner.isActive())
                    chainScore = gameState.getPlayerKillPoints();
            }
        }
        
        if (explosionChainCell.hasBomb())
        {
            if (explosionChainCell.getCellOwner() == null)
            {
                Logger.getInstance().log(LogLevel.WARNING, "Bomb with no cell owner at %d, %d. Restoring cell owner...", explosionChainCell.getRow(), explosionChainCell.getColumn());
                RESTORE_CELL_OWNER:
                for (BombermanPlayer nextPlayer : gameState.getPlayers())
                    for (BombermanCell nextBombCell : nextPlayer.getCellsWithBombs())
                        if (nextBombCell == explosionChainCell)
                        {
                            explosionChainCell.restoreCellOwner(nextPlayer);
                            break RESTORE_CELL_OWNER;
                        }
            }
            
            explosionChainCell.getCellOwner().getProcessingInfo().setIsPartOfChain();
            moveInfo.addMoveEvent(explosionChainCell.setBombDidExplode());
            explosionChainCell.tag = BOMB_DID_EXPLODE;
        }
        
        return chainScore;
    }

    @Override
    public BombermanMoveInfo applyMove(GameState<BombermanCell> gameState, Player player, Move move)
    {
        return applyMove(gameState, player, move, true);
    }
    
    public BombermanMoveInfo applyMove(GameState<BombermanCell> gameState, Player player, Move move, boolean checked)
    {
        BombermanState bombermanState = (BombermanState)gameState;
        if (move == BombermanMove.DO_NOTHING)
            return BombermanMoveInfo.EMPTY;

        BombermanMoveInfo moveInfo = new BombermanMoveInfo(new ArrayList<MoveEvent>(2));
        BombermanPlayer bombermanPlayer = (BombermanPlayer)player;
        if (move == BombermanMove.TRIGGER_BOMB)
            moveInfo.addMoveEvent(bombermanPlayer.getBombToTrigger().triggerBomb());
        else if (move == BombermanMove.PLACE_BOMB)
            moveInfo.addMoveEvent(bombermanState.getGrid().getCell(bombermanPlayer.getRow(), bombermanPlayer.getColumn()).plantBomb());
        else
        {
            BombermanGrid grid = bombermanState.getGrid();
            BombermanCell nextCell = BombermanCellHelpers.getDestinationCell(grid, bombermanPlayer, (BombermanMove)move);
            if ((!checked) || (nextCell.isOpenToMoveOn()))
            {
                moveInfo.addMoveEvent(grid.getCell(bombermanPlayer.getRow(), bombermanPlayer.getColumn()).performCellChange(CellType.CLEAR));
                moveInfo.addMoveEvent(nextCell.performCellChange(bombermanPlayer));
            }
        }
        
        return moveInfo;
    }
    
    @Override
    public boolean playerDidWin(GameState<BombermanCell> gameState, Player player)
    {
        BombermanPlayer myPlayer = (BombermanPlayer)player;
        if (!myPlayer.isAlive())
            return false;
        
        BombermanState bombermanState = (BombermanState)gameState;
        boolean hasOtherSurvivors = false;
        for (BombermanPlayer nextPlayer : bombermanState.getPlayers())
            if ((nextPlayer != player) && (nextPlayer.isAlive()))
            {
                hasOtherSurvivors = true;
                break;
            }

        if (!hasOtherSurvivors)
            return true;
        else if (bombermanState.getGameRound() <= bombermanState.getMaximumGameRounds())
            return false;
        
        int myScore = myPlayer.getScore();
        for (BombermanPlayer nextPlayer : bombermanState.getPlayers())
            if ((nextPlayer != player) && (nextPlayer.getScore() >= myScore))
                return false;
        
        return true;
    }
    
    @Override
    public boolean gameDidEnd(GameState<BombermanCell> gameState)
    {
        BombermanState bombermanState = (BombermanState)gameState;
        if (bombermanState.getGameRound() > bombermanState.getMaximumGameRounds())
            return true;
        
        boolean hasAlivePlayer = false;
        for (BombermanPlayer nextPlayer : bombermanState.getPlayers())
            if (nextPlayer.isAlive())
            {
                if (hasAlivePlayer)
                    return false;
                hasAlivePlayer = true;
            }
        return true;
    }
    
    
    private static class ReverseBombRadiusComparator implements Comparator<BombermanCell>
    {
        @Override
        public int compare(BombermanCell cell1, BombermanCell cell2)
        {
            return cell2.getBombRadius() - cell1.getBombRadius();
        }
    }
}
