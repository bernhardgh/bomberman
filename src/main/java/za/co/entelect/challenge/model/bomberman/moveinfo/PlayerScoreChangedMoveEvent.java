package za.co.entelect.challenge.model.bomberman.moveinfo;

import za.co.entelect.challenge.model.bomberman.BombermanPlayer;

public class PlayerScoreChangedMoveEvent implements MoveEvent
{
    private BombermanPlayer player;
    private int oldBaseScore;
    
    public PlayerScoreChangedMoveEvent(BombermanPlayer player)
    {
        this.player = player;
        oldBaseScore = player.getBaseScore();
    }

    @Override
    public void undo()
    {
        player.setBaseScore(oldBaseScore);
    }
}
