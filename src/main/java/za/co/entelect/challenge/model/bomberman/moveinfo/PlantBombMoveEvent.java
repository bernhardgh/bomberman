package za.co.entelect.challenge.model.bomberman.moveinfo;

import za.co.entelect.challenge.model.bomberman.BombermanCell;
import za.co.entelect.challenge.model.bomberman.BombermanPlayer;

public class PlantBombMoveEvent implements MoveEvent
{
    private BombermanCell cell;
    
    public PlantBombMoveEvent(BombermanCell cell)
    {
        this.cell = cell;
    }

    @Override
    public void undo()
    {
        BombermanPlayer player = cell.getCellOwner();
        player.returnBomb(cell);
        player.grantBomb();
        cell.setBomb(0, 0, player);
    }
}
