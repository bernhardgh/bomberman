package za.co.entelect.challenge.model.bomberman.moveinfo;

public interface MoveEvent
{
    public void undo();
}
