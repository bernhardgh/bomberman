package za.co.entelect.challenge.model.bomberman.players.helpers;

import java.util.List;

import za.co.entelect.challenge.model.bomberman.BombermanCell;
import za.co.entelect.challenge.model.bomberman.BombermanEngine;
import za.co.entelect.challenge.model.bomberman.BombermanGrid;
import za.co.entelect.challenge.model.bomberman.BombermanMove;
import za.co.entelect.challenge.model.bomberman.BombermanPlayer;
import za.co.entelect.challenge.model.bomberman.BombermanState;
import za.co.entelect.challenge.model.bomberman.BombermanCell.CellType;

public class BombermanCellHelpers
{
    public static BombermanCell getDestinationCell(BombermanGrid grid, BombermanPlayer player, BombermanMove move)
    {
        switch (move)
        {
            case UP: return grid.getCell(player.getRow() - 1, player.getColumn());
            case DOWN: return grid.getCell(player.getRow() + 1, player.getColumn());
            case LEFT: return grid.getCell(player.getRow(), player.getColumn() - 1);
            case RIGHT: return grid.getCell(player.getRow(), player.getColumn() + 1);
            default: return grid.getCell(player.getRow(), player.getColumn());
        }
    }
    
    public static boolean isInBombThreat(BombermanPlayer player, BombermanState gameState)
    {
    	return isInBombThreat(gameState.getGrid().getCell(player.getRow(), player.getColumn()), gameState, false);
    }
    
    public static boolean isInBombThreat(BombermanCell cell, BombermanState gameState, boolean isTriggeringABomb)
    {
        BombermanCell triggeredBombCell = isTriggeringABomb ? cell.getCellOwner().getBombToTrigger() : null;
        BombermanGrid grid = gameState.getGrid();
        for (BombermanPlayer nextPlayer : gameState.getPlayers())
        {
            List<BombermanCell> cellsWithBombs = nextPlayer.getCellsWithBombs();
            int bombCount = cellsWithBombs.size();
            for (int i = 0; i < bombCount; i++)
            {
                BombermanCell nextBombCell = cellsWithBombs.get(i);
                if (isCellInBombThreat(cell, nextBombCell, nextBombCell == triggeredBombCell, grid))
                    return true;
            }
        }
        return false;
    }
    
    private static boolean isCellInBombThreat(BombermanCell cell, BombermanCell bombCell, boolean bombIsTriggered, BombermanGrid grid)
    {
        if ((cell.getRow() != bombCell.getRow()) && (cell.getColumn() != bombCell.getColumn()))
            return false;
        
        int bombCountdown = bombIsTriggered ? 1 : bombCell.getBombCountdown();
        int distance = Math.abs(cell.getRow() - bombCell.getRow()) + Math.abs(cell.getColumn() - bombCell.getColumn());
        if (distance > bombCell.getBombRadius() - bombCountdown + 1)
            return false;
        
        if (distance == 0)
            return true;
        
        if (cell.getRow() == bombCell.getRow())
        {
            int row = cell.getRow();
            if ((bombCountdown > 1) || ((grid.getCell(row - 1, cell.getColumn()).isOpenToMoveOn()) || (grid.getCell(row + 1, cell.getColumn()).isOpenToMoveOn())))
                return false;
            
            int lowerColumn, upperColumn;
            if (cell.getColumn() < bombCell.getColumn())
            {
                lowerColumn = cell.getColumn() + 1;
                upperColumn = bombCell.getColumn() - 1;
            }
            else
            {
                lowerColumn = bombCell.getColumn() + 1;
                upperColumn = cell.getColumn() - 1;
            }
            
            for (int column = lowerColumn; column <= upperColumn; column++)
            {
                BombermanCell intermediateCell = grid.getCell(row, column);
                CellType cellType = intermediateCell.getCellType();
                if ((cellType == CellType.INDESTRUCTIBLE_WALL) || (cellType == CellType.DESTRUCTIBLE_WALL))
                    return false;
                else if (intermediateCell.hasBomb())
                {
                    distance = Math.abs(cell.getColumn() - column);
                    return distance <= intermediateCell.getBombRadius();
                }
            }
            return true;
        }
        
        int column = cell.getColumn();
        if ((bombCountdown > 1) || ((grid.getCell(cell.getRow(), column - 1).isOpenToMoveOn()) || (grid.getCell(cell.getRow(), column + 1).isOpenToMoveOn())))
            return false;
        
        int lowerRow, upperRow;
        if (cell.getRow() < bombCell.getRow())
        {
            lowerRow = cell.getRow() + 1;
            upperRow = bombCell.getRow() - 1;
        }
        else
        {
            lowerRow = bombCell.getRow() + 1;
            upperRow = cell.getRow() - 1;
        }
        
        for (int row = lowerRow; row <= upperRow; row++)
        {
            BombermanCell intermediateCell = grid.getCell(row, column);
            CellType cellType = intermediateCell.getCellType();
            if ((cellType == CellType.INDESTRUCTIBLE_WALL) || (cellType == CellType.DESTRUCTIBLE_WALL))
                return false;
            else if (intermediateCell.hasBomb())
            {
                distance = Math.abs(cell.getRow() - row);
                return distance <= intermediateCell.getBombRadius();
            }
        }
        return true;
    }
    
    public static boolean hasWallInRange(BombermanGrid grid, BombermanPlayer player)
    {
        int row = player.getRow(), column = player.getColumn();
        if (grid.getCell(row, column).hasBomb())
            return false;
        
        int bombRadius = player.getBombRadius();
        for (int[] direction : BombermanEngine.DIRECTIONS)
        {
            int rowDirection = direction[0], columnDirection = direction[1];
            int currentRow = row, currentColumn = column;
            for (int i = 0; i < bombRadius; i++)
            {
                currentRow += rowDirection;
                currentColumn += columnDirection;
                BombermanCell cell = grid.getCell(currentRow, currentColumn); 
                CellType cellType = cell.getCellType();
                
                if (cellType == CellType.DESTRUCTIBLE_WALL)
                    return true;
                if ((cellType == CellType.INDESTRUCTIBLE_WALL) || (cell.hasBomb()))
                    break;
            }
        }
        return false;
    }
}
