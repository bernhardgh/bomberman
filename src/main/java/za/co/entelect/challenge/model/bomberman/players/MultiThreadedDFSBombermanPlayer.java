package za.co.entelect.challenge.model.bomberman.players;

import za.co.entelect.challenge.logging.Logger;
import za.co.entelect.challenge.logging.Logger.LogLevel;
import za.co.entelect.challenge.model.GamePlayer;
import za.co.entelect.challenge.model.GameState;
import za.co.entelect.challenge.model.Move;
import za.co.entelect.challenge.model.Player;
import za.co.entelect.challenge.model.SearchThreadCoordinator;
import za.co.entelect.challenge.model.bomberman.BombermanCell;
import za.co.entelect.challenge.model.bomberman.BombermanMove;
import za.co.entelect.challenge.model.bomberman.BombermanState;
import za.co.entelect.challenge.model.bomberman.heuristics.BombermanHeuristic;
import za.co.entelect.challenge.model.bomberman.players.DFSBombermanPlayer.NearPlayersSelectionStrategyFactory;
import za.co.entelect.challenge.model.bomberman.players.DFSBombermanPlayer.NearestPlayerSelectionStrategyFactory;
import za.co.entelect.challenge.model.bomberman.players.DFSBombermanPlayer.PlayerSelection;
import za.co.entelect.challenge.model.bomberman.players.DFSBombermanPlayer.PlayerSelectionStrategyFactory;

public class MultiThreadedDFSBombermanPlayer extends GamePlayer<BombermanCell>
{
    private static final Logger logger = Logger.getInstance();
    
    private PlayerSelectionStrategyFactory opponentPlayerSelectionStrategyFactory;
    private BombermanHeuristic heuristic;

    public MultiThreadedDFSBombermanPlayer(Player player, PlayerSelection opponentPlayerSelection, BombermanHeuristic heuristic)
    {
        this(player, opponentPlayerSelection == PlayerSelection.NEAREST_PLAYER ? new NearestPlayerSelectionStrategyFactory() : new NearPlayersSelectionStrategyFactory(), heuristic);
    }
    
    public MultiThreadedDFSBombermanPlayer(Player player, PlayerSelectionStrategyFactory opponentPlayerSelectionStrategyFactory, BombermanHeuristic heuristic)
    {
        super(null, player);
        this.opponentPlayerSelectionStrategyFactory = opponentPlayerSelectionStrategyFactory;
        this.heuristic = heuristic;
    }

    @Override
    public Move getNextMove(GameState<BombermanCell> gameState) throws InterruptedException
    {
        MySearchThreadCoordinator searchThreadCoordinator = new MySearchThreadCoordinator();
        try
        {
            return searchThreadCoordinator.searchMove(gameState);
        }
        finally
        {
            if (logger.getLogLevel().ordinal() >= LogLevel.INFO.ordinal())
            {
                logger.log(LogLevel.INFO, "==== Depth reached: %d ====", searchThreadCoordinator.getCurrentMoveMaxDepth());
                int nodeCount = 0;
                for (Runnable nextRunnable : searchThreadCoordinator.getRunnables())
                    nodeCount += ((SearchRunnable)nextRunnable).getNodeCount();
                logger.log(LogLevel.INFO, "==== Node count: %d ====", nodeCount);
            }
        }
    }
    
    
    private class MySearchThreadCoordinator extends SearchThreadCoordinator<BombermanCell>
    {
        public MySearchThreadCoordinator()
        {
            super(MultiThreadedDFSBombermanPlayer.this, 1);
        }

        @Override
        protected Runnable createSearchRunnable(GameState<BombermanCell> gameState)
        {
            return new SearchRunnable(this, (BombermanState)gameState);
        }
    }
    
    
    private class SearchRunnable extends DFSBombermanPlayer implements Runnable
    {
        private MySearchThreadCoordinator searchThreadCoordinator;
        private BombermanState gameState;
        
        public SearchRunnable(MySearchThreadCoordinator searchThreadCoordinator, BombermanState gameState)
        {
            super(MultiThreadedDFSBombermanPlayer.this.getPlayer(), opponentPlayerSelectionStrategyFactory, heuristic);
            this.searchThreadCoordinator = searchThreadCoordinator;
            this.gameState = gameState;
        }
        
        @Override
        public void run()
        {
            try
            {
                getNextMove(gameState);
            }
            catch (InterruptedException e) {}
        }
        
        @Override
        public Move getNextMove(GameState<BombermanCell> gameState) throws InterruptedException
        {
            BombermanMove immediateMove = initializeSearch((BombermanState)gameState);
            if (immediateMove != null)
            {
                searchThreadCoordinator.perceiveSearchResult(0, immediateMove, false);
                return immediateMove;
            }
            
            BombermanMove bestMove = null;
            while (searchThreadCoordinator.isSearching())
            {
                int nextMaxDepth = searchThreadCoordinator.nextMaxDepth();
                SearchResult result = computeMove(nextMaxDepth);
                searchThreadCoordinator.perceiveSearchResult(nextMaxDepth, bestMove = result.getMove(), result.hasMoreDepthToSearch());
            }
            return bestMove;
        }
    }
}
