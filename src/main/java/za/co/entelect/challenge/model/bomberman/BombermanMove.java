package za.co.entelect.challenge.model.bomberman;

import za.co.entelect.challenge.model.Move;

public enum BombermanMove implements Move
{
    DO_NOTHING, UP, LEFT, RIGHT, DOWN, PLACE_BOMB, TRIGGER_BOMB
}
