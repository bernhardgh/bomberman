package za.co.entelect.challenge.model;

import java.util.ArrayList;
import java.util.Stack;

public class GameCoordinator<CellType extends Cell>
{
    private GameEngine<CellType> gameEngine;
	private GameState<CellType> gameState;
	private GamePlayer<CellType>[] players;
	private Stack<MoveInfo> moveInfoStack = new Stack<MoveInfo>();
	
	public GameCoordinator(GameEngine<CellType> gameEngine, GameState<CellType> gameState, GamePlayer<CellType>[] players)
	{
	    this.gameEngine = gameEngine;
		this.gameState = gameState;
		this.players = players;
	}
	
	public void setPlayers(GamePlayer<CellType>[] players)
	{
        this.players = players;
	}
	
    public GameState<CellType> move() throws InterruptedException, InvalidMoveException
	{
        ArrayList<Move> moves = new ArrayList<Move>(players.length);
        for (GamePlayer<CellType> nextPlayer : players)
        {
            moves.add(nextPlayer.getNextMove(gameState));
            clearHumanPlayerMoves(nextPlayer);
        }
        
	    MoveInfo moveInfo = null;
	    for (int i = 0; i < players.length; i++)
	        moveInfo = concatenate(moveInfo, gameEngine.applyMove(gameState, gameState.getPlayers()[i], moves.get(i)));
	    moveInfo = concatenate(moveInfo, gameEngine.advanceRound(gameState));
	    
	    moveInfoStack.push(moveInfo);
	    return gameState;
	}
	
	public boolean canUndoMove()
	{
	    return !moveInfoStack.isEmpty();
	}
	
	public GameState<CellType> undoMove()
	{
	    gameEngine.undo(moveInfoStack.pop());
	    return gameState;
	}
	
	public void addMoveInfo(MoveInfo moveInfo)
	{
	    moveInfoStack.push(concatenate(moveInfoStack.pop(), moveInfo));
	}
	
	
	private void clearHumanPlayerMoves(GamePlayer<CellType> currentPlayer)
	{
	    if (currentPlayer.isHumanPlayer())
	    {
	        for (GamePlayer<CellType> nextPlayer : players)
	            if ((nextPlayer != currentPlayer) && (nextPlayer.isHumanPlayer()))
	                nextPlayer.clearPreliminaryMove();
	    }
	}
    
	private static MoveInfo concatenate(MoveInfo one, MoveInfo two)
	{
	    return one == null ? two : one.concatenate(two);
	}
}
