package za.co.entelect.challenge.model;

import java.io.Serializable;

public class Player implements Cloneable, Serializable
{
    private static final long serialVersionUID = -592895262062889145L;
    
    private char playerKey;
    
    public Player(char playerKey)
    {
        this.playerKey = playerKey;
    }
    
    public char getPlayerKey()
    {
        return playerKey;
    }
    
    @Override
    public boolean equals(Object other)
    {
        return (other instanceof Player) && (((Player)other).playerKey == playerKey);
    }
    
    @Override
    public Object clone()
    {
        try
        {
            return super.clone();
        }
        catch (CloneNotSupportedException e) {}
        return null;
    }
}
