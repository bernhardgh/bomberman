package za.co.entelect.challenge.model;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

import za.co.entelect.challenge.logging.Logger;
import za.co.entelect.challenge.logging.Logger.LogLevel;

public abstract class SearchThreadCoordinator<CellType extends Cell>
{
    private static final Logger logger = Logger.getInstance();
    
    private GamePlayer<CellType> gamePlayer;
    private double currentScore;
    private int currentMoveMaxDepth = -1, nextMaxDepth;
    private boolean isSearching = false;
    private List<Runnable> runnables;

    public SearchThreadCoordinator(GamePlayer<CellType> gamePlayer, int startDepth)
    {
        this.gamePlayer = gamePlayer;
        nextMaxDepth = startDepth;
    }
    
    public boolean isSearching()
    {
        return isSearching;
    }
    
    public List<Runnable> getRunnables()
    {
        return runnables;
    }
    
    public boolean hasCurrentScore()
    {
        return currentMoveMaxDepth != -1;
    }
    
    public double getCurrentScore()
    {
        return currentScore;
    }
    
    public int getCurrentMoveMaxDepth()
    {
        return currentMoveMaxDepth;
    }
    
    public synchronized int nextMaxDepth()
    {
        return nextMaxDepth++;
    }
    
    public synchronized boolean perceiveSearchResult(int maxDepth, Move bestMove, boolean hasMoreDepthToSearch)
    {
        return perceiveSearchResult(maxDepth, bestMove, 0, hasMoreDepthToSearch);
    }
    
    public synchronized boolean perceiveSearchResult(int maxDepth, Move bestMove, double moveScore, boolean hasMoreDepthToSearch)
    {
        logger.log(LogLevel.TRACE, "Move for depth %d: %s", maxDepth, bestMove);
        
        if (maxDepth > currentMoveMaxDepth)
        {
            gamePlayer.setPreliminaryMove(bestMove);
            currentScore = moveScore;
            currentMoveMaxDepth = maxDepth;
        }
        if (!hasMoreDepthToSearch)
        {
            isSearching = false;
            notifyAll();
        }
        return isSearching;
    }
    
    public synchronized Move searchMove(GameState<CellType> gameState) throws InterruptedException
    {
        int threadCount = 2; // This is faster than getCpuCoreCount()  :)
            // HardwareInfoHelpers.getCpuCoreCount();
        ExecutorService executorService = Executors.newFixedThreadPool(threadCount, new ThreadFactory() {
            @Override
            public Thread newThread(Runnable r)
            {
                Thread thread = new Thread(r);
                thread.setDaemon(true);
                return thread;
            }
        });
        
        isSearching = true;
        runnables = new ArrayList<Runnable>(threadCount);
        List<Future<?>> futures = new ArrayList<>(threadCount);
        for (int i = 0; i < threadCount; i++)
        {
            @SuppressWarnings("unchecked")
            Runnable runnable = createSearchRunnable((GameState<CellType>)gameState.clone());
            runnables.add(runnable);
            futures.add(executorService.submit(runnable));
        }
        Thread.currentThread().setPriority(Thread.NORM_PRIORITY + 1);
        try
        {
            while (isSearching)
                wait();
            return gamePlayer.getPreliminaryMove();
        }
        finally
        {
            executorService.shutdownNow();
            executorService.awaitTermination(100, TimeUnit.MILLISECONDS);
            
            for (Future<?> nextFuture : futures)
            {
                try
                {
                    if (nextFuture.isDone())
                        nextFuture.get();
                }
                catch (ExecutionException e)
                {
                    logger.log(LogLevel.WARNING, "Search-runnable failed with ", e);
                    e.printStackTrace();
                }
            }
        }
    }
    
    protected abstract Runnable createSearchRunnable(GameState<CellType> gameState); 
}
