package za.co.entelect.challenge.model.bomberman.players;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.PriorityQueue;

import za.co.entelect.challenge.logging.Logger;
import za.co.entelect.challenge.logging.Logger.LogLevel;
import za.co.entelect.challenge.model.GamePlayer;
import za.co.entelect.challenge.model.GameState;
import za.co.entelect.challenge.model.Move;
import za.co.entelect.challenge.model.Player;
import za.co.entelect.challenge.model.bomberman.BombermanCell;
import za.co.entelect.challenge.model.bomberman.BombermanEngine;
import za.co.entelect.challenge.model.bomberman.BombermanMove;
import za.co.entelect.challenge.model.bomberman.BombermanPlayer;
import za.co.entelect.challenge.model.bomberman.BombermanState;
import za.co.entelect.challenge.model.bomberman.heuristics.BombermanHeuristic;
import za.co.entelect.challenge.model.bomberman.players.helpers.BombermanCellAssessor;
import za.co.entelect.challenge.model.bomberman.players.helpers.BombermanMoveHelpers;

public class BeamBombermanPlayer extends GamePlayer<BombermanCell>
{
    private static final int BEAM_SIZE = 100;
    private static final Logger logger = Logger.getInstance();
    
    private BombermanHeuristic heuristic;
    
    public BeamBombermanPlayer(Player player, BombermanHeuristic heuristic)
    {
        super(new BombermanEngine(), player);
        this.heuristic = heuristic;
    }

    @Override
    public Move getNextMove(GameState<BombermanCell> gameState) throws InterruptedException
    {
        BombermanState currentState = (BombermanState)gameState.clone();
        char playerKey = ((BombermanPlayer)getPlayer()).getPlayerKey();
        BombermanPlayer currentPlayer = currentState.getPlayer(playerKey);
        if (!currentPlayer.isAlive())
            return BombermanMove.DO_NOTHING;
        
        currentPlayer.setIsActive(true);
        for (BombermanPlayer nextPlayer : currentState.getPlayers())
            if (nextPlayer != currentPlayer)
                nextPlayer.setIsActive(false);
        
        BombermanEngine gameEngine = (BombermanEngine)getGameEngine();
        List<SearchNode> queue = new ArrayList<SearchNode>(BEAM_SIZE);
        queue.add(new SearchNode(currentState));
        PriorityQueue<SearchNode> nextPlyQueue = new PriorityQueue<SearchNode>();
        HashSet<BombermanState> nextPlyStates = new HashSet<>();
        BombermanCellAssessor assessor = new BombermanCellAssessor();
        
        int depthReached = 0, nodeCount = 0;
        try
        {
            for (int depth = 1; depth < 50; depth++)
            {
                boolean didExpandNodes = false;
                nextPlyQueue.clear();
                nextPlyStates.clear();
                for (SearchNode currentNode : queue)
                {
                    BombermanState bombermanState = currentNode.getGameState();
                    BombermanPlayer player = bombermanState.getPlayer(playerKey);
                    if ((gameEngine.gameDidEnd(bombermanState)) || (!player.isAlive()))
                    {
                        if (!nextPlyStates.contains(bombermanState))
                        {
                            nextPlyStates.add(bombermanState);
                            nextPlyQueue.offer(currentNode);
                        }
                    }
                    else
                    {
                        int playerPowerUpSum = player.getBombBagSize() + player.getBombRadius();
                        BombermanMove lastMove = currentNode.getLastMove();
                        boolean didGetPowerUp = currentNode.didGetPowerUp();
                        boolean hasBombsOrExplosions = bombermanState.hasBombsOrExplosions();
                        
                        for (BombermanMove nextMove : BombermanMove.values())
                            if (((didGetPowerUp) || (lastMove == null) || (!BombermanMoveHelpers.movesAreOpposite(lastMove, nextMove)))
                                    && ((nextMove != BombermanMove.DO_NOTHING) || (hasBombsOrExplosions))
                                    && (gameEngine.isMoveValid(bombermanState, player, nextMove)))
                            {
                                BombermanState nextState = (BombermanState)bombermanState.clone();
                                BombermanPlayer nextPlayer = nextState.getPlayer(playerKey);
                                gameEngine.applyMove(nextState, nextPlayer, nextMove);
                                BombermanPlayer threatPlayer = assessor.getThreatForLocation(nextPlayer.getRow(), nextPlayer.getColumn(), nextState, depth - 1);
                                if (threatPlayer != null)
                                    gameEngine.applyMove(nextState, threatPlayer, BombermanMove.TRIGGER_BOMB);
                                gameEngine.advanceRound(nextState);
                                
                                if (!nextPlyStates.contains(nextState))
                                {
                                    nextPlyStates.add(nextState);
                                    boolean nextDidGetPowerUp = nextPlayer.getBombBagSize() + nextPlayer.getBombRadius() > playerPowerUpSum;
                                    nextPlyQueue.offer(new SearchNode(currentNode, nextMove, nextDidGetPowerUp, nextState));
                                }
                                
                                nodeCount++;
                                didExpandNodes = true;
                            }
                    }
                    Thread.sleep(0);
                }
                
                if (!didExpandNodes)
                    break;
                
                SearchNode bestMoveNode = nextPlyQueue.poll();
                setPreliminaryMove(bestMoveNode.getFirstMove());
                depthReached = depth;
                
                queue.clear();
                queue.add(bestMoveNode);
                for (int i = 1; (i < BEAM_SIZE) && (!nextPlyQueue.isEmpty()); i++)
                    queue.add(nextPlyQueue.poll());
                
                logger.log(LogLevel.TRACE, "=== Next ply queue size: %d ===", queue.size());
                Thread.sleep(0);
            }
        }
        finally
        {
            logger.log(LogLevel.INFO, "==== Depth reached: %d ====", depthReached);
            logger.log(LogLevel.INFO, "==== Node count: %d ====", nodeCount);
        }
        
        return getPreliminaryMove();
    }
    
    
    private class SearchNode implements Comparable<SearchNode>
    {
        private static final double EPSILON = 1E-5;
        private static final double DECAY_FACTOR = 0.95;
        
        private BombermanState gameState;
        private BombermanMove firstMove = null;
        private BombermanMove lastMove = null;
        private boolean didGetPowerUp = false;
        private double evaluation = 0;
        private double compoundedEnumerator = 0, compoundedDenominator = 1;

        public SearchNode(BombermanState gameState)
        {
            this.gameState = gameState;
        }
        
        public SearchNode(SearchNode parent, BombermanMove currentMove, boolean didGetPowerUp, BombermanState gameState)
        {
            this(gameState);
            firstMove = parent.firstMove == null ? currentMove : parent.firstMove;
            lastMove = currentMove;
            this.didGetPowerUp = didGetPowerUp;
            
            evaluation = heuristic.evaluate(gameState, gameState.getPlayer(((BombermanPlayer)getPlayer()).getPlayerKey()));
            compoundedEnumerator = parent.compoundedEnumerator*DECAY_FACTOR + evaluation;
            compoundedDenominator = compoundedDenominator*DECAY_FACTOR + 1; 
        }
        
        public BombermanMove getFirstMove()
        {
            return firstMove;
        }
        
        public BombermanMove getLastMove()
        {
            return lastMove;
        }
        
        public boolean didGetPowerUp()
        {
            return didGetPowerUp;
        }
        
        public BombermanState getGameState()
        {
            return gameState;
        }

        @Override
        public int compareTo(SearchNode other)
        {
            if (Math.abs(evaluation - other.evaluation) < EPSILON)
            {
                double compounded = compoundedEnumerator / compoundedDenominator, otherCompounded = other.compoundedEnumerator / other.compoundedDenominator;
                if (compounded > otherCompounded)
                    return -1;
                return compounded == otherCompounded ? 0 : 1;
            }
            return evaluation > other.evaluation ? -1 : 1;
        }
    }
}
