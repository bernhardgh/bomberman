package za.co.entelect.challenge.model;

import java.util.Random;

public abstract class GamePlayer<CellType extends Cell>
{
	protected static Random generator = new Random();

    private GameEngine<CellType> gameEngine;
    private Player player;
    private Move preliminaryMove;
    
    protected GamePlayer(GameEngine<CellType> gameEngine, Player player)
    {
        this.gameEngine = gameEngine;
        this.player = player;
    }
    
    public GameEngine<CellType> getGameEngine()
    {
        return gameEngine;
    }
    
    public Player getPlayer()
    {
        return player;
    }
    
    public void setPlayer(Player player)
    {
        this.player = player;
    }
    
    public boolean isHumanPlayer()
    {
        return false;
    }
    
    public void clearPreliminaryMove()
    {
        preliminaryMove = null;
    }
    
    public Move getPreliminaryMove()
    {
        return preliminaryMove;
    }
    
    public void setPreliminaryMove(Move newPreliminary)
    {
        assert newPreliminary!=null : "Preliminary move must not be null!";
        
        preliminaryMove = newPreliminary;
    }
    
    public abstract Move getNextMove(GameState<CellType> gameState) throws InterruptedException;
}
