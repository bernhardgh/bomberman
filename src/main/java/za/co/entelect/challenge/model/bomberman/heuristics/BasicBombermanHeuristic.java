package za.co.entelect.challenge.model.bomberman.heuristics;

import za.co.entelect.challenge.model.bomberman.BombermanPlayer;
import za.co.entelect.challenge.model.bomberman.BombermanState;

public class BasicBombermanHeuristic extends BombermanHeuristic
{
    @Override
    public double evaluate(BombermanState gameState, BombermanPlayer player)
    {
        return getBaseScore(gameState, player);
    }
}
