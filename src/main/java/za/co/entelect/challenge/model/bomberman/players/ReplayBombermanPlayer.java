package za.co.entelect.challenge.model.bomberman.players;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import za.co.entelect.challenge.dto.reader.GameStateReaderFactory;
import za.co.entelect.challenge.logging.Logger;
import za.co.entelect.challenge.logging.Logger.LogLevel;
import za.co.entelect.challenge.model.GamePlayer;
import za.co.entelect.challenge.model.GameState;
import za.co.entelect.challenge.model.Move;
import za.co.entelect.challenge.model.Player;
import za.co.entelect.challenge.model.bomberman.BombermanCell;
import za.co.entelect.challenge.model.bomberman.BombermanMove;
import za.co.entelect.challenge.model.bomberman.BombermanPlayer;
import za.co.entelect.challenge.model.bomberman.BombermanState;

public class ReplayBombermanPlayer extends GamePlayer<BombermanCell>
{
    public static final String REPLAY_FILE_BASE = "/Users/bernhard/temp/bomberman/replay";
    public static final String FILE_NAME_FORMAT = "%d/state.json";
    
    public ReplayBombermanPlayer(Player player)
    {
        super(null, player);
    }

    @Override
    public Move getNextMove(GameState<BombermanCell> gameState)
    {
        BombermanState currentState = (BombermanState)gameState;
        BombermanPlayer currentPlayer = currentState.getPlayer(getPlayer().getPlayerKey());
        
        if (!currentPlayer.isAlive())
            return BombermanMove.DO_NOTHING;
        
        BombermanState nextState;
        Path nextStateFilePath = Paths.get(REPLAY_FILE_BASE, String.format(FILE_NAME_FORMAT, currentState.getGameRound() + 1));
        try (FileInputStream fileStream = new FileInputStream(nextStateFilePath.toFile()))
        {
            nextState = (BombermanState)GameStateReaderFactory.getDefaultGameStateReader().read(fileStream, null);
        }
        catch (IOException e)
        {
            return BombermanMove.DO_NOTHING;
        }
        
        BombermanPlayer nextPlayer = nextState.getPlayer(getPlayer().getPlayerKey());
        if (!nextPlayer.isAlive())
            return BombermanMove.DO_NOTHING;
        
        if ((nextPlayer.getColumn() == currentPlayer.getColumn()) && (nextPlayer.getRow() == currentPlayer.getRow()))
        {
            BombermanCell playerCell = nextState.getGrid().getCell(nextPlayer.getRow(), nextPlayer.getColumn());
            if ((playerCell.hasBomb()) && (playerCell.getBombCountdown() == nextPlayer.getBombTimer()))
                return BombermanMove.PLACE_BOMB;
            
            for (BombermanCell nextBombCell : nextPlayer.getCellsWithBombs())
                if (nextBombCell.getBombCountdown() == 1)
                {
                    BombermanCell currentCell = currentState.getGrid().getCell(nextBombCell.getRow(), nextBombCell.getColumn());
                    if ((currentCell.hasBomb()) && (currentCell.getBombCountdown() >= 2))
                        return BombermanMove.TRIGGER_BOMB;
                }
            return BombermanMove.DO_NOTHING;
        }
        
        if (nextPlayer.getColumn() == currentPlayer.getColumn())
        {
            if (nextPlayer.getRow() == currentPlayer.getRow() - 1)
                return BombermanMove.UP;
            if (nextPlayer.getRow() == currentPlayer.getRow() + 1)
                return BombermanMove.DOWN;
        }
        else if (nextPlayer.getRow() == currentPlayer.getRow())
        {
            if (nextPlayer.getColumn() == currentPlayer.getColumn() - 1)
                return BombermanMove.LEFT;
            if (nextPlayer.getColumn() == currentPlayer.getColumn() + 1)
                return BombermanMove.RIGHT;
        }
        
        Logger.getInstance().log(LogLevel.WARNING, "Move for player %c could not be determined; returning DO_NOTHING.", nextPlayer.getPlayerKey());
        return BombermanMove.DO_NOTHING;
    }
}
