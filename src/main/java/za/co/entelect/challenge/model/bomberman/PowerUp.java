package za.co.entelect.challenge.model.bomberman;

import java.io.Serializable;

public class PowerUp implements Cloneable, Serializable
{
    private static final long serialVersionUID = 5257550298782145523L;

    public enum PowerUpType { BOMB_RADIUS_POWER_UP, BOMB_BAG_POWER_UP }
    
    private int row, column;
    private PowerUpType powerUpType;
    
    public PowerUp(int row, int column, PowerUpType powerUpType)
    {
        this.row = row;
        this.column = column;
        this.powerUpType = powerUpType;
    }
    
    public int getRow()
    {
        return row;
    }
    
    public int getColumn()
    {
        return column;
    }
    
    public PowerUpType getPowerUpType()
    {
        return powerUpType;
    }
    
    @Override
    public Object clone()
    {
        try
        {
            return super.clone();
        }
        catch (CloneNotSupportedException e) {}
        return null;
    }
}
