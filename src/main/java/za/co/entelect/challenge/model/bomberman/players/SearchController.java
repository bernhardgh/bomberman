package za.co.entelect.challenge.model.bomberman.players;

import java.util.AbstractList;
import java.util.Map;
import java.util.Stack;

import za.co.entelect.challenge.model.bomberman.BombermanEngine;
import za.co.entelect.challenge.model.bomberman.BombermanMove;
import za.co.entelect.challenge.model.bomberman.BombermanPlayer;
import za.co.entelect.challenge.model.bomberman.BombermanState;
import za.co.entelect.challenge.util.ComputedHash;
import za.co.entelect.challenge.util.FixedSizeHashMap;

public class SearchController
{
    private BombermanEngine gameEngine;
    private BombermanState gameState;
    private BombermanPlayer mainPlayer;
    private SearchPly[] searchPlies;
    private int mainPlayerPlyIndex;
    private int currentSearchPlyIndex = 0;
    private int currentPlyDepth = 0;
    
    private Stack<double[]> intermediatePlayerScores = new Stack<>();
    
    public SearchController(BombermanEngine gameEngine, BombermanState gameState, BombermanPlayer mainPlayer, SearchPly... searchPlies)
    {
        this.gameEngine = gameEngine;
        this.gameState = gameState;
        this.mainPlayer = mainPlayer;
        this.searchPlies = searchPlies;

        for (int i = 0; i < searchPlies.length; i++)
        {
            SearchPly nextPly = searchPlies[i];
            if ((nextPly instanceof PlayerSearchPly) && (((PlayerSearchPly)nextPly).getPlayer().isMainPlayer()))
            {
                mainPlayerPlyIndex = i;
                break;
            }
        }
    }
    
    public BombermanEngine getGameEngine()
    {
        return gameEngine;
    }
    
    public BombermanState getGameState()
    {
        return gameState;
    }
    
    public BombermanPlayer getMainPlayer()
    {
        return mainPlayer;
    }
    
    public SearchPly[] getSearchPlies()
    {
        return searchPlies;
    }
    
    public void resetSearchPlies()
    {
        for (int i = 0; i < searchPlies.length - 1; i++)
            ((SearchController.PlayerSearchPly)searchPlies[i]).reset();
    }
    
    public int getCurrentSearchPlyIndex()
    {
        if (currentPlyDepth == 0)
            return mainPlayerPlyIndex;
        return currentPlyDepth <= mainPlayerPlyIndex ? currentSearchPlyIndex - 1 : currentSearchPlyIndex;
    }
    
    public SearchPly getCurrentPly()
    {
        return searchPlies[getCurrentSearchPlyIndex()];
    }
    
    public int getCurrentPlyDepth()
    {
        return currentPlyDepth;
    }
    
    public void nextPly()
    {
        currentSearchPlyIndex = (currentSearchPlyIndex + 1) % searchPlies.length;
        currentPlyDepth++;
    }
    
    public void previousPly()
    {
        if (--currentSearchPlyIndex < 0)
            currentSearchPlyIndex = searchPlies.length - 1;
        currentPlyDepth--;
    }
    
    public void pushIntermediateScores(double[] intermediateScore)
    {
        intermediatePlayerScores.push(intermediateScore);
    }
    
    public void popIntermediateScores()
    {
        intermediatePlayerScores.pop();
    }
    
    public AbstractList<double[]> getCurrentIntermediateScores()
    {
        return intermediatePlayerScores;
    }
    
    
    public static abstract class SearchPly
    {
        public static AdvanceRoundPly getAdvanceRoundPly()
        {
            return new AdvanceRoundPly();
        }
        
        public static PlayerSearchPly getPlayerPly(BombermanPlayer player)
        {
            return new PlayerSearchPly(player);
        }
    }
    
    public static class AdvanceRoundPly extends SearchPly
    {
        private AdvanceRoundPly() {}
    }
    
    public static class PlayerSearchPly extends SearchPly
    {
        private BombermanPlayer player;
        private BombermanMove lastMove = null;
        private boolean didGetPowerUp = false;
        private Score alpha = null;
        private BombermanMove bestMove;
        
        private Map<ComputedHash, CacheEntry> scoreCache = new FixedSizeHashMap<>(100000);
        
        private PlayerSearchPly(BombermanPlayer player)
        {
            this.player = player;
        }
        
        public BombermanPlayer getPlayer()
        {
            return player;
        }
        
        public void reset()
        {
            lastMove = null;
            didGetPowerUp = false;
        }
        
        public BombermanMove getLastMove()
        {
            return lastMove;
        }
        
        public void setLastMove(BombermanMove move)
        {
            lastMove = move;
        }
        
        public boolean didGetPowerUp()
        {
            return didGetPowerUp;
        }
        
        public void setDidGetPowerUp(boolean flag)
        {
            didGetPowerUp = flag;
        }
        
        public Score getAlpha()
        {
            return alpha;
        }
        
        public void setAlpha(Score newAlpha)
        {
            alpha = newAlpha;
        }
        
        public void setBestMove(BombermanMove bestMove)
        {
            this.bestMove = bestMove;
        }
        
        public BombermanMove getBestMove()
        {
            return bestMove;
        }
        
        public Map<ComputedHash, CacheEntry> getScoreCache()
        {
            return scoreCache;
        }
    }
    
    
    public static class CacheEntry
    {
        public enum EntryType { EXACT, LOWER_BOUND, UPPER_BOUND };
        
        private Score score;
        private int depth;
        private EntryType entryType;
        
        public CacheEntry(Score score, int depth, EntryType entryType)
        {
            this.score = score;
            this.depth = depth;
            this.entryType = entryType;
        }
        
        public Score getScore()
        {
            return score;
        }
        
        public int getDepth()
        {
            return depth;
        }
        
        public EntryType getEntryType()
        {
            return entryType;
        }
    }
}
