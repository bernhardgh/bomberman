package za.co.entelect.challenge.model;

import za.co.entelect.challenge.logging.Logger;
import za.co.entelect.challenge.logging.Logger.LogLevel;

public class TimeLimitedGamePlayer<CellType extends Cell> extends GamePlayer<CellType>
{
    private static final Logger logger = Logger.getInstance();
    
    private GamePlayer<CellType> innerPlayer, backupPlayer;
    private long timeLimitMillis, hardTimeLimitMillis;
    
    public TimeLimitedGamePlayer(GamePlayer<CellType> innerPlayer, GamePlayer<CellType> backupPlayer)
    {
        this(innerPlayer, backupPlayer, 0, 0);
    }
    
    public TimeLimitedGamePlayer(GamePlayer<CellType> innerPlayer, GamePlayer<CellType> backupPlayer, long timeLimitMillis, long hardTimeLimitMillis) 
    {
        super(innerPlayer.getGameEngine(), innerPlayer.getPlayer());
        this.innerPlayer = innerPlayer;
        this.backupPlayer = backupPlayer;
        this.timeLimitMillis = timeLimitMillis;
        this.hardTimeLimitMillis = hardTimeLimitMillis;
    }
    
    public void setPlayer(Player player)
    {
        innerPlayer.setPlayer(player);
        backupPlayer.setPlayer(player);
    }
    
    public GamePlayer<CellType> getInnerPlayer()
    {
        return innerPlayer;
    }
    
    public GamePlayer<CellType> getBackupPlayer()
    {
        return backupPlayer;
    }
    
    public long getTimeLimitMillis()
    {
        return timeLimitMillis;
    }
    
    public void setTimeLimitMillis(long newTimeLimit)
    {
        timeLimitMillis = newTimeLimit;
    }
    
    public long getHardTimeLimitMillis()
    {
        return hardTimeLimitMillis;
    }
    
    public void setHardTimeLimitMillis(long newTimeLimit)
    {
        hardTimeLimitMillis = newTimeLimit;
    }

    public synchronized Move getNextMove(GameState<CellType> gameState) throws InterruptedException
    {
        innerPlayer.clearPreliminaryMove();
        ComputeMoveThread t = new ComputeMoveThread(gameState);
        t.setDaemon(true);
        Thread.currentThread().setPriority(Thread.MAX_PRIORITY);
        try
        {
            t.start();
            wait(Math.max(timeLimitMillis, 1));
            if ((t.isAlive()) && (!t.isDone()))
            {
                t.interrupt();
                wait(Math.max(hardTimeLimitMillis - timeLimitMillis, 1));
            }
        }
        catch (InterruptedException e)
        {
            if (t.isAlive())
                t.interrupt();
            throw e;
        }
        finally
        {
            Move preliminaryMove = innerPlayer.getPreliminaryMove();
            if (preliminaryMove == null)
            {
                logger.log(LogLevel.WARNING, "No preliminary move! Use backup player.");
                setPreliminaryMove(backupPlayer.getNextMove(gameState));
            }
            else
                setPreliminaryMove(preliminaryMove);
        }
        return getPreliminaryMove();
    }
    
    
    private class ComputeMoveThread extends Thread
    {
        private GameState<CellType> gameState;
        private boolean isDone = false;
        
        public ComputeMoveThread(GameState<CellType> gameState)
        {
            this.gameState = gameState;
        }
        
        public boolean isDone()
        {
            return isDone;
        }
        
        public void run()
        {
            try
            {
                innerPlayer.setPreliminaryMove(innerPlayer.getNextMove(gameState));
            }
            catch (InterruptedException e) {}
            catch (Exception e)
            {
                logger.log(LogLevel.WARNING, "getNextMove() failed with ", e);
            }
            finally
            {
                isDone = true;
                synchronized(TimeLimitedGamePlayer.this)
                {
                    TimeLimitedGamePlayer.this.notify();
                }
            }
        }
    }
}
