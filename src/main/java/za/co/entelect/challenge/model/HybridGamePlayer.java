package za.co.entelect.challenge.model;

public abstract class HybridGamePlayer<CellType extends Cell> extends GamePlayer<CellType>
{
    private GamePlayer<CellType> primaryGamePlayer, secondaryGamePlayer;
    private boolean usingPrimaryPlayer;
    
    public HybridGamePlayer(GamePlayer<CellType> primaryGamePlayer, GamePlayer<CellType> secondaryGamePlayer)
    {
        super(primaryGamePlayer.getGameEngine(), primaryGamePlayer.getPlayer());
        this.primaryGamePlayer = primaryGamePlayer;
        this.secondaryGamePlayer = secondaryGamePlayer;
    }

    public GamePlayer<CellType> getPrimaryGamePlayer()
    {
        return primaryGamePlayer;
    }
    
    public GamePlayer<CellType> getSecondaryGamePlayer()
    {
        return secondaryGamePlayer;
    }
    
    public final Move getNextMove(GameState<CellType> gameState) throws InterruptedException
    {
        usingPrimaryPlayer = usePrimaryPlayer(gameState);
        return usingPrimaryPlayer ? primaryGamePlayer.getNextMove(gameState) : secondaryGamePlayer.getNextMove(gameState);
    }
    
    public Move getPreliminaryMove()
    {
        if (super.getPreliminaryMove() == null)
            return usingPrimaryPlayer ? primaryGamePlayer.getPreliminaryMove() : secondaryGamePlayer.getPreliminaryMove();
        return super.getPreliminaryMove();
    }
    
    protected abstract boolean usePrimaryPlayer(GameState<CellType> gameState);
}
