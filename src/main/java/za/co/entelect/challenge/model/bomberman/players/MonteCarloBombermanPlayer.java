package za.co.entelect.challenge.model.bomberman.players;

import java.util.ArrayList;
import java.util.List;

import za.co.entelect.challenge.model.GameEngine;
import za.co.entelect.challenge.model.GamePlayer;
import za.co.entelect.challenge.model.GameState;
import za.co.entelect.challenge.model.Move;
import za.co.entelect.challenge.model.Player;
import za.co.entelect.challenge.model.bomberman.BombermanCell;
import za.co.entelect.challenge.model.bomberman.BombermanEngine;
import za.co.entelect.challenge.model.bomberman.BombermanMove;
import za.co.entelect.challenge.model.bomberman.BombermanPlayer;
import za.co.entelect.challenge.model.bomberman.BombermanState;

public class MonteCarloBombermanPlayer extends GamePlayer<BombermanCell>
{
    private static final double EXPLORATION_FACTOR = Math.sqrt(2);
    private static final int EXPANSION_THRESHOLD = 30;
    
    public MonteCarloBombermanPlayer(Player player)
    {
        super(new BombermanEngine(), player);
    }

    @Override
    public Move getNextMove(GameState<BombermanCell> gameState) throws InterruptedException
    {
        if (!((BombermanPlayer) getPlayer()).isAlive())
            return BombermanMove.DO_NOTHING;

        BombermanState bombermanState = (BombermanState)gameState;
        BombermanPlayer[] players = bombermanState.getPlayers();
        int playerIndex = 0;
        for (int i = 0; i < players.length; i++)
            if (players[i] == getPlayer())
                playerIndex = i;
        
        SearchNode root = new SearchNode(bombermanState, playerIndex);
        while (true)
        {
            SearchNode node = selectNode(root);
            if (play(node))
                node.registerWin();
            else
                node.registerLose();
            
            if (node.getPlayCount() >= EXPANSION_THRESHOLD)
                node.expand();
            
            setPreliminaryMove(computePreliminaryMove(root));
            Thread.sleep(0);
        }
    }
    
    private boolean play(SearchNode startNode)
    {
        GameEngine<BombermanCell> gameEngine = getGameEngine();
        BombermanState gameState = (BombermanState)startNode.getGameState().clone();
        Player[] players = gameState.getPlayers();
        int playerIndex = startNode.getNextPlayerIndex();
        
        SimpleBombermanPlayer[] gamePlayers = new SimpleBombermanPlayer[players.length];
        for (int i = 0; i < players.length; i++)
            gamePlayers[i] = new SimpleBombermanPlayer(players[i]);
        
        while (!gameEngine.gameDidEnd(gameState))
        {
            gameEngine.applyMove(gameState, players[playerIndex], gamePlayers[playerIndex].getNextMove(gameState));
            playerIndex = (playerIndex + 1) % players.length;
            if (playerIndex == 0)
                gameEngine.advanceRound(gameState);
        }
        
        return gameEngine.playerDidWin(gameState, gameState.getPlayer(getPlayer().getPlayerKey()));
    }
    
    private static SearchNode selectNode(SearchNode root)
    {
        SearchNode node = root;
        while (!node.isLeaf())
        {
            double explorationEnumerator = Math.log(node.getPlayCount());
            SearchNode nextNode = null;
            double maxValue = Double.NEGATIVE_INFINITY;
            for (SearchNode nextChild : node.getChildren())
            {
                double value = nextChild.getPlayCount() == 0 
                        ? Double.MAX_VALUE 
                        : (double)nextChild.getWinCount()/nextChild.getPlayCount() + EXPLORATION_FACTOR*Math.sqrt(explorationEnumerator / nextChild.getPlayCount());
                if (value > maxValue)
                {
                    nextNode = nextChild;
                    maxValue = value;
                }
            }
            node = nextNode;
        }
        return node;
    }
    
    private static BombermanMove computePreliminaryMove(SearchNode root)
    {
        double maxValue = Double.NEGATIVE_INFINITY;
        BombermanMove move = null;
        for (SearchNode nextNode : root.getChildren())
        {
            double value = (double)nextNode.getWinCount() / nextNode.getPlayCount();
            if (value > maxValue)
            {
                maxValue = value;
                move = nextNode.getMove();
            }
        }
        return move;
    }
    
    
    private class SearchNode
    {
        private BombermanState gameState;
        private BombermanMove move;
        private int nextPlayerIndex;
        
        private SearchNode parent;
        private List<SearchNode> children = null;
        private int winCount = 0, playCount = 0;
        
        public SearchNode(BombermanState gameState, BombermanMove move, int nextPlayerIndex, SearchNode parent)
        {
            this.gameState = gameState;
            this.move = move;
            this.nextPlayerIndex = nextPlayerIndex;
            this.parent = parent;
        }
        
        public SearchNode(BombermanState gameState, int nextPlayerIndex)
        {
            this(gameState, null, nextPlayerIndex, null);
            expand();
        }
        
        public BombermanState getGameState()
        {
            return gameState;
        }
        
        public BombermanMove getMove()
        {
            return move;
        }
        
        public int getNextPlayerIndex()
        {
            return nextPlayerIndex;
        }
        
        public boolean isLeaf()
        {
            return children == null;
        }
        
        public List<SearchNode> getChildren()
        {
            return children;
        }
        
        public void expand()
        {
            GameEngine<BombermanCell> gameEngine = getGameEngine();
            if (gameEngine.gameDidEnd(gameState))
                return;
            
            Player player = gameState.getPlayers()[nextPlayerIndex];
            int nextNextPlayerIndex = (nextPlayerIndex + 1) % gameState.getPlayers().length;
            boolean doAdvanceRound = nextNextPlayerIndex == 0;
            
            children = new ArrayList<SearchNode>(7);
            for (BombermanMove move : BombermanMove.values())
                if (((move != BombermanMove.DO_NOTHING) || (gameState.hasBombsOrExplosions())) && (gameEngine.isMoveValid(gameState, player, move)))
                    children.add(new SearchNode(getNextState(move, doAdvanceRound), move, nextNextPlayerIndex, this));
            
            if (children.isEmpty())
                children.add(new SearchNode(getNextState(BombermanMove.DO_NOTHING, doAdvanceRound), BombermanMove.DO_NOTHING, nextNextPlayerIndex, this));
        }
        
        private BombermanState getNextState(BombermanMove move, boolean doAdvanceRound)
        {
            GameEngine<BombermanCell> gameEngine = getGameEngine();
            BombermanState nextState = (BombermanState)gameState.clone();
            gameEngine.applyMove(nextState, nextState.getPlayers()[nextPlayerIndex], move);
            if (doAdvanceRound)
                gameEngine.advanceRound(nextState);
            return nextState;
        }
        
        public int getWinCount()
        {
            return winCount;
        }
        
        public int getPlayCount()
        {
            return playCount;
        }
        
        public void registerWin()
        {
            winCount++;
            playCount++;
            
            if (parent != null)
                parent.registerWin();
        }
        
        public void registerLose()
        {
            playCount++;
            
            if (parent != null)
                parent.registerLose();
        }
    }
}
