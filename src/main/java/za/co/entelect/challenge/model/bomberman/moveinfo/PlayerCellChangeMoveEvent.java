package za.co.entelect.challenge.model.bomberman.moveinfo;

import za.co.entelect.challenge.model.bomberman.BombermanCell;
import za.co.entelect.challenge.model.bomberman.BombermanPlayer;

public class PlayerCellChangeMoveEvent extends CellChangeMoveEvent
{
    private BombermanPlayer newOwner;
    private int oldRow, oldColumn;
    private int bombRadius, availableBombCount, bombBagSize, baseScore;
    private boolean wasNewVisit;
    
    public PlayerCellChangeMoveEvent(BombermanCell cell, BombermanPlayer newOwner, boolean isNewVisit)
    {
        super(cell);
        
        this.newOwner = newOwner;
        oldRow = newOwner.getRow();
        oldColumn = newOwner.getColumn();
        bombRadius = newOwner.getBombRadius();
        availableBombCount = newOwner.getAvailableBombCount();
        bombBagSize = newOwner.getBombBagSize();
        baseScore = newOwner.getBaseScore();
        wasNewVisit = isNewVisit;
    }

    @Override
    public void undo()
    {
        super.undo();
        
        newOwner.setRowAndColumn(oldRow, oldColumn);
        newOwner.setBombRadius(bombRadius);
        newOwner.setBombCounts(availableBombCount, bombBagSize);
        newOwner.setBaseScore(baseScore);
        
        if (wasNewVisit)
        {
            cell.getPlayersVisited().remove((Character)newOwner.getPlayerKey());
            newOwner.decrementGridCoverage();
        }
    }
}
