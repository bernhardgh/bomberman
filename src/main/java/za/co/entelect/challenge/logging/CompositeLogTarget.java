package za.co.entelect.challenge.logging;

public class CompositeLogTarget implements LogTarget
{
    private LogTarget logTarget1, logTarget2;
    
    public CompositeLogTarget(LogTarget logTarget1, LogTarget logTarget2)
    {
        this.logTarget1 = logTarget1;
        this.logTarget2 = logTarget2;
    }

    @Override
    public void log(String message)
    {
        logTarget1.log(message);
        logTarget2.log(message);
    }
}
