package za.co.entelect.challenge.logging;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

public class Logger
{
    public enum LogLevel { ERROR, WARNING, INFO, TRACE };
    
    private static Logger instance = null;
    
    private LogLevel logLevel = LogLevel.WARNING;
    private LogTarget logTarget = new ConsoleLogTarget();
    
    private Logger() {}
    
    public static Logger getInstance()
    {
        if (instance == null)
            instance = new Logger();
        return instance;
    }
    
    public LogLevel getLogLevel()
    {
        return logLevel;
    }
    
    public void setLogLevel(LogLevel newLogLevel)
    {
        logLevel = newLogLevel;
    }
    
    public void setLogTarget(LogTarget newLogTarget)
    {
        logTarget = newLogTarget;
    }
    
    public void log(LogLevel logLevel, String formatString, Object... formatArgs)
    {
        if (logLevel.ordinal() <= this.logLevel.ordinal())
        {
            StringBuilder sb = new StringBuilder();
            sb.append(logLevel).append(": ").append(String.format(formatString, formatArgs));
            logTarget.log(sb.toString());
        }
    }
    
    public void log(LogLevel logLevel, String message, Exception exception)
    {
        if (logLevel.ordinal() <= this.logLevel.ordinal())
        {
            String stackTrace = null;
            try (StringWriter stringWriter = new StringWriter())
            {
                try (PrintWriter writer = new PrintWriter(stringWriter))
                {
                    exception.printStackTrace(writer);
                }
                stackTrace = stringWriter.toString();
            }
            catch (IOException e) {}
            
            log(logLevel, message + "%s", stackTrace);
        }
    }
}
