package za.co.entelect.challenge.logging;

public class ConsoleLogTarget implements LogTarget
{
    @Override
    public void log(String message)
    {
        System.out.println(message);
    }
}
