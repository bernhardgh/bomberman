package za.co.entelect.challenge.logging;

public interface LogTarget
{
    public void log(String message);
}
