package za.co.entelect.challenge.logging;

import java.util.ArrayList;
import java.util.List;

public class ListLogTarget implements LogTarget
{
    private List<String> logs = new ArrayList<String>();
    
    @Override
    public void log(String message)
    {
        logs.add(message);
    }
    
    public List<String> getLogs()
    {
        return logs;
    }
}
