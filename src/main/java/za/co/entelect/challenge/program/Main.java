package za.co.entelect.challenge.program;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.lang.management.ManagementFactory;
import java.nio.file.Paths;

import za.co.entelect.challenge.dto.Coverage;
import za.co.entelect.challenge.dto.reader.GameStateReaderFactory;
import za.co.entelect.challenge.logging.Logger;
import za.co.entelect.challenge.logging.Logger.LogLevel;
import za.co.entelect.challenge.model.GamePlayer;
import za.co.entelect.challenge.model.Move;
import za.co.entelect.challenge.model.Player;
import za.co.entelect.challenge.model.TimeLimitedGamePlayer;
import za.co.entelect.challenge.model.bomberman.BombermanCell;
import za.co.entelect.challenge.model.bomberman.BombermanEngine;
import za.co.entelect.challenge.model.bomberman.BombermanMove;
import za.co.entelect.challenge.model.bomberman.BombermanState;
import za.co.entelect.challenge.model.bomberman.heuristics.BombermanHeuristic;
import za.co.entelect.challenge.model.bomberman.heuristics.TermBombermanHeuristic;
import za.co.entelect.challenge.model.bomberman.players.BeamBombermanPlayer;
import za.co.entelect.challenge.model.bomberman.players.DFSBombermanPlayer;
import za.co.entelect.challenge.model.bomberman.players.HybridAdversarialSearchBombermanPlayer;
import za.co.entelect.challenge.model.bomberman.players.HybridSearchBombermanPlayer;
import za.co.entelect.challenge.model.bomberman.players.InstantKillBombermanPlayer;
import za.co.entelect.challenge.model.bomberman.players.MultiThreadedAlphaBetaBombermanPlayer;
import za.co.entelect.challenge.model.bomberman.players.MultiThreadedDFSBombermanPlayer;
import za.co.entelect.challenge.model.bomberman.players.SafeBombermanPlayer;

public class Main
{
    public static void main(String... args) throws IOException
    {
        char playerKey = args[0].charAt(0);
        String inputFolderPath = args[1].replace("\"", "");
        
        Logger logger = Logger.getInstance();
        logger.log(LogLevel.INFO, "Start");

        File coverageFile = new File("coverage");
        final BombermanState gameState = readGameState(Paths.get(inputFolderPath, "state.json").toFile(), coverageFile);
        logger.log(LogLevel.INFO, "Finished reading state.json");
        
        Thread updateCoverageFileThread = startUpdateCoverageFile(coverageFile, gameState);
        
        GamePlayer<BombermanCell> gamePlayer = getGamePlayer(gameState.getPlayer(playerKey));
        Move move;
        try
        {
            move = gamePlayer.getNextMove(gameState);
        }
        catch (InterruptedException e)
        {
            move = gamePlayer.getPreliminaryMove();
        }

        logger.log(LogLevel.INFO, "Write move.txt");
        try (FileWriter writer = new FileWriter(Paths.get(inputFolderPath, "move.txt").toFile()))
        {
            writer.write(Integer.toString(((BombermanMove)move).ordinal()));
        }

        logger.log(LogLevel.INFO, "Join coverage file thread...");
        try
        {
            updateCoverageFileThread.join(100);
        }
        catch (InterruptedException e) {}
        logger.log(LogLevel.INFO, "Done.");
    }
    
    private static BombermanState readGameState(File stateFile, File coverageFile) throws FileNotFoundException, IOException
    {
        InputStream coverageStream = null;
        try (FileInputStream jsonIn = new FileInputStream(stateFile))
        {
            if (coverageFile.exists())
                coverageStream = new BufferedInputStream(new FileInputStream(coverageFile));
            BombermanState gameState = (BombermanState)GameStateReaderFactory.getDefaultGameStateReader().read(jsonIn, coverageStream);
            
            (new BombermanEngine()).advanceRound(gameState);
            gameState.initializeCodeIntegration();
            return gameState;
        }
        finally
        {
            if (coverageStream != null)
                coverageStream.close();
        }
    }
    
    private static Thread startUpdateCoverageFile(File coverageFile, BombermanState gameState)
    {
        Thread updateCoverageFileThread = new Thread() {
            public void run()
            {
                try (FileOutputStream coverageOutStream = new FileOutputStream(coverageFile);
                        BufferedOutputStream bufferStream = new BufferedOutputStream(coverageOutStream))
                {
                    (new Coverage((BombermanState)gameState.clone())).saveCoverage(bufferStream);
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
        };
        updateCoverageFileThread.setDaemon(true);
        updateCoverageFileThread.setPriority(Thread.NORM_PRIORITY + 1);
        updateCoverageFileThread.start();
        return updateCoverageFileThread;
    }
    
    private static GamePlayer<BombermanCell> getGamePlayer(Player player)
    {
        BombermanHeuristic heuristic = new TermBombermanHeuristic();
        GamePlayer<BombermanCell> gamePlayer = new InstantKillBombermanPlayer(
                new HybridSearchBombermanPlayer(
                        new BeamBombermanPlayer(player, new TermBombermanHeuristic(true)), 
                        new HybridAdversarialSearchBombermanPlayer(
                                new MultiThreadedAlphaBetaBombermanPlayer(player, heuristic),
                                new MultiThreadedDFSBombermanPlayer(player, new DFSBombermanPlayer.NearPlayersSelectionStrategyFactory(HybridAdversarialSearchBombermanPlayer.DEFAULT_PLAYER_DISTANCE_THRESHOLD), heuristic))));
        
        long timeLimit = Math.max(200, 1300 - (System.currentTimeMillis() - ManagementFactory.getRuntimeMXBean().getStartTime()));
        Logger.getInstance().log(LogLevel.INFO, "Time limit: %d", timeLimit);
        return new TimeLimitedGamePlayer<BombermanCell>(gamePlayer, new SafeBombermanPlayer(player), Math.max(0, timeLimit - 200), timeLimit);
    }
}
