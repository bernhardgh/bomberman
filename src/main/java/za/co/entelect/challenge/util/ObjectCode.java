package za.co.entelect.challenge.util;

import java.io.Serializable;

public class ObjectCode implements Cloneable, Serializable
{
    private static final long serialVersionUID = -4198066458728631944L;
    
    private int hashCode = 0, checkCode = 0;
    
    public ComputedHash getComputedHash()
    {
        return new ComputedHash(hashCode, checkCode);
    }
    
    public void adjustCodes(int hashCodeAdjustment, int checkCodeAdjustment)
    {
        hashCode += hashCodeAdjustment;
        checkCode += checkCodeAdjustment;
    }

    @Override
    public int hashCode()
    {
        return hashCode;
    }
    
    @Override
    public boolean equals(Object other)
    {
        if (!(other instanceof ObjectCode))
            return false;
        ObjectCode otherHash = (ObjectCode)other;
        return (otherHash.hashCode == hashCode) && (otherHash.checkCode == checkCode);
    }
    
    @Override
    public Object clone()
    {
        try
        {
            return super.clone();
        }
        catch (CloneNotSupportedException e)
        {
            e.printStackTrace();
        }
        return null;
    }
}
