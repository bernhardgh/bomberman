package za.co.entelect.challenge.util;

import java.io.Serializable;
import java.util.Random;

public class ObjectCodeComponent implements Cloneable, Serializable
{
    private static final long serialVersionUID = -6350106284943883695L;

    private static Random generator = new Random();
    
    private ObjectCode objectCode;
    private int hashFactor, checkFactor;
    private int hashCode = 0, checkCode = 0;
    
    public ObjectCodeComponent(ObjectCode objectCode)
    {
        this.objectCode = objectCode;
        
        hashFactor = generator.nextInt();
        checkFactor = generator.nextInt();
    }
    
    public ObjectCode getObjectCode()
    {
        return objectCode;
    }
    
    public void setObjectCode(ObjectCode objectCode)
    {
        this.objectCode = objectCode;
    }
    
    public void refreshCodes(int baseCode)
    {
        int oldHashCode = hashCode, oldCheckCode = checkCode;
        
        hashCode = baseCode * hashFactor;
        checkCode = baseCode * checkFactor;
        
        objectCode.adjustCodes(hashCode - oldHashCode, checkCode - oldCheckCode);
    }
    
    @Override
    public Object clone()
    {
        try
        {
            return super.clone();
        }
        catch (CloneNotSupportedException e)
        {
            e.printStackTrace();
        }
        return null;
    }
}
