package za.co.entelect.challenge.util;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class FixedSizeHashMap<K,V> implements Map<K,V>
{
    private int size = 0;
    private Entry<K,V>[] entries;
    
    @SuppressWarnings("unchecked")
    public FixedSizeHashMap(int capacity)
    {
        entries = (Entry<K,V>[]) Array.newInstance(Entry.class,capacity);
        clear();
    }
    
    public int size()
    {
        return size;
    }

    public boolean isEmpty()
    {
        return size==0;
    }

    public boolean containsKey(Object key)
    {
        return get(key)!=null;
    }

    public boolean containsValue(Object value)
    {
        for (Entry<K,V> nextEntry : entries)
            if (nextEntry.getValue().equals(value))
                return true;
        return false;
    }

    public V get(Object key)
    {
        Entry<K,V> entry = entries[Math.abs(key.hashCode() % entries.length)];
        return ((entry==null) || (! entry.getKey().equals(key))) ? null : entry.getValue();
    }

    public V put(K key,V value)
    {
        int hashIndex = Math.abs(key.hashCode() % entries.length);
        Entry<K,V> previousEntry = entries[hashIndex];
        entries[hashIndex] = new MyEntry<K,V>(key,value);
        if (previousEntry==null)
            size++;
        return ((previousEntry==null) || (! previousEntry.getKey().equals(key))) ? null : previousEntry.getValue();
    }

    public V remove(Object key)
    {
        int hashIndex = Math.abs(key.hashCode() % entries.length);
        Entry<K,V> entry = entries[hashIndex];
        if ((entry!=null) && (entry.getKey().equals(key)))
        {
            size--;
            entries[hashIndex] = null;
            return entry.getValue();
        }
        return null;
    }

    public void putAll(Map<? extends K,? extends V> m)
    {
        for (Entry<? extends K,? extends V> nextEntry : m.entrySet())
            put(nextEntry.getKey(),nextEntry.getValue());
    }

    public void clear()
    {
        for (int i=0; i<entries.length; i++)
            entries[i] = null;
        size = 0;
    }

    public Set<K> keySet()
    {
        return new MyKeySet<K>();
    }

    public Collection<V> values()
    {
        return new MyValueCollection<V>();
    }

    public Set<Entry<K,V>> entrySet()
    {
        return new MyEntrySet<Entry<K,V>>();
    }
    
    public String toString()
    {
        StringBuilder sb = new StringBuilder("{");
        for (Entry<K,V> nextEntry : entrySet())
            sb.append(nextEntry.getKey()+"="+nextEntry.getValue()+", ");
        sb.replace(sb.length() - 2,sb.length(),"}");
        return sb.toString();
    }

    
    private static class MyEntry<KeyType,ValueType> implements Entry<KeyType,ValueType>
    {
        private KeyType key;
        private ValueType value;
        
        public MyEntry(KeyType key,ValueType value)
        {
            this.key = key;
            this.value = value;
        }
        
        public KeyType getKey()
        {
            return key;
        }

        public ValueType getValue()
        {
            return value;
        }

        public ValueType setValue(ValueType newValue)
        {
            ValueType oldValue = value;
            value = newValue;
            return oldValue;
        }
    }
    
    
    private class MyKeySet<KeyType> implements Set<KeyType>
    {
        public int size()
        {
            return size;
        }

        public boolean isEmpty()
        {
            return FixedSizeHashMap.this.isEmpty();
        }

        public boolean contains(Object o)
        {
            return containsKey(o);
        }

        public Iterator<KeyType> iterator()
        {
            return new MyKeyIterator<KeyType>();
        }

        public Object[] toArray()
        {
            Object[] arr = new Object[size];
            int i = 0;
            for (KeyType nextKey : this)
                arr[i] = nextKey;
            return arr;
        }

        @SuppressWarnings("unchecked")
        public <T> T[] toArray(T[] a)
        {
            T[] arr = a.length<size ? (T[]) Array.newInstance(a.getClass().getComponentType(),size) : a;
            int i = 0;
            for (KeyType nextKey : this)
                arr[i++] = (T) nextKey;
            if (arr.length>size)
                arr[i] = null;
            return arr;
        }

        public boolean add(KeyType e)
        {
            throw new UnsupportedOperationException();
        }

        public boolean remove(Object o)
        {
            return FixedSizeHashMap.this.remove(o)!=null;
        }

        public boolean containsAll(Collection<?> c)
        {
            for (Object nextItem : c)
                if (! contains(nextItem))
                    return false;
            return true;
        }

        public boolean addAll(Collection<? extends KeyType> c)
        {
            throw new UnsupportedOperationException();
        }

        public boolean retainAll(Collection<?> c)
        {
            boolean didChange = false;
            Iterator<KeyType> it = iterator();
            while (it.hasNext())
                if (! c.contains(it.next()))
                {
                    it.remove();
                    didChange = true;
                }
            return didChange;
        }

        public boolean removeAll(Collection<?> c)
        {
            boolean didChange = false;
            for (Object nextItem : c)
                didChange|=remove(nextItem);
            return didChange;
        }

        public void clear()
        {
            FixedSizeHashMap.this.clear();
        }
        
        
        private class MyKeyIterator<Key> implements Iterator<Key>
        {
            private int idx;
            
            public MyKeyIterator()
            {
                for (int i=0; i<entries.length; i++)
                    if (entries[i]!=null)
                    {
                        idx = i;
                        return;
                    }
                idx = -1;
            }
            
            public boolean hasNext()
            {
                return idx!=-1;
            }

            @SuppressWarnings("unchecked")
            public Key next()
            {
                Key key = (Key) entries[idx++].getKey();
                while ((idx<entries.length) && (entries[idx]==null)) 
                    idx++;
                if (idx==entries.length)
                    idx = -1;
                return key;
            }

            public void remove()
            {
                entries[idx++] = null;
                size--;
                while ((idx<entries.length) && (entries[idx]==null)) 
                    idx++;
                if (idx==entries.length)
                    idx = -1;
            }
        }
    }
    
    
    private class MyValueCollection<ValueType> implements Collection<ValueType>
    {
        public int size()
        {
            return size;
        }

        public boolean isEmpty()
        {
            return FixedSizeHashMap.this.isEmpty();
        }

        public boolean contains(Object o)
        {
            return containsValue(o);
        }

        public Iterator<ValueType> iterator()
        {
            return new MyValueIterator<ValueType>();
        }

        public Object[] toArray()
        {
            Object[] arr = new Object[size];
            int i = 0;
            for (ValueType nextItem : this)
                arr[i++] = nextItem;
            return arr;
        }

        @SuppressWarnings("unchecked")
        public <T> T[] toArray(T[] a)
        {
            T[] arr = a.length<size ? (T[]) Array.newInstance(a.getClass().getComponentType(),size) : a;
            int i = 0;
            for (ValueType nextItem : this)
                arr[i++] = (T) nextItem;
            if (arr.length>size)
                arr[i] = null;
            return arr;
        }

        public boolean add(ValueType e)
        {
            throw new UnsupportedOperationException();
        }

        public boolean remove(Object o)
        {
            for (int i=0; i<entries.length; i++)
            {
                Entry<K,V> nextEntry = entries[i];
                if ((nextEntry!=null) && (nextEntry.getValue().equals(o)))
                {
                    entries[i] = null;
                    size--;
                    return true;
                }
            }
            return false;
        }

        public boolean containsAll(Collection<?> c)
        {
            for (Object nextItem : c)
                if (! containsValue(nextItem))
                    return false;
            return true;
        }

        public boolean addAll(Collection<? extends ValueType> c)
        {
            throw new UnsupportedOperationException();
        }

        public boolean removeAll(Collection<?> c)
        {
            boolean didChange = false;
            for (Object nextItem : c)
                if (remove(nextItem))
                    didChange = true;
            return didChange;
        }

        public boolean retainAll(Collection<?> c)
        {
            boolean didChange = false;
            Iterator<ValueType> it = iterator();
            while (it.hasNext())
                if (! c.contains(it.next()))
                {
                    it.remove();
                    didChange = true;
                }
            return didChange;
        }

        public void clear()
        {
            FixedSizeHashMap.this.clear();
        }
        
        
        private class MyValueIterator<Value> implements Iterator<Value>
        {
            private int idx = -1;
            
            public MyValueIterator()
            {
                for (int i=0; i<entries.length; i++)
                    if (entries[i]!=null)
                    {
                        idx = i;
                        return;
                    }
                idx = -1;
            }
            
            public boolean hasNext()
            {
                return idx!=-1;
            }

            @SuppressWarnings("unchecked")
            public Value next()
            {
                Value value = (Value) entries[idx++].getValue();
                while ((idx<entries.length) && (entries[idx]==null))
                    idx++;
                if (idx==entries.length)
                    idx = -1;
                return value;
            }

            public void remove()
            {
                entries[idx++] = null;
                size--;
                while ((idx<entries.length) && (entries[idx]==null))
                    idx++;
                if (idx==entries.length)
                    idx = -1;
            }
        }
    }
    
    
    private class MyEntrySet<EntryType> implements Set<EntryType>
    {
        public int size()
        {
            return size;
        }

        public boolean isEmpty()
        {
            return FixedSizeHashMap.this.isEmpty();
        }

        public boolean contains(Object o)
        {
            Object value = get(((Entry<?,?>) o).getKey());
            return (value!=null) && (value.equals(o));
        }

        public Iterator<EntryType> iterator()
        {
            return new MyEntryIterator<EntryType>();
        }

        public Object[] toArray()
        {
            Object[] arr = new Object[size];
            int i = 0;
            for (Object nextItem : this)
                arr[i++] = nextItem;
            return arr;
        }

        @SuppressWarnings("unchecked")
        public <T> T[] toArray(T[] a)
        {
            T[] arr = a.length<size ? (T[]) Array.newInstance(a.getClass().getComponentType(),size) : a;
            int i = 0;
            for (Object nextEntry : this)
                arr[i++] = (T) nextEntry;
            if (arr.length>size)
                arr[i] = null;
            return arr;
        }

        public boolean add(EntryType e)
        {
            throw new UnsupportedOperationException();
        }

        public boolean remove(Object o)
        {
            Entry<?,?> entry = (Entry<?,?>) o;
            V value = get(entry.getKey());
            if ((value!=null) && (value.equals(entry.getValue())))
            {
                remove(entry.getKey());
                return true;
            }
            return false;
        }

        public boolean containsAll(Collection<?> c)
        {
            for (Object nextItem : c)
            {
                Entry<?,?> nextEntry = (Entry<?,?>) nextItem;
                V value = get(nextEntry.getKey());
                if ((value==null) || (! value.equals(nextEntry.getValue())))
                    return false;
            }
            return true;
        }

        public boolean addAll(Collection<? extends EntryType> c)
        {
            throw new UnsupportedOperationException();
        }

        public boolean retainAll(Collection<?> c)
        {
            boolean hasChanged = false;
            Iterator<EntryType> it = iterator();
            while (it.hasNext())
                if (! c.contains(it.next()))
                {
                    it.remove();
                    hasChanged = true;
                }
            return hasChanged;
        }

        public boolean removeAll(Collection<?> c)
        {
            boolean hasChanged = false;
            for (Object nextEntry : c)
                hasChanged|=remove(nextEntry);
            return hasChanged;
        }

        public void clear()
        {
            FixedSizeHashMap.this.clear();
        }
        
        
        private class MyEntryIterator<ET> implements Iterator<ET>
        {
            private int idx;
            
            public MyEntryIterator()
            {
                for (int i=0; i<entries.length; i++)
                    if (entries[i]!=null)
                    {
                        idx = i;
                        return;
                    }
                idx = -1;
            }
            
            public boolean hasNext()
            {
                return idx!=-1;
            }

            @SuppressWarnings("unchecked")
            public ET next()
            {
                ET entry = (ET) entries[idx++];
                while ((idx<entries.length) && (entries[idx]==null))
                    idx++;
                if (idx==entries.length)
                    idx = -1;
                return entry;
            }

            public void remove()
            {
                entries[idx++] = null;
                size--;
                while ((idx<entries.length) && (entries[idx]==null))
                    idx++;
                if (idx==entries.length)
                    idx = -1;
            }
        }
    }
    
    
    public static void main(String[] args)
    {
        Map<String,String> map = new FixedSizeHashMap<String,String>(100);
        map.put("Name","Bernhard");
        map.put("Surname","Häussermann");
        map.put("Occupation","Pianist");
        map.remove("Occupation");
        System.out.println(map+"\n"+map.size());
        map.put("Occupation","Programmer");
        map.put("Occupation","Developer");
        System.out.println(map+"\n"+map.size());
        System.out.println(map.get("Name")+", "+map.get("Surname")+", "+map.get("Occupation")+", "+map.get("Favourite_Food"));
    }
}
