package za.co.entelect.challenge.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class HardwareInfoHelpers
{
    private enum OperatingSystem { WINDOWS, UNIX, MAC }
    
    public static int getCpuCoreCount()
    {
        String[] commands = null;
        OperatingSystem operatingSystem = getOperatingSystem();
        switch (operatingSystem)
        {
            case WINDOWS: commands = new String[] { "cmd.exe", "/C", "WMIC CPU Get /Format:List" }; break;
            case UNIX: commands = new String[] { "lscpu" }; break;
            case MAC: commands = new String[] { "/bin/sh", "-c", "sysctl -n machdep.cpu.core_count" }; break;
        }
        
        try
        {
            Process process = Runtime.getRuntime().exec(commands);
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream())))
            {
                String line;
                int coreCount = 1, socketCount = 1;
                while ((line = reader.readLine()) != null)
                {
                    switch (operatingSystem)
                    {
                        case WINDOWS: {
                            if (line.contains("NumberOfCores"))
                                return Integer.parseInt(line.substring(line.indexOf('=') + 1));
                            break;
                        }
                        case UNIX: {
                            if (line.contains("Core(s) per socket:"))
                                coreCount = Integer.parseInt(line.substring(line.lastIndexOf(' ') + 1));
                            else if (line.contains("Socket(s):"))
                                socketCount = Integer.parseInt(line.substring(line.lastIndexOf(' ') + 1));
                            break;
                        }
                        case MAC: return line.length() == 0 ? 0 : Integer.parseInt(line);
                    }
                }
                return operatingSystem == OperatingSystem.UNIX ? coreCount * socketCount : 1;
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
            return 1;
        }
    }
    
    private static OperatingSystem getOperatingSystem()
    {
        String os = System.getProperty("os.name").toLowerCase();
        if (os.contains("win"))
            return OperatingSystem.WINDOWS;
        if (os.contains("mac"))
            return OperatingSystem.MAC;
        if ((os.contains("nix")) || (os.contains("nux")) || (os.contains("aix")))
            return OperatingSystem.UNIX;
        return null;
    }
}
