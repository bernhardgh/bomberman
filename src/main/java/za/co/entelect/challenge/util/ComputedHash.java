package za.co.entelect.challenge.util;

public class ComputedHash
{
    private int hashCode;
    private int checkCode;
    
    public ComputedHash(int hashCode, int checkCode)
    {
        this.hashCode = hashCode;
        this.checkCode = checkCode;
    }
    
    @Override
    public int hashCode()
    {
        return hashCode;
    }
    
    @Override
    public boolean equals(Object other)
    {
        if (!(other instanceof ComputedHash))
            return false;
        ComputedHash otherHash = (ComputedHash)other;
        return (otherHash.hashCode == hashCode) && (otherHash.checkCode == checkCode);
    }
}
