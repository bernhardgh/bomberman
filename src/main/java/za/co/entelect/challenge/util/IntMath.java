package za.co.entelect.challenge.util;

public class IntMath
{
    public static int logBase2(int n)
    {
        if (n == 0)
            return 0;
        return 31 - Integer.numberOfLeadingZeros(n);
    }
}
