if [ "$1" == "yesgo" ]; then
	rm ../bgh.zip
	mvn clean
	rm -R ../bgh
	mkdir ../bgh
	cp -R ./* ../bgh
	cd ../bgh
	rm coverage
	rm dodgeCounts
	rm preparerelease.sh
	rm -R ./.*
	cp ../project/.project ./
	cp ../project/.classpath ./
	cp -R ../project/.settings ./
	chmod 777 ./
	sed -i '' -- 's#//##g' ./src/test/java/za/co/entelect/challenge/tests/program/TestProgram.java ./src/test/java/za/co/entelect/challenge/tests/players/TestReplayBombermanPlayer.java ./src/test/java/za/co/entelect/challenge/tests/players/TestSearchBombermanPlayer.java
	zip -r ../bgh.zip .
else
	echo "Did you remember to revert the debug-wait change?"
fi
